.. DanteDpp:

DanteDpp
--------
.. image:: DanteDpp.png  

Introduction
``````````````
The XIA Digital X-ray Processor is a family of digital spectrometers using acquisition boards XMap/FalconX

Properties
````````````````````

====================================== ==================================================================== ==================== ===============================================
Property name                          Default value                                                        Type                 Description
====================================== ==================================================================== ==================== ===============================================
AutoLoad                               False                                                                Boolean              Allow to Reload the last used configuration file (*.ini*) at each init of the device.
BoardType                              XMAP                                                                 String               Define the board type : 
                                                                                                                                    XMAP [;path of the library handel.dll]
                                                                                                                                    FALCONX [;path of the library handel.dll]
                                                                                                                                    SIMULATOR [;nbModules] [;nbChannels] [;acquisitionClockMs] [;loadConfigurationFileDelayMs]
BoardTimeBase                          0.000000320                                                          Double               Timebase factor of the board. [in seconds]
ConfigurationFiles                     ALIAS;MODE;FILE_PATH_NAME                                            Array of string      Define the list of Configuration "*.INI*" files and their associated alias & mode.
RoisFiles                              ALIAS;FILE_PATH_NAME                                                 Array of string      Define the list of rois files "*.txt*" and their associated alias.
StreamItems                            Triggers, Outputs, Icr, Ocr, RealTime, LiveTime, DeadTime, Channel   Array of string      Define the list of Items managed by the Streamer. (Nexus, CSV, ...). Each item defined here wil be saved in the stream for all available channels. Availables values are (not sensitive case):
                                                                                                                                    Triggers
                                                                                                                                    Outputs
                                                                                                                                    Icr
                                                                                                                                    Ocr
                                                                                                                                    RealTime
                                                                                                                                    LiveTime
                                                                                                                                    DeadTime
                                                                                                                                    Channel
StreamItemsPrefix                      No default value                                                     String               Each item defined in StreamItems property will be saved in the stream (Nexus, CSV,...) with the Prefix defined here.
SpoolID                                TO_BE_DEFINED                                                        String               Used only by the FlyScan application
__MemorizedConfigurationAlias          TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedRoisAlias                   TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedNumChannel                  -1                                                                   uLong                Only the device could modify this property. The User should never change this property
__MemorizedPresetType                  FIXED_REAL                                                           String               Only the device could modify this property. The User should never change this property
__MemorizedPresetValue                 1                                                                    Double               Only the device could modify this property. The User should never change this property
__MemorizedFileGeneration              False                                                                Boolean              Only the device could modify this property. The User should never change this property
__MemorizedStreamType                  LOG_STREAM                                                           String               Only the device could modify this property. The User should never change this property
__MemorizedStreamTargetPath            TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedStreamTargetFile            TO_BE_DEFINED                                                        String               Only the device could modify this property. The User should never change this property
__MemorizedStreamNbDataPerAcq          2048                                                                 uLong                Only the device could modify this property. The User should never change this property
__MemorizedStreamNbAcqPerFile          1                                                                    int                  Only the device could modify this property. The User should never change this property
__ExpertStreamMemoryMode               COPY                                                                 String               Available only for Nexus format : set the SetDataItemMemoryMode(). Available values : 
                                                                                                                                 - COPY
                                                                                                                                 - NO_COPY
__ExpertStreamWriteMode                ASYNCHRONOUS                                                         String               Only an expert User could change this property. Applicable for StreamNexus Only !Available Values : 
                                                                                                                                 - ASYNCHRONOUS {default} 
                                                                                                                                 - SYNCHRONOUS - DELAYED
__ExpertMemoryMonitor                  False                                                                String               Enable/Disable the creation of a memory file in order to monitor the process memory usage. Example : 
                                                                                                                                    FALSE {by default } (means do not create a memory file) 
                                                                                                                                    TRUE [;c:\my_location\my_file.txt] (means create my_file.txt in c:\my_location)             
__ExpertAcquisitionCollectPeriod       5                                                                    uShort               Define the period of the acquisition collect task.[in ms]
====================================== ==================================================================== ==================== ===============================================


Commands
````````````````````

=========================== =============== ======================= ===========================================
Command name                Arg. in         Arg. out                Description
=========================== =============== ======================= ===========================================
Init                        Void            Void                    Do not use (Use Init of the Generic device)
State                       Void            uLong                    Return the device state
Status                      Void            String                  Return the device state as a string
Snap                        Void            String                  Starts the acquisition of N frames and stops at the end
Stop                        Void            String                  Stops the acquisition at user's request
LoadConfigFile              String          Void                    Load a new config file (.ini) designed by its associated alias. This will download firmware and initialize the boards/modules in the system
SaveConfigFile              Void            Void                    Config file (.ini) can be updated at any time using this command.
SetRoisFromList             Array of String Void                    Return the list of Rois configured and stored in Rois device property
SetRoisFromFile             String          Void                    Define a set of rois for each channel. The set of rois is read from a file indicated by its alias.
GetRois                     Void            Array of String         Returns the list of rois for each channel.
RemoveRois                  uLong            Void                    Remove all Rois for the selected channel. NB: put channel number=-1 to remove all rois for all channels.
StreamResetIndex            Void            Void                    Reset the stream Nexus buffer index to 1
GetDataStreams              Void            Array of String         Return the flyscan data streams associated with this device
GetConfigurationsFilesAlias Void            Array of String         Get the list of alias of Configurations Files (The contents of ConfigurationFiles property).
GetRoisFilesAlias           Void            Array of String         Get the list of alias of Rois Files (The contents of RoisFiles property)
=========================== =============== ======================= ===========================================


Dynamic Attributes
````````````````````````````

=============================== ======================== ================== ===================================
Attribute name                  Read/Write               Type               Description
=============================== ======================== ================== ===================================
currentAlias                    R                        String             Display the current Alias used to load the *.ini* file
currentConfigFile               R                        String             Display the path+name of current loaded *.ini* file         
currentRoisFile
boardType                       R                        String             The Board type Fixed by the BoardType property
currentMode                     R                        String             Display the current Mode : MCA, MAPPING_FULL, MAPPING_SCA
nbModules                       R                        ULong              Number of Xia boards
nbChannels                      R                        ULong              Number of total of Channels
nbBins                          R                        ULong              Number of bins per channel
triggerMode                     R/W                      Enum               The current trigger mode. Available values : INTERNAL / GATE
currentPixel                    R                        ULong              Current pixel of the mapping.
nbPixels                        R/W                      ULong              Number of pixels (step) for the Mapping acquisition.
presetValue                     R/W                      Double             This value is either the number of counts or the time (specified in seconds) it depends on preset_type parameter.
presetType                      R                        Double             Sets the preset run type: "NONE", "FIXED_REAL", "FIXED_LIVE", "FIXED_EVENTS", "FIXED_TRIGGERS"
realTime(i)                     R                        Double             The total time that the processor was taking data in seconds
liveTime(i)                     R                        Double             The total time that the processor was able to acquire new events in seconds.      
inputCountRate(i)               R                        Double             Input Count Rate: OCR/(1-(Deadtime/100)) => (scaler4/(scaler8/80000000.0))/(1-(scaler10 /100))
outputCountRate(i)              R                        Double             Output Count Rate: AllGood/Time => scaler4/(scaler8/80000000.0)
eventsInRun(i)                  R                        ULong              Number of all event triggers => scaler3
deadTime(i)                     R                        Double             Deadtime as calculated by (ICR - OCR) / ICR                                               
streamType                      W                        String             Available only for expert user. The curent stream type. Available values :NO_STREAM - NEXUS_STREAM - CSV_STREAM.
streamTargetPath                W                        String             Available only for expert user. The root path for generated Stream files.
streamTargetFile                W                        String             Available only for expert user. The file name for generated Stream files.
streamNbAcqPerFile              W                        uLong              Available only for expert user. The number of acquisitions for each Stream file.
fileGeneration                  R/W                      Boolean            Available only for expert user. Enable/Disable writing the data to a file.
enableDatasetrealtime           R                        Boolean            Allows to activate or inactivate the Real Time saving in the nexus files.
enableDatasetlivetime           R                        Boolean            Allows to activate or inactivate the Live Time saving in the nexus files.
enableDatasetdeadtime           R                        Boolean            Allows to activate or inactivate the Dead Time saving in the nexus files.
enableDatasetinputcountrate     R                        Boolean            Allows to activate or inactivate the Input Count Rate saving in the nexus files.
enableDatasetoutputcountrate    R                        Boolean            Allows to activate or inactivate the Output Count Rate saving in the nexus files.
enableDataseteventsinrun        R                        Boolean            Allows to activate or inactivate the Events In Run saving in the nexus files.
enableDatasetspectrum           R                        Boolean            Allows to activate or inactivate the Spectrum saving in the nexus files.
enableDatasetroi                R                        Boolean            Allows to activate or inactivate the Roi saving in the nexus files.
=============================== ======================== ================== ===================================