//------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <string>

#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <sstream>
#include <yat/time/Timer.h>

#include <tango.h>
#include "Controller.h"
#include "Helper.h"

using namespace DanteDpp_ns;

//------------------------------------------------------------------------------------------------------
// main test
//------------------------------------------------------------------------------------------------------
int main (int /*argc*/, char* /*argv*/[])
{
    std::cout << "Starting test..." << std::endl;

    HardwareDanteInitParameters hardware_parameters(Helper::convert_tango_to_detector_type("DANTE_SIMULATOR")); // DANTE_SIMULATOR or DANTE

    StreamParameters stream_parameters(StreamParameters::Types::LOG_STREAM, // in_type
                                       "", //in_target_path
                                       "", // in_target_file    
                                       0, // in_nb_acq_per_file 
                                       0, // in_nb_data_per_acq 
                                       StreamParameters::WriteModes::SYNCHRONOUS, // in_write_mode      
                                       StreamParameters::MemoryModes::NO_COPY, // in_memory_mode     
                                       false, // in_file_generation 
                                       "real_time",  // in_label_real_time        
                                       "live_time",  // in_label_live_time        
                                       "dead_time",  // in_label_dead_time        
                                       "input_count_rate",  // in_label_input_count_rate 
                                       "output_count_rate",  // in_label_output_count_rate
                                       "events_in_run",  // in_label_events_in_run    
                                       "channel",  // in_label_spectrum         
                                       "roi"); // in_label_roi             
    try
    {
        Controller::create(NULL, hardware_parameters, stream_parameters);
        Controller::get_instance()->load_configuration("", "./config/configuration.ini");

        while(Controller::get_instance()->get_state() == Tango::DISABLE)
        {
            // waiting time between two update calls
            ::usleep(1000 * 1000);
        }

        for(int i = 0 ; i < 4 ; i++)
        {
            std::cout << "State: " << Controller::get_instance()->get_status() << std::endl;

            // waiting time between two update calls
            ::usleep(1000 * 1000);
        }

        Controller::get_instance()->set_preset_value(2.0);

        Controller::get_instance()->start_acquisition();

        while(Controller::get_instance()->get_state() == Tango::RUNNING)
        {
            std::cout << "State: " << Controller::get_instance()->get_status() << std::endl;

            // waiting time between two update calls
            ::usleep(1000 * 1000);
        }

        Controller::get_instance()->stop_acquisition(true);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        std::cout << "Exception occured!"  << std::endl << in_ex.to_string();
    }

    try
    {
        Controller::release();
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        std::cout << "Exception occured!"  << std::endl << in_ex.to_string();
    }

    return 0;
}
//------------------------------------------------------------------------------------------------------
