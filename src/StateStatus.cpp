/*************************************************************************************/
/*! 
 *  \file   StateStatus.h
 *  \brief  status class implementation 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************************/

#include "StateStatus.h"
#include "Log.h"

using namespace DanteDpp_ns;

/************************************************************************
 * \brief constructor without parameter
 ************************************************************************/
StateStatus::StateStatus()
{
    // default status management
    set_status("Starting...");
    set_state (Tango::INIT  );
}

/*******************************************************************
 * \brief Setter of the state and status
*******************************************************************/
void StateStatus::set(const Tango::DevState & in_state ,
                      const std::string     & in_status)
{
    // autoLock the following
    yat::MutexLock scoped_lock(m_lock);
    m_state = in_state;
    m_status.str("");
    m_status << in_status.c_str();
}

/*******************************************************************
 * \brief Setter of the state
*******************************************************************/
void StateStatus::set_state(const Tango::DevState & in_state)
{
    // autoLock the following
    yat::MutexLock scoped_lock(m_lock);
    m_state = in_state;
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState StateStatus::get_state(void) const
{
    // autoLock the following
    yat::MutexLock scoped_lock(m_lock);
    return m_state;
}

/*******************************************************************
 * \brief Setter of the status
*******************************************************************/
void StateStatus::set_status(const std::string & in_status)
{
    // autoLock the following
    yat::MutexLock scoped_lock(m_lock);
    m_status.str("");
    m_status << in_status.c_str();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string StateStatus::get_status(void) const
{
    // autoLock the following
    yat::MutexLock scoped_lock(m_lock);
    return (m_status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with a TANGO exception
*******************************************************************/
void StateStatus::on_fault(const Tango::DevFailed & in_df)
{
    std::stringstream status;
    status.str("");
    status << "Origin\t: " << in_df.errors[0].origin << std::endl;
    status << "Desc\t: "   << in_df.errors[0].desc   << std::endl;
    status << "Reason\t: " << in_df.errors[0].reason << std::endl;

    on_fault(status.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with a YAT exception
*******************************************************************/
void StateStatus::on_fault(const yat::Exception & in_ex)
{
    std::stringstream error_msg("");
    error_msg << "Origin\t: " << in_ex.errors[0].origin << std::endl;
    error_msg << "Desc\t: "   << in_ex.errors[0].desc   << std::endl;
    error_msg << "Reason\t: " << in_ex.errors[0].reason << std::endl;
    
    on_fault(error_msg.str());
}

/*******************************************************************
 * \brief Manage the FAULT status with a DANTE exception
*******************************************************************/
void StateStatus::on_fault(const DanteDpp_ns::Exception & in_ex)
{
    on_fault(in_ex.to_string());
}

/*******************************************************************
 * \brief Manage the FAULT status with an error message
*******************************************************************/
void StateStatus::on_fault(const std::string & in_status_message)
{
    ERROR_STRM << in_status_message << std::endl;
    set(Tango::FAULT, in_status_message);
}

/*******************************************************************
 * \brief Return the status mutex
*******************************************************************/
yat::Mutex & StateStatus::get_mutex(void)
{
    return m_lock;
}

//========================================================================================
