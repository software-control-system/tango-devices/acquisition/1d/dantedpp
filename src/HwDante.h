/*************************************************************************/
/*! 
 *  \file   HwDante.h
 *  \brief  DANTE detector hardware class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_HW_DANTE_H
#define DANTE_DPP_HW_DANTE_H

// SYSTEM
#include <vector>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>

// PROJECT
#include "HardwareDante.h"

/**********************************************************************/
namespace DanteDpp_ns
{
    /***********************************************************************
     * \class HwDante
     * \brief Hardware control object interface
     ***********************************************************************/

    class HwDante : public HardwareDante
    {
        // we need to gain access to the contructor
        friend class HardwareDante;

    public:
        //------------------------------------------------------------------
        // init/terminate management
        //------------------------------------------------------------------
        // init the detector
        virtual void init(void);

        // terminate the detector
        virtual void terminate(void);

        //------------------------------------------------------------------
        // connection management
        //------------------------------------------------------------------
        // connection to the detector
        virtual void connect(const std::string & in_complete_file_path);

        // disconnection from the detector
        virtual void disconnect(void);

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // start detector acquisition
        virtual bool start_detector_acquisition(void);

        // stop detector acquisition
        virtual void stop_detector_acquisition(void);

        // treat acquisitions
        virtual void treat_acquisitions(void);

    protected:
        // request the firmwares versions of all boards
        std::vector<std::string> request_firmwares_versions();

    private:
        // constructor
        explicit HwDante();

        // destructor (needs to be virtual)
        virtual ~HwDante();

        //------------------------------------------------------------------
        // system informations
        //------------------------------------------------------------------
        // request the library version
        std::string request_library_version();

        // gets the master identifier
        std::string get_master_identifier();

        //------------------------------------------------------------------
        // configuration management
        //------------------------------------------------------------------
        // requests the change of configuration of all boards
        void request_change_configuration();

        // requests the change of input for all boards
        void request_change_input();

        // requests the change of offsets for all boards
        void request_change_offsets();

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // requests the change of the trigger mode for all the boards
        void request_trigger_mode_change();

        // requests the start of a MCA acquisition
        void request_start();

        // requests the start of a MAPPING acquisition
        void request_start_map();

        // requests the stop of acquisition
        void request_stop();

        // treat MCA acquisitions
        void treat_mca_acquisitions(void);

        // treat MAPPING acquisitions
        bool treat_mapping_acquisitions(void);

        // check if all the images were received
        bool is_last_data_received(void);

        //------------------------------------------------------------------
        // state management
        //------------------------------------------------------------------
        // requests the states of all boards and computes a general state
        virtual Tango::DevState request_state();

        //------------------------------------------------------------------
        // error management
        //------------------------------------------------------------------
        // manages the current error and throws an exception
        virtual void manage_last_error(const std::string & in_reason,
                                       const std::string & in_desc  ,
                                       const std::string & in_origin);

        // checks the content of the last error
        bool check_last_error(const uint16_t in_error_code);

    private:
        std::string m_master_identifier      ;
        bool        m_first_spectrum_received; // to compute the spectrum index
        uint32_t    m_first_spectrum_id      ; // to compute the spectrum index

        static const std::vector<std::string> g_system_input_mode_labels     ;
        static const int                      g_nb_elem_basic_stats          ; // number of elements in basic stats returned data by sdk (for one channel)
        static const int                      g_nb_elem_adv_stats            ; // number of elements in advanced stats returned data by sdk (for one channel)

        //------------------------------------------------------------------
        // mutex stuff
        //------------------------------------------------------------------
        // used to protect the concurrent access to sdk methods
        // mutable keyword is used to allow const methods even if they use this class member
        mutable yat::Mutex m_sdk_lock;
    };
}
#endif // DANTE_DPP_HW_DANTE_H

/*************************************************************************/