/*************************************************************************/
/*! 
 *  \file   Error.cpp
 *  \brief  class used to manage the errors
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// SYSTEM
#include <cstddef>
#include <stdint.h>

// PROJECT
#include "Error.h"
#include "Log.h"

// SDK
#include "DLL_DPP_Callback.h"

namespace DanteDpp_ns
{
// severity labels
std::vector<std::string> Error::g_severity_labels { "WARNING", "ERROR", "CRITICAL" }; 

// errors list
const Error::ErrorsMap::value_type Error::g_errors_values[] = 
{
    // error(s) added for SOLEIL device:
    Error::ErrorsMap::value_type(DANTE_DPP_ERROR_CODE_UNKNOWN              , "Unknown error."),
    Error::ErrorsMap::value_type(DANTE_DPP_ERROR_CODE_ASYNC_REQUEST_PROBLEM, "Error during the call of an asynchronous function to the detector."),
    Error::ErrorsMap::value_type(DANTE_DPP_ERROR_CODE_ASYNC_REPLY_PROBLEM  , "Error in the reply of an asynchronous function of the detector."),

    // official Dante sdk errors:
    Error::ErrorsMap::value_type(DLL_NO_ERROR                        , "No errors."),
    Error::ErrorsMap::value_type(DLL_MULTI_THREAD_ERROR              , "Another thread has a lock on the library functions."),
    Error::ErrorsMap::value_type(DLL_NOT_INITIALIZED                 , "DLL is not initialized. Call InitLibrary()."),
    Error::ErrorsMap::value_type(DLL_CHAR_STRING_SIZE                , "Supplied char buffer size is too short. Return value updated with minimum length."),
    Error::ErrorsMap::value_type(DLL_ARRAY_SIZE                      , "An array passed as parameter to a function has not enough space to contain all the data that the function should return."),
    Error::ErrorsMap::value_type(DLL_ALREADY_INITIALIZED             , "DLL is already initialized."),
    Error::ErrorsMap::value_type(DLL_COMM_ERROR                      , "Communication error or timeout."),
    Error::ErrorsMap::value_type(DLL_ARGUMENT_OUT_OF_RANGE           , "An argument supplied to the library is out of valid range."),
    Error::ErrorsMap::value_type(DLL_WRONG_ID                        , "The supplied identifier (serial or IP) is not present among the connected systems."),
    Error::ErrorsMap::value_type(DLL_TIMEOUT                         , "An operation timed out. Result is unspecified."),
    Error::ErrorsMap::value_type(DLL_CLOSING                         , "Error during library closing."),
    Error::ErrorsMap::value_type(DLL_RUNNING                         , "The operation cannot be completed while the system is running."),
    Error::ErrorsMap::value_type(DLL_WRONG_MODE                      , "The function called is not appropriate for the current mode."),
    Error::ErrorsMap::value_type(DLL_DECRYPT_FAILED                  , "An error occured during decryption."),
    Error::ErrorsMap::value_type(DLL_INVALID_BITSTREAM               , "Trying to upload an invalid bistream file."),
    Error::ErrorsMap::value_type(DLL_FILE_NOT_FOUND                  , "The specified file hasn't been found."),
    Error::ErrorsMap::value_type(DLL_INVALID_FIRMWARE                , "Invalid firmware detected on one board. Upload a new one with load_firmware function."),
    Error::ErrorsMap::value_type(DLL_UNSUPPORTED_BY_FIRMWARE         , "Function not supported by current firmware of the board. Or firmware on the board is not present or corrupted."),
    Error::ErrorsMap::value_type(DLL_THREAD_COMM_ERROR               , "Error during communication between threads."),
    Error::ErrorsMap::value_type(DLL_MISSED_SPECTRA                  , "One ore more spectra missed."),
    Error::ErrorsMap::value_type(DLL_MULTIPLE_INSTANCES              , "Library open by multiple processes."),
    Error::ErrorsMap::value_type(DLL_THROUGHPUT_ISSUE                , "Download rate from boards at least 15% lower than expected, check your connection speed."),
    Error::ErrorsMap::value_type(DLL_INCOMPLETE_CMD                  , "A communication error caused a command to be truncated."),
    Error::ErrorsMap::value_type(DLL_MEMORY_FULL                     , "The hardware memory became full during the acquisition. Likely the effective throughput is not enough to handle all the data."),
    Error::ErrorsMap::value_type(DLL_SLAVE_COMM_ERROR                , "Communication error with slave boards."),
    Error::ErrorsMap::value_type(DLL_SOFTWARE_MEMORY_FULL            , "Allowed memory for the DLL became full. Likely some data/event have been discarded."),
    // debug Dante sdk error - If following errors are returned, contact the XGLab team for further investigation:
    Error::ErrorsMap::value_type(DLL_WIN32API_FAILED_INIT            , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_GET_DEVICE             , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API                        , "Generic WIN32 API error."),
    Error::ErrorsMap::value_type(DLL_WIN32API_INIT_EVENTS            , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_RD_INIT_EVENTS         , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_REPORTED_FAILED_INIT   , "Win32 errors - Debugging - Possible hardware/driver error."),
    Error::ErrorsMap::value_type(DLL_WIN32API_UNEXPECTED_FAILED_INIT , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_MULTI_THREAD_INIT_SET  , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_LOCK_HMODULE_SET       , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_HWIN_SET               , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_WMSG_SET               , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_READ_SET               , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_GET_DEVICE_SIZE        , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_DEVICE_UPD_LOCK_G      , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_FT_CREATE_IFL                   , "FT errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_FT_GET_IFL                      , "FT errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_CREATE_DEVCLASS_RUNTIME         , "Library errors - Debugging - Possible hardware/driver error."),
    Error::ErrorsMap::value_type(DLL_CREATE_DEVCLASS_ARGUMENT        , "Library errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_CREATE_DEVCLASS_COMM            , "Library errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_RUNTIME_ERROR                   , "Generic runtime error."),
    Error::ErrorsMap::value_type(DLL_WIN32API_HMODULE                , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_WIN32API_DEVICE_UPD_LOCK_F      , "Win32 errors - Debugging."),
    Error::ErrorsMap::value_type(DLL_INIT_EXCEPTION                  , "Library errors - Debugging."),
};

// errors labels
Error::ErrorsMap Error::g_errors_labels; 

// mutex used to protect the multithread access to g_errors_labels container.
yat::Mutex Error::g_errors_labels_mutex;

/*******************************************************************
 * \brief default constructor
 *******************************************************************/
Error::Error()
{
    m_reason   = "unknown";
    m_desc     = "unknown exception";
    m_origin   = "unknown";
    m_code     = DANTE_DPP_ERROR_CODE_UNKNOWN;
    m_severity = Error::EnumSeverity::ERROR;
}

/*******************************************************************
 * \brief constructor
 * \param[in] in_reason   error reason
 * \param[in] in_desc     error description
 * \param[in] in_origin   error origin
 * \param[in] in_code     error code
 * \param[in] in_severity error severity
 *******************************************************************/
Error::Error(const std::string & in_reason  ,
             const std::string & in_desc    ,
             const std::string & in_origin  ,
             int                 in_code    ,
             Error::EnumSeverity in_severity)
{
    m_reason   = in_reason  ;
    m_desc     = in_desc    ;
    m_origin   = in_origin  ;
    m_code     = in_code    ;
    m_severity = in_severity;

    INFO_STRM << "An error occured..."        << std::endl;
    INFO_STRM << "> Reason: "      << in_reason << std::endl;
    INFO_STRM << "> Description: " << in_desc   << std::endl;
    INFO_STRM << "> Origin: "      << in_origin << std::endl;
    INFO_STRM << "> Code: "        << in_code   << std::endl;
    INFO_STRM << "> Severity: "    << g_severity_labels[in_severity] << std::endl;
}

/*******************************************************************
 * \brief copy constructor
 * \param[in] in_src reference to an Error instance
 *******************************************************************/
Error::Error(const Error & in_src)
{
    m_reason   = in_src.m_reason  ;
    m_desc     = in_src.m_desc    ;
    m_origin   = in_src.m_origin  ;
    m_code     = in_src.m_code    ;
    m_severity = in_src.m_severity;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Error::~Error()
{
}

/*******************************************************************
 * \brief = operator
 * \param[in] in_src reference to an Error instance
 * \return the modified error instance
 *******************************************************************/
Error & Error::operator =(const Error & in_src)
{
    // no self assign
    if (this != &in_src)
    {
        m_reason   = in_src.m_reason  ;
        m_desc     = in_src.m_desc    ;
        m_origin   = in_src.m_origin  ;
        m_code     = in_src.m_code    ;
        m_severity = in_src.m_severity;
    }

    return *this;
}

/*******************************************************************
 * \brief convert the error to a complete description string
 * \return a string complete description
 *******************************************************************/
std::string Error::to_string() const
{
    std::stringstream error_stream("");

    error_stream << "Severity: " << g_severity_labels[m_severity] << std::endl;
    error_stream << "Origin\t: " << m_origin << std::endl;
    error_stream << "Desc\t: "   << m_desc   << std::endl;
    error_stream << "Reason\t: " << m_reason << std::endl;
    error_stream << "Code\t: "   << get_description_of_error_code(m_code) << std::endl;

    return error_stream.str();
}

/*******************************************************************
 * \brief convert the error code to a description string
 * \return the error code description
 *******************************************************************/
std::string Error::get_description_of_error_code(const int in_error_code) const
{
    std::string description = "Unknown error code received from Dante Sdk!";

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(g_errors_labels_mutex);

        // map is not filled ?
        if(g_errors_labels.size() == 0)
        {
            const int num_elements = sizeof(g_errors_values) / sizeof(g_errors_values[0]);
            g_errors_labels = ErrorsMap(g_errors_values, g_errors_values + num_elements);
        }
    }

    ErrorsMap::iterator search = g_errors_labels.find(in_error_code);

    // found the error
    if(search != g_errors_labels.end()) 
    {
        description = search->second;
    }

    return description;
}

} // namespace DanteDpp_ns

