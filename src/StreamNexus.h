/*************************************************************************/
/*! 
 *  \file   StreamNexus.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_STREAM_NEXUS_H_
#define DANTE_DPP_STREAM_NEXUS_H_

// TANGO
#include <tango.h>

// YAT
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h>

// NEXUS
#include <nexuscpp/nexuscpp.h>

// LOCAL
#include "Stream.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class StreamNexus : public Stream, public nxcpp::IExceptionHandler
{
    // we need to gain access to the contructor
    friend class Stream;

    public:
        // Update the stream with new acquisition data
        virtual void update(yat::SharedPtr<const ModuleData> in_module_data);

        // Reset the file index
        virtual void reset_index(void);

        // Manage a nexus exception
        void OnNexusException(const nxcpp::NexusException &ex);

        // Abort the stream
        void abort(void);

        //==================================================================
        // acquisition management
        //==================================================================
        // Manage the start of acquisition
        virtual void start_acquisition(void);

        // Manage the acquisition stop
        virtual void stop_acquisition(void);

    protected:
        // destructor
        virtual ~StreamNexus();

    private:
        // constructor
        StreamNexus();

    private :
		nxcpp::DataStreamer *                 m_writer   ;
        yat::Mutex                            m_data_lock;

        std::vector<std::string>              m_real_time_names  ;
        std::vector<std::string>              m_live_time_names  ;
        std::vector<std::string>              m_dead_time_names  ;
        std::vector<std::string>              m_input_count_rate ;
        std::vector<std::string>              m_output_count_rate;
        std::vector<std::string>              m_events_in_run    ;
        std::vector<std::string>              m_spectrum_names   ;
        std::vector<std::vector<std::string>> m_roi_names        ;

        //- Nexus stuff	
    #if defined(USE_NX_FINALIZER)
        static nxcpp::NexusDataStreamerFinalizer m_data_streamer_finalizer;
        static bool m_is_data_streamer_finalizer_started;
    #endif
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_STREAM_NEXUS_H_

//###########################################################################
