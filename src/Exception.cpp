/*************************************************************************/
/*! 
 *  \file   Error.cpp
 *  \brief  class used to manage the exceptions
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// PROJECT
#include "Exception.h"
#include "Error.h"
#include "Log.h"

namespace DanteDpp_ns
{

/*******************************************************************
 * \brief default constructor
 *******************************************************************/
Exception::Exception() : m_errors(0)
{
    push_error(Error());
}

/*******************************************************************
 * \brief constructor
 * \param[in] in_reason   error reason
 * \param[in] in_desc     error description
 * \param[in] in_origin   error origin
 * \param[in] in_code     error code
 * \param[in] in_severity error severity
 *******************************************************************/
Exception::Exception(const char * in_reason, 
                     const char * in_desc  ,
                     const char * in_origin,
                     int          in_code  ,
                     Error::EnumSeverity in_severity) : m_errors(0)
{
    push_error(Error(in_reason, in_desc, in_origin, in_code, in_severity));
}

/*******************************************************************
 * \brief constructor
 * \param[in] in_reason   error reason
 * \param[in] in_desc     error description
 * \param[in] in_origin   error origin
 * \param[in] in_code     error code
 * \param[in] in_severity error severity
 *******************************************************************/
Exception::Exception(const std::string & in_reason  ,
                     const std::string & in_desc    ,
                     const std::string & in_origin  ,
                     int                 in_code    ,
                     Error::EnumSeverity in_severity) : m_errors(0)
{
    push_error(Error(in_reason, in_desc, in_origin, in_code, in_severity));
}

/*******************************************************************
 * \brief copy constructor
 * \param[in] in_src reference to an Exception instance
 *******************************************************************/
Exception::Exception(const Exception & in_src) : m_errors(0)
{
    for (std::size_t idx = 0; idx < in_src.m_errors.size(); idx++)
    {
        push_error(in_src.m_errors[idx]);
    }
}

/*******************************************************************
 * \brief = operator
 * \param[in] in_src reference to an Exception instance
 * \return the modified exception instance
 *******************************************************************/
Exception & Exception::operator = (const Exception & in_src)
{
    /// no self assign
    if (this != &in_src)
    {
        m_errors.clear();

        for (std::size_t idx = 0; idx < in_src.m_errors.size(); idx++)
        {
            push_error(in_src.m_errors[idx]);
        }
    }

    return *this;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Exception::~Exception()
{
    m_errors.clear();
}

/*******************************************************************
 * \brief Pushes the specified error into the errors list.
 * \param[in] in_error Error instance
 *******************************************************************/
void Exception::push_error(const Error & in_error)
{
    m_errors.push_back(in_error);
}

/*******************************************************************
 * \brief convert the error to a complete description string
 * \return a string complete description
 *******************************************************************/
std::string Exception::to_string() const
{
    std::string error_msg;

    for (std::size_t idx = 0; idx < m_errors.size(); idx++)
    {
        std::stringstream error_stream("");
        error_stream << "Error(" << idx << ")" << std::endl;
        error_msg += error_stream.str();

        error_msg += m_errors[idx].to_string();
    }
    return error_msg;
}

} // namespace DanteDpp_ns

