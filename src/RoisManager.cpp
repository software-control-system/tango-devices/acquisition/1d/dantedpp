/*************************************************************************/
/*! 
 *  \file   RoisManager.cpp
 *  \brief  class used to manage the Rois
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>
#include <yat/utils/StringTokenizer.h>
#include <yat/file/FileName.h>

// PROJECT
#include "RoisManager.h"
#include "Exception.h"
#include "Log.h"

namespace DanteDpp_ns
{
#define DANTE_DPP_ROIS_MANAGER_ALL_CHANNELS (-1)

//============================================================================================================
// RoisManager class
//============================================================================================================
const std::string RoisManager::g_groups_delimiter = "|";
const std::string RoisManager::g_rois_delimiter   = ";";
    
/************************************************************************
 * \brief constructor
 ************************************************************************/
RoisManager::RoisManager()
{
    INFO_STRM << "construction of RoisManager" << std::endl;
    m_channels_nb = 0;
    m_bins_nb     = 0;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
RoisManager::~RoisManager()
{
    INFO_STRM << "destruction of RoisManager" << std::endl;
}

/**************************************************************************
* \brief Set the channels number
**************************************************************************/
void RoisManager::set_channels_nb(const std::size_t in_channels_nb)
{
    m_channels_nb = in_channels_nb;
}

/**************************************************************************
* \brief Set the bins number
**************************************************************************/
void RoisManager::set_bins_nb(const std::size_t in_bins_nb)
{
    m_bins_nb = in_bins_nb;
}

/**************************************************************************
* \brief Get the groups delimiter (between channel rois)
* \return groups delimiter
**************************************************************************/
std::string RoisManager::get_groups_delimiter() const
{
    return RoisManager::g_groups_delimiter;
}

/**************************************************************************
* \brief Clear all the ROIs of all channels
**************************************************************************/
void RoisManager::reset_rois()
{
    m_groups.clear();
}

/**************************************************************************
* \brief Erase all the ROIs of a channel
* \param[in] in_channel_index channel index
**************************************************************************/
void RoisManager::erase_rois(std::size_t in_channel_index)
{
    // searching the channel
    RoisGroupMap::iterator search = m_groups.find(in_channel_index);

    // channel exists... 
    if(search != m_groups.end()) 
    {
        m_groups.erase(search);
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Channel (" << in_channel_index << ") does not have any ROI!";
        throw DanteDpp_ns::Exception("TANGO_DEVICE_ERROR", temp_stream.str(), "RoisManager::erase_rois");
    }
}

/**************************************************************************
* \brief Get the number of rois for all the channels
* \return total number of rois
**************************************************************************/
std::size_t RoisManager::get_nb_rois() const
{
    std::size_t nb_rois = 0;

    for (RoisGroupMap::const_iterator search = m_groups.begin(); search != m_groups.end(); ++search)
    {
        yat::SharedPtr<const RoisGroup> group = search->second;
        nb_rois += group->get_nb_rois();
    }

    return nb_rois;
}

/**************************************************************************
* \brief Get the number of rois for the channel
* \param[in] in_channel_index channel index
* \return number of rois for the channel
**************************************************************************/
std::size_t RoisManager::get_nb_rois(std::size_t in_channel_index) const
{
    std::size_t nb_rois = 0;

    // searching the channel
    RoisGroupMap::const_iterator search = m_groups.find(in_channel_index);

    // channel exists... 
    if(search != m_groups.end()) 
    {
        yat::SharedPtr<const RoisGroup> group = search->second;
        nb_rois = group->get_nb_rois();
    }

    return nb_rois;
}

/**************************************************************************
* \brief Get access to the roi informations of a channel
* \param[in] in_channel_index channel index
* \param[in] m_roi_index roi index
* \return roi informations
**************************************************************************/
yat::SharedPtr<const RoiRange> RoisManager::get_roi(std::size_t in_channel_index, std::size_t in_roi_index) const
{
    // searching the channel
    RoisGroupMap::const_iterator search = m_groups.find(in_channel_index);

    // channel exists... 
    if(search != m_groups.end()) 
    {
        yat::SharedPtr<const RoisGroup> group = search->second;
        return group->get_roi(in_roi_index);
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Channel (" << in_channel_index << ") does not have any ROI!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "RoisManager::get_roi");
    }
}

/**************************************************************************
* \brief Create a ROI (version with boundaries)
* \param[in] in_channel_index channel index
* \param[in] in_roi_index roi index
* \param[in] in_low_index index of the first bin of the range
* \param[in] in_high_index index of the latest bin of the range
**************************************************************************/
yat::SharedPtr<RoiRange> RoisManager::create_roi_with_boundaries(std::size_t in_channel_index,
                                                                 std::size_t in_roi_index    ,
                                                                 std::size_t in_low_index    ,
                                                                 std::size_t in_high_index   )
{
    yat::SharedPtr<RoiRange> roi_range;
    roi_range.reset(new RoiRange(in_channel_index, in_roi_index, in_low_index, (in_high_index - in_low_index + 1)));
    return roi_range;
}

/**************************************************************************
* \brief Add a ROI to a channel (version with boundaries)
* \param[in] in_channel_index channel index
* \param[in] in_roi_index roi index
* \param[in] in_low_index index of the first bin of the range
* \param[in] in_high_index index of the latest bin of the range
**************************************************************************/
void RoisManager::add_roi_boundaries(std::size_t in_channel_index,
                                     std::size_t in_roi_index    ,
                                     std::size_t in_low_index    ,
                                     std::size_t in_high_index   )
{
    yat::SharedPtr<RoiRange> roi_range = create_roi_with_boundaries(in_channel_index, in_roi_index, in_low_index, in_high_index);
    add_roi(roi_range);
}

/**************************************************************************
* \brief Add a ROI to a channel
* \param[in] in_roi_range new ROI
**************************************************************************/
void RoisManager::add_roi(yat::SharedPtr<RoiRange> in_roi_range)
{
    yat::SharedPtr<RoisGroup> group;
    
    // searching the ROIs group for the channel
    RoisGroupMap::iterator search = m_groups.find(in_roi_range->get_channel_index());

    // group exists... 
    if(search != m_groups.end()) 
    {
        group = search->second;
    }
    // new group
    else
    {
        group.reset(new RoisGroup(in_roi_range->get_channel_index()));

        // inserting the group in the map container.
        std::pair<RoisGroupMap::iterator, bool> insert_result = 
            m_groups.insert(std::make_pair(in_roi_range->get_channel_index(), group));

        // this should never happen.
        if(!insert_result.second)
        {
            std::stringstream temp_stream;
            temp_stream << "Problem! ROI group (" << in_roi_range->get_channel_index() << ") should not be in the groups container!";
            throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "RoisManager::add_roi");
        }
    }

    group->add_roi(in_roi_range);
}

/**************************************************************************
* \brief Add a ROI to a channel
* \param[in] in_channel_index channel index
* \param[in] in_roi_index roi index
* \param[in] in_bin_index index of the first bin of the range
* \param[in] in_bin_size number of bin of the range
**************************************************************************/
void RoisManager::add_roi(std::size_t in_channel_index,
                          std::size_t in_roi_index    ,
                          std::size_t in_bin_index    ,
                          std::size_t in_bin_size     )
{
    yat::SharedPtr<RoiRange> roi_range;
    roi_range.reset(new RoiRange(in_channel_index, in_roi_index, in_bin_index, in_bin_size));
    add_roi(roi_range);
}

//============================================================================================================
// ROIs list
//============================================================================================================
/**************************************************************************
* \brief Add the ROIs from a rois list
* \param[in] in_rois list of roi definitions (with bounderies)
**************************************************************************/
void RoisManager::add_with_rois_list(const std::vector<std::string> & in_rois)
{
    // we need to create a container of ROIs
    std::vector<yat::SharedPtr<RoiRange>> rois_ranges;

    for(std::size_t i = 0; i < in_rois.size(); i++)
    {
        std::string user_input = in_rois[i];
	    std::vector<std::string> vec_rois;
	    yat::StringUtil::split(user_input, g_rois_delimiter[0], &vec_rois, true);

	    //fix nb rois
	    int channel_num = yat::StringUtil::to_num<int>(vec_rois.at(0));
	    DEBUG_STRM << "channel_num = " << channel_num << endl;

	    int roi_count = (int) ((vec_rois.size() - 1) / 2);//vector size minus channel_num divided by 2 is the roi count
	    DEBUG_STRM << "roi_count = " << roi_count << endl;

        // if there are already rois for this channel, we erase them
        if(get_nb_rois(channel_num) && (roi_count > 0))
        {
            erase_rois(channel_num);
        }

	    //for each roi defined par user, fix the low/high
	    int roi_num = 0; 
        
        while(roi_num < roi_count)
        {
            yat::SharedPtr<RoiRange> roi_range = 
                create_roi_with_boundaries(channel_num, // channel
                                           roi_num    , // roi_num
                                           yat::StringUtil::to_num<std::size_t>(vec_rois.at(1 + 2 * roi_num     )), // low
                                           yat::StringUtil::to_num<std::size_t>(vec_rois.at(1 + 2 * roi_num + 1))); // high
            rois_ranges.push_back(roi_range);

            roi_num++;
        }
    }

    // Now, add all the ROIs
    if(!rois_ranges.empty())
    {
        for(std::size_t i = 0; i < rois_ranges.size(); i++)
        {
            add_roi(rois_ranges[i]);
        }
    }
}

/*****************************************************************************
 * \brief load the rois file content in memory
 * \param[in]  in_file_name rois file 
 * \return rois list which will be filled with the file content
 *****************************************************************************/
std::vector<std::string> RoisManager::load_rois_file_to_rois_list(const std::string & in_file_name)
{
    std::vector<std::string> rois_list;

    INFO_STRM << "Roi file name = " << in_file_name << std::endl;

    yat::File rois_file(in_file_name);
	std::string file_data;
	rois_file.load((std::string*) & file_data);

	DEBUG_STRM << "file_data :" << std::endl;
    DEBUG_STRM << file_data     << std::endl;

	std::vector<std::string> vec_lines;
	yat::StringUtil::split(file_data, '\n', &vec_lines, true);

    for(std::size_t i = 0; i < vec_lines.size(); i++)
	{
		INFO_STRM << "line[" << i << "] = " << vec_lines.at(i) << endl;

		std::string user_input = vec_lines.at(i);
		if(!user_input.empty())//ignore blank line (i.e LF at the end)
			if(user_input.at(0)!='#') //allow comments in the file
                rois_list.push_back(user_input);
	}

    return rois_list;
}

/*******************************************************************
 * \brief get the Rois list for all channel
 * \return Rois by channel
 *******************************************************************/
std::vector<std::string> RoisManager::get_rois_list() const
{
    std::vector<std::string> rois_list;
    
    DEBUG_STRM << "nb channels = " << m_channels_nb << endl;

	for(std::size_t channel_index = 0; channel_index < m_channels_nb; channel_index++)
	{
		std::stringstream ss;
		ss.str("");
		ss << channel_index;

        std::size_t nb_rois = get_nb_rois(channel_index);
		DEBUG_STRM << "channel : " << channel_index << " - nb rois = " << nb_rois << endl;

        if(nb_rois)
        {
		    for(std::size_t roi_index = 0; roi_index < nb_rois; roi_index++)
		    {
			    ss << g_rois_delimiter;

                // Get access to the roi informations of a channel
                yat::SharedPtr<const RoiRange> roi_range = get_roi(channel_index, roi_index);

			    ss << roi_range->get_low_index() << g_rois_delimiter << roi_range->get_high_index();
		    }
        }
        else
        {
		    ss << g_rois_delimiter;
        }

		rois_list.push_back(ss.str());
		DEBUG_STRM << rois_list.at(channel_index) << endl;
	}

    return rois_list;
}

/*****************************************************************************
 * \brief convert a rois string to a rois list
 * \param[in] rois_string rois string
 * \return rois list which will be filled with the rois string content
 *****************************************************************************/
std::vector<std::string> RoisManager::convert_rois_string_to_rois_list(const std::string & rois_string) const
{
    std::vector<std::string> rois_list;

    INFO_STRM << "Rois string = " << rois_string << std::endl;

	std::vector<std::string> vec_lines;
	yat::StringUtil::split(rois_string, g_groups_delimiter[0], &vec_lines, true);

    for(std::size_t i = 0; i < vec_lines.size(); i++)
	{
		INFO_STRM << "line[" << i << "] = " << vec_lines.at(i) << endl;
        rois_list.push_back(vec_lines.at(i));
	}

    return rois_list;
}

/*****************************************************************************
 * \brief check the rois list and manage the all channels tag
 * \param[in-out] rois_string rois string
 *****************************************************************************/
void RoisManager::check_rois_list(std::vector<std::string> & in_out_rois_list) const
{
    std::vector<std::string> rois_list;
    std::size_t index;

    // first, create an empty rois list of all channels
    for(index = 0; index < m_channels_nb; index++)
	{
        std::stringstream rois_string;
        rois_string << index;
        rois_list.push_back(rois_string.str());
    }

    // parse the rois list to manage the all channels tag and check the limits too
    for(index = 0; index < in_out_rois_list.size(); index++)
	{
        std::string rois = in_out_rois_list[index];

	    std::vector<std::string> vec_rois;
	    yat::StringUtil::split(rois, g_rois_delimiter[0], &vec_rois, true);

        //check user input : nb tokens must be ODD (1 channel_number ; low; high; low; high; ....)
	    if(vec_rois.size() % 2 == 0)
	    {
            std::stringstream temp_stream;
            temp_stream << "ROIs definition must be in the format: 'channel_num; roi0_low; roi0_high; roi1_low; roi1_high; ...'";
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::manage_all_channels_tag_in_rois_list");
	    }
        
        // check the channel number
	    int channel_num = yat::StringUtil::to_num<int>(vec_rois.at(0));

        if(channel_num != DANTE_DPP_ROIS_MANAGER_ALL_CHANNELS)
        {
            if((channel_num < 0)||(channel_num >= static_cast<int>(m_channels_nb)))
            {
                std::stringstream temp_stream;
                temp_stream << "ROIs channel value must be between [0..." << (m_channels_nb - 1) << "]" << std::endl;
                temp_stream << "A channel value of " << channel_num << " is incorrect." << std::endl;
    		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::manage_all_channels_tag_in_rois_list");
            }
        }

        // vector size minus channel_num divided by 2 is the roi count
	    int roi_count = (int) ((vec_rois.size() - 1) / 2);

	    //for each roi defined par user, fix the low/high
	    int roi_num = 0; 
        
        while(roi_num < roi_count)
        {
            // check the limits
            int low  = yat::StringUtil::to_num<int>(vec_rois.at(1 + 2 * roi_num    ));
            int high = yat::StringUtil::to_num<int>(vec_rois.at(1 + 2 * roi_num + 1));

            if((low < 0) || (low >= static_cast<int>(m_bins_nb)))
            {
                std::stringstream temp_stream;
                temp_stream << "ROIs low bin value must be between [0..." << (m_bins_nb - 1) << "]" << std::endl;
                temp_stream << "The low bin value of " << low 
                            << " for channel "  << channel_num 
                            << " is incorrect." << std::endl;
    		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::manage_all_channels_tag_in_rois_list");
            }

            if((high < 0) || (high >= static_cast<int>(m_bins_nb)))
            {
                std::stringstream temp_stream;
                temp_stream << "ROIs high bin value must be between [0..." << (m_bins_nb - 1) << "]" << std::endl;
                temp_stream << "The high bin value of " << high 
                            << " for channel "  << channel_num 
                            << " is incorrect." << std::endl;
    		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::manage_all_channels_tag_in_rois_list");
            }

            if(low > high)
            {
                std::stringstream temp_stream;
                temp_stream << "ROIs low bin value cannot be superior to high bin value." << std::endl;
                temp_stream << "The low bin value of "      << low 
                            << " and the high bin value of " << high
                            << " for channel "   << channel_num 
                            << " are incorrect." << std::endl;
    		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::manage_all_channels_tag_in_rois_list");
            }

            // merge the roi data to the channel(s) data
            if(channel_num != DANTE_DPP_ROIS_MANAGER_ALL_CHANNELS)
            {
                std::stringstream temp_stream;
                temp_stream << rois_list[channel_num] << g_rois_delimiter << low << g_rois_delimiter << high;
                rois_list[channel_num] = temp_stream.str();
            }
            else
            {
                for(std::size_t channel_index = 0; channel_index < m_channels_nb; channel_index++)
	            {
                    std::stringstream temp_stream;
                    temp_stream << rois_list[channel_index] << g_rois_delimiter << low << g_rois_delimiter << high;
                    rois_list[channel_index] = temp_stream.str();
                }
            }

            roi_num++;
        }
    }

    in_out_rois_list = rois_list;
}

//============================================================================================================
// ROIs file
//============================================================================================================
/*******************************************************************
 * \brief Load a ROIs file
 * \param[in] in_complete_file_path rois file
 *******************************************************************/
void RoisManager::load_rois_file(const std::string & in_complete_file_path)
{
    // remove the ROIs from memory
    reset_rois();

    // load the file content in memory
    std::vector<std::string> rois_list = load_rois_file_to_rois_list(in_complete_file_path);

    // check the rois list content (limits and all channels tag)
    check_rois_list(rois_list);

    // add the ROIs
    add_with_rois_list(rois_list);
}

//============================================================================================================
// ROIs string
//============================================================================================================
/**************************************************************************
* \brief Get the rois string for all the channels
* \return rois string
**************************************************************************/
std::string RoisManager::get_rois_string() const
{
    std::stringstream rois_string;

    if(get_nb_rois() > 0)
    {
        std::vector<std::string> rois_list = get_rois_list();

        for (std::vector<std::string>::const_iterator search = rois_list.begin(); search != rois_list.end(); ++search)
        {
            if(search != rois_list.begin())
            {
                rois_string << g_groups_delimiter;
            }

            rois_string << *search;
        }
    }

    return rois_string.str();
}

/*******************************************************************
 * \brief Load a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void RoisManager::load_rois_string(const std::string & in_rois_string)
{
    // remove the ROIs from memory
    reset_rois();

    // convert the string to a list
    std::vector<std::string> rois_list = convert_rois_string_to_rois_list(in_rois_string);

    // check the rois list content (limits and all channels tag)
    check_rois_list(rois_list);

    // add the ROIs
    add_with_rois_list(rois_list);
}

/*******************************************************************
 * \brief Change ROIs using a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void RoisManager::change_rois_string(const std::string & in_rois_string)
{
    // convert the string to a list
    std::vector<std::string> rois_list = convert_rois_string_to_rois_list(in_rois_string);

    // check the rois list content (limits and all channels tag)
    check_rois_list(rois_list);

    // add the ROIs
    add_with_rois_list(rois_list);
}

/**************************************************************************************************
 * USER ERRORS MANAGEMENT
 **************************************************************************************************/
/**************************************************************************
* \brief Check the data before the erase of all the ROIs of a channel
* \param[in] in_channel_index channel index
**************************************************************************/
void RoisManager::check_before_erase_rois(int in_channel_index) const
{
    if(in_channel_index == DANTE_DPP_ROIS_MANAGER_ALL_CHANNELS)
    {
        if(get_nb_rois() == 0)
        {
            std::stringstream temp_stream;
            temp_stream << "There is no ROI to erase!";
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::check_before_erase_rois");
        }
    }
    else
    {
        if(get_nb_rois(static_cast<std::size_t>(in_channel_index)) == 0)
        {
            std::stringstream temp_stream;
            temp_stream << "Problem! Channel (" << in_channel_index << ") does not have any ROI!";
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (temp_stream.str()).c_str(), "RoisManager::erase_rois");
        }
    }
}

/*******************************************************************
 * \brief Check the data before the change of ROIs using a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void RoisManager::check_before_change_rois_string(const std::string & in_rois_string) const
{
    // convert the string to a list
    std::vector<std::string> rois_list = convert_rois_string_to_rois_list(in_rois_string);

    // check the rois list content (limits and all channels tag)
    check_rois_list(rois_list);
}

/**************************************************************************************************
 * SINGLETON MANAGEMENT
 **************************************************************************************************/
/*******************************************************************
 * \brief Create the manager
 *******************************************************************/
void RoisManager::create()
{
    RoisManager::g_singleton.reset(new RoisManager());
}

//###########################################################################
}