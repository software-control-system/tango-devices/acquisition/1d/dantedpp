/*************************************************************************/
/*! 
 *  \file   RoisViewParameters.h
 *  \brief  class used to manage the RoisView parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROIS_VIEW_PARAMETERS_H_
#define DANTE_DPP_ROIS_VIEW_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the RoisView initialization*/
class RoisViewParameters
{
friend class Controller;
friend class RoisView  ;

public:
    // constructor
    RoisViewParameters();

private :
    std::size_t m_modules_nb ;
    std::size_t m_channels_nb;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ROIS_VIEW_PARAMETERS_H_

//###########################################################################
