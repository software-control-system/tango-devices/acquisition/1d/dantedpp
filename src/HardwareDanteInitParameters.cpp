/*************************************************************************/
/*! 
 *  \file   HardwareDanteInitParameters.cpp
 *  \brief  class used to manage the HardwareDante init parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "HardwareDanteInitParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// HardwareDanteInitParameters class
//============================================================================================================
/**************************************************************************
* \brief simple constructor
**************************************************************************/
HardwareDanteInitParameters::HardwareDanteInitParameters()
{
}

/**************************************************************************
* \brief constructor
* \param[in] in_type type of HardwareDante
**************************************************************************/
HardwareDanteInitParameters::HardwareDanteInitParameters(HardwareDanteInitParameters::Type in_type)
{
    m_type = in_type;
}

//###########################################################################
}