/*************************************************************************/
/*! 
 *  \file   AsyncRequestGroup.cpp
 *  \brief  class used to manage the AsyncRequest container
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// SYSTEM
#include <algorithm>

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "Log.h"
#include "Error.h"
#include "AsyncRequestGroup.h"

namespace DanteDpp_ns
{
#define ASYNC_REQUEST_GROUP_WAIT_TIME_OUT_MSECS ((unsigned long)(10 * 1000))

/************************************************************************
 * \brief constructor
 * \param[in] in_max_nb_replies maximum number of replies for the requests
 ************************************************************************/
AsyncRequestGroup::AsyncRequestGroup(std::size_t in_max_nb_replies) : m_condition_is_not_full(m_mutex_is_not_full)
{
    m_nb_replies     = 0;
    m_max_nb_replies = in_max_nb_replies;
}

/*******************************************************************
 * \brief add a new request 
 * \param[in] in_request_id identifier of the request
 *******************************************************************/
void AsyncRequestGroup::add_request(uint32_t in_request_id)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    yat::SharedPtr<AsyncRequest> request;
    request.reset(new AsyncRequest(in_request_id));

    m_requests.push_back(request);
}

/*******************************************************************
 * \brief search a request 
 * \param[in] in_request_id identifier of the request
 * \return a smart pointer with the request or a null value
 *******************************************************************/
yat::SharedPtr<AsyncRequest> AsyncRequestGroup::search_request(uint32_t in_request_id)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    yat::SharedPtr<AsyncRequest> request;

    for(std::size_t request_index = 0 ; request_index < m_requests.size() ; request_index++)
    {
        if(m_requests[request_index]->m_request_id == in_request_id)
        {
            request = m_requests[request_index];
            break;
        }
    }

    return request;
}

/*******************************************************************
 * \brief get a request 
 * \param[in] in_request_index index of the request
 * \return a smart pointer with the request or a null value
 *******************************************************************/
yat::SharedPtr<AsyncRequest> AsyncRequestGroup::get_request(std::size_t in_request_index)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return m_requests[in_request_index];
}

/*******************************************************************
 * \brief get the number of requests 
 * \return number of requests
 *******************************************************************/
std::size_t AsyncRequestGroup::get_requests_nb()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return m_requests.size();
}

/*******************************************************************
 * \brief manage the reply of a request
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 *******************************************************************/
void AsyncRequestGroup::manage_reply(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data)
{
    //  1) search the request
    yat::SharedPtr<AsyncRequest> request = search_request(in_call_id);

    if(request.is_null())
    {
    #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
        INFO_STRM << "Problem! Request id (" << in_call_id << ") is not present in the container of its request group!" << std::endl;
    #endif
        return;
    }

    // 2) check if the type, the in_length and the data pointer are correct    
    if((in_type == DANTE_DPP_ASYNC_REQUEST_OP_TYPE_ERROR)||(!in_length)||(!in_data))
    {
        request->m_error_code = DANTE_DPP_ERROR_CODE_ASYNC_REPLY_PROBLEM;
    }

    // 3) check if the lenght is coherent with a write type
    if((in_type == DANTE_DPP_ASYNC_REQUEST_OP_TYPE_WRITE)&&(in_length > 1))
    {
    #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
        INFO_STRM << "Problem! Incoherent lenght (" << in_length << ") for a write request id (" << in_call_id << ")!" << std::endl;
    #endif
        request->m_error_code = DANTE_DPP_ERROR_CODE_ASYNC_REPLY_PROBLEM;
    }

    // 4) store the data into the request
    request->m_data = std::vector<uint32_t>(in_data, in_data + in_length); 

    {
        // 5) update the number of filled requests in the requests group
        // protecting the multi-threads access
        yat::AutoMutex<> lock(m_mutex);

        m_nb_replies++;

        // 6) check if all the replies were received
        if(all_replies_received())
        {
        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
            INFO_STRM << "All replies received for requests group id (" << m_requests[0]->m_request_id << ")!" << std::endl;
        #endif

            // unblocking threads because all replies were received
            yat::AutoMutex<> lock(m_mutex_is_not_full);
            m_condition_is_not_full.broadcast();
        }
    }
}

/*******************************************************************
 * \brief manage the wait of all replies
 * \return true if all replies were received, else false
 *******************************************************************/
bool AsyncRequestGroup::waiting_all_replies() const
{
    bool result = true;

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex_is_not_full);

    // if all the replies were received, do not block.
    if(!all_replies_received())
    {
        result = m_condition_is_not_full.timed_wait(ASYNC_REQUEST_GROUP_WAIT_TIME_OUT_MSECS);
    }

    return result;
}

/*******************************************************************
 * \brief check if all replies were received
 * \return true if all replies were received, else false
 *******************************************************************/
bool AsyncRequestGroup::all_replies_received() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return (m_nb_replies == m_max_nb_replies);
}

} // namespace DanteDpp_ns
