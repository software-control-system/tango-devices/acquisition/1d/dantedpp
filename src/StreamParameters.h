/*************************************************************************/
/*! 
 *  \file   StreamParameters.h
 *  \brief  class used to manage the stream parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_STREAM_PARAMETERS_H_
#define DANTE_DPP_STREAM_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the stream initialization*/
class StreamParameters
{
    friend class Stream;
    friend class StreamNo;
    friend class StreamLog;
    friend class StreamCsv;
    friend class StreamNexus;
    friend class Controller;

    public:
        // TYPES
        typedef enum 
        { 
            NO_STREAM   = 0,
            LOG_STREAM  ,
            CSV_STREAM  ,
            NEXUS_STREAM,
            TYPE_UNDEFINED,
        } Types;

        // WRITE MODES
        typedef enum 
        { 
            ASYNCHRONOUS     = 0,
            SYNCHRONOUS         ,
            DELAYED             ,
            WRITE_MODE_UNDEFINED,
        } WriteModes;

        // MEMORY MODES
        typedef enum 
        { 
            NO_COPY           = 0,
            COPY                 ,
            MEMORY_MODE_UNDEFINED,
        } MemoryModes;

        // simple constructor
        StreamParameters();
        
        // constructor
        StreamParameters(const StreamParameters::Types       in_type                   ,
                         const std::string &                 in_target_path            ,
                         const std::string &                 in_target_file            ,
                         const uint32_t                      in_nb_acq_per_file        ,
                         const uint32_t                      in_nb_data_per_acq        ,
                         const StreamParameters::WriteModes  in_write_mode             ,
                         const StreamParameters::MemoryModes in_memory_mode            ,
                         const bool                          in_file_generation        ,
                         const std::string &                 in_label_real_time        ,
                         const std::string &                 in_label_live_time        ,
                         const std::string &                 in_label_dead_time        ,
                         const std::string &                 in_label_input_count_rate ,
                         const std::string &                 in_label_output_count_rate,
                         const std::string &                 in_label_events_in_run    ,
                         const std::string &                 in_label_spectrum         ,
                         const std::string &                 in_label_roi              );

    private :
        StreamParameters::Types       m_type                   ; // type of stream
        std::string                   m_target_path            ; // path for the stream files
        std::string                   m_target_file            ; // name for the stream file
        uint32_t                      m_nb_acq_per_file        ; // number of acquisition per file
	    uint32_t                      m_nb_data_per_acq        ; // number of acquired frames
        StreamParameters::WriteModes  m_write_mode             ; // stream write mode
        StreamParameters::MemoryModes m_memory_mode            ; // stream memory mode
        bool                          m_file_generation        ; // file generation
        std::string                   m_label_real_time        ; // label in nexus file
        std::string                   m_label_live_time        ; // label in nexus file
        std::string                   m_label_dead_time        ; // label in nexus file
        std::string                   m_label_input_count_rate ; // label in nexus file
        std::string                   m_label_output_count_rate; // label in nexus file
        std::string                   m_label_events_in_run    ; // label in nexus file
        std::string                   m_label_spectrum         ; // label of spectrum data in nexus file
        std::string                   m_label_roi              ; // label of roi data in nexus file
    };

} // namespace DanteDpp_ns

#endif // DANTE_DPP_STREAM_PARAMETERS_H_

//###########################################################################
