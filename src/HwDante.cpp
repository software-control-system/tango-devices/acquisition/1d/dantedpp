/*************************************************************************/
/*! 
 *  \file   HwDante.cpp
 *  \brief  DANTE detector hardware class implementation
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

// SYSTEM
#include <cstdlib>
#include <cmath>

// PROJECT
#include "HwDante.h"
#include "Log.h"
#include "AsyncManager.h"
#include "ChannelData.h"
#include "DataStore.h"
#include "RoisManager.h"

// SDK
#include "DLL_DPP_Callback.h"

using namespace DanteDpp_ns;

#define HW_DANTE_WAITING_TIME_MS_AFTER_ADD_TO_QUERY_CALL (4000)
#define HW_DANTE_WAITING_TIME_MS_AFTER_START_CALL        (40000)

const std::vector<std::string> 
    HwDante::g_system_input_mode_labels{"DC_HighImp", "DC_LowImp", "AC_Slow", "AC_Fast"};

// number of elements in basic stats returned data by sdk (for one channel)
const int HwDante::g_nb_elem_basic_stats = 4 ;

// number of elements in advanced stats returned data by sdk (for one channel)
const int HwDante::g_nb_elem_adv_stats   = 22;

/************************************************************************
 * \brief constructor
 ************************************************************************/
HwDante::HwDante()
{
    INFO_STRM << "construction of HwDante" << std::endl;

    m_first_spectrum_received = false; 
    m_first_spectrum_id       = 0;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
HwDante::~HwDante()
{
    INFO_STRM << "destruction of HwDante" << std::endl;
}

//------------------------------------------------------------------
// init/terminate management
//------------------------------------------------------------------
/************************************************************************
 * \brief init the detector
 ************************************************************************/
void HwDante::init(void)
{
    HardwareDante::init();
}

/************************************************************************
 * \brief terminate the detector
 ************************************************************************/
void HwDante::terminate(void)
{
    HardwareDante::terminate();
}

//------------------------------------------------------------------
// connection management
//------------------------------------------------------------------
/************************************************************************
 * \brief connection to the detector
 ************************************************************************/
void HwDante::connect(const std::string & in_complete_file_path)
{
    // start generic code - load the configuration file in memory
    HardwareDante::generic_start_code_for_connect(in_complete_file_path);

    // first, initialize the library
    INFO_STRM << "DANTE Sdk init library" << std::endl;

    if(!InitLibrary())
    {
        manage_last_error("CONNECT_ERROR", "Library not initialized!", "HwDante::connect");
    }

    // register the replies callback
    if(!register_callback(reply_callback))
    {
        manage_last_error("CONNECT_ERROR", "Replies callback not registered!", "HwDante::connect");
    }

    // check if we are working with an IP or USB connection
    if(!m_config_parameters.m_master_board_ip.empty())
    {
        if(!add_to_query(const_cast<char*>(m_config_parameters.m_master_board_ip.c_str())))
        {
            std::stringstream temp_stream;
            temp_stream << "Problem with the IP: " << m_config_parameters.m_master_board_ip;
            manage_last_error("CONNECT_ERROR", temp_stream.str(), "HwDante::connect");
        }
        else
        {
            INFO_STRM << "Connection using IP:" << m_config_parameters.m_master_board_ip << std::endl;
        }
    }
    else
    {
        INFO_STRM << "USB connection selected." << std::endl;
    }

    // waiting for a few seconds (from XGLab advice)
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_ADD_TO_QUERY_CALL * 1000);

    // library version
    m_library_version = request_library_version();
    log_library_version();

    // check if there is a detector available
    uint16_t detectors = 0;

    if(!get_dev_number(detectors))
    {
        manage_last_error("LIBRARY_ERROR", "get_dev_number function failed!", "HwDante::connect");
    }

    INFO_STRM << "Detectors number found: " << detectors << std::endl;

    if(detectors == 0)
    {
        std::stringstream temp_stream;
        temp_stream << "Could not find any detector using IP: " << m_config_parameters.m_master_board_ip;
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::connect");
    }
    else
    if(detectors > 1)
    {
        std::stringstream temp_stream;
        temp_stream << "An IP address conflict occurs. Found several detector which use IP: " << m_config_parameters.m_master_board_ip;
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::connect");
    }
    else
    {
        m_master_identifier = get_master_identifier();
        INFO_STRM << "Master identifier: " << m_master_identifier << std::endl;
        
        uint16_t boards_nb;

        if(!get_boards_in_chain(m_master_identifier.c_str(), boards_nb))
        {
            manage_last_error("LIBRARY_ERROR", "get_boards_in_chain function failed!", "HwDante::connect");
        }

        m_modules_nb = static_cast<std::size_t>(boards_nb);
        INFO_STRM << "Boards number: " << m_modules_nb << std::endl;
    }

    // log the firmware versions
    m_firmwares_versions = request_firmwares_versions();
    log_firmwares_versions();

    // change the configuration of all boards (throws an exception in case of error)
    INFO_STRM << "Changing the general configuration..." << std::endl;
    request_change_configuration();

    // change the input for all boards (throws an exception in case of error)
    INFO_STRM << "Changing the input configuration..." << std::endl;
    request_change_input();

    // change the offsets for all boards (throws an exception in case of error)
    INFO_STRM << "Changing the offsets configuration..." << std::endl;
    request_change_offsets();

    // end generic code 
    HardwareDante::generic_end_code_for_connect();
}

/************************************************************************
 * \brief disconnection to the detector
 ************************************************************************/
void HwDante::disconnect(void)
{
    // start generic code
    HardwareDante::generic_start_code_for_disconnect();

    if(m_is_connected)
    {
        // at the moment there is an exception problem during the close library
        try
        {
            INFO_STRM << "DANTE Sdk closing library" << std::endl;
            if(!CloseLibrary())
            {
                manage_last_error("DISCONNECT_ERROR", "Library not closed!", "HwDante::disconnect");
            }
        }
        catch(...) {}
    }

    // end generic code 
    HardwareDante::generic_end_code_for_disconnect();
}

//------------------------------------------------------------------
// system informations
//------------------------------------------------------------------
/************************************************************************
 * \brief requests the library version
 * \return library version
 ************************************************************************/
std::string HwDante::request_library_version()
{
    std::string library_version;
    uint32_t    version_size   = 0   ;
    char    *   version        = NULL;
    
    // first, answering the string size
    if(!libVersion(version, version_size))
    {
        // normal error -> to get the string size
        if(!check_last_error(DLL_CHAR_STRING_SIZE))
        {
            manage_last_error("LIBRARY_ERROR", "Function failed!", "HwDante::get_library_version");
        }
    }
    else
    {
        throw DanteDpp_ns::Exception("LIBRARY_ERROR", "Incoherent function result!", "HwDante::get_library_version");
    }
    
    version_size++; // for the end of string character

    // secondly, getting the data
    version = new char[version_size];

    if(!libVersion(version, version_size))
    {
        manage_last_error("LIBRARY_ERROR", "Function failed!", "HwDante::get_library_version");
    }

    library_version = version;

    delete [] version;

    return library_version;
}

/************************************************************************
 * \brief gets the master identifier
 * \return master identifier
 ************************************************************************/
std::string HwDante::get_master_identifier()
{
    std::string master_identifier ;
    uint16_t    identifier_size   = 0   ;
    char    *   identifier        = NULL;
    uint16_t    nb                = 0   ;
    
    if(!get_ids(identifier, nb, identifier_size))
    {
        // normal error -> to get the string size
        if(!check_last_error(DLL_CHAR_STRING_SIZE))
        {
            manage_last_error("LIBRARY_ERROR", "Function failed!", "HwDante::get_master_identifier");
        }
    }
    else
    {
        throw DanteDpp_ns::Exception("LIBRARY_ERROR", "Incoherent function result!", "HwDante::get_master_identifier");
    }
    
    identifier_size++; // for the end of string character

    // secondly, getting the data
    identifier = new char[identifier_size];

    if(!get_ids(identifier, nb, identifier_size))
    {
        manage_last_error("LIBRARY_ERROR", "Function failed!", "HwDante::get_master_identifier");
    }

    master_identifier = identifier;

    delete [] identifier;

    return master_identifier;
}

/************************************************************************
 * \brief requests the firmwares versions of all boards
 * \return boards firmwares versions
 ************************************************************************/
std::vector<std::string> HwDante::request_firmwares_versions()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id	= getFirmware(m_master_identifier.c_str(), module_index);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_firmwares_versions");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
        std::vector<std::string> firmwares_versions;

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);
            const std::vector<uint32_t> & data    = request->get_data();
            
            std::stringstream temp_stream;
            temp_stream << data[0] << "." << data[1] << "." << data[2]; 
            firmwares_versions.push_back(temp_stream.str());
        }
    
        return firmwares_versions;
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not get the firmware versions of the boards!";
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_firmwares_versions");
    }
}

//------------------------------------------------------------------
// configuration management
//------------------------------------------------------------------
/************************************************************************
 * \brief requests the change of configuration of all boards
 ************************************************************************/
void HwDante::request_change_configuration()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        std::size_t acq_parameters_index = 0; // master board by default if we cannot found a specific configuration
        
        // search the board index in the acq paramaters container
        std::vector<HardwareDanteConfigParameters::AcqParameters>::const_iterator it;

        for (it = m_config_parameters.m_acq_parameters.begin(); it != m_config_parameters.m_acq_parameters.end(); ++it) 
        {
            if((*it).m_board_index == module_index)
            {
                acq_parameters_index = it - m_config_parameters.m_acq_parameters.begin();
                DEBUG_STRM << "found specific acquisition for board: " << module_index 
                           << " in the position: " << acq_parameters_index << std::endl;
                break;
            }
        }

        const HardwareDanteConfigParameters::AcqParameters & acq_parameters = m_config_parameters.m_acq_parameters[acq_parameters_index];

        // fill the configuration structure
        configuration cfg;

        // default values (for information)
        // cfg.fast_filter_thr     = 0;    // Detection threshold [unit: spectrum BINs]
        // cfg.energy_filter_thr   = 0;    // Detection threshold [unit: spectrum BINs]
        // cfg.energy_baseline_thr = 0;    // Energy threshold for baseline [unit: spectrum BINs]
        // cfg.max_risetime        = 30;   // Max risetime setting for pileup detection 
        // cfg.gain                = 1;    // Main gain: (spectrum BIN)/(ADC's LSB)
        // cfg.peaking_time        = 25;   // Main energy filter peaking time   [unit: 32 ns samples]
        // cfg.max_peaking_time    = 0;    // Max energy filter peaking time   [unit: 32 ns samples]
        // cfg.flat_top            = 5;    // Main energy filter flat top   [unit: 32 ns samples]
        // cfg.edge_peaking_time   = 4;    // Edge detection filter peaking time   [unit: 8 ns samples]
        // cfg.edge_flat_top       = 1;    // Edge detection filter flat top   [unit: 8 ns samples]
        // cfg.reset_recovery_time = 300;  // Reset recovery time [unit: 8 ns samples]
        // cfg.zero_peak_freq      = 1;    // Zero peak rate [kcps]
        // cfg.baseline_samples    = 64;   // Baseline samples for baseline correction [32 ns samples]
        // cfg.inverted_input      = 0;    // pulse inversion
        // cfg.time_constant       = 0;    // Time constant for continuos reset signal
        // cfg.base_offset         = 0;    // Baseline of the continous reset signal [ADC's LSB]
        // cfg.overflow_recovery   = 0;    // Overflow recovery time [8ns samples]
        // cfg.reset_threshold     = 1000; // Reset detection threshold [ADC's LSB]
        // cfg.tail_coefficient    = 0;    // Tail coefficient
        // cfg.other_param         = 0;    // currently ignored    

        cfg.fast_filter_thr     = acq_parameters.m_fast_filter_thr    ; // Detection threshold [unit: spectrum BINs]
        cfg.energy_filter_thr   = acq_parameters.m_energy_filter_thr  ; // Detection threshold [unit: spectrum BINs]
        cfg.energy_baseline_thr = acq_parameters.m_energy_baseline_thr; // Energy threshold for baseline [unit: spectrum BINs]
        cfg.max_risetime        = acq_parameters.m_max_risetime       ; // Max risetime setting for pileup detection 
        cfg.gain                = acq_parameters.m_gain               ; // Main gain: (spectrum BIN)/(ADC's LSB)
        cfg.peaking_time        = acq_parameters.m_peaking_time       ; // Main energy filter peaking time   [unit: 32 ns samples]
        cfg.max_peaking_time    = acq_parameters.m_max_peaking_time   ; // Max energy filter peaking time   [unit: 32 ns samples]
        cfg.flat_top            = acq_parameters.m_flat_top           ; // Main energy filter flat top   [unit: 32 ns samples]
        cfg.edge_peaking_time   = acq_parameters.m_edge_peaking_time  ; // Edge detection filter peaking time   [unit: 8 ns samples]
        cfg.edge_flat_top       = acq_parameters.m_edge_flat_top      ; // Edge detection filter flat top   [unit: 8 ns samples]
        cfg.reset_recovery_time = acq_parameters.m_reset_recovery_time; // Reset recovery time [unit: 8 ns samples]
        cfg.zero_peak_freq      = acq_parameters.m_zero_peak_freq     ; // Zero peak rate [kcps]
        cfg.baseline_samples    = acq_parameters.m_baseline_samples   ; // Baseline samples for baseline correction [32 ns samples]
        cfg.inverted_input      = acq_parameters.m_inverted_input     ; // pulse inversion
        cfg.time_constant       = acq_parameters.m_time_constant      ; // Time constant for continuous reset signal
        cfg.base_offset         = acq_parameters.m_base_offset        ; // Baseline of the continuous reset signal [ADC's LSB]
        cfg.overflow_recovery   = acq_parameters.m_overflow_recovery  ; // Overflow recovery time [8ns samples]
        cfg.reset_threshold     = acq_parameters.m_reset_threshold    ; // Reset detection threshold [ADC's LSB]
        cfg.tail_coefficient    = acq_parameters.m_tail_coefficient   ; // Tail coefficient
        cfg.other_param         = acq_parameters.m_other_param        ; // currently ignored

        uint32_t request_id	= configure(m_master_identifier.c_str(), module_index, cfg);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_change_configuration");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);

            if(request->get_data()[0] != 1)
            {
                std::stringstream temp_stream;
                temp_stream << "Could not change the configuration of board: " << module_index;
                throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_configuration");
            }
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not change the configuration of boards!";
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_configuration");
    }
}

/************************************************************************
 * \brief requests the change of input for all boards
 ************************************************************************/
void HwDante::request_change_input()
{
    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // select the good input
    InputMode input_mode;

    switch (m_config_parameters.m_input_mode)
    {
        case HardwareDanteConfigParameters::InputMode::DC_HighImp: input_mode = InputMode::DC_HighImp; break;
        case HardwareDanteConfigParameters::InputMode::DC_LowImp : input_mode = InputMode::DC_LowImp ; break;
        case HardwareDanteConfigParameters::InputMode::AC_Slow   : input_mode = InputMode::AC_Slow   ; break;
        case HardwareDanteConfigParameters::InputMode::AC_Fast   : input_mode = InputMode::AC_Fast   ; break;
        default: throw DanteDpp_ns::Exception("CONNECT_ERROR", "Unkown input mode!", "HwDante::request_change_input"); break;
    }

    INFO_STRM << "Selected input configuration: " << g_system_input_mode_labels[input_mode] << std::endl;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id	= configure_input(m_master_identifier.c_str(), module_index, input_mode);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_change_input");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);

            if(request->get_data()[0] != 1)
            {
                std::stringstream temp_stream;
                temp_stream << "Could not change the input of board: " << module_index;
                throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_input");
            }
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not change the input of boards!";
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_input");
    }
}

/************************************************************************
 * \brief requests the change of offsets for all boards
 ************************************************************************/
void HwDante::request_change_offsets()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // fill the offset structure
    configuration_offset cfg_offset;
    
    cfg_offset.offset_val1 = m_config_parameters.m_offset_val_digpot1;
    cfg_offset.offset_val2 = m_config_parameters.m_offset_val_digpot2;

    INFO_STRM << "Selected offset_val1: " << cfg_offset.offset_val1 << std::endl;
    INFO_STRM << "Selected offset_val2: " << cfg_offset.offset_val2 << std::endl;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id	= configure_offset(m_master_identifier.c_str(), module_index, cfg_offset);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_change_offsets");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);

            if(request->get_data()[0] != 1)
            {
                std::stringstream temp_stream;
                temp_stream << "Could not change the offsets of board: " << module_index;
                throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_offsets");
            }
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not change the offsets of boards!";
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_change_offsets");
    }
}

//------------------------------------------------------------------
// state management
//------------------------------------------------------------------
/************************************************************************
 * \brief requests the states of all boards and computes a general state
 * \return hardware state
 ************************************************************************/
Tango::DevState HwDante::request_state()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id	= isRunning_system( m_master_identifier.c_str(), module_index);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_state");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
        uint32_t is_running = 0;

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);
            is_running |= request->get_data()[0]; // if there is at least a 1 for a board, isRunning value will be also 1
        }

        return (is_running) ? Tango::RUNNING : Tango::STANDBY;
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not get the boards'state!";
        throw DanteDpp_ns::Exception("CONNECT_ERROR", temp_stream.str(), "HwDante::request_state");
    }
}

//------------------------------------------------------------------
// acquisition management
//------------------------------------------------------------------
/************************************************************************
 * \brief start detector acquisition
 * \return true if detector is properly started, false otherwise
 ************************************************************************/
bool HwDante::start_detector_acquisition(void)
{
    bool result = true;

    try
    {
        // always need to stop the previous acquisition
        request_stop();

        // reinit the first spectrum management
        m_first_spectrum_received = false; 
        m_first_spectrum_id       = 0;

        // for MCA acquisition mode
        if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
        {
            request_start();
        }
        else
        // for MAPPING acquisition mode
        if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MAPPING)
        {
            request_trigger_mode_change();
            request_start_map();
        }
        else
        {
            result = false;
        }

        // waiting for the status change to running
        if(result)
        {
            int elapsed_time = 0;
            
            // forcing a state request
            update_state();

            while(m_state_status.get_state() != Tango::RUNNING)
            {
                // check if the acquisition is already ended
                if(is_last_data_received())
                {
                    break;
                }
                                
                // timeout ?
                if(elapsed_time >= HW_DANTE_WAITING_TIME_MS_AFTER_START_CALL)
                {
                    result = false;
                    break;
                }

                ::usleep(100 * 1000);
                elapsed_time += 100; // in ms
                update_state();
            }

            INFO_STRM << "HwDante::start_detector_acquisition - acquisition is running..." << std::endl;
        }
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        m_state_status.on_fault(in_ex);
        result = false;
    }

    return result;
}

/************************************************************************
 * \brief stop detector acquisition
 ************************************************************************/
void HwDante::stop_detector_acquisition(void)
{
    try
    {
        if(m_state_status.get_state() == Tango::RUNNING)
        {
            INFO_STRM << "--------------------------------------------------" << std::endl;
            INFO_STRM << "HwDante::stop_detector_acquisition"                 << std::endl;

            request_stop();
        }
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        m_state_status.on_fault(in_ex);
    }
}

/************************************************************************
 * \brief treat acquisitions
 ************************************************************************/
void HwDante::treat_acquisitions(void)
{
    // MCA acquisition ?        
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        treat_mca_acquisitions();
    }
    else
    // MAPPING acquisition ?        
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        for(;;)
        {
            if(!treat_mapping_acquisitions())
                break;
        }
    }
}

/************************************************************************
 * \brief treat MCA acquisitions
 ************************************************************************/
void HwDante::treat_mca_acquisitions(void)
{
    // end of acquisition ?
    if((m_state_status.get_state() == Tango::STANDBY) && (is_last_data_received()))
    {
        // prepare the data to store the spectra and statistics
        uint32_t              id;
        statistics            stats;
        std::vector<uint64_t> spectrum;

        uint32_t    bins_nb    = get_bins_nb();
        std::size_t bins_shift = get_bins_shift();

        spectrum.resize(bins_nb);

        memset(&stats, 0, sizeof(statistics));

        // get the spectra
        std::size_t channels_nb = get_channels_nb();

        for(std::size_t channel_index = 0; channel_index < channels_nb; channel_index++)
        {
            if(!getData(m_master_identifier.c_str(), channel_index, spectrum.data(), id, stats, bins_nb))
            {
                std::stringstream temp_stream;
                temp_stream << "Could not get the spectrum of board:" << channel_index << "!";
                manage_last_error("ACQ_ERROR", temp_stream.str(), "HwDante::treat_mca_acquisitions");
            }

            DEBUG_STRM << "channel index: " << channel_index   << std::endl;
            DEBUG_STRM << "real_time: "     << stats.real_time << std::endl;
            DEBUG_STRM << "live_time: "     << stats.live_time << std::endl;
            DEBUG_STRM << "ICR: "           << stats.ICR       << std::endl;
            DEBUG_STRM << "OCR: "           << stats.OCR       << std::endl;
            DEBUG_STRM << "measured: "      << stats.measured  << std::endl;

            yat::SharedPtr<ChannelData> channel_data;

            std::size_t rois_nb = RoisManager::get_const_instance()->get_nb_rois(channel_index);

            channel_data = new ChannelData(channel_index, bins_nb, rois_nb);

            double   real_time         = static_cast<double>(stats.real_time) / (1000.0 * 1000.0);
	        double   live_time         = static_cast<double>(stats.live_time) / (1000.0 * 1000.0);
            double   dead_time         = (real_time != 0.0) ? (100.0 * (1.0 - (live_time / real_time))) : 0.0;
            double   input_count_rate  = stats.ICR * 1000.0;
            double   output_count_rate = stats.OCR * 1000.0;
            uint32_t events_in_run     = stats.measured;

            channel_data->set_real_time        (real_time        );
            channel_data->set_live_time        (live_time        );
            channel_data->set_dead_time        (dead_time        );
            channel_data->set_input_count_rate (input_count_rate );
            channel_data->set_output_count_rate(output_count_rate);
            channel_data->set_events_in_run    (events_in_run    );

            channel_data->copy_to_spectrum(spectrum.data() + bins_shift, 
                                           bins_shift,
                                           bins_nb - bins_shift);
            
            // compute the ROIs data
            if(rois_nb > 0)
            {
                for(std::size_t roi_index = 0; roi_index < rois_nb; roi_index++)
                {
                    yat::SharedPtr<const RoiRange> roi_range = RoisManager::get_const_instance()->get_roi(channel_index, roi_index);
                    channel_data->compute_roi(roi_index, roi_range->get_bin_index(), roi_range->get_bin_size());
                }
            }

            DataStore::get_instance()->add_channel_data(0, channel_data);
        }
    }
}

/************************************************************************
 * \brief treat MAPPING acquisitions
 * \return true if there was at least one spectrum received, else false
 ************************************************************************/
bool HwDante::treat_mapping_acquisitions(void)
{
    bool result = false;

    uint32_t    bins_nb    = get_bins_nb();
    std::size_t bins_shift = get_bins_shift();

    bool last_data_received = is_last_data_received();

    // get the spectra
    std::size_t channels_nb = get_channels_nb();

    for(std::size_t channel_index = 0; channel_index < channels_nb; channel_index++)
    {
        uint32_t spectra_number;

        // check the number of spectra available for the board
        if(!getAvailableData(m_master_identifier.c_str(), channel_index, spectra_number))
        {
            std::stringstream temp_stream;
            temp_stream << "Could not get the number of spectra of board:" << channel_index << "!";
            manage_last_error("ACQ_ERROR", temp_stream.str(), "HwDante::treat_mapping_acquisitions");
        }

        if(spectra_number == 0)
        {
            continue;
        }

        result = true;

        // prepare the data to store the spectra and statistics
        std::vector<uint16_t> spectra_16;
        std::vector<uint32_t> ids       ;
        std::vector<double>   stats     ;
        std::vector<uint64_t> advstats  ;

        spectra_16.resize(spectra_number * bins_nb);
        ids.resize(spectra_number);
        stats.resize(spectra_number * g_nb_elem_basic_stats);
        advstats.resize(spectra_number * g_nb_elem_adv_stats);
        
        if(!getAllData(m_master_identifier.c_str(),
                       channel_index, 
                       spectra_16.data(),
                       ids.data(),
                       stats.data(),
                       advstats.data(),
                       bins_nb,
                       spectra_number))
        {
            std::stringstream temp_stream;
            temp_stream << "Could not get the spectra of board:" << channel_index << "!";
            manage_last_error("ACQ_ERROR", temp_stream.str(), "HwDante::treat_mapping_acquisitions");
        }

        // manage the first spectrum received id 
        if(!m_first_spectrum_received)
        {
            m_first_spectrum_id       = ids[0];
            m_first_spectrum_received = true  ;
        }

        // convert 16 bits spectra to 64 bits spectra (till a sdk change)
        std::vector<uint64_t> spectra;

        {
            std::size_t spectra_elements = spectra_16.size();
            spectra.resize(spectra_elements);

            uint64_t       * dest   = spectra.data();
            const uint16_t * source = spectra_16.data();

            while(spectra_elements--)
            {
                *dest++ = static_cast<uint64_t>(*source++);
            }
        }

        std::size_t rois_nb = RoisManager::get_const_instance()->get_nb_rois(channel_index);

        for(std::size_t spectrum_index = 0; spectrum_index < spectra_number; spectrum_index++)
        {
            yat::SharedPtr<ChannelData> channel_data;

            channel_data = new ChannelData(channel_index, bins_nb, rois_nb);

            double   real_time         = stats[spectrum_index * g_nb_elem_basic_stats    ] / (1000.0 * 1000.0);
	        double   live_time         = stats[spectrum_index * g_nb_elem_basic_stats + 1] / (1000.0 * 1000.0);
            double   dead_time         = (real_time != 0.0) ? (100.0 * (1.0 - (live_time / real_time))) : 0.0;
            double   input_count_rate  = stats[spectrum_index * g_nb_elem_basic_stats + 2] * 1000.0;
            double   output_count_rate = stats[spectrum_index * g_nb_elem_basic_stats + 3] * 1000.0;
            uint32_t events_in_run     = advstats[spectrum_index * g_nb_elem_adv_stats + 2];

            channel_data->set_real_time        (real_time        );
            channel_data->set_live_time        (live_time        );
            channel_data->set_dead_time        (dead_time        );
            channel_data->set_input_count_rate (input_count_rate );
            channel_data->set_output_count_rate(output_count_rate);
            channel_data->set_events_in_run    (events_in_run    );

            channel_data->copy_to_spectrum(spectra.data() + (spectrum_index * bins_nb) + bins_shift, 
                                           bins_shift,
                                           bins_nb - bins_shift);

            // compute the ROIs data
            if(rois_nb > 0)
            {
                for(std::size_t roi_index = 0; roi_index < rois_nb; roi_index++)
                {
                    yat::SharedPtr<const RoiRange> roi_range = RoisManager::get_const_instance()->get_roi(channel_index, roi_index);
                    channel_data->compute_roi(roi_index, roi_range->get_bin_index(), roi_range->get_bin_size());
                }
            }

            DataStore::get_instance()->add_channel_data((ids[spectrum_index] - m_first_spectrum_id), channel_data);
        }
    }

    // if there is no read data and the last data were already received, we return false to exit from the call loop
    if(result)
    {
        if(last_data_received)
            result = false;
    }

    return result;
}

/************************************************************************
 * \brief requests the change of the trigger mode for all the boards
 ************************************************************************/
void HwDante::request_trigger_mode_change()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // select the gating mode
    enum GatingMode gating_mode;

    if(m_trigger_mode == TriggerMode::TRIGGER_INTERNAL_MULTI)
    {
        gating_mode = GatingMode::FreeRunning;
    }
    else
    if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_GATE)
    {
        gating_mode = GatingMode::GatedHigh; 
    }
    else
    if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGRISE)
    {
        gating_mode = GatingMode::TriggerRising;
    }
    else
    if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGFALL)
    {
        gating_mode = GatingMode::TriggerFalling;
    }
    else
    if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGBOTH)
    {
        gating_mode = GatingMode::TriggerBoth;
    }
    else
    if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_GATELOW)
    {
        gating_mode = GatingMode::GatedLow;
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Unmanaged trigger mode: " << m_trigger_mode << " !";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_trigger_mode_change");
    }

    INFO_STRM << "Selected gating mode: " << gating_mode << std::endl;

    // request the change
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id	= configure_gating(m_master_identifier.c_str(), gating_mode, module_index);
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwDante::request_trigger_mode_change");
    }

    // wait all the replies
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);

        for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
        {
            yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);

            if(request->get_data()[0] != 1)
            {
                std::stringstream temp_stream;
                temp_stream << "Could not change the trigger mode of board: " << module_index;
                throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_trigger_mode_change");
            }
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not change the trigger mode of boards!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_trigger_mode_change");
    }
}

/************************************************************************
 * \brief requests the start of a MCA acquisition
 ************************************************************************/
void HwDante::request_start()
{
    // only for MCA acquisition mode
    if(get_acquisition_mode() != HardwareDanteConfigParameters::AcqMode::MCA)
    {
        std::stringstream temp_stream;
        temp_stream << "Could not start a MCA acquisition if we are not in MCA mode!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start");
    }

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    double   time        = get_preset_value(); // in seconds
    uint16_t spect_depth = get_bins_nb();

    INFO_STRM << "Starting a MCA acquisition of spectrum (" << spect_depth << ") for a duration of " 
              << time << " second(s)" << std::endl;

    // request the change
    std::vector<uint32_t> request_ids;
    uint32_t request_id	= start(m_master_identifier.c_str(), time, spect_depth);
    add_request_id(request_id, request_ids, 0, 1, "HwDante::request_start");

    // wait all the replies (just one this time)
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
        yat::SharedPtr<AsyncRequest>      request       = request_group->get_request(0);

        if(request->get_data()[0] != 1)
        {
            std::stringstream temp_stream;
            temp_stream << "Could not start the MCA acquisition for the boards!";
            throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start");
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not start the MCA acquisition for the boards!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start");
    }
}

/************************************************************************
 * \brief requests the start of a MAPPING acquisition
 ************************************************************************/
void HwDante::request_start_map()
{
    // only for MAPPING acquisition mode
    if(get_acquisition_mode() != HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        std::stringstream temp_stream;
        temp_stream << "Could not start a MAPPING acquisition if we are not in MAPPING mode!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start_map");
    }

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

	uint32_t sp_time =0;
	if(m_trigger_mode == TriggerMode::TRIGGER_INTERNAL_MULTI)
	{
		sp_time = get_preset_value() * 1000.0; // in milliseconds		
	}
	else if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_GATE)
	{
		sp_time = get_sp_time() * 1.0; // in milliseconds		
	}
    else if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGRISE)
    {
        sp_time = get_sp_time() * 1.0; // in milliseconds	
    }
    else if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGFALL)
    {
        sp_time = get_sp_time() * 1.0; // in milliseconds	
    }  
    else if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_TRIGBOTH)
    {
        sp_time = get_sp_time() * 1.0; // in milliseconds	
    }
    else if(m_trigger_mode == TriggerMode::TRIGGER_EXTERNAL_GATELOW)
    {
        sp_time = get_sp_time() * 1.0; // in milliseconds	
    }
	else
    {
        std::stringstream temp_stream;
        temp_stream << "Unmanaged trigger mode: " << m_trigger_mode << " !";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start_map");
    }
	
    uint32_t points      = get_nb_pixels();
    uint16_t spect_depth = get_bins_nb();

    INFO_STRM << "Starting a MAPPING acquisition of " << points << " spectra (" 
              << spect_depth << ") for a duration of " 
              << sp_time << " millisecond(s)" << std::endl;

    // request the change
    std::vector<uint32_t> request_ids;
    uint32_t request_id	= start_map(m_master_identifier.c_str(), sp_time, points, spect_depth);
    add_request_id(request_id, request_ids, 0, 1, "HwDante::request_start_map");

    // wait all the replies (just one this time)
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
        yat::SharedPtr<AsyncRequest>      request       = request_group->get_request(0);

        if(request->get_data()[0] != 1)
        {
            std::stringstream temp_stream;
            temp_stream << "Could not start the MAPPING acquisition for the boards!";
            throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start_map");
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not start the MAPPING acquisition for the boards!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_start_map");
    }
}

/************************************************************************
 * \brief requests the stop of acquisition
 ************************************************************************/
void HwDante::request_stop()
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_sdk_lock);

    INFO_STRM << "Stopping the acquisition" << std::endl;

    // request the change
    std::vector<uint32_t> request_ids;
    uint32_t request_id	= stop(m_master_identifier.c_str());
    add_request_id(request_id, request_ids, 0, 1, "HwDante::request_stop");

    // wait all the replies (just one this time)
    bool ok = wait_all_replies(request_ids);

    if(ok)
    {
        // treatment of the replies
        yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
        yat::SharedPtr<AsyncRequest>      request       = request_group->get_request(0);

        if(request->get_data()[0] != 1)
        {
            std::stringstream temp_stream;
            temp_stream << "Could not stop the acquisition for the boards!";
            throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_stop");
        }
        else
        {
            clear_chain(m_master_identifier.c_str());
        }
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Could not stop the acquisition for the boards!";
        throw DanteDpp_ns::Exception("ACQ_ERROR", temp_stream.str(), "HwDante::request_stop");
    }
}

/************************************************************************
 * \brief check if all the images were received
 * \return true if all the images were received, else false
 ************************************************************************/
bool HwDante::is_last_data_received(void)
{
    bool last_data_received = true;

    // request the informations
    for(uint16_t module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        bool temp_last_data_received;

        if(!isLastDataReceived(m_master_identifier.c_str(), module_index, temp_last_data_received))
        {
            manage_last_error("LIBRARY_ERROR", "Function failed!", "HwDante::is_last_data_received");
        }

        last_data_received &= temp_last_data_received;
    }

    return last_data_received;
}

//------------------------------------------------------------------
// error management
//------------------------------------------------------------------
/************************************************************************
 * \brief manages the current error and throws an exception
 * \param[in] in_reason   error reason
 * \param[in] in_desc     error description
 * \param[in] in_origin   error origin
 ************************************************************************/
void HwDante::manage_last_error(const std::string & in_reason,
                                const std::string & in_desc  ,
                                const std::string & in_origin)
{
    uint16_t error_code = DLL_NO_ERROR;
    bool result = getLastError(error_code);

    if(!result)
    {
        error_code = DANTE_DPP_ERROR_CODE_UNKNOWN;
    }
    else
    {
        resetLastError();
    }

    throw DanteDpp_ns::Exception(in_reason, in_desc, in_origin, error_code, Error::EnumSeverity::ERROR);
}

/************************************************************************
 * \brief checks the content of the last error
 * \param[in] in_error_code checks this error code
 * \return true if the last error code corresponds to in_error_code, else false
 ************************************************************************/
bool HwDante::check_last_error(const uint16_t in_error_code)
{
    uint16_t error_code = DLL_NO_ERROR;
    bool result = getLastError(error_code);

    if(!result)
    {
        error_code = DANTE_DPP_ERROR_CODE_UNKNOWN;
    }

    return (error_code == in_error_code);
}

//========================================================================================
