/*************************************************************************/
/*! 
 *  \file   Controller.cpp
 *  \brief  implementation of Controller methods.
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// SYSTEM
#include <vector>
#include <string>
#include <sstream> // std::ostringstream

// PROJECT
#include "Controller.h"
#include "Log.h"
#include "HwDante.h"
#include "HwSimulator.h"
#include "Helper.h"
#include "RoisManager.h"
#include "DataStore.h"
#include "CollectThread.h"
#include "StoreThread.h"

#ifndef DANTE_DPP_TANGO_INDEPENDANT
#include "DanteDpp.h"
#endif

#define INFO_DANTE_SDK_LABEL        "DANTE_SDK_VERSION"
#define INFO_DANTE_BOARDS_FIRMWARES "DANTE_BOARDS_FIRMWARES"

#define DANTE_DPP_VOID_CONFIGURATION_ALIAS "----"
#define DANTE_DPP_VOID_ROIS_ALIAS          "----"
#define DANTE_DPP_VOID_CONFIGURATION_FILE  "----"
#define DANTE_DPP_VOID_ROIS_FILE           "----"
#define DANTE_DPP_ROIS_ALIAS_CUSTOM        "**** CUSTOM ****"

#define DANTE_DPP_CONTROLLER_ROIS_CMD_WAITING_TIME_MS    (2000)
#define DANTE_DPP_CONTROLLER_LOAD_CONFIG_WAITING_TIME_MS (2000)

namespace DanteDpp_ns
{
//============================================================================================================
// Controller class
//============================================================================================================
/*******************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management
 *******************************************************************/
Controller::Controller(Tango::DeviceImpl * in_device) : yat4tango::DeviceTask(in_device), m_device(in_device)
{
    Log::create(m_device);
    INFO_STRM << "construction of Controller" << std::endl;

    m_configuration_state     = ConfigurationState::NOT_LOADED;
    m_rois_state              = ConfigurationState::NOT_LOADED;
    m_configuration_alias     = DANTE_DPP_VOID_CONFIGURATION_ALIAS;
    m_configuration_file_path = DANTE_DPP_VOID_CONFIGURATION_FILE;
    m_rois_alias              = DANTE_DPP_VOID_ROIS_ALIAS;
    m_rois_file_path          = DANTE_DPP_VOID_ROIS_FILE;

    enable_timeout_msg (false);
    enable_periodic_msg(false);
    set_periodic_msg_period(CONTROLLER_TASK_PERIODIC_MS);
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Controller::~Controller()
{
    INFO_STRM << "destruction of Controller" << std::endl;

    Log::release();
}

/*******************************************************************
 * \brief initializer - connection to the device
 * \param[in] in_hardware_parameters Allow to pass the needed data for
 *            the Hardware initialization.
 * \param[in] in_stream_parameters Allow to pass the needed data for
 *            the Stream initialization.
 *******************************************************************/
void Controller::init(const HardwareDanteInitParameters & in_hardware_parameters,
                      const StreamParameters            & in_stream_parameters  )
{
    INFO_STRM << "Initing the Controller" << std::endl;

    // create the detector : can be hardware or a simulator
    HardwareDante::create(in_hardware_parameters);
    IHardware::get_instance()->init();

    // create the ROIs manager
    RoisManager::create();

    // create the Stream manager
    Stream::create(in_stream_parameters);

    // create the DataStore
    DataStore::create();

    go(); // post the INIT msg         
}

/*******************************************************************
 * \brief releaser - disconnection from the device
 *******************************************************************/
void Controller::terminate(void)
{
    INFO_STRM << "Releasing the Controller" << std::endl;

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_threads_access_mutex);

        // store thread is running ?
        StoreThread::release();

        // collect thread is running ?
        CollectThread::release();
    }

    // remove the dynamics attributes which could have been created
#ifndef DANTE_DPP_TANGO_INDEPENDANT
    m_config_view.reset();
    m_rois_view.reset  ();
#endif

    // release the DataStore
    DataStore::release();

    // release the stream
    Stream::release();

    // release the ROIs manager
    RoisManager::release();

    // release the detector
    IHardware::get_instance()->terminate();
    IHardware::release();
}

//============================================================================================================
/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState Controller::get_state(void)
{
    compute_state_status();
    return m_state_status.get_state();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string Controller::get_status(void)
{
    return m_state_status.get_status();
}

/*******************************************************************
 * \brief Get a read and write access to the thread status instance
*******************************************************************/
StateStatus & Controller::get_threads_status(void)
{
    return m_threads_status;
}

/*******************************************************************
 * \brief Manage the abort command
*******************************************************************/
void Controller::abort(std::string status)
{
    DEBUG_STRM << "Controller::abort() - [BEGIN]" << std::endl;    

    yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_ABORT_MSG, DEFAULT_MSG_PRIORITY, true);
    msg->attach_data(status);
    post(msg);

    DEBUG_STRM << "Controller::abort() - [END]" << std::endl;
}

/*******************************************************************
 * \brief Compute the state and status 
*******************************************************************/
void Controller::compute_state_status()
{
    //- AutoLock the following
    yat::MutexLock scoped_lock(m_state_status.get_mutex());

    // error from one of the threads
    if(m_threads_status.get_state() == Tango::FAULT)
    {
        m_state_status.on_fault(m_threads_status.get_status());
    }
    else
    if(m_configuration_state == ConfigurationState::NOT_LOADED)
    {
        std::string status = "Use the 'LoadConfigFile' command to load a configuration file (*.ini) on the board !";
        m_state_status.set(Tango::OFF, status);
    }
    else
    if(m_configuration_state == ConfigurationState::LOADING)
    {
        std::ostringstream status;

        status << "Loading Configuration File " 
               << "[" << m_configuration_parameters.m_complete_file_path << "]" 
               << " of alias [" << m_configuration_parameters.m_config_alias << "]...";
        status << "\nPlease wait...";

        m_state_status.set(Tango::DISABLE, status.str());
    }
    else
    if(m_rois_state == ConfigurationState::LOADING)
    {
        std::ostringstream status;

        // load file command ?
        if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::LOAD_FILE)
        {
            status << "Loading Rois File " 
                   << "[" << m_rois_parameters.m_complete_file_path << "]" 
                   << " of alias [" << m_rois_parameters.m_rois_alias << "]...";
        }
        else
        // load list command ?
        if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::LOAD_LIST)
        {
            status << "Loading saved custom ROIs...";
        }
        else
        // change command ?
        if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::CHANGE)
        {
            status << "Changing ROIs...";
        }
        else
        // remove command ?
        if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::REMOVE)
        {
            if(m_rois_parameters.m_channels_index == ROIS_COMMAND_APPLY_ON_ALL_CHANNELS)
            {
                status << "Removing all ROIs...";
            }
            else
            {
                status << "Removing ROIs from channel [" << m_rois_parameters.m_channels_index << "]...";
            }
        }

        status << "\nPlease wait...";

        m_state_status.set(Tango::DISABLE, status.str());
    }
    else // ConfigurationState::LOADED
    {
        Tango::DevState detector_state  = IHardware::get_instance()->get_state();
        std::string     detector_status = IHardware::get_const_instance()->get_status();

        Tango::DevState stream_state    = Stream::get_const_instance()->get_state ();
        std::string     stream_status   = Stream::get_const_instance()->get_status();

        bool collect_thread_is_running;
        bool store_thread_is_running  ;

        // protecting the multi-threads access
        {
            yat::AutoMutex<> lock(m_threads_access_mutex);

            collect_thread_is_running  = (CollectThread::exist()) ? CollectThread::get_const_instance()->is_treatment_running() : false;
            store_thread_is_running    = (StoreThread::exist())   ? StoreThread::get_const_instance()->is_treatment_running()   : false;
        }

        if(detector_state == Tango::FAULT)
        {
            m_state_status.on_fault(detector_status);
        }
        else
        if(stream_state == Tango::FAULT)
        {
            m_state_status.on_fault(stream_status);
        }
        else
        if((detector_state == Tango::RUNNING) ||
           (stream_state   == Tango::RUNNING) ||
           (collect_thread_is_running       ) ||
           (store_thread_is_running         ))
        {
            m_state_status.set(Tango::RUNNING, "Acquisition is running...");
        }
        else
        {
            m_state_status.set(detector_state, detector_status);
        }
    }
}

/*******************************************************************
 * \brief Check if an acquisition is running
 * \return true if an acquisition is running, else false
*******************************************************************/
bool Controller::acquisition_is_running(void)
{
    return (IHardware::get_instance()->get_state() == Tango::RUNNING);
}

//============================================================================================================
/*******************************************************************
 * \brief Gets the number of modules
 * \return number of modules
*******************************************************************/
std::size_t Controller::get_modules_nb(void) const
{
    return IHardware::get_const_instance()->get_modules_nb();
}

/************************************************************************
 * \brief gets the number of channels
 * \return number of channels
 ************************************************************************/
std::size_t Controller::get_channels_nb() const
{
    return IHardware::get_const_instance()->get_channels_nb();
}

/************************************************************************
 * \brief gets the number of bins
 * \return number of bins
 ************************************************************************/
std::size_t Controller::get_bins_nb() const
{
    return IHardware::get_const_instance()->get_bins_nb();
}

/*******************************************************************
 * \brief Get informations about boards and sdk
 * \return informations about boards and sdk
*******************************************************************/
std::vector<std::pair<std::string, std::string> > Controller::get_versions_informations(void) const
{
    yat::SharedPtr<const HardwareDante> hardware = IHardware::get_const_instance();

    std::vector<std::pair<std::string, std::string> > informations;

    std::string         tempString      ;
    vector<std::string> tempVectorString;
    std::size_t         index           ;

    informations.clear();

    // SDK library version
    {
        std::stringstream tempStream;
        tempStream << hardware->get_library_version();
        informations.push_back(std::pair<std::string, std::string>(INFO_DANTE_SDK_LABEL, tempStream.str()));
    }

    // Boards firmwares
    {
        std::stringstream tempStream;
        tempVectorString = hardware->get_firmwares_versions();

        for(index = 0 ; index < tempVectorString.size() ; index++)
        {
            if(index > 0)
            {
                tempStream << ", ";
            }

            tempStream << "(" << (index + 1) << ") " << tempVectorString[index];
        }

        informations.push_back(std::pair<std::string, std::string>(INFO_DANTE_BOARDS_FIRMWARES, tempStream.str()));
    }

    return informations;
}

//============================================================================================================
/************************************************************************
 * \brief gets the acquisition mode
 * \return acquisition mode
 ************************************************************************/
HardwareDanteConfigParameters::AcqMode Controller::get_acquisition_mode() const
{
    yat::SharedPtr<const HardwareDante> hardware = IHardware::get_const_instance();
    return hardware->get_acquisition_mode();
}

/************************************************************************
 * \brief gets the acquisition mode label
 * \return acquisition mode label
 ************************************************************************/
std::string Controller::get_acquisition_mode_label() const
{
    return Helper::convert_acq_mode_to_tango(get_acquisition_mode());
}

/************************************************************************
 * \brief gets the trigger mode label
 * \return acquisition mode label
 ************************************************************************/
std::string Controller::get_trigger_mode_label() const
{
    yat::SharedPtr<const HardwareDante> hardware = IHardware::get_const_instance();
    return Helper::convert_trigger_mode_to_tango(hardware->get_trigger_mode());
}

/************************************************************************
 * \brief sets the trigger mode with a label
 * \param[in] in_tango_trigger_mode trigger mode label
 ************************************************************************/
void Controller::set_trigger_mode_label(const std::string & in_tango_trigger_mode)
{
    yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
    hardware->set_trigger_mode(Helper::convert_tango_to_trigger_mode(in_tango_trigger_mode));
}

/************************************************************************
 * \brief gets the detector type label
 * \return detector type label
 ************************************************************************/
std::string Controller::get_detector_type_label() const
{
    yat::SharedPtr<const HardwareDante> hardware = IHardware::get_const_instance();
    return Helper::convert_detector_type_to_tango(hardware->get_detector_type());
}

//============================================================================================================
/************************************************************************
 * \brief gets the realtime value of channel
 * \return realtime value
 ************************************************************************/
double Controller::get_realtime(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_real_time();
    }

    return result;
}

/************************************************************************
 * \brief gets the livetime value of channel
 * \return livetime value
 ************************************************************************/
double Controller::get_livetime(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_live_time();
    }

    return result;
}

/************************************************************************
 * \brief gets the deadtime value of channel
 * \return deadtime value
 ************************************************************************/
double Controller::get_deadtime(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_dead_time();
    }

    return result;
}

/************************************************************************
 * \brief gets the input count rate value of channel
 * \return input count rate value
 ************************************************************************/
double Controller::get_input_count_rate(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_input_count_rate();
    }

    return result;
}

/************************************************************************
 * \brief gets the output count rate value of channel
 * \return output count rate value
 ************************************************************************/
double Controller::get_output_count_rate(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_output_count_rate();
    }

    return result;
}

/************************************************************************
 * \brief gets the events in run value of channel
 * \return events in run value
 ************************************************************************/
uint32_t Controller::get_events_in_run(std::size_t in_channel_index) const
{
    double result = 0.0;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_events_in_run();
    }

    return result;
}

/************************************************************************
 * \brief gets the spectrum of a channel
 * \return channel spectrum value
 ************************************************************************/
std::vector<uint32_t> Controller::get_channel(std::size_t in_channel_index) const
{
    std::vector<uint32_t> result;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_spectrum();
    }

    return result;
}

/************************************************************************
 * \brief gets the roi value
 * \return roi value
 ************************************************************************/
uint32_t Controller::get_roi(std::size_t in_channel_index, std::size_t in_roi_index) const
{
    uint64_t result = 0LL;

    yat::SharedPtr<const ModuleData> module_data = DataStore::get_const_instance()->get_latest_module_data();

    if(!module_data.is_null())
    {
        yat::SharedPtr<const ChannelData> channel_data = module_data->get_channel(in_channel_index);
        result = channel_data->get_rois(in_roi_index);
    }

    return result;
}

/************************************************************************
 * \brief gets the preset value
 * \return events in run value
 ************************************************************************/
double Controller::get_preset_value() const
{
    return IHardware::get_const_instance()->get_preset_value();
}

/************************************************************************
 * \brief sets the preset value
 * \param[in] in_preset_value new value
 ************************************************************************/
void Controller::set_preset_value(const double & in_preset_value)
{
    IHardware::get_instance()->set_preset_value(in_preset_value);
}

/************************************************************************
 * \brief gets the preset type label
 * \return preset type label
 ************************************************************************/
std::string Controller::get_preset_type_label() const
{
    yat::SharedPtr<const HardwareDante> hardware = IHardware::get_const_instance();
    return Helper::convert_preset_type_to_tango(hardware->get_preset_type());
}

/************************************************************************
 * \brief sets the preset type with a label
 * \param[in] in_tango_preset_type preset type label
 ************************************************************************/
void Controller::set_preset_type_label(const std::string & in_tango_preset_type)
{
    yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
    hardware->set_preset_type(Helper::convert_tango_to_preset_type(in_tango_preset_type));
}

/************************************************************************
 * \brief gets the total number of pixels
 * \return number of pixels
 ************************************************************************/
uint32_t Controller::get_nb_pixels() const
{
    return IHardware::get_const_instance()->get_nb_pixels();
}

/************************************************************************
 * \brief sets the total number of pixels
 * \param[in] in_nb_pixels new value
 ************************************************************************/
void Controller::set_nb_pixels(const uint32_t & in_nb_pixels)
{
    IHardware::get_instance()->set_nb_pixels(in_nb_pixels);
}

/************************************************************************
 * \brief gets the current number of pixels
 * \return number of pixels
 ************************************************************************/
uint32_t Controller::get_current_pixel() const
{
    return static_cast<uint32_t>(DataStore::get_const_instance()->get_current_pixel());
}

/************************************************************************
 * \brief set spectrum time value
 * \param[in] in_sp_time new value
 ************************************************************************/
void Controller::set_sp_time(const uint32_t & in_sp_time)
{
    IHardware::get_instance()->set_sp_time(in_sp_time);
}

/************************************************************************
 * \brief Get spectrum time value
 * \return Spectrum time value
 ************************************************************************/
uint32_t Controller::get_sp_time() const
{
    return IHardware::get_const_instance()->get_sp_time();
}

//============================================================================
/***************************************************************************
 * \brief Manage the start of acquisition
 ***************************************************************************/
void Controller::start_acquisition(void)
{
    try
    {
        // CONTROLLER_TASK_PERIODIC_MS is posted in order to stop the acquisition
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_START_MSG, DEFAULT_MSG_PRIORITY, true);
        wait_msg_handled(msg, CONTROLLER_TASK_START_DELAY_MS); //to ensure that start is done
    }
    catch (Tango::DevFailed& df)
    {
        //ERROR_STRM << df << std::endl;
        m_state_status.on_fault(df);
    }
}

/*******************************************************************
 * \brief Manage the acquisition stop
 *******************************************************************/
void Controller::stop_acquisition(bool in_sync)
{
    try
    {
        // CONTROLLER_TASK_PERIODIC_MS is posted in order to stop the acquisition
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_STOP_MSG, DEFAULT_MSG_PRIORITY, true);

        if(!in_sync)
        {
            post(msg);
        }
        else
        {
            wait_msg_handled(msg, CONTROLLER_TASK_STOP_DELAY_MS); //to ensure that stop is done
        }
    }
    catch (Tango::DevFailed& df)
    {
        //ERROR_STRM << df << std::endl;
        m_state_status.on_fault(df);
    }
}

/************************************************************************
 * \brief make the stream manager treats all the frames in the container
 ************************************************************************/
/*void Controller::update_stream(FramesListContainer & in_frame_list)
{
    INFO_STRM << "update stream : " << in_frame_list.size() << " frame(s)" << std::endl;

    Stream::get_instance()->update(in_frame_list);
}*/

//============================================================================
//- Controller::process_message
//============================================================================
void Controller::process_message(yat::Message & msg) throw (Tango::DevFailed)
{
    try
    {
        try
        {
            switch (msg.type())
            {
                //-----------------------------------------------------
                case yat::TASK_INIT:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::TASK_INIT"                       << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    m_state_status.set(Tango::STANDBY, "Waiting for request...");
                }
                break;

                //-----------------------------------------------------
                case yat::TASK_EXIT:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::TASK_EXIT"                       << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                }
                break;

                //-----------------------------------------------------
                case yat::TASK_TIMEOUT:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::TASK_TIMEOUT"                    << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                }
                break;

                //-----------------------------------------------------------------------------------------------
                case yat::TASK_PERIODIC:
                {
                    DEBUG_STRM << " Controller::TASK_PERIODIC" << std::endl;

                    bool collect_thread_is_running;
                    bool store_thread_is_running  ;

                    // protecting the multi-threads access
                    {
                        yat::AutoMutex<> lock(m_threads_access_mutex);

                        collect_thread_is_running = CollectThread::get_const_instance()->is_treatment_running();
                        store_thread_is_running   = StoreThread::get_const_instance()->is_treatment_running();
                    }

                    Tango::DevState detector_state = IHardware::get_instance()->get_state();

                    // is acquisition finished ?
                    if((detector_state != Tango::RUNNING) && (!collect_thread_is_running) && (!store_thread_is_running))
                    {
                        INFO_STRM << "Controller is stopping periodic messages" << std::endl;
                        enable_periodic_msg(false); // no periodic message till the next acquisition round

                        DataStore::get_const_instance()->log_stats();
                        Stream::get_instance()->stop_acquisition();

                        INFO_STRM << "Acquisition finished." << std::endl;
                    }
                }
                break;

                //-----------------------------------------------------                
                case CONTROLLER_TASK_START_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_START_MSG"       << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;

                    std::size_t module_data_nb = static_cast<std::size_t>(IHardware::get_const_instance()->get_nb_pixels());

                    // we need to set the number of frames before the acquisition
                    StreamParameters new_stream_parameters  = Stream::get_const_instance()->get_parameters();
                    new_stream_parameters.m_nb_data_per_acq = module_data_nb;

                    // update or rebuild the stream
                    Stream::update_parameters(new_stream_parameters);

                    // start the data store for a new acquisition
                    DataStore::get_instance()->init(module_data_nb);

                    try
                    {
                        // start a new acquisition in the detector
                        IHardware::get_instance()->start_acquisition();
                    }
                    catch(...)
                    {
                        // stop the data store -> invalidate the containers to free the consumers threads
                        DataStore::get_instance()->stop_acquisition();
                        throw;
                    }

                    // restart the DataStore timers (for stats)
                    DataStore::get_instance()->restart_timers();

                    // protecting the multi-threads access
                    {
                        yat::AutoMutex<> lock(m_threads_access_mutex);

                        // start the collect thread
                        CollectThread::create();
                    }

                    Stream::get_instance()->start_acquisition();

                    // protecting the multi-threads access
                    {
                        yat::AutoMutex<> lock(m_threads_access_mutex);

                        // start the store thread
                        StoreThread::create();
                    }

                    enable_periodic_msg(true); //only when start is done    
                }
                break;

                //-----------------------------------------------------                
                case CONTROLLER_TASK_STOP_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_STOP_MSG"        << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;

                    enable_periodic_msg(false); // no periodic message till the next acquisition round

                    IHardware::get_instance()->stop_acquisition(true);

                    // stop the data store -> invalidate the containers to free the consumers threads
                    DataStore::get_instance()->stop_acquisition();

                    // protecting the multi-threads access
                    {
                        yat::AutoMutex<> lock(m_threads_access_mutex);

                        // release the store thread
                        CollectThread::release();

                        // release the collect thread
                        StoreThread::release();
                    }

                    Stream::get_instance()->stop_acquisition();
                }
                break;

                //-----------------------------------------------------                
                case CONTROLLER_TASK_ABORT_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_ABORT_MSG"       << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;

                    std::string error_msg = msg.get_data<std::string>();
                    INFO_STRM << error_msg << std::endl;

                    enable_periodic_msg(false); // no periodic message till the next acquisition round

                    IHardware::get_instance()->stop_acquisition(true);
                    DataStore::get_instance()->stop_acquisition();

                    // protecting the multi-threads access
                    {
                        yat::AutoMutex<> lock(m_threads_access_mutex);

                        // release the collect thread
                        StoreThread::release();

                        // release the store thread
                        CollectThread::release();
                    }

                    Stream::get_instance()->stop_acquisition();
                }
                break;

                //-----------------------------------------------------                
                // The configuration process
                //-----------------------------------------------------                
                case CONTROLLER_TASK_SETUP_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_SETUP_MSG"       << std::endl;
                    INFO_STRM << "--------------------------------------------" << std::endl;

                    manage_configuration();
                }
                break;

                //-----------------------------------------------------                
                // The rois command process
                //-----------------------------------------------------                
                case CONTROLLER_TASK_ROIS_COMMAND_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "---------------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_ROIS_COMMAND_MSG      " << std::endl;
                    INFO_STRM << "---------------------------------------------------" << std::endl;

                    manage_rois_command();
                }
                break;

                //-----------------------------------------------------                
                // The configuration and rois command process
                //-----------------------------------------------------                
                case CONTROLLER_TASK_SETUP_AND_ROIS_CMD_MSG:
                {
                    INFO_STRM << " " << std::endl;
                    INFO_STRM << "---------------------------------------------------" << std::endl;
                    INFO_STRM << " Controller::CONTROLLER_TASK_SETUP_AND_ROIS_CMD_MSG" << std::endl;
                    INFO_STRM << "---------------------------------------------------" << std::endl;

                    manage_configuration();
                    manage_rois_command ();
                }
                break;
            }
        }
        catch (Tango::DevFailed & in_ex)
        {
            StateStatus & threads_status = get_threads_status();
            threads_status.on_fault(in_ex);
            throw;
        }
        catch (yat::Exception & in_ex)
        {
            in_ex.dump();

            StateStatus & threads_status = get_threads_status();
            threads_status.on_fault(in_ex);
            throw;
        }
        catch(const DanteDpp_ns::Exception & in_ex)
        {
            StateStatus & threads_status = get_threads_status();
            threads_status.on_fault(in_ex);
            throw;
        }
        catch(...)
        {
            StateStatus & threads_status = get_threads_status();
            threads_status.on_fault("Unknown exception!");
            throw;
        }
    }
    catch(...)
    {
        ERROR_STRM << "Exception occured!" << get_threads_status().get_status() << std::endl;
        //throw;
    }
}

/*******************************************************************
 * \brief Getter of the stream type
 * \return stream type
*******************************************************************/
std::string Controller::get_stream_type(void) const
{
    const StreamParameters & stream_parameters = Stream::get_const_instance()->get_parameters();
    return Helper::convert_stream_type_to_tango(stream_parameters.m_type);
}

/*******************************************************************
 * \brief Setter of the stream type
 * \param[in] in_stream_type stream type
*******************************************************************/
void Controller::set_stream_type(const std::string & in_stream_type)
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();
    new_stream_parameters.m_type = Helper::convert_tango_to_stream_type(in_stream_type);

    // update or rebuild the stream
    Stream::update_parameters(new_stream_parameters);
}

/*******************************************************************
 * \brief Getter of the stream target path
 * \return stream target path
*******************************************************************/
std::string Controller::get_stream_target_path(void) const
{
    const StreamParameters & stream_parameters = Stream::get_const_instance()->get_parameters();
    return stream_parameters.m_target_path;
}

/*******************************************************************
 * \brief Setter of the stream target path
 * \param[in] in_stream_target_path stream target path
*******************************************************************/
void Controller::set_stream_target_path(const std::string & in_stream_target_path)
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();
    new_stream_parameters.m_target_path = in_stream_target_path;

    // update or rebuild the stream
    Stream::update_parameters(new_stream_parameters);
}

/*******************************************************************
 * \brief Getter of the stream target file
 * \return stream target file
*******************************************************************/
std::string Controller::get_stream_target_file(void) const
{
    const StreamParameters & stream_parameters = Stream::get_const_instance()->get_parameters();
    return stream_parameters.m_target_file;
}

/*******************************************************************
 * \brief Setter of the stream target file
 * \param[in] in_stream_target_file stream target file
*******************************************************************/
void Controller::set_stream_target_file(const std::string & in_stream_target_file)
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();
    new_stream_parameters.m_target_file = in_stream_target_file;

    // update or rebuild the stream
    Stream::update_parameters(new_stream_parameters);
}

/*******************************************************************
 * \brief Getter of the stream number of data per acquisition
 * \return stream number of acquisition per file
*******************************************************************/
uint32_t Controller::get_stream_nb_acq_per_file(void) const
{
    const StreamParameters & stream_parameters = Stream::get_const_instance()->get_parameters();
    return stream_parameters.m_nb_acq_per_file;
}

/*********************************************************************
 * \brief Setter of the stream number of acquisition per file
 * \param[in] in_stream_nb_acq_per_file number of acquisition per file
**********************************************************************/
void Controller::set_stream_nb_acq_per_file(const uint32_t & in_stream_nb_acq_per_file)
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();
    new_stream_parameters.m_nb_acq_per_file = in_stream_nb_acq_per_file;

    // update or rebuild the stream
    Stream::update_parameters(new_stream_parameters);
}

/*******************************************************************
 * \brief Reset the stream file index
 *******************************************************************/
void Controller::stream_reset_index(void)
{
    Stream::get_instance()->reset_index();
}

/*******************************************************************
 * \brief Get the data stream string
 * \return the data stream string (stream file and data items)
 *******************************************************************/
std::string Controller::get_data_streams(void) const
{
    return Stream::get_const_instance()->get_data_streams();
}

/************************************************************************
 * \brief Checks if an acquisition generates data files 
 * \return true if it generates data files, else false
*************************************************************************/
bool Controller::get_file_generation(void) const
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();
    return (new_stream_parameters.m_type == StreamParameters::Types::NEXUS_STREAM);
}

/*******************************************************************
 * \brief Set the file generation boolean
 * \param[in]  in_file_generation file generation new value
 *******************************************************************/
void Controller::set_file_generation(const bool & in_file_generation)
{
    StreamParameters new_stream_parameters = Stream::get_const_instance()->get_parameters();

    if(in_file_generation)
    {
        new_stream_parameters.m_type = StreamParameters::Types::NEXUS_STREAM;
    }
    else
    {
        if(new_stream_parameters.m_type == StreamParameters::Types::NEXUS_STREAM)
        {
            new_stream_parameters.m_type = StreamParameters::Types::NO_STREAM;
        }
    }

    new_stream_parameters.m_file_generation = in_file_generation;

    // update or rebuild the stream
    Stream::update_parameters(new_stream_parameters);
}

/************************************************************************
 * \brief Get the enable dataset real time boolean
 * \return enable dataset real time boolean
*************************************************************************/
bool Controller::get_enable_dataset_real_time(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset live time boolean
 * \return enable dataset live time boolean
*************************************************************************/
bool Controller::get_enable_dataset_live_time(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset dead time boolean
 * \return enable dataset dead time boolean
*************************************************************************/
bool Controller::get_enable_dataset_dead_time(void) const
{
    DEBUG_STRM << "Controller::get_enable_dataset_dead_time" << std::endl;    
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset input count rate boolean
 * \return enable dataset input count rate boolean
*************************************************************************/
bool Controller::get_enable_dataset_input_count_rate(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset output count rate boolean
 * \return enable dataset output count rate boolean
*************************************************************************/
bool Controller::get_enable_dataset_output_count_rate(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset events in run boolean
 * \return enable dataset events in run boolean
*************************************************************************/
bool Controller::get_enable_dataset_events_in_run(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset spectrum boolean
 * \return enable dataset spectrum boolean
*************************************************************************/
bool Controller::get_enable_dataset_spectrum(void) const
{
    // always forced to true
    return true;
}

/************************************************************************
 * \brief Get the enable dataset roi boolean
 * \return enable dataset roi boolean
*************************************************************************/
bool Controller::get_enable_dataset_roi(void) const
{
    // always forced to true
    return true;
}

/***************************************************************************************************
  * CONFIGURATION MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Load the configuration
 * \param[in] in_configuration_alias configuration alias
 * \param[in] in_complete_file_path configuration file
 *******************************************************************/
void Controller::load_configuration(const std::string & in_configuration_alias,
                                    const std::string & in_complete_file_path )
{
    LoadConfigParameters parameters;

    // set the load configuration parameters
    parameters.m_config_alias       = in_configuration_alias;
    parameters.m_complete_file_path = in_complete_file_path ;

    // keep the data
    m_configuration_parameters = parameters;

    // start the configuration
    m_configuration_state = ConfigurationState::LOADING;

    notify_configuration();
}

/*******************************************************************
 * \brief get the configuration complete file path
 * \return configuration complete file path
 *******************************************************************/
std::string Controller::get_configuration_file_path() const
{
    return m_configuration_file_path;
}

/*******************************************************************
 * \brief get the configuration alias
 * \return configuration alias
 *******************************************************************/
std::string Controller::get_configuration_alias() const
{
    return m_configuration_alias;
}

/*******************************************************************
 * \brief Notify a configuration is needed
 *******************************************************************/
void Controller::notify_configuration()
{
    INFO_STRM << "Controller::notify_configuration()" << endl;

    try
    {
        //- create an msg to pass it some data (Conf)
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_SETUP_MSG, DEFAULT_MSG_PRIORITY, true);
        post(msg);
    }
    catch (Tango::DevFailed & df)
    {
        m_state_status.on_fault(df);

        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "Controller::notify_configuration");
    }
}

/*******************************************************************
 * \brief manage the configuration
 *******************************************************************/
void Controller::manage_configuration()
{
#ifndef DANTE_DPP_TANGO_INDEPENDANT
    // remove the rois dynamics attributes
    m_rois_view.reset();

    // remove the configuration dynamics attributes
    m_config_view.reset();
#endif

    // reset previous configuration
    m_configuration_alias     = DANTE_DPP_VOID_CONFIGURATION_ALIAS;
    m_configuration_file_path = DANTE_DPP_VOID_CONFIGURATION_FILE ;

#ifndef DANTE_DPP_TANGO_INDEPENDANT
    // memorize the config alias (empty)
    dynamic_cast<DanteDpp*>(m_device)->memorize_config_alias("");
#endif

    // reset the data store
    DataStore::get_instance()->reset();

    // reset the previous ROIs
    reset_rois();

    // connect the hardware
    yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
    hardware->connect(m_configuration_parameters.m_complete_file_path);

    // give the channels and bins numbers to the rois manager
    RoisManager::get_instance()->set_channels_nb(get_channels_nb());
    RoisManager::get_instance()->set_bins_nb    (get_bins_nb    ());

#ifndef DANTE_DPP_TANGO_INDEPENDANT
    // update the info attributes
    dynamic_cast<DanteDpp*>(m_device)->update_info_attributes();

    // create the dynamic attributes
    m_config_view.reset(new ConfigView(m_device));

    ConfigViewParameters init_parameters;
    init_parameters.m_modules_nb  = get_modules_nb      ();
    init_parameters.m_channels_nb = get_channels_nb     ();
    init_parameters.m_bins_nb     = get_bins_nb         ();
    init_parameters.m_acqMode     = get_acquisition_mode();

    m_config_view->create(init_parameters);
#endif

    // waiting for a few seconds
    ::usleep(DANTE_DPP_CONTROLLER_LOAD_CONFIG_WAITING_TIME_MS * 1000);

    // the configuration is now loaded
    m_configuration_state     = ConfigurationState::LOADED;
    m_configuration_file_path = m_configuration_parameters.m_complete_file_path;
    m_configuration_alias     = m_configuration_parameters.m_config_alias      ;

#ifndef DANTE_DPP_TANGO_INDEPENDANT
    // init the values of dynamics write attributes (the status can not be DISABLE to write attributes)
    m_config_view->init();

    // memorize the config alias
    dynamic_cast<DanteDpp*>(m_device)->memorize_config_alias(m_configuration_alias);

    // memorize the new rois alias (empty)
    dynamic_cast<DanteDpp*>(m_device)->memorize_rois_alias("");
#endif
}

/*******************************************************************
 * \brief check if a configuration was loaded
 * \return true if a cofiguration was loaded, else false
 *******************************************************************/
bool Controller::is_configuration_loaded() const
{
    return (m_configuration_state == ConfigurationState::LOADED);
}

/***************************************************************************************************
  * ROIS MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief manage a rois command
 *******************************************************************/
void Controller::manage_rois_command()
{
    // this line is usefull only in the case of a sequence of setup and rois command
    m_rois_state = ConfigurationState::LOADING;

    bool create_dyn_attributes = false;

    //------------------------------------------------------------------------------
    // load file command ?
    if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::LOAD_FILE)
    {
    #ifndef DANTE_DPP_TANGO_INDEPENDANT
        // remove the rois dynamics attributes
        m_rois_view.reset();

        // memorize the rois alias (empty)
        dynamic_cast<DanteDpp*>(m_device)->memorize_rois_alias("");
        dynamic_cast<DanteDpp*>(m_device)->memorize_rois_list ("");
    #endif

        // reset the previous ROIs
        reset_rois();

        // load the file
        load_rois_file(m_rois_parameters.m_rois_alias, m_rois_parameters.m_complete_file_path);

    #ifndef DANTE_DPP_TANGO_INDEPENDANT
        // memorize the rois alias
        dynamic_cast<DanteDpp*>(m_device)->memorize_rois_alias(m_rois_alias);
    #endif

        create_dyn_attributes = true;
    }
    else
    //------------------------------------------------------------------------------
    // load list command ? (used only on init)
    if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::LOAD_LIST)
    {
        // load the rois from the rois string
        load_rois_string(m_rois_parameters.m_rois_string);

        create_dyn_attributes = true;
    }
    else
    //------------------------------------------------------------------------------
    // change command ?
    if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::CHANGE)
    {
        std::string previous_rois_string = RoisManager::get_instance()->get_rois_string();
        change_rois_string(m_rois_parameters.m_rois_string);
        std::string new_rois_string = RoisManager::get_instance()->get_rois_string();

        if(previous_rois_string != new_rois_string)
        {
            m_rois_file_path = DANTE_DPP_VOID_ROIS_FILE;
            m_rois_alias     = new_rois_string.empty() ? DANTE_DPP_VOID_ROIS_ALIAS : DANTE_DPP_ROIS_ALIAS_CUSTOM;

        #ifndef DANTE_DPP_TANGO_INDEPENDANT
            // remove the rois dynamics attributes
            m_rois_view.reset();

            // memorize the rois alias (empty)
            dynamic_cast<DanteDpp*>(m_device)->memorize_rois_alias("");
            dynamic_cast<DanteDpp*>(m_device)->memorize_rois_list (new_rois_string);
        #endif

            create_dyn_attributes = true;
        }
    }
    else
    //------------------------------------------------------------------------------
    // remove command ?
    if(m_rois_parameters.m_command == RoisCommandParameters::RoisCommand::REMOVE)
    {
        std::string previous_rois_string = RoisManager::get_instance()->get_rois_string();
        remove_rois(m_rois_parameters.m_channels_index);
        std::string new_rois_string = RoisManager::get_instance()->get_rois_string();

        if(previous_rois_string != new_rois_string)
        {
            m_rois_file_path = DANTE_DPP_VOID_ROIS_FILE;
            m_rois_alias     = new_rois_string.empty() ? DANTE_DPP_VOID_ROIS_ALIAS : DANTE_DPP_ROIS_ALIAS_CUSTOM;

        #ifndef DANTE_DPP_TANGO_INDEPENDANT
            // remove the rois dynamics attributes
            m_rois_view.reset();

            // memorize the rois alias (empty)
            dynamic_cast<DanteDpp*>(m_device)->memorize_rois_alias("");
            dynamic_cast<DanteDpp*>(m_device)->memorize_rois_list (new_rois_string);
        #endif

            create_dyn_attributes = true;
        }
    }

    std::size_t total_nb_rois = RoisManager::get_const_instance()->get_nb_rois();

#ifndef DANTE_DPP_TANGO_INDEPENDANT
    if((create_dyn_attributes) && (total_nb_rois > 0))
    {
        // create the dynamic attributes
        m_rois_view.reset(new RoisView(m_device));

        RoisViewParameters init_parameters;
        init_parameters.m_modules_nb  = get_modules_nb ();
        init_parameters.m_channels_nb = get_channels_nb();

        m_rois_view->create(init_parameters);
    }
#endif

    // waiting for a few seconds
    ::usleep(DANTE_DPP_CONTROLLER_ROIS_CMD_WAITING_TIME_MS * 1000);

    if(total_nb_rois > 0)
    {
        // the configuration was loaded
        m_rois_state = ConfigurationState::LOADED;
    }
    else
    {
        // the configuration is not loaded
        m_rois_state = ConfigurationState::NOT_LOADED;
    }
}

/*******************************************************************
 * \brief Load the rois configuration
 * \param[in] in_rois_alias rois alias
 * \param[in] in_complete_file_path rois file
 *******************************************************************/
void Controller::command_load_rois_file(const std::string & in_rois_alias        ,
                                        const std::string & in_complete_file_path)
{
    RoisCommandParameters parameters;

    // set the rois command
    parameters.m_command            = RoisCommandParameters::RoisCommand::LOAD_FILE;
    parameters.m_rois_alias         = in_rois_alias;
    parameters.m_complete_file_path = in_complete_file_path;

    // keep the data
    m_rois_parameters = parameters;

    // start the rois command
    m_rois_state = ConfigurationState::LOADING;

    notify_rois_command();
}

/*******************************************************************
 * \brief Change a rois list
 * \param[in] in_rois_list list of rois
 *******************************************************************/
void Controller::command_change_rois_list(const std::string & in_rois_list)
{
    RoisCommandParameters parameters;

    // set the rois command
    parameters.m_command     = RoisCommandParameters::RoisCommand::CHANGE;
    parameters.m_rois_string = in_rois_list;

    // keep the data
    m_rois_parameters = parameters;

    // start the rois command
    m_rois_state = ConfigurationState::LOADING;

    notify_rois_command();
}

/*******************************************************************
 * \brief Remove the rois of a channel or all channels
 * \param[in] m_channel channel index
 *******************************************************************/
void Controller::command_remove_rois(int m_channel)
{
    RoisCommandParameters parameters;

    // set the rois command
    parameters.m_command        = RoisCommandParameters::RoisCommand::REMOVE;
    parameters.m_channels_index = m_channel; 

    // keep the data
    m_rois_parameters = parameters;

    // start the rois command
    m_rois_state = ConfigurationState::LOADING;

    notify_rois_command();
}

/*******************************************************************
 * \brief Notify a rois command is needed
 *******************************************************************/
void Controller::notify_rois_command()
{
    INFO_STRM << "Controller::notify_rois_command()" << endl;

    try
    {
        //- create an msg to pass it some data (Conf)
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_ROIS_COMMAND_MSG, DEFAULT_MSG_PRIORITY, true);
        post(msg);
    }
    catch (Tango::DevFailed & df)
    {
        m_state_status.on_fault(df);

        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "Controller::notify_rois_command");
    }
}

/**************************************************************************
* \brief Reset the ROIs
**************************************************************************/
void Controller::reset_rois()
{
    remove_rois(ROIS_COMMAND_APPLY_ON_ALL_CHANNELS);

    m_rois_alias     = DANTE_DPP_VOID_ROIS_ALIAS;
    m_rois_file_path = DANTE_DPP_VOID_ROIS_FILE ;
}

/**************************************************************************
* \brief Remove the ROIs of a channel
* \param[in] in_channel_index channel index
**************************************************************************/
void Controller::remove_rois(int in_channel_index)
{
    if(in_channel_index == ROIS_COMMAND_APPLY_ON_ALL_CHANNELS)
    {
        // remove the ROIs from memory
        RoisManager::get_instance()->reset_rois();
    }
    else
    {
        // remove the ROIs from memory
        RoisManager::get_instance()->erase_rois(static_cast<std::size_t>(in_channel_index));
    }
}

/*******************************************************************
 * \brief Load a ROIs file
 * \param[in] in_rois_alias rois alias
 * \param[in] in_complete_file_path rois file
 *******************************************************************/
void Controller::load_rois_file(const std::string & in_rois_alias        ,
                                const std::string & in_complete_file_path)
{
    // keep the configuration data
    m_rois_file_path = in_complete_file_path;
    m_rois_alias     = in_rois_alias;

    // load the file content
    RoisManager::get_instance()->load_rois_file(in_complete_file_path);
}

/*******************************************************************
 * \brief Load a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void Controller::load_rois_string(const std::string & in_rois_string)
{
    // change the configuration data
    m_rois_file_path = DANTE_DPP_VOID_ROIS_FILE   ;
    m_rois_alias     = DANTE_DPP_ROIS_ALIAS_CUSTOM;

    // load the string content
    RoisManager::get_instance()->load_rois_string(in_rois_string);
}

/*******************************************************************
 * \brief Change rois using a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void Controller::change_rois_string(const std::string & in_rois_string)
{
    // change the configuration data
    m_rois_file_path = DANTE_DPP_VOID_ROIS_FILE   ;
    m_rois_alias     = DANTE_DPP_ROIS_ALIAS_CUSTOM;

    // Change the string content
    RoisManager::get_instance()->change_rois_string(in_rois_string);
}

/**************************************************************************
 * \brief Check the data before the erase of all the ROIs of a channel
 * \param[in] in_channel_index channel index
 **************************************************************************/
void Controller::check_before_erase_rois(int in_channel_index) const
{
    RoisManager::get_const_instance()->check_before_erase_rois(in_channel_index);
}

/**************************************************************************
 * \brief Check the data before the change of ROIs using a ROIs string
 * \param[in] in_rois_string rois string
 *******************************************************************/
void Controller::check_before_change_rois_string(const std::string & in_rois_string) const
{
    RoisManager::get_const_instance()->check_before_change_rois_string(in_rois_string);
}

/*******************************************************************
 * \brief get the rois complete file path
 * \return rois complete file path
 *******************************************************************/
std::string Controller::get_rois_file_path() const
{
    return m_rois_file_path;
}

/*******************************************************************
 * \brief get the rois alias
 * \return rois alias
 *******************************************************************/
std::string Controller::get_rois_alias() const
{
    return m_rois_alias;
}

/*******************************************************************
 * \brief get the Rois list for all channel
 * \return Rois by channel
 *******************************************************************/
std::vector<std::string> Controller::get_rois_list() const
{
    return RoisManager::get_const_instance()->get_rois_list();
}

/**************************************************************************
* \brief Get the groups delimiter (between channel rois)
* \return groups delimiter
**************************************************************************/
std::string Controller::get_rois_groups_delimiter() const
{
    return RoisManager::get_const_instance()->get_groups_delimiter();
}

/***************************************************************************************************
  * CONFIGURATION AND ROIS MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Load the configuration and a rois file
 * \param[in] in_configuration_alias configuration alias
 * \param[in] in_configuration_complete_file_path configuration file
 * \param[in] in_rois_alias rois alias
 * \param[in] in_rois_complete_file_path rois file
 *******************************************************************/
void Controller::load_configuration_and_rois_file(const std::string & in_configuration_alias             ,
                                                  const std::string & in_configuration_complete_file_path,
                                                  const std::string & in_rois_alias                      ,
                                                  const std::string & in_rois_complete_file_path         )
{
    LoadConfigParameters config_parameters;
    RoisCommandParameters rois_parameters ;

    // set the load configuration parameters
    config_parameters.m_config_alias       = in_configuration_alias;
    config_parameters.m_complete_file_path = in_configuration_complete_file_path;

    // set the rois command
    rois_parameters.m_command            = RoisCommandParameters::RoisCommand::LOAD_FILE;
    rois_parameters.m_rois_alias         = in_rois_alias             ;
    rois_parameters.m_complete_file_path = in_rois_complete_file_path;

    // keep the data
    m_configuration_parameters = config_parameters;
    m_rois_parameters          = rois_parameters  ;

    // start the configuration
    m_configuration_state = ConfigurationState::LOADING;

    notify_configuration_and_rois_command();
}

/*******************************************************************
 * \brief Load the configuration file and a rois list
 * \param[in] in_configuration_alias configuration alias
 * \param[in] in_configuration_complete_file_path configuration file
 * \param[in] in_rois_list rois list
 *******************************************************************/
void Controller::load_configuration_and_rois_list(const std::string & in_configuration_alias             ,
                                                  const std::string & in_configuration_complete_file_path,
                                                  const std::string & in_rois_list                       )
{
    LoadConfigParameters config_parameters;
    RoisCommandParameters rois_parameters ;

    // set the load configuration parameters
    config_parameters.m_config_alias       = in_configuration_alias;
    config_parameters.m_complete_file_path = in_configuration_complete_file_path;

    // set the rois command
    rois_parameters.m_command     = RoisCommandParameters::RoisCommand::LOAD_LIST;
    rois_parameters.m_rois_string = in_rois_list;

    // keep the data
    m_configuration_parameters = config_parameters;
    m_rois_parameters          = rois_parameters  ;

    // start the configuration
    m_configuration_state = ConfigurationState::LOADING;

    notify_configuration_and_rois_command();
}

/*******************************************************************
 * \brief Notify a configuration and a rois comand are needed
 *******************************************************************/
void Controller::notify_configuration_and_rois_command()
{
    INFO_STRM << "Controller::notify_configuration_and_rois_command()" << endl;

    try
    {
        //- create an msg to pass it some data (Conf)
        yat::Message* msg = yat::Message::allocate(CONTROLLER_TASK_SETUP_AND_ROIS_CMD_MSG, DEFAULT_MSG_PRIORITY, true);
        post(msg);
    }
    catch (Tango::DevFailed & df)
    {
        m_state_status.on_fault(df);

        //- rethrow exception
        Tango::Except::re_throw_exception(df,
                                          "TANGO_DEVICE_ERROR",
                                          string(df.errors[0].desc).c_str(),
                                          "Controller::notify_configuration_and_rois_command");
    }
}

/***************************************************************************************************
  * SINGLETON MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Create the Controller manager
 * \param[in] in_device access to tango device for log management/controller
 * \param[in] in_hardware_parameters Allow to pass the needed data for
 *            the Hardware initialization.
 * \param[in] in_stream_parameters Allow to pass the needed data for
 *            the Stream initialization.
 *******************************************************************/
void Controller::create(Tango::DeviceImpl                 * in_device             ,
                        const HardwareDanteInitParameters & in_hardware_parameters,
                        const StreamParameters            & in_stream_parameters  )
{
    Controller::g_singleton.reset(new Controller(in_device), yat4tango::DeviceTaskExiter());
    Controller::g_singleton->init(in_hardware_parameters, in_stream_parameters);
}

/************************************************************************
 * \brief release the singleton (execute a specific code for an derived class)
 ************************************************************************/
void Controller::specific_release()
{
    terminate();
}

//###########################################################################
}