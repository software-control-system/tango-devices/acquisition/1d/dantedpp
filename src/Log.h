/*************************************************************************/
/*! 
 *  \file   Log.h
 *  \brief  DANTE detector log class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_LOG_H_
#define DANTE_DPP_LOG_H_

// TANGO
#include <tango.h>

// YAT
#include <yat/memory/SharedPtr.h>

// SYSTEM
#include <string>
#include <vector>
#include <iostream>

// PROJECT
#include "Singleton.h"

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class Log
 ***********************************************************************/
class Log : public Tango::LogAdapter, public Singleton<Log>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<Log>;

public:
    /*******************************************************************
     * \brief get the tango logger - to avoid the removing of const in
     * the caller method which uses STRM macros.
     * This macro is used inside the class which inherit from 
     * Tango::LogAdapter.
     *******************************************************************/
    inline log4tango::Logger * const_get_logger() const
    {
        return const_cast<Tango::LogAdapter*>(dynamic_cast<const Tango::LogAdapter*>(this))->get_logger();
    } 

  /***************************************************************************************************
    * SINGLETON MANAGEMENT
    **************************************************************************************************/
    // create the manager
    static void create(Tango::DeviceImpl * in_device);

protected:
    // destructor
    virtual ~Log();

    // constructor
    Log(Tango::DeviceImpl* in_device);

private :
    // Owner Device server object
    Tango::DeviceImpl * m_device;
};

/*************************************************************************/
/* Macros used to log on cout or a tango logger.                         */
/* The const_get_logger method is used to keep CollectTask const methods */
/* DANTE_DPP_TANGO_INDEPENDANT needs to be defined in the pom.xml for an */
/* integration in a tango independant software (like automatic testing). */
/*************************************************************************/
#ifdef DANTE_DPP_TANGO_INDEPENDANT
    #define INFO_STRM  std::cout
    #define DEBUG_STRM std::cout
    #define ERROR_STRM std::cout
#else
    #define INFO_STRM \
        if (Log::get_const_instance()->const_get_logger()->is_info_enabled()) \
            Log::get_const_instance()->const_get_logger()->info_stream() << log4tango::LogInitiator::_begin_log

    #define DEBUG_STRM \
        if (Log::get_const_instance()->const_get_logger()->is_debug_enabled()) \
            Log::get_const_instance()->const_get_logger()->debug_stream() << log4tango::LogInitiator::_begin_log

    #define ERROR_STRM \
        if (Log::get_const_instance()->const_get_logger()->is_error_enabled()) \
            Log::get_const_instance()->const_get_logger()->error_stream() << log4tango::LogInitiator::_begin_log
#endif

} /// namespace DanteDpp_ns

#endif // DANTE_DPP_LOG_H_

