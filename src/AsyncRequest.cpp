/*************************************************************************/
/*! 
 *  \file   AsyncRequest.cpp
 *  \brief  class used to manage an asynchronous reply from a command
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
// SYSTEM
#include <cstddef>
#include <stdint.h>

// PROJECT
#include "AsyncRequest.h"
#include "Error.h"
#include "Log.h"

namespace DanteDpp_ns
{

/*******************************************************************
 * \brief constructor
 * \param[in] in_request_id identifier of the request
 *******************************************************************/
AsyncRequest::AsyncRequest(uint32_t in_request_id)
{
    m_request_id = in_request_id;
    m_error_code = DANTE_DPP_ERROR_CODE_UNKNOWN;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
AsyncRequest::~AsyncRequest()
{
}

/*******************************************************************
 * \brief get the identifier of the request
 * \return identifier of the request
 *******************************************************************/
uint32_t AsyncRequest::get_request_id()
{
    return m_request_id;
}

/*******************************************************************
 * \brief get the data of the request
 * \return data of the request
 *******************************************************************/
const std::vector<uint32_t> & AsyncRequest::get_data()
{
    return m_data;
}

} // namespace DanteDpp_ns

