/*************************************************************************/
/*! 
 *  \file   ChannelData.cpp
 *  \brief  class used to store a channel acquisition
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "ChannelData.h"
#include "Exception.h"

namespace DanteDpp_ns
{

//============================================================================================================
// ChannelData class
//============================================================================================================
/**************************************************************************
* \brief constructor
* \param[in] in_index index of the channel data
* \param[in] in_bins_nb number of bins for the channel
* \param[in] in_rois_nb number of rois for the channel
**************************************************************************/
ChannelData::ChannelData(std::size_t in_index, std::size_t in_bins_nb, std::size_t in_rois_nb)
{
    m_index             = in_index;
    m_real_time         = 0.0;
    m_live_time         = 0.0;
    m_dead_time         = 0.0;
    m_input_count_rate  = 0.0;
    m_output_count_rate = 0.0;
    m_events_in_run     = 0  ;

    m_spectrum.resize(in_bins_nb);
    m_rois.resize(in_rois_nb);

    memset(m_spectrum.data(), 0, m_spectrum.size() * sizeof(uint32_t)); 
    memset(m_rois.data()    , 0, m_rois.size()     * sizeof(uint32_t)); 
}

/**************************************************************************
* \brief get the index of the channel data
* \return index of the channel data
**************************************************************************/
std::size_t ChannelData::get_index() const
{
    return m_index;
}

/**************************************************************************
* \brief get the real time value
* \return real time value
**************************************************************************/
double ChannelData::get_real_time() const
{
    return m_real_time;
}

/**************************************************************************
* \brief set the real time value
* \param[in] in_real_time new value
**************************************************************************/
void ChannelData::set_real_time(const double in_real_time)
{
    m_real_time = in_real_time;
}

/**************************************************************************
* \brief get the live time value
* \return live time value
**************************************************************************/
double ChannelData::get_live_time() const
{
    return m_live_time;
}

/**************************************************************************
* \brief set the live time value
* \param[in] in_live_time new value
**************************************************************************/
void ChannelData::set_live_time(const double in_live_time)
{
    m_live_time = in_live_time;
}

/**************************************************************************
* \brief get the dead time value
* \return dead time value
**************************************************************************/
double ChannelData::get_dead_time() const
{
    return m_dead_time;
}

/**************************************************************************
* \brief set the dead time value
* \param[in] in_dead_time new value
**************************************************************************/
void ChannelData::set_dead_time(const double in_dead_time)
{
    m_dead_time = in_dead_time;
}

/**************************************************************************
* \brief get the input count rate value
* \return input count rate value
**************************************************************************/
double ChannelData::get_input_count_rate() const
{
    return m_input_count_rate;
}

/**************************************************************************
* \brief set the input count rate value
* \param[in] in_input_count_rate new value
**************************************************************************/
void ChannelData::set_input_count_rate(const double in_input_count_rate)
{
    m_input_count_rate = in_input_count_rate;
}

/**************************************************************************
* \brief get the output count rate value
* \return output count rate value
**************************************************************************/
double ChannelData::get_output_count_rate() const
{
    return m_output_count_rate;
}

/**************************************************************************
* \brief set the output count rate value
* \param[in] in_output_count_rate new value
**************************************************************************/
void ChannelData::set_output_count_rate(const double in_output_count_rate)
{
    m_output_count_rate = in_output_count_rate;
}

/**************************************************************************
* \brief get the event in run value
* \return event in run value
**************************************************************************/
uint32_t ChannelData::get_events_in_run() const
{
    return m_events_in_run;
}

/**************************************************************************
* \brief set the event in run value
* \param[in] in_events_in_run new value
**************************************************************************/
void ChannelData::set_events_in_run(const uint32_t in_events_in_run)
{
    m_events_in_run = in_events_in_run;
}

/**************************************************************************
* \brief get read-only access to the spectrum 
* \return read-only access to the spectrum
**************************************************************************/
const std::vector<uint32_t> & ChannelData::get_spectrum() const
{
    return m_spectrum;
}

/**************************************************************************
* \brief copy spectrum data
* \param[in] in_source_spectrum source spectrum part
* \param[in] in_dest_bin_index first index in the internal spectrum
* \param[in] in_copy_bins_nb number of bins to copy
**************************************************************************/
void ChannelData::copy_to_spectrum(const uint64_t * in_source_spectrum,
                                   std::size_t      in_dest_bin_index ,
                                   std::size_t      in_copy_bins_nb   )
{
    // check the data
    if((in_dest_bin_index >= m_spectrum.size()) || ((in_dest_bin_index + in_copy_bins_nb) > m_spectrum.size()))
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Copy overflow when trying to copy the spectrum!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ChannelData::copy_to_spectrum");
    }

    //@@memcpy(m_spectrum.data(), in_source_spectrum, in_copy_bins_nb * sizeof(uint64_t));
	for(int i =0;i<in_copy_bins_nb;i++)		
	{	
		 m_spectrum[i] = (uint32_t)in_source_spectrum[i];
	}
}

/**************************************************************************
* \brief get read-only access to the ROIs values 
* \return read-only access to the ROIs values
**************************************************************************/
const std::vector<uint32_t> & ChannelData::get_rois() const
{
    return m_rois;
}

/**************************************************************************
* \brief get a ROI value
* \param[in] in_roi_index roi index
* \return ROI value
**************************************************************************/
uint32_t ChannelData::get_rois(std::size_t in_roi_index) const
{
    // check the data
    if(in_roi_index >= m_rois.size())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Access overflow when trying to get the ROI n�" << in_roi_index 
                    << " of channel data n�" << m_index << "!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ChannelData::get_rois");
    }

    return m_rois[in_roi_index];
}

/**************************************************************************
* \brief compute a roi value
* \param[in] in_roi_index roi index
* \param[in] in_bin_index index of the first bin of the range
* \param[in] in_bin_size  number of bin of the range
**************************************************************************/
void ChannelData::compute_roi(std::size_t in_roi_index,
                              std::size_t in_bin_index,
                              std::size_t in_bin_size )
{
    // check the data
    if(in_roi_index >= m_rois.size())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Index overflow when trying to compute the roi (" << in_roi_index << ") " 
                    << "for a channel data with " 
                    << m_rois.size() << " Rois!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::add_channel");
    }

    if((in_bin_index >= m_spectrum.size()) || ((in_bin_index + in_bin_size) > m_spectrum.size()))
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Range overflow when trying to compute the roi (" << in_roi_index << ", " 
                    << in_bin_index << ", " 
                    << in_bin_size  << ")" 
                    << " for a channel data of "
                    << m_spectrum.size() << " bins!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::add_channel");
    }

    uint32_t roi = 0LL;

    std::vector<uint32_t>::const_iterator it_begin = m_spectrum.begin() + in_bin_index;
    std::vector<uint32_t>::const_iterator it_end   = it_begin           + in_bin_size ;

    for(std::vector<uint32_t>::const_iterator it=it_begin; it!=it_end; ++it)
    {
        roi += (uint32_t)*it;
    }

    m_rois[in_roi_index] = roi;
}

/**************************************************************************
* \brief compute the size of several instances (used for stats)
* \param[in] in_channels_nb number of channels
* \param[in] in_bins_nb number of bins for the channels (same for all)
* \param[in] in_rois_nb total number of rois (for all channels)
* \return size in bytes of several instances
**************************************************************************/
std::size_t ChannelData::compute_size_in_bytes(std::size_t in_channels_nb, 
                                               std::size_t in_bins_nb    , 
                                               std::size_t in_rois_nb    )
{
    return (sizeof(ChannelData) + sizeof(uint64_t) * in_bins_nb) * in_channels_nb + 
           (sizeof(uint64_t) * in_rois_nb);
}

//###########################################################################
}