/*************************************************************************/
/*! 
 *  \file   ControllerParameters.h
 *  \brief  class used to manage the Controller parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_CONTROLLER_PARAMETERS_H_
#define DANTE_DPP_CONTROLLER_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the Controller initialization*/
class ControllerParameters
{
    friend class Controller;

    public:
        // constructor
        ControllerParameters(const std::string & in_detector_type);

    private :
        std::string m_detector_type; // detector type : hardware or simulator
    };

} // namespace DanteDpp_ns

#endif // DANTE_DPP_CONTROLLER_PARAMETERS_H_

//###########################################################################
