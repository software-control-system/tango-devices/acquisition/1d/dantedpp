/*************************************************************************/
/*! 
 *  \file   RoiRange.h
 *  \brief  class used to manage the range of a Roi
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROI_RANGE_H_
#define DANTE_DPP_ROI_RANGE_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to store the range data of a Roi                     */
class RoiRange
{
public:
    // complete constructor
    RoiRange(std::size_t in_channel_index,
             std::size_t in_roi_index    ,
             std::size_t in_bin_index    ,
             std::size_t in_bin_size     );

    // get the channel index
    std::size_t get_channel_index() const;

    // get the roi index
    std::size_t get_roi_index() const;

    // get the bin index
    std::size_t get_bin_index() const;

    // get the bin size
    std::size_t get_bin_size() const;

    // get the low index (first bin of the ROI)
    std::size_t get_low_index() const;

    // get the high index (last bin of the ROI)
    std::size_t get_high_index() const;

private :
    std::size_t m_channel_index;
    std::size_t m_roi_index    ;
    std::size_t m_bin_index    ; // index of the first bin of the range
    std::size_t m_bin_size     ; // number of bin of the range
};

/***********************************************************************
 * RoiRange containers types
 ***********************************************************************/
/**
 * \typedef RoiRangeMap
 * \brief type of container which contains a map of Roi ranges
 */
typedef std::map<std::size_t, yat::SharedPtr<RoiRange>> RoiRangeMap;

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ROI_RANGE_H_

//###########################################################################
