/*************************************************************************/
/*! 
 *  \file   RoisGroup.cpp
 *  \brief  class used to manage the range of a Roi
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "RoisGroup.h"
#include "Exception.h"

namespace DanteDpp_ns
{
//============================================================================================================
// RoisGroup class
//============================================================================================================
/**************************************************************************
* \brief complete constructor
 * \param[in] in_channel_index channel index
**************************************************************************/
RoisGroup::RoisGroup(std::size_t in_channel_index)
{
    m_channel_index = in_channel_index;
}

/**************************************************************************
* \brief get the channel index
* \return channel index
**************************************************************************/
std::size_t RoisGroup::get_channel_index() const
{
    return m_channel_index;
}

/**************************************************************************
* \brief Get the number of rois
* \return number of rois for the group
**************************************************************************/
std::size_t RoisGroup::get_nb_rois() const
{
    return m_rois.size();
}

/**************************************************************************
* \brief Get access to the roi informations of a group
* \param[in] in_channel_index channel index
* \param[in] m_roi_index roi index
* \return roi informations
**************************************************************************/
yat::SharedPtr<const RoiRange> RoisGroup::get_roi(std::size_t in_roi_index) const
{
    // searching the roi
    RoiRangeMap::const_iterator search = m_rois.find(in_roi_index);

    // ROI exists... 
    if(search != m_rois.end()) 
    {
        return search->second;
    }
    else
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Channel (" 
                    << m_channel_index 
                    << ") does not contain the requested ROI (" 
                    << in_roi_index 
                    << ")!";

        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "RoisGroup::get_roi");
    }
}

/**************************************************************************
* \brief Add a ROI to a group
* \param[in] in_channel_index channel index
* \param[in] in_roi_index roi index
* \param[in] in_bin_index index of the first bin of the range
* \param[in] in_bin_size number of bin of the range
**************************************************************************/
void RoisGroup::add_roi(std::size_t in_channel_index,
                        std::size_t in_roi_index    ,
                        std::size_t in_bin_index    ,
                        std::size_t in_bin_size     )
{
    yat::SharedPtr<RoiRange> roi_range;
    roi_range.reset(new RoiRange(in_channel_index, in_roi_index, in_bin_index, in_bin_size));
    add_roi(roi_range);
}

/**************************************************************************
* \brief Add a ROI to a group
* \param[in] in_roi_range new ROI
**************************************************************************/
void RoisGroup::add_roi(yat::SharedPtr<RoiRange> in_roi_range)
{
    // inserting the ROI in the map container.
    std::pair<RoiRangeMap::iterator, bool> insert_result = 
        m_rois.insert(std::make_pair(in_roi_range->get_roi_index(), in_roi_range));

    // this should never happen.
    if(!insert_result.second)
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! ROI (" 
                    << in_roi_range->get_roi_index() 
                    << ") should not be in the groups container (" 
                    << in_roi_range->get_channel_index() 
                    << ")!";

        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "RoisGroup::add_roi");
    }
}

//###########################################################################
}