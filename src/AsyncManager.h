/*************************************************************************/
/*! 
 *  \file   AsyncManager.h
 *  \brief  class used to manage the asynchronous replies
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ASYNC_MANAGER_H_
#define DANTE_DPP_ASYNC_MANAGER_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>
#include <yat/memory/SharedPtr.h>

// PROJECT
#include "Log.h"
#include "AsyncRequestGroup.h"
#include "AsyncBuffer.h"
#include "Singleton.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class AsyncManager : public Singleton<AsyncManager>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<AsyncManager>;

    public:
        // add a new request 
        void add_request(uint32_t in_request_id, uint32_t in_group_request_id, std::size_t in_max_nb_replies);

        // manage the reply of a request
        void manage_reply(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data);

        // remove a request group
        yat::SharedPtr<AsyncRequestGroup> remove_request_group(uint32_t in_group_request_id);

        // manage the wait of all replies
        bool waiting_all_replies_of_group(uint32_t in_group_request_id);

        /***************************************************************************************************
          * SINGLETON MANAGEMENT
          **************************************************************************************************/
         // Create the manager
        static void create();

    protected:
        //==================================================================
        // constructor
        explicit AsyncManager();

        // destructor (needs to be virtual)
        virtual ~AsyncManager();

    private:
        // get a request group
        yat::SharedPtr<AsyncRequestGroup> get_request_group(uint32_t in_group_request_id);

        // manage the reply of a request (internal version which allows to temporary store the unknown requests)
        void manage_reply_aux(uint16_t in_type, uint32_t in_call_id, 
                              uint32_t in_length, const uint32_t * in_data,
                              bool in_store_unknown_request);

    private:
      /** container used to link the request identifiers to an async replies instance (group of several async reply)
        */
        AsyncRequestGroupMap m_requests;

      /** mutex used to protect the multithread access to m_requests container.
        * The mutable keyword is used to allow using this mutex into const methods.
        */
        mutable yat::Mutex m_requests_mutex;

      /** container used to temporary store the unknown requests (due to a sdk problem)
        */
        AsyncBufferContainer m_unknown_requests_buffer;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ASYNC_MANAGER_H_

//###########################################################################
