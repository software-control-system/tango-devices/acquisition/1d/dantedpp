/*************************************************************************/
/*! 
 *  \file   DataStore.cpp
 *  \brief  class used to store the acquisitions
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "DataStore.h"
#include "Log.h"
#include "Exception.h"
#include "RoisManager.h"
#include "IHardware.h"
#include "Stream.h"

namespace DanteDpp_ns
{
//============================================================================================================
// DataStore class
//============================================================================================================
//#define DANTE_DPP_DEBUG_DATA_STORE

/************************************************************************
 * \brief constructor
 ************************************************************************/
DataStore::DataStore()
{
    INFO_STRM << "construction of DataStore" << std::endl;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
DataStore::~DataStore()
{
    INFO_STRM << "destruction of DataStore" << std::endl;
    reset();
}

/************************************************************************
 * \brief reset
 ************************************************************************/
void DataStore::reset()
{
    yat::SharedPtr<const ModuleData> null_module_data;
    set_latest_module_data(null_module_data);

    m_received_data.clear();

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_init_complete_data_lock);
    m_complete_data.reset();
}

/************************************************************************
 * \brief init
 * \param[in] in_data_nb_to_acquire number of acquisitions
 ************************************************************************/
void DataStore::init(std::size_t in_data_nb_to_acquire)
{
    reset();

    m_data_nb_to_acquire = in_data_nb_to_acquire;

    m_channels_nb = IHardware::get_const_instance()->get_channels_nb();
    m_bins_nb     = IHardware::get_const_instance()->get_bins_nb();
    m_rois_nb     = RoisManager::get_const_instance()->get_nb_rois();

    std::size_t module_data_size_in_bytes = 
        ModuleData::compute_size_in_bytes(m_channels_nb, m_bins_nb, m_rois_nb);

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_init_complete_data_lock);
        m_complete_data.reset(new ModuleDataList("m_complete_data", m_data_nb_to_acquire, module_data_size_in_bytes));
    }

    // logs usefull informations
    INFO_STRM << "-----------------------------------------------------" << std::endl;
    INFO_STRM << " DataStore : Setup of a new acquisition"               << std::endl;
    INFO_STRM << "-----------------------------------------------------" << std::endl;
    INFO_STRM << "module data number to acquire : " << m_data_nb_to_acquire      << std::endl;
    INFO_STRM << "module data size : "              << module_data_size_in_bytes << " Bytes" << std::endl;

    // starting the timers of the smart lists
    restart_timers();
}

/************************************************************************
 * \brief terminate
 ************************************************************************/
void DataStore::terminate()
{
    stop_acquisition();
}

/************************************************************************
 * \brief get the current pixel value
 * \return current pixel value
 ************************************************************************/
std::size_t DataStore::get_current_pixel() const
{
    std::size_t result = 0LL;

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_init_complete_data_lock);

        if(!m_complete_data.is_null())
        {
            result = m_complete_data->get_total_elements_nb();
        }
    }

    return result;
}

/************************************************************************
 * \brief restart the acquisition timers
 ************************************************************************/
void DataStore::restart_timers()
{
    // re-starting the timers of the smart lists
    m_complete_data->start_timers();
}

/***************************************************************************************************
  * STOP MANAGEMENT
  **************************************************************************************************/
/************************************************************************
 * \brief stop the acquisition
 ************************************************************************/
void DataStore::stop_acquisition()
{
    INFO_STRM << "-----------------------------------------------------" << std::endl;
    INFO_STRM << " DataStore : Stopping acquisition treatments"          << std::endl;
    INFO_STRM << "-----------------------------------------------------" << std::endl;

    // Invalidates the containers contents to stop the treatments.
    if(!m_complete_data.is_null())
    {
        m_complete_data->invalidate();
    }
}

/************************************************************************
 * \brief add a new acquired channel data
 * \param[in] in_module_data_index module data index
 * \param[in] in_channel_data channel data to add to the module data
 ************************************************************************/
void DataStore::add_channel_data(std::size_t in_module_data_index, yat::SharedPtr<ChannelData> in_channel_data)
{
    yat::SharedPtr<ModuleData> module_data;

    // add or retrieve the module data
    {
        // searching the module data using its index
        ModuleDataMap::iterator search = m_received_data.find(in_module_data_index);

        // channel data was already received for this module data
        if(search != m_received_data.end()) 
        {
            module_data = search->second;
        }
        // frame part was never received for this frame
        else
        {
            // create a new module data
            module_data = new ModuleData(in_module_data_index, m_channels_nb);

            // inserting the module data in the received container.
            std::pair<ModuleDataMap::iterator, bool> insert_result = m_received_data.insert(std::make_pair(in_module_data_index, module_data));

            // this should never happen.
            if(!insert_result.second)
            {
                std::stringstream temp_stream;
                temp_stream << "Problem! module data (" << in_module_data_index << ") should not be in the received data container!";
                throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "DataStore::add_channel_data");
            }

        #ifdef DANTE_DPP_DEBUG_DATA_STORE
            INFO_STRM << "Created a new module data (" << module_data->get_index() << ")" << std::endl;
        #endif
        }
    }

    // add the channel data to the module data
    #ifdef DANTE_DPP_DEBUG_DATA_STORE
        INFO_STRM << "Adding a new channel (" << in_channel_data->get_index() << ") to module data (" << module_data->get_index() << ")" << std::endl;
    #endif

    module_data->add_channel(in_channel_data);

    // check if the module data is complete
    if(module_data->is_complete())
    {
        // searching the module data using its index
        ModuleDataMap::iterator search = m_received_data.find(in_module_data_index);

        // channel data was already received for this module data
        if(search != m_received_data.end()) 
        {
            // removing the module data from the received container
        #ifdef FRAMES_DEBUG_FRAMES
            INFO_STRM << "New module data to store (" << in_module_data_index << ")" << std::endl;
        #endif 

            m_received_data.erase(search);

            // change the latest module data (no copy with the smart pointer use)
            set_latest_module_data(module_data);

            // putting the module data in the complete container
            m_complete_data->put(module_data);
        }
        else
        // this should never happen.
        {
            std::stringstream temp_stream;
            temp_stream << "Problem! module data (" << in_module_data_index << ") was already removed from of the received data container!";
            throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "DataStore::add_channel_data");
        }
    }
}

/*******************************************************************
 * \brief Waiting for a complete data
 *******************************************************************/
void DataStore::waiting_complete_data() const
{
    m_complete_data->waiting_while_empty();
}

/*******************************************************************
 * \brief Tell if the collect process is over
 *******************************************************************/
bool DataStore::end_of_collect() const
{
    // end of reception if :
    // the acquisition was stopped or encountered an error -> invalid container
    // the reception is done -> all elements were inserted into the container
    return ((!m_complete_data->is_valid ()) || 
            (m_complete_data->completely_filled()));
}

/*******************************************************************
 * \brief Tell if the store process is over
 *******************************************************************/
bool DataStore::end_of_store() const
{
    // end of transfer if :
    // the acquisition was stopped or encountered an error -> invalid container
    // the transfer is done -> all elements were removed from the container
    return ((!m_complete_data->is_valid()) ||
            (m_complete_data->completely_emptied()));
}

/*******************************************************************
 * \brief logs stats
 *******************************************************************/
void DataStore::log_stats() const
{
    m_complete_data->log_stats();
}

/*******************************************************************
 * \brief store the complete data
 * \return true if the store process is complete, else false
 *******************************************************************/
bool DataStore::store_complete_data()
{
    waiting_complete_data();

    while(!m_complete_data->empty())
    {
        // accessing first module data in the container
        yat::SharedPtr<ModuleData> module_data = m_complete_data->front();

        // logging or saving the data
        Stream::get_instance()->update(module_data);

        // removing the module data from the container
        m_complete_data->remove_front();
    }

    //returns true if the store process is complete, else false  
    return end_of_store();
}

/*******************************************************************
 * \brief get the latest module data
 * \return latest module data (can be null)
 *******************************************************************/
yat::SharedPtr<const ModuleData> DataStore::get_latest_module_data() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_latest_module_data_lock);
    return m_latest_module_data;
}

/*******************************************************************
 * \brief set the latest module data
 * \param[in] in_latest_module_data new module data
 *******************************************************************/
void DataStore::set_latest_module_data(yat::SharedPtr<const ModuleData> in_latest_module_data)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_latest_module_data_lock);
    m_latest_module_data = in_latest_module_data;
}

/***************************************************************************************************
  * ACTORS MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief add a reception producer
 * \param[in] in_producer producer name
 *******************************************************************/
void DataStore::add_reception_producer(const std::string & in_producer)
{
    m_complete_data->register_producer(in_producer);
}

/*******************************************************************
 * \brief remove a reception producer
 * \param[in] in_producer producer name
 *******************************************************************/
void DataStore::remove_reception_producer(const std::string & in_producer)
{
    m_complete_data->unregister_producer(in_producer);
}

/*******************************************************************
 * \brief add a reception customer
 * \param[in] in_customer customer name
 *******************************************************************/
void DataStore::add_reception_customer(const std::string & in_customer)
{
    m_complete_data->register_customer(in_customer);
}

/*******************************************************************
 * \brief remove a reception customer
 * \param[in] in_customer customer name
 *******************************************************************/
void DataStore::remove_reception_customer(const std::string & in_customer)
{
    m_complete_data->unregister_customer(in_customer);
}

/**************************************************************************************************
 * SINGLETON MANAGEMENT
 **************************************************************************************************/
/*******************************************************************
 * \brief Create the manager
 *******************************************************************/
void DataStore::create()
{
    DataStore::g_singleton.reset(new DataStore());
}

/************************************************************************
 * \brief release the singleton (execute a specific code for an derived class)
 ************************************************************************/
void DataStore::specific_release()
{
    terminate();
}

//###########################################################################
}