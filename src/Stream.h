/*************************************************************************/
/*! 
 *  \file   Stream.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_STREAM_H_
#define DANTE_DPP_STREAM_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>
#include <yat/memory/SharedPtr.h>

// LOCAL
#include "Log.h"
#include "Singleton.h"
#include "StateStatus.h"
#include "StreamParameters.h"
#include "ModuleData.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class Controller;

/*************************************************************************/
class Stream : public Singleton<Stream>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<Stream>;

    public:
        //==================================================================
        // parameters management
        //==================================================================
        // Get all the parameters
        const StreamParameters & get_parameters(void) const; 

        // Set the parameters
        void set_parameters(const StreamParameters & in_parameters); 

        //==================================================================
        // state management
        //==================================================================
        // Getter of the state
        Tango::DevState get_state(void) const;

        // Getter of the status
        std::string get_status(void) const;

        // Update the stream with new acquisition data
        virtual void update(yat::SharedPtr<const ModuleData> in_module_data) = 0;

        // Reset the file index
        virtual void reset_index(void) = 0;

        // Get the data stream string
        std::string get_data_streams(void) const;

        //==================================================================
        // acquisition management
        //==================================================================
        // Manage the start of acquisition
        virtual void start_acquisition(void);

        // Manage the acquisition stop
        virtual void stop_acquisition(void);

      /***************************************************************************************************
        * SINGLETON MANAGEMENT
        **************************************************************************************************/
        // create the Stream manager
        static void create(const StreamParameters & in_parameters);

        // Instanciates the correct stream object and/or change the parameters
        // in_device can be NULL is a singleton already exists
        static void update_parameters(const StreamParameters & in_new_parameters);

    protected:
        // constructor
        Stream();

        // destructor
        virtual ~Stream();

    protected:
        StreamParameters m_parameters  ;
        StateStatus      m_state_status; // state and status informations
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_STREAM_H_

//###########################################################################
