/*************************************************************************/
/*! 
 *  \file   HardwareDanteConfigParameters.cpp
 *  \brief  class used to manage the HardwareDante config parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "HardwareDanteConfigParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// HardwareDanteConfigParameters class
//============================================================================================================
/**************************************************************************
* \brief simple constructor
**************************************************************************/
HardwareDanteConfigParameters::HardwareDanteConfigParameters()
{
    // defines default values
    m_default_acq_parameters.m_fast_filter_thr     = 1;     // Detection threshold [unit: spectrum BINs]
    m_default_acq_parameters.m_energy_filter_thr   = 4;     // Detection threshold [unit: spectrum BINs]
    m_default_acq_parameters.m_energy_baseline_thr = 0;     // Energy threshold for baseline [unit: spectrum BINs]
    m_default_acq_parameters.m_max_risetime        = 8.0;   // Max risetime setting for pileup detection 
    m_default_acq_parameters.m_gain                = 1.0;   // Main gain: (spectrum BIN)/(ADC's LSB)
    m_default_acq_parameters.m_peaking_time        = 25;    // Main energy filter peaking time   [unit: 32 ns samples]
    m_default_acq_parameters.m_max_peaking_time    = 32;    // Max energy filter peaking time   [unit: 32 ns samples]
    m_default_acq_parameters.m_flat_top            = 4;     // Main energy filter flat top   [unit: 32 ns samples]
    m_default_acq_parameters.m_edge_peaking_time   = 4;     // Edge detection filter peaking time   [unit: 8 ns samples]
    m_default_acq_parameters.m_edge_flat_top       = 1;     // Edge detection filter flat top   [unit: 8 ns samples]
    m_default_acq_parameters.m_reset_recovery_time = 100;   // Reset recovery time [unit: 8 ns samples]
    m_default_acq_parameters.m_zero_peak_freq      = 0.0;   // Zero peak rate [kcps]
    m_default_acq_parameters.m_baseline_samples    = 256;   // Baseline samples for baseline correction [32 ns samples]
    m_default_acq_parameters.m_inverted_input      = false; // pulse inversion
    m_default_acq_parameters.m_time_constant       = 0.0;   // Time constant for continuous reset signal
    m_default_acq_parameters.m_base_offset         = 0;     // Baseline of the continuous reset signal [ADC's LSB]
    m_default_acq_parameters.m_overflow_recovery   = 0;     // Overflow recovery time [8ns samples]
    m_default_acq_parameters.m_reset_threshold     = 1000;  // Reset detection threshold [ADC's LSB]
    m_default_acq_parameters.m_tail_coefficient    = 0.0;   // Tail coefficient
    m_default_acq_parameters.m_other_param         = 0;     // currently ignored

    m_offset_val_digpot1 = 0; // First offset value
    m_offset_val_digpot2 = 0; // Second offset value.

    m_input_mode = HardwareDanteConfigParameters::InputMode::DC_HighImp;

    m_master_board_ip = ""; // usb connection

    m_acquisition_mode = HardwareDanteConfigParameters::AcqMode::MCA;
    m_nb_bins = 1024;
}

//###########################################################################
}