/*************************************************************************/
/*! 
 *  \file   Controller.h
 *  \brief  Interface class which calls StoreTask methods.
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_CONTROLLER_H_
#define DANTE_DPP_CONTROLLER_H_

// TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat4tango/DeviceTask.h>
#include <yat/memory/SharedPtr.h>
#include <yat/memory/UniquePtr.h>
#include <yat/threading/Mutex.h>

// PROJECT
#include "Singleton.h"
#include "Stream.h"
#include "HardwareDante.h"
#include "StateStatus.h"
#include "RoisCommandParameters.h"
#include "LoadConfigParameters.h"

#ifndef DANTE_DPP_TANGO_INDEPENDANT
    #include "ConfigView.h"
    #include "RoisView.h"
#endif

/*************************************************************************/

namespace DanteDpp_ns
{
    //-------------------------------------------------------------------------
    // DEVICE TASK
    //-------------------------------------------------------------------------
    static const size_t CONTROLLER_TASK_PERIODIC_MS            = 100;
    static const size_t CONTROLLER_TASK_START_MSG              = yat::FIRST_USER_MSG + 300;
    static const size_t CONTROLLER_TASK_STOP_MSG               = yat::FIRST_USER_MSG + 301;
    static const size_t CONTROLLER_TASK_ABORT_MSG              = yat::FIRST_USER_MSG + 302;
    static const size_t CONTROLLER_TASK_SETUP_MSG              = yat::FIRST_USER_MSG + 303;
    static const size_t CONTROLLER_TASK_ROIS_COMMAND_MSG       = yat::FIRST_USER_MSG + 304;
    static const size_t CONTROLLER_TASK_SETUP_AND_ROIS_CMD_MSG = yat::FIRST_USER_MSG + 305;
    static const size_t CONTROLLER_TASK_STOP_DELAY_MS          = 5000;
    static const size_t CONTROLLER_TASK_START_DELAY_MS         = 5000;

/*************************************************************************/
class Controller : public yat4tango::DeviceTask, public Singleton<Controller>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<Controller>;

    // to gain access to get_threads_status protected method
    friend class CollectThread;
    friend class StoreThread  ;

    // configuration states
    typedef enum { NOT_LOADED, LOADING, LOADED } ConfigurationState;
    
    public:
        // Manage the abort command
        void abort(std::string status);

        //==================================================================
        // Gets the number of modules
        std::size_t get_modules_nb(void) const;

        // gets the number of channels
        std::size_t get_channels_nb() const;

        // gets the number of bins
        std::size_t get_bins_nb() const;

        // Get informations about boards and sdk
        std::vector<std::pair<std::string, std::string> > get_versions_informations(void) const;

        //==================================================================
        // gets the acquisition mode
        HardwareDanteConfigParameters::AcqMode get_acquisition_mode() const;

        // gets the acquisition mode label
        std::string get_acquisition_mode_label() const;

        // gets the trigger mode label
        std::string get_trigger_mode_label() const;

        // sets the trigger mode with a label
        void set_trigger_mode_label(const std::string & in_tango_trigger_mode);

        // gets the detector type label
        std::string get_detector_type_label() const;

        //==================================================================
        // gets the realtime value of channel
        double get_realtime(std::size_t in_channel_index) const;

        // gets the livetime value of channel
        double get_livetime(std::size_t in_channel_index) const;

        // gets the deadtime value of channel
        double get_deadtime(std::size_t in_channel_index) const;

        // gets the input count rate value of channel
        double get_input_count_rate(std::size_t in_channel_index) const;

        // gets the output count rate value of channel
        double get_output_count_rate(std::size_t in_channel_index) const;

        // gets the events in run value of channel
        uint32_t get_events_in_run(std::size_t in_channel_index) const;

        // gets the spectrum of a channel
        std::vector<uint32_t> get_channel(std::size_t in_channel_index) const;

        // gets the roi value
        uint32_t get_roi(std::size_t in_channel_index, std::size_t in_roi_index) const;

        // gets the preset value
        double get_preset_value() const;

        // sets the preset value
        void set_preset_value(const double & in_preset_value);

        // gets the preset type label
        std::string get_preset_type_label() const;

        // sets the preset type with a label
        void set_preset_type_label(const std::string & in_tango_preset_type);

        // gets the total number of pixels
        uint32_t get_nb_pixels() const;

        // sets the total number of pixels
        void set_nb_pixels(const uint32_t & in_nb_pixels);

        // gets the current number of pixels
        uint32_t get_current_pixel() const;
		
		// set spectrum time value
        void set_sp_time(const uint32_t & in_sp_time);

        // get spectrum time value
        uint32_t get_sp_time() const;

        //==================================================================
        // state management
        //==================================================================
        // Getter of the state
        Tango::DevState get_state(void);

        // Getter of the status
        std::string get_status(void);

        //==================================================================
        // acquisition management
        //==================================================================
        // Manage the start of acquisition
        void start_acquisition(void);

        // Manage the acquisition stop
        void stop_acquisition(bool in_sync);

        // Manage the treatment of a frame
//        void treat_frame(yat::SharedPtr<Frame> in_frame);

        // make the stream manager treats all the frames in the container
//        void update_stream(FramesListContainer & in_frame_list);

        // get the last treated intensities during an acquisition.
//        std::vector<int32_t> get_last_treated_intensities(void);

        // get the last treated gains during an acquisition.
//        std::vector<int32_t> get_last_treated_gains(void);

        // Check if an acquisition is running
        bool acquisition_is_running(void);

        //==================================================================
        // Stream management
        //==================================================================
        void        set_stream_type(const std::string & in_stream_type);
        std::string get_stream_type(void) const;

        void        set_stream_target_path(const std::string & in_stream_target_path);
        std::string get_stream_target_path(void) const;

        void        set_stream_target_file(const std::string & in_stream_target_file);
        std::string get_stream_target_file(void) const;

        void        set_stream_nb_acq_per_file(const uint32_t & in_stream_nb_acq_per_file);
        uint32_t    get_stream_nb_acq_per_file(void) const;

        // Reset the stream file index
        void stream_reset_index(void);

        // Get the data stream string
        std::string get_data_streams(void) const;

		// Checks if an acquisition generates data files 
		bool get_file_generation(void) const;
		
        // Set the file generation boolean
        void set_file_generation(const bool & in_file_generation);

        // Get the enable dataset real time boolean
        bool get_enable_dataset_real_time(void) const;

        // Get the enable dataset live time boolean
        bool get_enable_dataset_live_time(void) const;

        // Get the enable dataset dead time boolean
        bool get_enable_dataset_dead_time(void) const;

        // Get the enable dataset input count rate boolean
        bool get_enable_dataset_input_count_rate(void) const;

        // Get the enable dataset output count rate boolean
        bool get_enable_dataset_output_count_rate(void) const;

        // Get the enable dataset events in run boolean
        bool get_enable_dataset_events_in_run(void) const;

        // Get the enable dataset spectrum boolean
        bool get_enable_dataset_spectrum(void) const;

        // Get the enable dataset roi boolean
        bool get_enable_dataset_roi(void) const;

      /***************************************************************************************************
        * CONFIGURATION MANAGEMENT
        **************************************************************************************************/
        // Load the configuration
        void load_configuration(const std::string & in_configuration_alias, const std::string & in_complete_file_path);

        // get the configuration complete file path
        std::string get_configuration_file_path() const;

        // get the configuration alias
        std::string get_configuration_alias() const;

        // Notify a configuration is needed
        void notify_configuration();

        // check if a configuration was loaded
        bool is_configuration_loaded() const;

        /***************************************************************************************************
          * ROIS MANAGEMENT
          **************************************************************************************************/
        // Load the rois configuration
        void command_load_rois_file(const std::string & in_rois_alias        ,
                                    const std::string & in_complete_file_path);

        // Change a rois list
        void command_change_rois_list(const std::string & in_rois_list);

        // Remove the rois of a channel or all channels
        void command_remove_rois(int m_channel);

        // Notify a rois command is needed
        void notify_rois_command();

        // Load a ROIs file
        void load_rois_file(const std::string & in_rois_alias        ,
                            const std::string & in_complete_file_path);

        // Load a ROIs string
        void load_rois_string(const std::string & in_rois_string);

        // Change rois using a ROIs string
        void change_rois_string(const std::string & in_rois_string);

        // Remove the ROIs of a channel
        void remove_rois(int in_channel_index);

        // Reset the ROIs
        void reset_rois();

        // get the rois complete file path
        std::string get_rois_file_path() const;

        // get the rois alias
        std::string get_rois_alias() const;

        // get the Rois list for all channel
        std::vector<std::string> get_rois_list() const;

        // Get the groups delimiter (between channel rois)
        std::string get_rois_groups_delimiter() const;

        // Check the data before the erase of all the ROIs of a channel
        void check_before_erase_rois(int in_channel_index) const;

        // Check the data before the change of ROIs using a ROIs string
        void check_before_change_rois_string(const std::string & in_rois_string) const;

      /***************************************************************************************************
        * CONFIGURATION AND ROIS MANAGEMENT
        **************************************************************************************************/
        // Load the configuration and a rois file
        void load_configuration_and_rois_file(const std::string & in_configuration_alias             ,
                                              const std::string & in_configuration_complete_file_path,
                                              const std::string & in_rois_alias                      ,
                                              const std::string & in_rois_complete_file_path         );

        // Load the configuration file and a rois list
        void load_configuration_and_rois_list(const std::string & in_configuration_alias             ,
                                              const std::string & in_configuration_complete_file_path,
                                              const std::string & in_rois_list                       );

        // Notify a configuration and a rois comand are needed
        void notify_configuration_and_rois_command();

      /***************************************************************************************************
        * SINGLETON MANAGEMENT
        **************************************************************************************************/
        // create the manager
        static void create(Tango::DeviceImpl             * in_device             ,
                           const HardwareDanteInitParameters & in_hardware_parameters,
                           const StreamParameters        & in_stream_parameters  );

        // release the singleton (execute a specific code for an derived class)
        virtual void specific_release();

    protected:
        // destructor
        virtual ~Controller();

        // [yat4tango::DeviceTask implementation]
        virtual void process_message(yat::Message & msg) throw(Tango::DevFailed);

        // Get a read and write access to the threads status instance
        StateStatus & get_threads_status(void);

      /***************************************************************************************************
        * CONFIGURATION MANAGEMENT
        **************************************************************************************************/
        // manage the configuration
        void manage_configuration();

      /***************************************************************************************************
        * ROIS MANAGEMENT
        **************************************************************************************************/
        // manage a rois command
        void manage_rois_command();

    private:
        // constructor 
        explicit Controller(Tango::DeviceImpl * in_device);

        // initializer - connection to the device
        void init(const HardwareDanteInitParameters & in_hardware_parameters,
                  const StreamParameters            & in_stream_parameters  );

        // terminate - disconnection from the device
        void terminate(void);

        // Manage the FAULT status with an exception
        void on_fault(Tango::DevFailed df);

        // Manage the FAULT status with an error message
        void on_fault(const std::string& status_message);

        // Compute the state and status 
        void compute_state_status();

    private:
        Tango::DeviceImpl * m_device             ; // owner Device server object
        StateStatus         m_state_status       ; // state and status informations
        StateStatus         m_threads_status     ; // allows to treat the errors from the threads

      /** mutex used to protect the multithread access to store and collect threads.
        * The mutable keyword is used to allow using this mutex into const methods.
        */
        mutable yat::Mutex m_threads_access_mutex;

        ConfigurationState  m_configuration_state; // to know if the configuration was loaded (state is OFF/DISABLED till it is not done)
        ConfigurationState  m_rois_state         ; // to know if Rois were loaded (state is DISABLED during the loading)

#ifndef DANTE_DPP_TANGO_INDEPENDANT
        yat::UniquePtr<ConfigView> m_config_view; // config dynamic attributes manager
        yat::UniquePtr<RoisView>   m_rois_view  ; // ROIs dynamic attributes manager
#endif

        // configuration load command
        std::string          m_configuration_file_path ; // complete path of the loaded/loading configuration file
        std::string          m_configuration_alias     ; // configuration alias
        LoadConfigParameters m_configuration_parameters; // to keep the current load configuration parameters

        // rois commands
        std::string           m_rois_file_path ; // complete path of the loaded/loading rois file
        std::string           m_rois_alias     ; // rois alias
        RoisCommandParameters m_rois_parameters; // to keep the current command parameters
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_CONTROLLER_H_