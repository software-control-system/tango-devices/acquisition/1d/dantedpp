//=============================================================================
//
// file :        RoisView.cpp
//
// description : 
//
// project :	DANTE Project
//
// $Author: Cédric Castel
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

//TANGO
#include <DanteDpp.h>

// SYSTEM
#include <iomanip>      

// PROJECT
#include "RoisView.h"
#include "Log.h"
#include "RoisManager.h"

namespace DanteDpp_ns
{
//-----------------------------------------------------------------------------
// method :  RoisView::RoisView()
// description : Ctor
//-----------------------------------------------------------------------------
RoisView::RoisView(Tango::DeviceImpl *dev) : m_device(dev), m_dim(dev)
{
    INFO_STRM << "construction of RoisView" << std::endl;
}

//-----------------------------------------------------------------------------
// method :  RoisView::~RoisView()
// description : Dtor
//-----------------------------------------------------------------------------
RoisView::~RoisView()
{
    INFO_STRM << "destruction of RoisView" << std::endl;

	// remove all dynamics attributes
    remove_dynamic_attributes();

    for(std::size_t index = 0; index < m_dyn_rois.size() ; index++)
    { 
        delete m_dyn_rois[index];
    }

	m_dyn_rois.clear();

    INFO_STRM << "RoisView::~RoisView() - [END]" << endl;
}

/*******************************************************************
 * \brief To remove the dynamics attributes from ROIs file
 *******************************************************************/
void RoisView::remove_dynamic_attributes()
{
	// remove all previous dyn attributes
	m_dim.dynamic_attributes_manager().remove_attributes();
}

//-----------------------------------------------------------------------------
// method :  RoisView::create
// description : Create all dynamic attributes
//-----------------------------------------------------------------------------
void RoisView::create(const RoisViewParameters & in_parameters)
{
    INFO_STRM << "Creation of RoisView dynamics attributes" << endl;    

    // keep the create parameters
    m_create_parameters = in_parameters;

    std::string attribute_name;
    DanteDpp * device = dynamic_cast<DanteDpp*>(m_device);

	// remove all previous dynamics attributes
    remove_dynamic_attributes();

    // create ROI attributes
    std::size_t total_nb_rois = RoisManager::get_const_instance()->get_nb_rois();

    if(total_nb_rois > 0)
    {
        m_dyn_rois.resize(total_nb_rois);

        std::size_t absolute_roi_index = 0;

        for(std::size_t channel_index = 0 ; channel_index < m_create_parameters.m_channels_nb ; channel_index++)
        {
            // Get the number of rois for the channel
            std::size_t nb_rois = RoisManager::get_const_instance()->get_nb_rois(channel_index);

            for(std::size_t roi_index = 0 ; roi_index < nb_rois ; roi_index++)
            {
                // Get access to the roi informations of a channel
                yat::SharedPtr<const RoiRange> roi_range = RoisManager::get_const_instance()->get_roi(channel_index, roi_index);

                // create roi for channel
                const string attribute_name = yat::String::str_format("roi%02d_%02d", channel_index, roi_index + 1);
                string description;

                stringstream ss("");
                ss << "number of counts on the interval [" 
                   << roi_range->get_low_index() 
                   << "..." 
                   << roi_range->get_high_index()
                   << "]";
                description = ss.str();

                m_dyn_rois[absolute_roi_index] = new ULongUserData(attribute_name);
                m_dyn_rois[absolute_roi_index]->add_index(channel_index);
                m_dyn_rois[absolute_roi_index]->add_index(roi_index    );

                device->create_dynamic_attribute
                    (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
                     0, // max_dim_x
                     "cts", "%d", description,
                     &DanteDpp::read_roi_callback, &DanteDpp::write_callback_null,
                     static_cast<UserData*>(m_dyn_rois[absolute_roi_index]));

                absolute_roi_index++;
            }
        }
    }
}

}// namespace DanteDpp_ns
