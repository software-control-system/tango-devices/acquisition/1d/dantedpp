/*************************************************************************/
/*! 
 *  \file   AsyncBuffer.h
 *  \brief  class used to temporary store an async reply
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ASYNC_BUFFER_H_
#define DANTE_DPP_ASYNC_BUFFER_H_

// SYSTEM
#include <vector>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>

/*************************************************************************/
namespace DanteDpp_ns
{
/*************************************************************************/
class AsyncBuffer
{
    friend class AsyncManager; // for access to the attributes

    public:
        // constructor
        AsyncBuffer(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data);

    private:
        uint16_t              m_type   ; // type of operation
        uint32_t              m_call_id; // request identifier
        std::vector<uint32_t> m_data   ; // reply data
};

/***********************************************************************
 * AsyncBuffer containers types
 ***********************************************************************/
/**
 * \typedef AsyncBufferContainer
 * \brief type of container which contains a list of async replies
 */
typedef std::vector<yat::SharedPtr<AsyncBuffer>> AsyncBufferContainer;

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ASYNC_BUFFER_H_

//###########################################################################
