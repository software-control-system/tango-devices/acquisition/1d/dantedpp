/*************************************************************************/
/*! 
 *  \file   StreamParameters.cpp
 *  \brief  class used to manage the stream parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "StreamParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// StreamParameters class
//============================================================================================================
/**************************************************************************
* \brief simple constructor
**************************************************************************/
StreamParameters::StreamParameters()
{
    m_type = TYPE_UNDEFINED;
}

/**************************************************************************
* \brief constructor
* \param[in] in_type                    type of stream
* \param[in] in_target_path             path for the stream files
* \param[in] in_target_file             name for the stream file 
* \param[in] in_nb_acq_per_file         number of files per acquisition
* \param[in] in_nb_data_per_acq         number of data per acquisition
* \param[in] in_write_mode              stream write mode
* \param[in] in_memory_mode             stream memory mode
* \param[in] in_file_generation         file generation
* \param[in] in_label_real_time         label of real time data in nexus file
* \param[in] in_label_live_time         label of live time data in nexus file
* \param[in] in_label_dead_time         label of dead time data in nexus file
* \param[in] in_label_input_count_rate  label of input count rate data in nexus file
* \param[in] in_label_output_count_rate label of output count rate data in nexus file
* \param[in] in_label_events_in_run     label of events in run data in nexus file
* \param[in] in_label_spectrum          label of spectrum data in nexus file
* \param[in] in_label_roi               label of roi data in nexus file
**************************************************************************/
StreamParameters::StreamParameters(const StreamParameters::Types       in_type                   ,
                                   const std::string &                 in_target_path            ,
                                   const std::string &                 in_target_file            ,
                                   const uint32_t                      in_nb_acq_per_file        ,
                                   const uint32_t                      in_nb_data_per_acq        ,
                                   const StreamParameters::WriteModes  in_write_mode             ,
                                   const StreamParameters::MemoryModes in_memory_mode            ,
                                   const bool                          in_file_generation        ,
                                   const std::string &                 in_label_real_time        ,
                                   const std::string &                 in_label_live_time        ,
                                   const std::string &                 in_label_dead_time        ,
                                   const std::string &                 in_label_input_count_rate ,
                                   const std::string &                 in_label_output_count_rate,
                                   const std::string &                 in_label_events_in_run    ,
                                   const std::string &                 in_label_spectrum         ,
                                   const std::string &                 in_label_roi              )
{
    m_type                    = in_type                   ;
    m_target_path             = in_target_path            ;
    m_target_file             = in_target_file            ;
    m_nb_acq_per_file         = in_nb_acq_per_file        ;
    m_nb_data_per_acq         = in_nb_data_per_acq        ;
    m_write_mode              = in_write_mode             ;
    m_memory_mode             = in_memory_mode            ;
    m_file_generation         = in_file_generation        ; 
    m_label_real_time         = in_label_real_time        ;
    m_label_live_time         = in_label_live_time        ;
    m_label_dead_time         = in_label_dead_time        ;
    m_label_input_count_rate  = in_label_input_count_rate ;
    m_label_output_count_rate = in_label_output_count_rate;
    m_label_events_in_run     = in_label_events_in_run    ;
    m_label_spectrum          = in_label_spectrum         ;
    m_label_roi               = in_label_roi              ;
}

//###########################################################################
}