//=============================================================================
//
// file :        ConfigView.cpp
//
// description : 
//
// project :	DANTE Project
//
// $Author: Cédric Castel
//
// copyleft :    Synchrotron SOLEIL
//               L'Orme des merisiers - Saint Aubin
//               BP48 - 91192 Gif sur Yvette
//               FRANCE
//=============================================================================

//TANGO
#include <DanteDpp.h>

// SYSTEM
#include <iomanip>      

// PROJECT
#include "ConfigView.h"
#include "Log.h"
#include "Helper.h"

namespace DanteDpp_ns
{
//-----------------------------------------------------------------------------
// method :  ConfigView::ConfigView()
// description : Ctor
//-----------------------------------------------------------------------------
ConfigView::ConfigView(Tango::DeviceImpl *dev) : m_device(dev), m_dim(dev)
{
    INFO_STRM << "construction of ConfigView" << std::endl;

    m_dyn_preset_value  = NULL;
    m_dyn_nb_pixels     = NULL;
    m_dyn_current_pixel = NULL;
    m_dyn_preset_type   = NULL;
    m_dyn_trigger_mode  = NULL;
	m_dyn_sp_time       = NULL;
}

//-----------------------------------------------------------------------------
// method :  ConfigView::~ConfigView()
// description : Dtor
//-----------------------------------------------------------------------------
ConfigView::~ConfigView()
{
    INFO_STRM << "destruction of ConfigView" << std::endl;

	// remove all dynamics attributes
    remove_dynamic_attributes();

    for(std::size_t index = 0; index < m_dyn_realtimes.size() ; index++)
    { 
        delete m_dyn_realtimes         [index];
        delete m_dyn_livetimes         [index];
        delete m_dyn_deadtimes         [index];
        delete m_dyn_input_count_rates [index];
        delete m_dyn_output_count_rates[index];
        delete m_dyn_events_in_runs    [index];
        delete m_dyn_channels          [index];
    }

	m_dyn_realtimes.clear();
	m_dyn_livetimes.clear();
	m_dyn_deadtimes.clear();
    m_dyn_input_count_rates.clear();
	m_dyn_output_count_rates.clear();
	m_dyn_events_in_runs.clear();
	m_dyn_channels.clear();

    if(m_dyn_preset_value != NULL)
    {
        delete m_dyn_preset_value;
        m_dyn_preset_value = NULL;
    }

    if(m_dyn_nb_pixels != NULL)
    {
        delete m_dyn_nb_pixels;
        m_dyn_nb_pixels = NULL;
    }

    if(m_dyn_current_pixel != NULL)
    {
        delete m_dyn_current_pixel;
        m_dyn_current_pixel = NULL;
    }

    if(m_dyn_preset_type != NULL)
    {
        delete m_dyn_preset_type;
        m_dyn_preset_type = NULL;
    }

    if(m_dyn_trigger_mode != NULL)
    {
        delete m_dyn_trigger_mode;
        m_dyn_trigger_mode = NULL;
    }
	
	if(m_dyn_sp_time != NULL)
    {
        delete m_dyn_sp_time;
        m_dyn_sp_time = NULL;
    }

    INFO_STRM << "ConfigView::~ConfigView() - [END]" << endl;
}

/*******************************************************************
 * \brief To remove the dynamics attributes from config file
 *******************************************************************/
void ConfigView::remove_dynamic_attributes()
{
	// remove all previous dyn attributes
	m_dim.dynamic_attributes_manager().remove_attributes();
}

//-----------------------------------------------------------------------------
// method :  ConfigView::build_channel_attribute_name
// description : build a dynamic attribute for a channel index
//-----------------------------------------------------------------------------
std::string ConfigView::build_channel_attribute_name(const std::string & in_label, const std::size_t in_channel_index) const
{
    stringstream ss("");
    ss << in_label << std::setfill('0') << std::setw(2) << in_channel_index;
    return ss.str();
}

//-----------------------------------------------------------------------------
// method :  ConfigView::create
// description : Create all dynamic attributes
//-----------------------------------------------------------------------------
void ConfigView::create(const ConfigViewParameters & in_parameters)
{
    INFO_STRM << "Creation of ConfigView dynamics attributes" << endl;    

    // keep the create parameters
    m_create_parameters = in_parameters;

    std::string attribute_name;
    DanteDpp * device = dynamic_cast<DanteDpp*>(m_device);

	// remove all previous dynamics attributes
    remove_dynamic_attributes();

    // resize the attributes containers
    m_dyn_realtimes.resize         (in_parameters.m_channels_nb);
    m_dyn_livetimes.resize         (in_parameters.m_channels_nb);
    m_dyn_deadtimes.resize         (in_parameters.m_channels_nb);
    m_dyn_input_count_rates.resize (in_parameters.m_channels_nb);
    m_dyn_output_count_rates.resize(in_parameters.m_channels_nb);
    m_dyn_events_in_runs.resize    (in_parameters.m_channels_nb);
	m_dyn_channels.resize          (in_parameters.m_channels_nb);

    // ===================================================================================
    // TRIGGER MODE
    // ===================================================================================
    // specific to MCA acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        // create read-only trigger mode
        attribute_name = "triggerMode";
        m_dyn_trigger_mode = new StringUserData(attribute_name, TANGO_TRIGGER_MODES_LABELS_SIZE_MAX);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_STRING, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%s", "Gets the trigger mode type:<br>\nINTERNAL_SINGLE",
             &DanteDpp::read_trigger_mode_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_trigger_mode));
    }
    else
    // specific to MAPPING acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        // create read-write trigger mode
        attribute_name = "triggerMode";
        m_dyn_trigger_mode = new StringUserData(attribute_name, TANGO_TRIGGER_MODES_LABELS_SIZE_MAX);
        m_dyn_trigger_mode->set_memorized_name("MemorizedTriggerMode");

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_STRING, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%s", "Gets/sets the trigger mode type:\nINTERNAL_MULTI<br>\nEXTERNAL_GATE, \nEXTERNAL_TRIGRISE, \nEXTERNAL_TRIGFALL, \nEXTERNAL_TRIGBOTH, \nEXTERNAL_GATELOW",
             &DanteDpp::read_trigger_mode_callback, &DanteDpp::write_trigger_mode_callback,
             static_cast<UserData*>(m_dyn_trigger_mode));
    }

    // ===================================================================================
    // PIXEL
    // ===================================================================================
    // specific to MCA acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        // create current pixels
        attribute_name = "currentPixel";
        m_dyn_current_pixel = new ULongUserData(attribute_name);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%5d", "Current pixel of the Mca acquisition.",
             &DanteDpp::read_current_pixel_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_current_pixel));

        // create read-write nb pixels
        attribute_name = "nbPixels";
        m_dyn_nb_pixels = new ULongUserData(attribute_name);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%5d", "Number of pixels (step) for the Mca acquisition. Forced to 1.",
             &DanteDpp::read_nb_pixels_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_nb_pixels));
    }
    else
    // specific to MAPPING acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        // create current pixels
        attribute_name = "currentPixel";
        m_dyn_current_pixel = new ULongUserData(attribute_name);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%5d", "Current pixel of the Mapping acquisition.",
             &DanteDpp::read_current_pixel_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_current_pixel));

        // create read-write nb pixels
        attribute_name = "nbPixels";
        m_dyn_nb_pixels = new ULongUserData(attribute_name);
        m_dyn_nb_pixels->set_memorized_name("MemorizedNbPixels");

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             " ", "%5d", "Number of pixels (step) for the Mapping acquisition.",
             &DanteDpp::read_nb_pixels_callback, &DanteDpp::write_nb_pixels_callback,
             static_cast<UserData*>(m_dyn_nb_pixels));
    }
	
	 // ===================================================================================
    // SP TIME 
    // ===================================================================================
    // specific to MAPPING acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        // create read-write sp time
        attribute_name = "spTime";
        m_dyn_sp_time = new ULongUserData(attribute_name);
        m_dyn_sp_time->set_memorized_name("MemorizedSpTime");

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "ms", "%5d", "Spectrum time (ms) for the Mapping acquisition.",
             &DanteDpp::read_sp_time_callback, &DanteDpp::write_sp_time_callback,
             static_cast<UserData*>(m_dyn_sp_time));
    }

    // ===================================================================================
    // PRESET VALUE & TYPE
    // ===================================================================================
    // specific to MCA acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        // create preset value
        attribute_name = "presetValue";
        m_dyn_preset_value = new DoubleUserData(attribute_name);
        m_dyn_preset_value->set_memorized_name("MemorizedPresetValue");

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "secs", "%8.4f", "This value is the Mca acquisition time (specified in seconds).",
             &DanteDpp::read_preset_value_callback, &DanteDpp::write_preset_value_callback,
             static_cast<UserData*>(m_dyn_preset_value));
    }
    else
    // specific to MAPPING acquisition mode
    if(in_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        // create preset value
        attribute_name = "presetValue";
        m_dyn_preset_value = new DoubleUserData(attribute_name);
        m_dyn_preset_value->set_memorized_name("MemorizedPresetValue");

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "secs", "%8.4f", "This value is the spectrum acquisition time (specified in seconds).<br>\nUsed only in INTERNAL_MULTI trigger mode.",
             &DanteDpp::read_preset_value_callback, &DanteDpp::write_preset_value_callback,
             static_cast<UserData*>(m_dyn_preset_value));
    }

    // create preset type (for compatibility with Synchrotron tools)
    attribute_name = "presetType";
    m_dyn_preset_type = new StringUserData(attribute_name, TANGO_PRESET_TYPES_LABELS_SIZE_MAX);
    m_dyn_preset_type->set_memorized_name("MemorizedPresetType");

    device->create_dynamic_attribute
        (m_dim, attribute_name, Tango::DEV_STRING, Tango::SCALAR, Tango::READ_WRITE, Tango::OPERATOR, 0, // no polling
         0, // max_dim_x
         " ", "%s", "Gets/sets the preset run type:<br>\nFIXED_REAL",
         &DanteDpp::read_preset_type_callback, &DanteDpp::write_preset_type_callback,
         static_cast<UserData*>(m_dyn_preset_type));

    // ===================================================================================
    // COMMON ATTRIBUTES
    // ===================================================================================
    for(std::size_t index = 0 ; index < in_parameters.m_channels_nb; index++)
    {
        // create real time for channel
        attribute_name = build_channel_attribute_name("realTime", index);
        m_dyn_realtimes[index] = new DoubleUserData(attribute_name);
        m_dyn_realtimes[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "secs", "%f", "The total time that the processor was taking data in seconds.",
             &DanteDpp::read_realtime_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_realtimes[index]));

        // create live time for channel
        attribute_name = build_channel_attribute_name("liveTime", index);
        m_dyn_livetimes[index] = new DoubleUserData(attribute_name);
        m_dyn_livetimes[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "secs", "%f", "The total time that the processor was able to acquire new events in seconds.",
             &DanteDpp::read_livetime_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_livetimes[index]));

        // create dead time for channel
        attribute_name = build_channel_attribute_name("deadTime", index);
        m_dyn_deadtimes[index] = new DoubleUserData(attribute_name);
        m_dyn_deadtimes[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "%", "%f", "Deadtime as calculated by 100.0 * liveTime / realTime.",
             &DanteDpp::read_deadtime_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_deadtimes[index]));

        // create input count rate for channel
        attribute_name = build_channel_attribute_name("inputCountRate", index);
        m_dyn_input_count_rates[index] = new DoubleUserData(attribute_name);
        m_dyn_input_count_rates[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "cts/sec", "%f", "This is the total number of triggers divided by the runtime in counts per second.",
             &DanteDpp::read_input_count_rate_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_input_count_rates[index]));

        // create output count rate for channel
        attribute_name = build_channel_attribute_name("outputCountRate", index);
        m_dyn_output_count_rates[index] = new DoubleUserData(attribute_name);
        m_dyn_output_count_rates[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_DOUBLE, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "cts/sec", "%f", "This is the total number of events in the run divided by the runtime in counts per second.",
             &DanteDpp::read_output_count_rate_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_output_count_rates[index]));

        // create events in run for channel
        attribute_name = build_channel_attribute_name("eventsInRun", index);
        m_dyn_events_in_runs[index] = new ULongUserData(attribute_name);
        m_dyn_events_in_runs[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SCALAR, Tango::READ, Tango::OPERATOR, 0, // no polling
             0, // max_dim_x
             "cts", "%d", "The total number of events in a run that are written into the spectrum.",
             &DanteDpp::read_events_in_run_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_events_in_runs[index]));

        // create spectrum for channel
        attribute_name = build_channel_attribute_name("channel", index);
        m_dyn_channels[index] = new VectorULongUserData(attribute_name, in_parameters.m_bins_nb);
        m_dyn_channels[index]->add_index(index);

        device->create_dynamic_attribute
            (m_dim, attribute_name, Tango::DEV_ULONG, Tango::SPECTRUM, Tango::READ, Tango::OPERATOR, 0, // no polling
             in_parameters.m_bins_nb, // max_dim_x
             "", "", std::string("Fluo spectrum for ") + attribute_name,
             &DanteDpp::read_channel_callback, &DanteDpp::write_callback_null,
             static_cast<UserData*>(m_dyn_channels[index]));
    }
}

//-----------------------------------------------------------------------------
// method :  ConfigView::init
// description : init all dynamic write attributes
//-----------------------------------------------------------------------------
void ConfigView::init()
{
    INFO_STRM << "Init of ConfigView dynamics write attributes" << endl;    

    DanteDpp * device = dynamic_cast<DanteDpp*>(m_device);

    // update the memorized values of writable dynamic attributes
    device->write_property_in_dynamic_user_attribute<Tango::DevDouble>(m_dim, "presetValue", &DanteDpp::write_preset_value_callback);
    device->write_property_in_dynamic_user_attribute<std::string>(m_dim, "presetType" , &DanteDpp::write_preset_type_callback );

    // specific to MAPPING acquisition mode
    if(m_create_parameters.m_acqMode == HardwareDanteConfigParameters::AcqMode::MAPPING)
    {
        // update the memorized values of writable dynamic attributes
        device->write_property_in_dynamic_user_attribute<std::string>(m_dim, "triggerMode" , &DanteDpp::write_trigger_mode_callback);
        device->write_property_in_dynamic_user_attribute<Tango::DevULong>(m_dim, "nbPixels", &DanteDpp::write_nb_pixels_callback);
		device->write_property_in_dynamic_user_attribute<Tango::DevULong>(m_dim, "spTime", &DanteDpp::write_sp_time_callback);
    }
}

}// namespace DanteDpp_ns
