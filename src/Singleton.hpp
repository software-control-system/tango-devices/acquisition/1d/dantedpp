/**
 *  \file Singleton.hpp
 *  \brief implementation file of Singleton template class
 *         Should not be included, use only Singleton.h as include file.
 *  \author C�dric Castel
 *  \version 0.1
 *  \date November 29 2019
 *  Created on: November 07 2019
 */

/***************************************************************************************************
  * CLASS GENERAL DATA
  **************************************************************************************************/
/** used to store the singleton
  */
template <class Elem>
yat::SharedPtr<Elem> Singleton<Elem>::g_singleton = NULL;

/************************************************************************
 * \brief constructor
 ************************************************************************/
template <class Elem>
Singleton<Elem>::Singleton()
{
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
template <class Elem>
Singleton<Elem>::~Singleton()
{
}

/************************************************************************
 * \brief release the singleton (execute a specific code for an derived class)
 ************************************************************************/
template <class Elem>
void Singleton<Elem>::specific_release()
{
}

/*******************************************************************
 * \brief release the singleton
*******************************************************************/
template <class Elem>
void Singleton<Elem>::release()
{
    if(!Singleton::g_singleton.is_null())
    {
        Singleton::g_singleton->specific_release();
        Singleton::g_singleton.reset();
        Singleton::g_singleton = NULL;
    }
}

/*******************************************************************
 * \brief return the singleton
*******************************************************************/
template <class Elem>
yat::SharedPtr<Elem> Singleton<Elem>::get_instance()
{
    return Singleton::g_singleton;
}

/*******************************************************************
 * \brief return the singleton (const version)
*******************************************************************/
template <class Elem>
yat::SharedPtr<const Elem> Singleton<Elem>::get_const_instance()
{
    return Singleton::g_singleton;
}
