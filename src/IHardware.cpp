/*************************************************************************************/
/*! 
 *  \file   IHardware.h
 *  \brief  detector abstract class implementation
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************************/

// PROJECT
#include "IHardware.h"
#include "Log.h"

using namespace DanteDpp_ns;

/************************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management
 ************************************************************************/
IHardware::IHardware()
{
    INFO_STRM << "construction of IHardware" << std::endl;

    // default value
    m_state_status.set(Tango::STANDBY, "detector is waiting for request...");
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
IHardware::~IHardware()
{
    INFO_STRM << "destruction of IHardware" << std::endl;
}

//------------------------------------------------------------------
// state management
//------------------------------------------------------------------
/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string IHardware::get_status(void) const
{
    return m_state_status.get_status();
}

/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState IHardware::get_state(void)
{
    return m_state_status.get_state();
}

//------------------------------------------------------------------
// acquisition management
//------------------------------------------------------------------
/***************************************************************************
 * \brief Manage the start of acquisition
 ***************************************************************************/
void IHardware::start_acquisition(void)
{
    INFO_STRM << "--------------------------------------------" << std::endl;
    INFO_STRM << "IHardware::start_acquisition()"               << std::endl;

    check_configuration_before_acquisition();

    if(!start_detector_acquisition())
    {
        // throwing exception
        std::ostringstream msg_err;
        msg_err << "Could not run an acquisition for the device!" << std::endl;

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       msg_err.str().c_str(),
                                       "IHardware::start_acquisition");
    }
}

/*******************************************************************
 * \brief Manage the acquisition stop
 * \param[in] in_sync true for waiting the stop is done
 *******************************************************************/
void IHardware::stop_acquisition(bool /*in_sync*/)
{
    INFO_STRM << "--------------------------------------------" << std::endl;
    INFO_STRM << "IHardware::stop_acquisition()"                << std::endl;

    try
    {
        stop_detector_acquisition();
    }
    catch (Tango::DevFailed& df)
    {
        m_state_status.on_fault(df);
    }
}

//========================================================================================
