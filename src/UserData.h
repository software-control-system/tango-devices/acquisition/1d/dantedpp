/*************************************************************************/
/*! 
 *  \file   UserData.h
 *  \brief  Contains classes used to store informations for dynamics attributs
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_USER_DATA_H
#define DANTE_DPP_USER_DATA_H

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>

//#define MAX_ATTRIBUTE_STRING_LENGTH 255

namespace DanteDpp_ns
{
    // abstract class
    class UserData
    {
    public:
        UserData(const std::string & in_name);
        virtual ~UserData();

        virtual void set_value(const void * in_new_value) = 0;
        virtual void fill_read_attr(Tango::Attribute * tga) const = 0;

        const std::string & get_name() const;

        const std::string & get_memorized_name() const;
        void set_memorized_name(const std::string & in_memorized_name);
        bool has_memorized_name() const;

        void add_index(std::size_t in_index);
        std::size_t get_index(std::size_t in_number) const;
        std::size_t get_indexes_nb() const;

        std::size_t get_max_size() const;

    protected:
        // mutex used to protect state and status access
        // mutable keyword is used to allow const methods even if they use this class member
        mutable yat::Mutex       m_lock          ;
        std::string              m_name          ; // attribute name
        std::string              m_memorized_name; // memorized property name
        std::vector<std::size_t> m_indexes       ; // optional indexes
        std::size_t              m_max_size      ; // optional max size
    };

    // double class
    class DoubleUserData : public UserData
    {
    public:
        DoubleUserData(const std::string & in_name);
        virtual ~DoubleUserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable Tango::DevDouble m_value;
    };

    // ULong class
    class ULongUserData : public UserData
    {
    public:
        ULongUserData(const std::string & in_name);
        virtual ~ULongUserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable Tango::DevULong m_value;
    };

    // string class
    class StringUserData : public UserData
    {
    public:
        StringUserData(const std::string & in_name, std::size_t in_max_size);
        virtual ~StringUserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;


    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable Tango::DevString * m_value;
    };

    // ULong64 class
    class ULong64UserData : public UserData
    {
    public:
        ULong64UserData(const std::string & in_name);
        virtual ~ULong64UserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable Tango::DevULong64 m_value;
    };

    // vector of ULong64 class
    class VectorULong64UserData : public UserData
    {
    public:
        VectorULong64UserData(const std::string & in_name, std::size_t in_size);
        virtual ~VectorULong64UserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable std::vector<Tango::DevULong64> m_values;
    };
	
    // vector of Double class
    class VectorDoubleUserData : public UserData
    {
    public:
        VectorDoubleUserData(const std::string & in_name, std::size_t in_size);
        virtual ~VectorDoubleUserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable std::vector<Tango::DevDouble> m_values;
    };	

    // vector of ULong class
    class VectorULongUserData : public UserData
    {
    public:
        VectorULongUserData(const std::string & in_name, std::size_t in_size);
        virtual ~VectorULongUserData();

        virtual void set_value(const void * in_new_value);
        virtual void fill_read_attr(Tango::Attribute * tga) const;

    private:
        // mutable keyword is used to allow const methods even if they use this class member
        mutable std::vector<Tango::DevULong> m_values;
    };	
} // namespace DanteDpp_ns

#endif // DANTE_DPP_USER_DATA_H
