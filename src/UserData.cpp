/*************************************************************************/
/*! 
 *  \file   UserData.cpp
 *  \brief  implementation file of UserData classes.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "UserData.h"
#include <PogoHelper.h>

namespace DanteDpp_ns
{
//----------------------------------------------------------------------------------------------------------------------
// UserData
//----------------------------------------------------------------------------------------------------------------------
UserData::UserData(const std::string & in_name) : m_name(in_name)
{
    m_max_size = 0;
}

UserData::~UserData()
{
}

// attribute name
const std::string & UserData::get_name() const
{
    return m_name;
}

// memorized property name (for write attributes)
const std::string & UserData::get_memorized_name() const
{
    return m_memorized_name;
}

void UserData::set_memorized_name(const std::string & in_memorized_name)
{
    m_memorized_name = in_memorized_name;
}

bool UserData::has_memorized_name() const
{
    return !m_memorized_name.empty();
}

// optional indexes
void UserData::add_index(std::size_t in_index)
{
    m_indexes.push_back(in_index);
}

std::size_t UserData::get_index(std::size_t in_number) const
{
    return m_indexes[in_number];
}

std::size_t UserData::get_indexes_nb() const
{
    return m_indexes.size();
}

std::size_t UserData::get_max_size() const
{
    return m_max_size;
}

//----------------------------------------------------------------------------------------------------------------------
// DoubleUserData
//----------------------------------------------------------------------------------------------------------------------
DoubleUserData::DoubleUserData(const std::string & in_name) : UserData(in_name)
{
}

DoubleUserData::~DoubleUserData()
{
}

void DoubleUserData::set_value(const void * in_new_value)
{
    const double * value = reinterpret_cast<const double *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    m_value = *value;
}

void DoubleUserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(&m_value);
}

//----------------------------------------------------------------------------------------------------------------------
// ULongUserData
//----------------------------------------------------------------------------------------------------------------------
ULongUserData::ULongUserData(const std::string & in_name) : UserData(in_name)
{
}

ULongUserData::~ULongUserData()
{
}

void ULongUserData::set_value(const void * in_new_value)
{
    const uint32_t * value = reinterpret_cast<const uint32_t *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    m_value = *value;
}

void ULongUserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(&m_value);
}

//----------------------------------------------------------------------------------------------------------------------
// StringUserData
//----------------------------------------------------------------------------------------------------------------------
StringUserData::StringUserData(const std::string & in_name, std::size_t in_max_size) : UserData(in_name)
{
    m_max_size = in_max_size;
    CREATE_DEVSTRING_ATTRIBUTE(m_value, m_max_size);
}

StringUserData::~StringUserData()
{
    DELETE_DEVSTRING_ATTRIBUTE(m_value);
}

void StringUserData::set_value(const void * in_new_value)
{
    const std::string * value = reinterpret_cast<const std::string *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    strcpy(*m_value, value->c_str());
}

void StringUserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(m_value);
}

//----------------------------------------------------------------------------------------------------------------------
// ULong64UserData
//----------------------------------------------------------------------------------------------------------------------
ULong64UserData::ULong64UserData(const std::string & in_name) : UserData(in_name)
{
}

ULong64UserData::~ULong64UserData()
{
}

void ULong64UserData::set_value(const void * in_new_value)
{
    const uint64_t * value = reinterpret_cast<const uint64_t *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    m_value = *value;
}

void ULong64UserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(&m_value);
}

//----------------------------------------------------------------------------------------------------------------------
// VectorULong64UserData
//----------------------------------------------------------------------------------------------------------------------
VectorULong64UserData::VectorULong64UserData(const std::string & in_name, std::size_t in_size) : UserData(in_name)
{
    m_values.resize(in_size);
}

VectorULong64UserData::~VectorULong64UserData()
{
    m_values.clear();
}

void VectorULong64UserData::set_value(const void * in_new_value)
{
    const std::vector<uint64_t> * values = reinterpret_cast<const std::vector<uint64_t> *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    memcpy(m_values.data(), values->data(), values->size() * sizeof(uint64_t));
}

void VectorULong64UserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(m_values.data(), m_values.size());
}

//----------------------------------------------------------------------------------------------------------------------
// VectorDoubleUserData
//----------------------------------------------------------------------------------------------------------------------
VectorDoubleUserData::VectorDoubleUserData(const std::string & in_name, std::size_t in_size) : UserData(in_name)
{
    m_values.resize(in_size);
}

VectorDoubleUserData::~VectorDoubleUserData()
{
    m_values.clear();
}

void VectorDoubleUserData::set_value(const void * in_new_value)
{
    const std::vector<double> * values = reinterpret_cast<const std::vector<double> *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    memcpy(m_values.data(), values->data(), values->size() * sizeof(double));
}

void VectorDoubleUserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(m_values.data(), m_values.size());
}

//----------------------------------------------------------------------------------------------------------------------
// VectorULongUserData
//----------------------------------------------------------------------------------------------------------------------
VectorULongUserData::VectorULongUserData(const std::string & in_name, std::size_t in_size) : UserData(in_name)
{
    m_values.resize(in_size);
}

VectorULongUserData::~VectorULongUserData()
{
    m_values.clear();
}

void VectorULongUserData::set_value(const void * in_new_value)
{
    const std::vector<uint32_t> * values = reinterpret_cast<const std::vector<uint32_t> *>(in_new_value);
    yat::MutexLock scoped_lock(m_lock);
    memcpy(m_values.data(), values->data(), values->size() * sizeof(uint32_t));
}

void VectorULongUserData::fill_read_attr(Tango::Attribute * tga) const
{
    yat::MutexLock scoped_lock(m_lock);
    tga->set_value(m_values.data(), m_values.size());
}
/////////////////////////////////////////////////////////////////////////////////
} // namespace
