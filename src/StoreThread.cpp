/*************************************************************************/
/*! 
 *  \file   StoreThread.cpp
 *  \brief  DANTE detector store thread class implementation
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"
#include "StoreThread.h"
#include "HardwareDante.h"
#include "Controller.h"
#include "DataStore.h"

namespace DanteDpp_ns
{
// thread used to store the acquisition data
StoreThread * StoreThread::g_thread = NULL; 

/*******************************************************************
 * \brief constructor
 * \param[in] in_name thread name (for logging)
 *******************************************************************/
StoreThread::StoreThread(const std::string in_name) : SyncThread(in_name)
{
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StoreThread::~StoreThread()
{
}

/*******************************************************************
 * \brief The thread entry point called by yat::Thread::start_undetached.
 *        SyncThread process in managed inside this method.
 *******************************************************************/
yat::Thread::IOArg StoreThread::run_undetached(yat::Thread::IOArg /*ioa*/)
{
    try
    {
        try
        {
            // registering the thread as a new customer for the reception
            DataStore::get_instance()->add_reception_customer(m_name);

            SyncThread::start_treatment();

            /// decoding loop
            while (SyncThread::can_go_on())
            {
                if(DataStore::get_instance()->store_complete_data())
                {
                    SyncThread::treatment_completed();
                }
            }

            SyncThread::finish_treatment();

            // unregistering the thread for the reception
            DataStore::get_instance()->remove_reception_customer(m_name);
        }
        catch (Tango::DevFailed & in_ex)
        {
            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault(in_ex);
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
        catch (yat::Exception & in_ex)
        {
            in_ex.dump();

            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault(in_ex);
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
        catch(const DanteDpp_ns::Exception & in_ex)
        {
            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault(in_ex);
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
        catch(...)
        {
            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault("Unknown exception!");
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
    }
    catch(...)
    {
        std::cout << "Exception occured!" << Controller::get_instance()->get_threads_status().get_status() << std::endl;
        SyncThread::treatment_completed();
        SyncThread::finish_treatment   ();

        // unregistering the thread for the reception
        DataStore::get_instance()->remove_reception_customer(m_name);

        throw;
    }

    return 0;
}

/*******************************************************************
 * \brief Create the store thread
 *******************************************************************/
void StoreThread::create()
{
    // creating and starting the thread
    g_thread = new StoreThread("Store thread");
    g_thread->start();
}

/*******************************************************************
 * \brief Release the store thread
 *******************************************************************/
void StoreThread::release()
{
    // stops the thread and waits for its end
    if(g_thread != NULL)
    {
        g_thread->exit();
        g_thread = NULL;
    }
}

/*******************************************************************
 * \brief tell if the store thread was created
 * \return true if the thread was created, else false
 *******************************************************************/
bool StoreThread::exist()
{
    return (g_thread != NULL);
}

/*******************************************************************
 * \brief return the singleton
*******************************************************************/
StoreThread * StoreThread::get_instance()
{
    return g_thread;
}

/*******************************************************************
 * \brief return the singleton (const version)
*******************************************************************/
const StoreThread * StoreThread::get_const_instance()
{
    return g_thread;
}

} /// namespace DanteDpp_ns
