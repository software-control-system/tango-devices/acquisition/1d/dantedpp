/*************************************************************************/
/*! 
 *  \file   StreamCsv.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamCsv.h"
#include "Log.h"
#include "IHardware.h"
#include "RoisManager.h"
#include "Exception.h"

namespace DanteDpp_ns
{
/*******************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management/controller
 *******************************************************************/
StreamCsv::StreamCsv()
{
    INFO_STRM << "construction of StreamCsv" << std::endl;
    reset_index();
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamCsv::~StreamCsv() 
{
    INFO_STRM << "destruction of StreamCsv" << std::endl;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_module_data module data to be treated
 *******************************************************************/
void StreamCsv::update(yat::SharedPtr<const ModuleData> in_module_data)
{
    DEBUG_STRM << "===============================" << std::endl;
    DEBUG_STRM << "StreamCsv::update - received data (" << in_module_data->get_index() << ")" << std::endl;

    const std::string index_label       = "index"     ;
    const std::string nb_channels_label = "nbchannels";
    const std::string nb_bins_label     = "nbbins"    ;
    const std::string nb_rois_label     = "nbrois"    ;

    const std::size_t channels_nb = IHardware::get_const_instance()->get_channels_nb();
    const std::size_t bins_nb     = IHardware::get_const_instance()->get_bins_nb();

    char filename[255];
    sprintf(filename, "%s/%s_%06d.csv", m_parameters.m_target_path.c_str(), m_parameters.m_target_file.c_str(), m_file_index);
    std::ofstream output_file(filename, std::ios::out | std::ofstream::binary);

    if (output_file.fail()) 
    {
        std::string text = std::string("Couldn't open the file: ") + filename + std::string("!");
        throw DanteDpp_ns::Exception("SAVE_ERROR", text.c_str(), "StreamCsv::update");
    }

    m_file_index++;

    DEBUG_STRM << "filename " << filename << std::endl;

    output_file << index_label       << ": " << in_module_data->get_index() << std::endl; // index
    output_file << nb_channels_label << ": " << channels_nb                 << std::endl; // nbchannels
    output_file << nb_bins_label     << ": " << bins_nb                     << std::endl; // nbbins
    output_file << std::endl;

    for (std::size_t channels_index = 0 ; channels_index < channels_nb ; channels_index++)
    {
        const std::size_t rois_nb_for_channel = RoisManager::get_const_instance()->get_nb_rois(channels_index);
        yat::SharedPtr<const ChannelData> channel_data = in_module_data->get_channel(channels_index);

        output_file << m_parameters.m_label_real_time         << channels_index << ": " << channel_data->get_real_time()         << std::endl;
        output_file << m_parameters.m_label_live_time         << channels_index << ": " << channel_data->get_live_time()         << std::endl;
        output_file << m_parameters.m_label_dead_time         << channels_index << ": " << channel_data->get_dead_time()         << std::endl;
        output_file << m_parameters.m_label_input_count_rate  << channels_index << ": " << channel_data->get_input_count_rate()  << std::endl;
        output_file << m_parameters.m_label_output_count_rate << channels_index << ": " << channel_data->get_output_count_rate() << std::endl;
        output_file << m_parameters.m_label_events_in_run     << channels_index << ": " << channel_data->get_events_in_run()     << std::endl;

        // spectrum
        {
            output_file << m_parameters.m_label_spectrum << channels_index << ": ";

            const std::vector<uint32_t> & spectrum = channel_data->get_spectrum();
            std::ostream_iterator<uint32_t> output_iterator(output_file, ";");
            std::copy(spectrum.begin(), spectrum.end(), output_iterator);
            output_file << std::endl;
        }

        // rois
        output_file << nb_rois_label << channels_index << ": " << rois_nb_for_channel << std::endl;

        if(rois_nb_for_channel > 0)
        {
            output_file << m_parameters.m_label_roi << channels_index << ": ";

            for (std::size_t roi_index = 0 ; roi_index < rois_nb_for_channel ; roi_index++)
            {
                output_file << channel_data->get_rois(roi_index);

                if(roi_index != (rois_nb_for_channel - 1))
                {
                    output_file << ";";
                }
            }
            output_file << std::endl;
        }

        output_file << std::endl;
    }

    output_file.flush();
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamCsv::reset_index(void)
{
    m_file_index = 1;
}

//###########################################################################
}