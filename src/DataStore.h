/*************************************************************************/
/*! 
 *  \file   DataStore.h
 *  \brief  class used to store the acquisitions
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_DATA_STORE_H_
#define DANTE_DPP_DATA_STORE_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Singleton.h"
#include "ModuleData.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class DataStore : public Singleton<DataStore>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<DataStore>;

    public:
        // reset
        void reset();

        // init
        void init(std::size_t in_data_nb_to_acquire);

        // restart the acquisition timers
        void restart_timers();

        // get the current pixel value
        std::size_t get_current_pixel() const;

        // stop the acquisition
        void stop_acquisition();

        // add a new acquired channel data
        void add_channel_data(std::size_t in_module_data_index, yat::SharedPtr<ChannelData> in_channel_data);

        // Tell if the collect process is over
        bool end_of_collect() const;

        // Tell if the store process is over
        bool end_of_store() const;

        // logs stats
        void log_stats() const;

        // store the complete data
        bool store_complete_data();

        // get the latest module data
        yat::SharedPtr<const ModuleData> get_latest_module_data() const;

        // set the latest module data
        void set_latest_module_data(yat::SharedPtr<const ModuleData> in_latest_module_data);

      /***************************************************************************************************
        * ACTORS MANAGEMENT
        **************************************************************************************************/
        // add a reception producer
        void add_reception_producer(const std::string & in_producer);

        // remove a reception producer
        void remove_reception_producer(const std::string & in_producer);

        // add a reception customer
        void add_reception_customer(const std::string & in_customer);

        // remove a reception customer
        void remove_reception_customer(const std::string & in_customer);

        /***************************************************************************************************
          * SINGLETON MANAGEMENT
          **************************************************************************************************/
         // Create the manager
        static void create();

        // release the singleton (execute a specific code for an derived class)
        virtual void specific_release();

    protected:
        // constructor
        explicit DataStore();

        // destructor (needs to be virtual)
        virtual ~DataStore();

        // terminate
        void terminate();

    private:
        // Waiting for a complete data
        void waiting_complete_data() const;

    private:
      /** container used to store module data during reception (only incomplet data)
        */
        ModuleDataMap m_received_data;

      /** container used to store complete module data waiting for being processed by the stream manager
        */
        yat::SharedPtr<ModuleDataList> m_complete_data;

      /** latest complete module data
        */
        yat::SharedPtr<const ModuleData> m_latest_module_data;

        // mutex used to protect the access to the latest complete module data
        // mutable keyword is used to allow const methods even if they use this class member
        mutable yat::Mutex m_latest_module_data_lock;

        // mutex used to protect the access to the complete_data during its init
        // mutable keyword is used to allow const methods even if they use this class member
        mutable yat::Mutex m_init_complete_data_lock;

      /** used to store the number of module data we are acquiring
        */
        std::size_t    m_data_nb_to_acquire;

        std::size_t    m_channels_nb;
        std::size_t    m_bins_nb    ;
        std::size_t    m_rois_nb    ;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_DATA_STORE_H_

//###########################################################################
