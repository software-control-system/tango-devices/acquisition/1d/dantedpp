/********************************************************************************/
/*! 
 *  \file   IHardware.h
 *  \brief  detector abstract class interface 
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/********************************************************************************/

#ifndef DANTE_DPP_IHARDWARE_H
#define DANTE_DPP_IHARDWARE_H

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>

// PROJECT
#include "Log.h"
#include "StateStatus.h"
#include "Singleton.h"

namespace DanteDpp_ns
{
/***********************************************************************
 * \class IHardware
 * \brief abstract class used to control a detector
 ***********************************************************************/

class IHardware : public Singleton<IHardware>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<IHardware>;

    public:
        //------------------------------------------------------------------
        // state management
        //------------------------------------------------------------------
        // can not be const because internal member is updated during the call
        // Getter of the state
        Tango::DevState get_state(void);

        // Getter of the status
        std::string get_status(void) const;

        //------------------------------------------------------------------
        // init/terminate management
        //------------------------------------------------------------------
        // init the detector
        virtual void init(void) = 0;

        // terminate the detector
        virtual void terminate(void) = 0;

        //------------------------------------------------------------------
        // system informations
        //------------------------------------------------------------------
        // gets the number of modules (number of boards)
        virtual std::size_t get_modules_nb() const = 0;

        // gets the number of channels (total for all the boards)
        virtual std::size_t get_channels_nb() const = 0;

        // gets the number of bins
        virtual std::size_t get_bins_nb() const = 0;

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // start detector acquisition
        virtual bool start_detector_acquisition(void) = 0;

        // stop detector acquisition
        virtual void stop_detector_acquisition(void) = 0;

        // treat acquisitions
        virtual void treat_acquisitions(void) = 0;

        // Manage the start of acquisition
        void start_acquisition(void);

        // Manage the acquisition stop
        void stop_acquisition(bool in_sync);

        // gets the total number of pixels
        virtual uint32_t get_nb_pixels() const = 0;

        // sets the total number of pixels
        virtual void set_nb_pixels(const uint32_t & in_nb_pixels) = 0;

        // gets the preset value
        virtual double get_preset_value() const = 0;

        // sets the preset value
        virtual void set_preset_value(const double & in_preset_value) = 0;
		
		// set spectrum time value
        virtual void set_sp_time(const uint32_t & in_sp_time) = 0;

        // get spectrum time value
        virtual uint32_t get_sp_time() const = 0;

    protected:
        // constructor
        explicit IHardware();

        // destructor (needs to be virtual)
        virtual ~IHardware();

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // Check the configuration before the start of an acquisition
        virtual void check_configuration_before_acquisition() const = 0;

    protected:
        // state and status informations
        StateStatus m_state_status;
    };
}
#endif // DANTE_DPP_IHARDWARE_H

/*************************************************************************/