/*************************************************************************/
/*! 
 *  \file   RoisGroup.h
 *  \brief  class used to manage a group of ROI ranges
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROIS_GROUP_H_
#define DANTE_DPP_ROIS_GROUP_H_

//TANGO
#include <tango.h>

// LOCAL
#include "RoiRange.h"

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to manage a group of ROI ranges                      */
class RoisGroup
{
public:
    // complete constructor
    RoisGroup(std::size_t in_channel_index);

    // get the channel index
    std::size_t get_channel_index() const;

    // Get the number of rois
    std::size_t get_nb_rois() const;

    // Get access to the roi informations of a group
    yat::SharedPtr<const RoiRange> get_roi(std::size_t in_roi_index) const;

    // Add a ROI to a group
    void add_roi(yat::SharedPtr<RoiRange> in_roi_range);

    // Add a ROI to a group
    void add_roi(std::size_t in_channel_index,
                 std::size_t in_roi_index    ,
                 std::size_t in_bin_index    ,
                 std::size_t in_bin_size     );

private :
    std::size_t m_channel_index;
    RoiRangeMap m_rois         ;
};

/***********************************************************************
 * RoisGroup containers types
 ***********************************************************************/
/**
 * \typedef RoisGroupMap
 * \brief type of container which contains a map of ROIs groups
 */
typedef std::map<std::size_t, yat::SharedPtr<RoisGroup>> RoisGroupMap;

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ROIS_GROUP_H_

//###########################################################################
