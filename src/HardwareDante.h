/********************************************************************************/
/*! 
 *  \file   HardwareDante.h
 *  \brief  DANTE detectors class interface 
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/********************************************************************************/

#ifndef DANTE_DPP_HARDWARE_DANTE_H
#define DANTE_DPP_HARDWARE_DANTE_H

//TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat/file/FileName.h>

// PROJECT
#include "IHardware.h"
#include "HardwareDanteConfigParameters.h"
#include "HardwareDanteInitParameters.h"
#include "SmartList.h"
#include "StateThread.h"
#include "AsyncRequestGroup.h"

namespace DanteDpp_ns
{
    /***********************************************************************
     * \class HardwareDante
     * \brief class used to control a DANTE detector
     ***********************************************************************/

    class HardwareDante : public IHardware
    {
        // only the StateThread can call the request_state method
        friend class StateThread;

    public:
        // trigger mode values
        typedef enum 
        { 
            TRIGGER_INTERNAL_SINGLE = 0,
            TRIGGER_INTERNAL_MULTI     ,
            TRIGGER_EXTERNAL_GATE      ,
            TRIGGER_EXTERNAL_TRIGRISE  ,
            TRIGGER_EXTERNAL_TRIGFALL  ,
            TRIGGER_EXTERNAL_TRIGBOTH  ,
            TRIGGER_EXTERNAL_GATELOW   ,
        } TriggerMode;

        // preset type values
        typedef enum 
        { 
            PRESET_TYPE_FIXED_REAL = 0,
        } PresetType;

        //------------------------------------------------------------------
        // init/terminate management
        //------------------------------------------------------------------
        // init the detector
        virtual void init(void);

        // terminate the detector
        virtual void terminate(void);

        //------------------------------------------------------------------
        // connection management
        //------------------------------------------------------------------
        // connection to the detector
        virtual void connect(const std::string & in_complete_file_path) = 0;

        // disconnection from the detector
        virtual void disconnect(void) = 0;

        //------------------------------------------------------------------
        // system informations (from abstract class)
        //------------------------------------------------------------------
        // gets the number of modules (number of boards)
        virtual std::size_t get_modules_nb() const;

        // gets the number of channels
        virtual std::size_t get_channels_nb() const;

        // gets the number of bins
        virtual std::size_t get_bins_nb() const;

        // gets the number of bins to shift in the spectrum
        virtual std::size_t get_bins_shift() const;

        //------------------------------------------------------------------
        // system informations (specific to DANTE hardware)
        //------------------------------------------------------------------
        // gets the acquisition mode
        HardwareDanteConfigParameters::AcqMode get_acquisition_mode() const;

        // gets the detector type
        HardwareDanteInitParameters::Type get_detector_type() const;

        // gets the library version
        std::string get_library_version() const;

        // gets the firmwares versions of all boards
        std::vector<std::string> get_firmwares_versions() const;

        // gets the preset type
        HardwareDante::PresetType get_preset_type() const;

        // sets the preset type
        void set_preset_type(const HardwareDante::PresetType & in_preset_type);

        //------------------------------------------------------------------
        // callback management
        //------------------------------------------------------------------
        // callback used to receive the replies of the requests
        static void reply_callback(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, uint32_t * in_data);

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // gets the total number of pixels
        virtual uint32_t get_nb_pixels() const;

        // sets the total number of pixels
        virtual void set_nb_pixels(const uint32_t & in_nb_pixels);

        // gets the preset value
        virtual double get_preset_value() const;

        // sets the preset value
        virtual void set_preset_value(const double & in_preset_value);

        // gets the trigger mode
        HardwareDante::TriggerMode get_trigger_mode() const;

        // sets the trigger mode
        void set_trigger_mode(const HardwareDante::TriggerMode & in_trigger_mode);
		
		// get spectrum time 
        virtual uint32_t get_sp_time() const;

        // set spectrum time
        virtual void set_sp_time(const uint32_t & in_sp_time);

        /***************************************************************************************************
          * SINGLETON MANAGEMENT
          **************************************************************************************************/
        // Create the manager
        static void create(const HardwareDanteInitParameters & in_parameters);

    protected:
        //==================================================================
        // constructor
        explicit HardwareDante();

        // destructor (needs to be virtual)
        virtual ~HardwareDante();

        //------------------------------------------------------------------
        // system informations
        //------------------------------------------------------------------
        // logs the firmwares versions of all boards
        void log_firmwares_versions();

        // logs the the library version
        void log_library_version();

        //------------------------------------------------------------------
        // connection management
        //------------------------------------------------------------------
        // connection to the detector (generic start code to call in herited classes)
        void generic_start_code_for_connect(const std::string & in_complete_file_path);

        // connection to the detector (generic end code to call in herited classes)
        void generic_end_code_for_connect();

        // disconnection of the detector (generic start code to call in herited classes)
        void generic_start_code_for_disconnect();

        // disconnection of the detector (generic end code to call in herited classes)
        void generic_end_code_for_disconnect();

        //------------------------------------------------------------------
        // state management
        //------------------------------------------------------------------
        // Update the state (use by the StateThread)
        void update_state(void);

        //------------------------------------------------------------------
        // ASYNC REQUEST MANAGEMENT
        //------------------------------------------------------------------
        // add a request id and manage the errors
        void add_request_id(uint32_t                in_request_id  ,
                            std::vector<uint32_t> & out_request_ids, 
                            uint16_t                in_module_index,
                            uint16_t                in_modules_nb  ,
                            const std::string &     in_callerName  );

        // wait the replies of all requests
        bool wait_all_replies(const std::vector<uint32_t> & in_request_ids);

        // get the group of request for an access to all the replies
        yat::SharedPtr<AsyncRequestGroup> get_replies(const std::vector<uint32_t> & in_request_ids);

        //------------------------------------------------------------------
        // error management
        //------------------------------------------------------------------
        // manages the current error and throws an exception
        virtual void manage_last_error(const std::string & in_reason,
                                       const std::string & in_desc  ,
                                       const std::string & in_origin) = 0;

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // Check the configuration before the start of an acquisition
        virtual void check_configuration_before_acquisition() const;

        // gets the internal preset value
        double get_internal_preset_value() const;

    private:
        //------------------------------------------------------------------
        // configuration management
        //------------------------------------------------------------------
        // Load the configuration content from a file
        void load_configuration(const std::string & in_complete_file_path, HardwareDanteConfigParameters & out_config_parameters);

        // Select a new section in the configuration file
        void select_section(yat::CfgFile & in_cfg_file, const std::string in_section_name);

        // get a key value (of the current section) in the configuration file 
        template< typename T1>
        void get_key_value(yat::CfgFile & in_cfg_file, const std::string & in_key_name, T1 & out_value) const;

        // get a key value enum (of the current section) in the configuration file
        template< typename T1>
        void get_key_value_enum(yat::CfgFile                   & in_cfg_file, 
                                const std::string              & in_key_name,
                                T1                             & out_value  ,
                                const std::vector<std::string> & in_labels  ,
                                const std::vector<T1>          & in_values  ) const;

        //------------------------------------------------------------------
        // state management
        //------------------------------------------------------------------
        // creates the state thread
        void create_state_thread();

        // releases the state thread
        void release_state_thread();

        // requests the states of all boards and computes a general state
        virtual Tango::DevState request_state() = 0;

    protected:
        HardwareDanteInitParameters   m_init_parameters  ; // to keep the creation parameters
        HardwareDanteConfigParameters m_config_parameters; // to keep the configuration parameters
        std::size_t                   m_modules_nb       ; // number of boards
        bool                          m_is_connected     ; // used for connection/disconnection management

        std::vector<std::string>      m_firmwares_versions; // firmwares versions for each board
        std::string                   m_library_version   ; // sdk version
        PresetType                    m_preset_type       ; // preset type

        StateThread                 * m_state_thread      ; // thread used to compute the detector state

        uint32_t                      m_nb_pixels         ; // number of pixels to acquire
        double                        m_preset_value      ; // time of the acquisition (mca) or for a pixel (mapping) in seconds
        TriggerMode                   m_trigger_mode      ; // trigger mode
		uint32_t                      m_sp_time           ; // spectrum time

        //------------------------------------------------------------------
        // mutex stuff
        //------------------------------------------------------------------
        // used to protect the concurrent access to request_state method
        // mutable keyword is used to allow const methods even if they use this class member
        //mutable yat::Mutex m_request_state_lock;

    private:
        // configuration sections
        static const std::string g_connection_section_name ; // in the config file, contains the connection parameters
        static const std::string g_acquisition_section_name; // in the config file, contains the acquisition parameters
        static const std::string g_system_section_name     ; // in the config file, contains the system parameters

        // configuration keys
        static const std::string g_connection_key_master_board_ip     ;

        static const std::string g_acquisition_key_fast_filter_thr    ;
        static const std::string g_acquisition_key_energy_filter_thr  ;
        static const std::string g_acquisition_key_energy_baseline_thr;
        static const std::string g_acquisition_key_max_risetime       ;
        static const std::string g_acquisition_key_gain               ;
        static const std::string g_acquisition_key_peaking_time       ;
        static const std::string g_acquisition_key_max_peaking_time   ;
        static const std::string g_acquisition_key_flat_top           ;
        static const std::string g_acquisition_key_edge_peaking_time  ;
        static const std::string g_acquisition_key_edge_flat_top      ;
        static const std::string g_acquisition_key_reset_recovery_time;
        static const std::string g_acquisition_key_zero_peak_freq     ;
        static const std::string g_acquisition_key_baseline_samples   ;
        static const std::string g_acquisition_key_inverted_input     ;
        static const std::string g_acquisition_key_time_constant      ;
        static const std::string g_acquisition_key_base_offset        ;
        static const std::string g_acquisition_key_overflow_recovery  ;
        static const std::string g_acquisition_key_reset_threshold    ;
        static const std::string g_acquisition_key_tail_coefficient   ;
        static const std::string g_acquisition_key_other_param        ;

        // system keys
        static const std::string g_system_key_acquisition_mode    ;
        static const std::string g_system_key_nb_bins             ;
        static const std::string g_system_key_offset_val_digpot1  ;
        static const std::string g_system_key_offset_val_digpot2  ;
        static const std::string g_system_key_input_mode          ;
        static const std::string g_system_key_spectrum_shift_bins ;
        static const std::string g_system_key_spectrum_shift_value;

        // relations between value labels in the configuration file and enums used into the project 
        static const std::vector<std::string> g_acquisition_mode_labels;
        static const std::vector<HardwareDanteConfigParameters::AcqMode> g_acquisition_mode_values;

        static const std::vector<std::string> g_acquisition_boolean_labels;
        static const std::vector<bool> g_acquisition_boolean_values;

        static const std::vector<std::string> g_system_input_mode_labels;
        static const std::vector<HardwareDanteConfigParameters::InputMode> g_system_input_mode_values;
    };

    // include the implementation file of the HardwareDante class' template methods to separate interface and implementation
    #include "HardwareDante.hpp"
}
#endif // DANTE_DPP_HARDWARE_DANTE_H

/*************************************************************************/