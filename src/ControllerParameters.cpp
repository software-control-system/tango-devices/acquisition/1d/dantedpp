/*************************************************************************/
/*! 
 *  \file   ControllerParameters.cpp
 *  \brief  class used to manage the Controller parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "ControllerParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// ControllerParameters class
//============================================================================================================
/**************************************************************************
* \brief constructor
* \param[in] in_detector_type allows to use hardware or simulator
**************************************************************************/
ControllerParameters::ControllerParameters(const std::string & in_detector_type)
{
    m_detector_type = in_detector_type;
}

//###########################################################################
}