/*************************************************************************/
/*! 
 *  \file   CollectThread.h
 *  \brief  DANTE detector acquisition thread class interface
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_COLLECT_THREAD_H_
#define DANTE_DPP_COLLECT_THREAD_H_

// PROJECT
#include "SyncThread.h"

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class CollectThread
 * \brief DANTE detector acquisition thread class
 ***********************************************************************/
class CollectThread: public SyncThread
{
public:
    // Create the collect thread
    static void create();

    // Release the collect thread
    static void release();

    // return the singleton
    static CollectThread * get_instance();

    // return the singleton (const version)
    static const CollectThread * get_const_instance();

    // tell if the collect thread was created
    static bool exist();

protected:
    // constructor
    CollectThread(const std::string in_name);

    // the thread entry point - called by yat::Thread::start_undetached. 
    // SyncThread process in managed inside this method.
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg ioa);

    // destructor
    virtual ~CollectThread();

private:
    // thread used to collect the acquisition data
    static CollectThread * g_thread;
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_COLLECT_THREAD_H_
