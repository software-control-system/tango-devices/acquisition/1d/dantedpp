/*************************************************************************/
/*! 
 *  \file   RoisCommandParameters.h
 *  \brief  class used to setup a Rois command
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROIS_COMMAND_PARAMETERS_H_
#define DANTE_DPP_ROIS_COMMAND_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
#define ROIS_COMMAND_APPLY_ON_ALL_CHANNELS (-1)
    
/*************************************************************************/
/*\brief Class used to pass the needed data for a Rois command*/
class RoisCommandParameters
{
    friend class Controller;

    // rois commands
    typedef enum 
    { 
        LOAD_FILE, // load a new file and change the ROIs 
        LOAD_LIST, // create the ROIs with a ROIs list
        CHANGE   , // change the ROIs
        REMOVE   , // remove the ROIs of a channel or all (-1)
        UNSET    , // when the command was not set
    } RoisCommand;

public:
    // constructor
    RoisCommandParameters();

private :
    RoisCommand m_command           ;
    std::string m_rois_alias        ; // used during LOAD_FILE command
    std::string m_complete_file_path; // used during LOAD_FILE command
    std::string m_rois_string       ; // used during LOAD_FILE & CHANGE commands
    int         m_channels_index    ; // used during REMOVE command
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ROIS_COMMAND_PARAMETERS_H_

//###########################################################################
