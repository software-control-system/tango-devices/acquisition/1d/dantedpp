/*************************************************************************/
/*! 
 *  \file   CollectThread.cpp
 *  \brief  DANTE detector acquisition thread class implementation
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"
#include "CollectThread.h"
#include "HardwareDante.h"
#include "Controller.h"
#include "DataStore.h"

namespace DanteDpp_ns
{
#define COLLECT_THREAD_WAITING_TIME_MS (1)

// thread used to collect the acquisition data
CollectThread * CollectThread::g_thread = NULL; 

/*******************************************************************
 * \brief constructor
 * \param[in] in_name thread name (for logging)
 *******************************************************************/
CollectThread::CollectThread(const std::string in_name) : SyncThread(in_name)
{
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
CollectThread::~CollectThread()
{
}

/*******************************************************************
 * \brief The thread entry point called by yat::Thread::start_undetached.
 *        SyncThread process in managed inside this method.
 *******************************************************************/
yat::Thread::IOArg CollectThread::run_undetached(yat::Thread::IOArg /*ioa*/)
{
    try
    {
        try
        {
            // registering the thread as a new producer for the reception
            DataStore::get_instance()->add_reception_producer(m_name);

            SyncThread::start_treatment();

            /// collect loop
            while (SyncThread::can_go_on())
            {
                yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
                hardware->treat_acquisitions();

                if(DataStore::get_const_instance()->end_of_collect())
                {
                    SyncThread::treatment_completed();
                }
                else
                {
                    // waiting time between two update calls
                    ::usleep(COLLECT_THREAD_WAITING_TIME_MS * 1000);
                }
            }

            SyncThread::finish_treatment();

            // unregistering the thread for the reception
            DataStore::get_instance()->remove_reception_producer(m_name);
        }
        catch (Tango::DevFailed & in_ex)
        {
            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault(in_ex);
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
        catch (yat::Exception & in_ex)
        {
            in_ex.dump();

            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault(in_ex);
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
        catch(const DanteDpp_ns::Exception & in_ex)
        {
            // sometimes the acquisition stop command can be followed by errors from the sdk
            // It is not necessary to treat these errors in the TANGO status.
            if (SyncThread::can_go_on())
            {
                StateStatus & threads_status = Controller::get_instance()->get_threads_status();
                threads_status.on_fault(in_ex);
                std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
                throw;
            }
            else
            {
                SyncThread::treatment_completed();
                SyncThread::finish_treatment   ();

                // unregistering the thread for the reception
                DataStore::get_instance()->remove_reception_producer(m_name);
            }
        }
        catch(...)
        {
            StateStatus & threads_status = Controller::get_instance()->get_threads_status();
            threads_status.on_fault("Unknown exception!");
            std::cout << "Exception occured!" << threads_status.get_status() << std::endl;
            throw;
        }
    }
    catch(...)
    {
        std::cout << "Exception occured!" << Controller::get_instance()->get_threads_status().get_status() << std::endl;
        SyncThread::treatment_completed();
        SyncThread::finish_treatment   ();

        // unregistering the thread for the reception
        DataStore::get_instance()->remove_reception_producer(m_name);
    }

    return 0;
}

/*******************************************************************
 * \brief Create the collect thread
 *******************************************************************/
void CollectThread::create()
{
    // creating and starting the thread
    g_thread = new CollectThread("Collect thread");
    g_thread->start();
}

/*******************************************************************
 * \brief Release the collect thread
 *******************************************************************/
void CollectThread::release()
{
    // stops the thread and waits for its end
    if(g_thread != NULL)
    {
        g_thread->exit();
        g_thread = NULL;
    }
}

/*******************************************************************
 * \brief tell if the collect thread was created
 * \return true if the thread was created, else false
 *******************************************************************/
bool CollectThread::exist()
{
    return (g_thread != NULL);
}

/*******************************************************************
 * \brief return the singleton
*******************************************************************/
CollectThread * CollectThread::get_instance()
{
    return g_thread;
}

/*******************************************************************
 * \brief return the singleton (const version)
*******************************************************************/
const CollectThread * CollectThread::get_const_instance()
{
    return g_thread;
}

} /// namespace DanteDpp_ns
