/*************************************************************************/
/*! 
 *  \file   Error.h
 *  \brief  DANTE detector exception class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_ERROR_H_
#define DANTE_DPP_ERROR_H_

// SYSTEM
#include <string>
#include <vector>
#include <map>

// YAT
#include <yat/threading/Mutex.h> // g_errors_labels_mutex

/**********************************************************************/
namespace DanteDpp_ns
{
#define DANTE_DPP_ERROR_CODE_UNKNOWN               (-1)
#define DANTE_DPP_ERROR_CODE_ASYNC_REQUEST_PROBLEM (100)
#define DANTE_DPP_ERROR_CODE_ASYNC_REPLY_PROBLEM   (101)

/***********************************************************************
 * \class Error
 * \brief Error abstraction base class
 ***********************************************************************/
class Error
{
public:
    // error severities
    typedef enum { WARNING, ERROR, CRITICAL } EnumSeverity;

    typedef std::map<int, std::string> ErrorsMap;

public:
    // default constructor
    Error();

    // constructor
    Error(const std::string & in_reason,
          const std::string & in_desc  ,
          const std::string & in_origin,
          int                 in_code  = DANTE_DPP_ERROR_CODE_UNKNOWN,
          Error::EnumSeverity in_severity = Error::EnumSeverity::ERROR);

    // Copy constructor
    Error(const Error & in_src);

    // destructor
    virtual ~Error();

    // = operator
    Error & operator = (const Error & in_src);

    // convert the error to a complete description string
    std::string to_string() const;

    // convert the error code to a description string
    std::string get_description_of_error_code(const int in_error_code) const;

private :
    std::string m_reason; // reason
    std::string m_desc  ; // description
    std::string m_origin; // origin
    int         m_code  ; // code

    Error::EnumSeverity m_severity; // severity

    static std::vector<std::string> g_severity_labels; // severity labels

    static ErrorsMap g_errors_labels; // errors labels
    static const ErrorsMap::value_type g_errors_values[]; // list of all hardware errors

    // mutex used to protect the multithread access to g_errors_labels container.
    // The mutable keyword is used to allow using this mutex into const methods.
    static yat::Mutex g_errors_labels_mutex;
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_ERROR_H_

