/*************************************************************************************/
/*! 
 *  \file   HardwareDante.h
 *  \brief  DANTE detectors class implementation
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************************/
// PROJECT
#include "HardwareDante.h"
#include "HwDante.h"
#include "HwSimulator.h"
#include "AsyncManager.h"
#include "Log.h"

using namespace DanteDpp_ns;

// GLOBAL DATA
// in the config file, contains the connection parameters
const std::string HardwareDante::g_connection_section_name        = "CONNECTION"     ; 
const std::string HardwareDante::g_connection_key_master_board_ip = "master_board_ip";

// in the config file, contains the system parameters
const std::string HardwareDante::g_system_section_name = "SYSTEM";

const std::string HardwareDante::g_system_key_acquisition_mode     = "acquisition_mode"     ;
const std::string HardwareDante::g_system_key_nb_bins              = "nb_bins"              ;
const std::string HardwareDante::g_system_key_offset_val_digpot1   = "offset_val_digpot1"   ;
const std::string HardwareDante::g_system_key_offset_val_digpot2   = "offset_val_digpot2"   ;
const std::string HardwareDante::g_system_key_input_mode           = "input_mode"           ;
const std::string HardwareDante::g_system_key_spectrum_shift_bins  = "spectrum_shift_bins_" ;
const std::string HardwareDante::g_system_key_spectrum_shift_value = "spectrum_shift_value_";

const std::vector<std::string> 
    HardwareDante::g_system_input_mode_labels{"DC_HighImp", "DC_LowImp", "AC_Slow", "AC_Fast"};

const std::vector<HardwareDanteConfigParameters::InputMode> 
HardwareDante::g_system_input_mode_values{HardwareDanteConfigParameters::InputMode::DC_HighImp, 
                                          HardwareDanteConfigParameters::InputMode::DC_LowImp ,
                                          HardwareDanteConfigParameters::InputMode::AC_Slow   ,
                                          HardwareDanteConfigParameters::InputMode::AC_Fast   };

// in the config file, contains the acquisition parameters
const std::string HardwareDante::g_acquisition_section_name            = "ACQUISITION_"       ; 
const std::string HardwareDante::g_acquisition_key_fast_filter_thr     = "fast_filter_thr"    ;
const std::string HardwareDante::g_acquisition_key_energy_filter_thr   = "energy_filter_thr"  ;
const std::string HardwareDante::g_acquisition_key_energy_baseline_thr = "energy_baseline_thr";
const std::string HardwareDante::g_acquisition_key_max_risetime        = "max_risetime"       ;
const std::string HardwareDante::g_acquisition_key_gain                = "gain"               ;
const std::string HardwareDante::g_acquisition_key_peaking_time        = "peaking_time"       ;
const std::string HardwareDante::g_acquisition_key_max_peaking_time    = "max_peaking_time"   ;
const std::string HardwareDante::g_acquisition_key_flat_top            = "flat_top"           ;
const std::string HardwareDante::g_acquisition_key_edge_peaking_time   = "edge_peaking_time"  ;
const std::string HardwareDante::g_acquisition_key_edge_flat_top       = "edge_flat_top"      ;
const std::string HardwareDante::g_acquisition_key_reset_recovery_time = "reset_recovery_time";
const std::string HardwareDante::g_acquisition_key_zero_peak_freq      = "zero_peak_freq"     ;
const std::string HardwareDante::g_acquisition_key_baseline_samples    = "baseline_samples"   ;
const std::string HardwareDante::g_acquisition_key_inverted_input      = "inverted_input"     ;
const std::string HardwareDante::g_acquisition_key_time_constant       = "time_constant"      ;
const std::string HardwareDante::g_acquisition_key_base_offset         = "base_offset"        ;
const std::string HardwareDante::g_acquisition_key_overflow_recovery   = "overflow_recovery"  ;
const std::string HardwareDante::g_acquisition_key_reset_threshold     = "reset_threshold"    ;
const std::string HardwareDante::g_acquisition_key_tail_coefficient    = "tail_coefficient"   ;
const std::string HardwareDante::g_acquisition_key_other_param         = "other_param"        ;

const std::vector<std::string> HardwareDante::g_acquisition_mode_labels{"MCA", "MAPPING"};

const std::vector<HardwareDanteConfigParameters::AcqMode> 
    HardwareDante::g_acquisition_mode_values{HardwareDanteConfigParameters::AcqMode::MCA    ,
                                             HardwareDanteConfigParameters::AcqMode::MAPPING};

const std::vector<std::string> HardwareDante::g_acquisition_boolean_labels{"false", "true", "FALSE", "TRUE"};

const std::vector<bool> HardwareDante::g_acquisition_boolean_values{false, true, false, true};

/************************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management
 ************************************************************************/
HardwareDante::HardwareDante()
{
    INFO_STRM << "construction of HardwareDante" << std::endl;

    m_is_connected = false;
    m_state_thread = NULL ;
    m_preset_type  = PresetType::PRESET_TYPE_FIXED_REAL;
    m_nb_pixels    = 0  ;
    m_preset_value = 0.0;
    m_trigger_mode = TriggerMode::TRIGGER_INTERNAL_SINGLE;
	m_sp_time      = 100;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
HardwareDante::~HardwareDante()
{
    INFO_STRM << "destruction of HardwareDante" << std::endl;
}

//------------------------------------------------------------------
// init/terminate management
//------------------------------------------------------------------
/************************************************************************
 * \brief init the detector
 ************************************************************************/
void HardwareDante::init(void)
{
}

/************************************************************************
 * \brief terminate the detector
 ************************************************************************/
void HardwareDante::terminate(void)
{
    disconnect(); // virtual call
}

//------------------------------------------------------------------
// connection management
//------------------------------------------------------------------
/************************************************************************
 * \brief connection to the detector (generic start code to call in herited classes)
 ************************************************************************/
void HardwareDante::generic_start_code_for_connect(const std::string & in_complete_file_path)
{
    disconnect(); // virtual call

    // creating the async messages manager
    AsyncManager::create();

    load_configuration(in_complete_file_path, m_config_parameters);

    // manage the init of class members which are constants for MCA acquisition mode
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        set_trigger_mode(HardwareDante::TriggerMode::TRIGGER_INTERNAL_SINGLE);
        set_nb_pixels(1);
    }
}

/************************************************************************
 * \brief connection to the detector (generic end code to call in herited classes)
 ************************************************************************/
void HardwareDante::generic_end_code_for_connect()
{
    m_is_connected = true;

    // creating and starting the state thread
    create_state_thread();
}

/************************************************************************
 * \brief disconnection of the detector (generic start code to call in herited classes)
 ************************************************************************/
void HardwareDante::generic_start_code_for_disconnect()
{
    // releasing the state thread
    release_state_thread();

    // releasing the async messages manager
    AsyncManager::release();
}

/************************************************************************
 * \brief disconnection of the detector (generic end code to call in herited classes)
 ************************************************************************/
void HardwareDante::generic_end_code_for_disconnect()
{
    m_is_connected = false;
}

//------------------------------------------------------------------
// state thread management
//------------------------------------------------------------------
/*******************************************************************
 * \brief creates the state thread
 *******************************************************************/
void HardwareDante::create_state_thread()
{
    // creating and starting the state thread
    m_state_thread = new StateThread("State thread");
    m_state_thread->start();
}

/*******************************************************************
 * \brief releases the state thread
 *******************************************************************/
void HardwareDante::release_state_thread()
{
    // stops the thread and waits for its end
    if(m_state_thread != NULL)
    {
        m_state_thread->exit();
        m_state_thread = NULL;
    }
}

//------------------------------------------------------------------
// system informations
//------------------------------------------------------------------
/************************************************************************
 * \brief gets the number of modules (number of boards)
 * \return number of modules
 ************************************************************************/
std::size_t HardwareDante::get_modules_nb() const
{
    return m_modules_nb;
}

/************************************************************************
 * \brief gets the number of channels
 * \return number of channels
 ************************************************************************/
std::size_t HardwareDante::get_channels_nb() const
{
    return get_modules_nb(); // one channel per board for DANTE
}

/************************************************************************
 * \brief gets the number of bins
 * \return number of bins
 ************************************************************************/
std::size_t HardwareDante::get_bins_nb() const
{
    return static_cast<std::size_t>(m_config_parameters.m_nb_bins);
}

/************************************************************************
 * \brief gets the number of bins to shift in the spectrum
 * \return number of bins to shift in the spectrum
 ************************************************************************/
std::size_t HardwareDante::get_bins_shift() const
{
    std::size_t result = 0;

    // using find to search for the bins value  
    map<uint32_t, uint32_t>::const_iterator it = m_config_parameters.m_shift_by_spectrum_bins.find(m_config_parameters.m_nb_bins); 
      
    if(it != m_config_parameters.m_shift_by_spectrum_bins.end()) 
    {
        result = it->second;
    }

    return result;
}

/************************************************************************
 * \brief gets the acquisition mode
 * \return trigger mode
 ************************************************************************/
HardwareDanteConfigParameters::AcqMode HardwareDante::get_acquisition_mode() const
{
    return m_config_parameters.m_acquisition_mode;
}

/************************************************************************
 * \brief gets the detector type
 * \return detector type
 ************************************************************************/
HardwareDanteInitParameters::Type HardwareDante::get_detector_type() const
{
    return m_init_parameters.m_type;
}

/************************************************************************
 * \brief logs the firmwares versions of all boards
 ************************************************************************/
void HardwareDante::log_firmwares_versions()
{
    for(std::size_t modules_index = 0 ; modules_index < m_firmwares_versions.size() ; modules_index++)
    {
        INFO_STRM << "Firmware of board (" << modules_index << "):" << m_firmwares_versions[modules_index] << std::endl;
    }
}

/************************************************************************
 * \brief gets the firmwares versions of all boards
 * \return boards firmwares versions
 ************************************************************************/
std::vector<std::string> HardwareDante::get_firmwares_versions() const
{
    return m_firmwares_versions;
}

/************************************************************************
 * \brief gets the library version
 * \return library version
 ************************************************************************/
std::string HardwareDante::get_library_version() const
{
    return m_library_version;
}

/************************************************************************
 * \brief logs the the library version
 ************************************************************************/
void HardwareDante::log_library_version()
{
    INFO_STRM << "Library version: " << m_library_version << std::endl;
}

/************************************************************************
 * \brief gets the preset type
 * \return preset type
 ************************************************************************/
HardwareDante::PresetType HardwareDante::get_preset_type() const
{
    return m_preset_type;
}

/************************************************************************
 * \brief sets the preset type
 * \param[in] in_preset_type new value
 ************************************************************************/
void HardwareDante::set_preset_type(const HardwareDante::PresetType & in_preset_type)
{
    m_preset_type = in_preset_type;
}

//------------------------------------------------------------------
// configuration management
//------------------------------------------------------------------
/*******************************************************************
 * \brief Load the configuration content from a file
 * \param[in] in_complete_file_path configuration file
 *******************************************************************/
void HardwareDante::load_configuration(const std::string & in_complete_file_path, HardwareDanteConfigParameters & out_config_parameters)
{
    yat::CfgFile cfg_file;

    INFO_STRM << "loading config file: " << in_complete_file_path << std::endl;

    cfg_file.load(in_complete_file_path);

    // loading connection parameters
    select_section(cfg_file, HardwareDante::g_connection_section_name);
    get_key_value(cfg_file, g_connection_key_master_board_ip, out_config_parameters.m_master_board_ip);

    // loading system parameters
    select_section(cfg_file, HardwareDante::g_system_section_name);

    get_key_value_enum(cfg_file, 
                       g_system_key_acquisition_mode,
                       out_config_parameters.m_acquisition_mode,
                       g_acquisition_mode_labels, 
                       g_acquisition_mode_values);

    get_key_value(cfg_file, g_system_key_nb_bins           , out_config_parameters.m_nb_bins           );
    get_key_value(cfg_file, g_system_key_offset_val_digpot1, out_config_parameters.m_offset_val_digpot1);
    get_key_value(cfg_file, g_system_key_offset_val_digpot2, out_config_parameters.m_offset_val_digpot2);

    get_key_value_enum(cfg_file, 
                       g_system_key_input_mode,
                       out_config_parameters.m_input_mode,
                       g_system_input_mode_labels, 
                       g_system_input_mode_values);

    // read the shifts for each spectrum bins
    uint32_t shift_index = 0;

    for(;;)
    {
        std::stringstream temp_stream_bins ;
        std::stringstream temp_stream_value;

        temp_stream_bins  << HardwareDante::g_system_key_spectrum_shift_bins  << shift_index;
        temp_stream_value << HardwareDante::g_system_key_spectrum_shift_value << shift_index;

        // check if the keys exist in the configuration file
        if(cfg_file.has_parameter(temp_stream_bins.str()) && cfg_file.has_parameter(temp_stream_value.str()))
        {
            uint32_t bins ;
            uint32_t value;

            get_key_value(cfg_file, temp_stream_bins.str (), bins );
            get_key_value(cfg_file, temp_stream_value.str(), value);

            // add the spectrum shift to the container
            out_config_parameters.m_shift_by_spectrum_bins.insert(pair<uint32_t, uint32_t>(bins, value));

            shift_index++;
        }
        else
        // no more data to read
        {
            break;
        }
    }

    // loading acquisition parameters of master board
    // first, copy the default values
    HardwareDanteConfigParameters::AcqParameters acq_parameters = out_config_parameters.m_default_acq_parameters;
    
    // set the master board index
    acq_parameters.m_board_index = 0;

    // build the section name to use
    std::string acquisition_section_name;

    {
        std::stringstream temp_stream;
        temp_stream << HardwareDante::g_acquisition_section_name << acq_parameters.m_board_index;
        acquisition_section_name = temp_stream.str();
    }

    // select the section
    select_section(cfg_file, acquisition_section_name);

    // read the values
    get_key_value_enum(cfg_file, 
                       g_acquisition_key_inverted_input,
                       acq_parameters.m_inverted_input,
                       g_acquisition_boolean_labels, 
                       g_acquisition_boolean_values);

    get_key_value(cfg_file, g_acquisition_key_fast_filter_thr    , acq_parameters.m_fast_filter_thr    );
    get_key_value(cfg_file, g_acquisition_key_energy_filter_thr  , acq_parameters.m_energy_filter_thr  );
    get_key_value(cfg_file, g_acquisition_key_energy_baseline_thr, acq_parameters.m_energy_baseline_thr);
    get_key_value(cfg_file, g_acquisition_key_max_risetime       , acq_parameters.m_max_risetime       );
    get_key_value(cfg_file, g_acquisition_key_gain               , acq_parameters.m_gain               );
    get_key_value(cfg_file, g_acquisition_key_peaking_time       , acq_parameters.m_peaking_time       );
    get_key_value(cfg_file, g_acquisition_key_max_peaking_time   , acq_parameters.m_max_peaking_time   );
    get_key_value(cfg_file, g_acquisition_key_flat_top           , acq_parameters.m_flat_top           );
    get_key_value(cfg_file, g_acquisition_key_edge_peaking_time  , acq_parameters.m_edge_peaking_time  );
    get_key_value(cfg_file, g_acquisition_key_edge_flat_top      , acq_parameters.m_edge_flat_top      );
    get_key_value(cfg_file, g_acquisition_key_reset_recovery_time, acq_parameters.m_reset_recovery_time);
    get_key_value(cfg_file, g_acquisition_key_zero_peak_freq     , acq_parameters.m_zero_peak_freq     );
    get_key_value(cfg_file, g_acquisition_key_baseline_samples   , acq_parameters.m_baseline_samples   );

    get_key_value(cfg_file, g_acquisition_key_time_constant      , acq_parameters.m_time_constant      );
    get_key_value(cfg_file, g_acquisition_key_base_offset        , acq_parameters.m_base_offset        );
    get_key_value(cfg_file, g_acquisition_key_overflow_recovery  , acq_parameters.m_overflow_recovery  );
    get_key_value(cfg_file, g_acquisition_key_reset_threshold    , acq_parameters.m_reset_threshold    );
    get_key_value(cfg_file, g_acquisition_key_tail_coefficient   , acq_parameters.m_tail_coefficient   );
    get_key_value(cfg_file, g_acquisition_key_other_param        , acq_parameters.m_other_param        );

    // add the master board parameters into the container
    out_config_parameters.m_acq_parameters.push_back(acq_parameters);

    // get all the sections to check if there are specifics parameters for other boards
    // before the detector connection, we don't know the number of boards.
    std::list<std::string> all_sections;
    cfg_file.get_sections(&all_sections);

    std::list<std::string>::const_iterator it;

    for (it = all_sections.begin(); it != all_sections.end(); ++it) 
    {
        // check if the section name starts with the acquisition section name
        std::size_t pos = (*it).rfind(HardwareDante::g_acquisition_section_name); 

        if(pos == 0)
        {
            std::string temp = *it;
            temp.erase(pos, HardwareDante::g_acquisition_section_name.length());

            std::size_t board_index;

            try
            {
                std::istringstream ss(temp);
                ss >> board_index;

                if (ss.fail() || (ss.rdbuf()->in_avail() > 0))
                {
                    ss.clear();
                    throw DanteDpp_ns::Exception();
                }
                else
                {
                    // master board was already treated, we jump to the next section
                    if(board_index == 0)
                    {
                        continue;
                    }
                    else
                    {
                        INFO_STRM << "found specific acquisition section for board: " << board_index << std::endl;
                    }
                }
            }
            catch(...)
            {
                std::ostringstream error_text;
                error_text << "Incorrect board index: " << temp << " for section: " << *it;
                throw DanteDpp_ns::Exception("LOAD_CONFIG_ERROR", error_text.str(), "HardwareDante::load_configuration");
            }

            // copy the default values of the master board
            acq_parameters = out_config_parameters.m_acq_parameters[0];

            // set the master board index
            acq_parameters.m_board_index = board_index;

            // select the section
            select_section(cfg_file, *it);

            // read the values
            get_key_value_enum(cfg_file, 
                               g_acquisition_key_inverted_input,
                               acq_parameters.m_inverted_input,
                               g_acquisition_boolean_labels, 
                               g_acquisition_boolean_values);

            get_key_value(cfg_file, g_acquisition_key_fast_filter_thr    , acq_parameters.m_fast_filter_thr    );
            get_key_value(cfg_file, g_acquisition_key_energy_filter_thr  , acq_parameters.m_energy_filter_thr  );
            get_key_value(cfg_file, g_acquisition_key_energy_baseline_thr, acq_parameters.m_energy_baseline_thr);
            get_key_value(cfg_file, g_acquisition_key_max_risetime       , acq_parameters.m_max_risetime       );
            get_key_value(cfg_file, g_acquisition_key_gain               , acq_parameters.m_gain               );
            get_key_value(cfg_file, g_acquisition_key_peaking_time       , acq_parameters.m_peaking_time       );
            get_key_value(cfg_file, g_acquisition_key_max_peaking_time   , acq_parameters.m_max_peaking_time   );
            get_key_value(cfg_file, g_acquisition_key_flat_top           , acq_parameters.m_flat_top           );
            get_key_value(cfg_file, g_acquisition_key_edge_peaking_time  , acq_parameters.m_edge_peaking_time  );
            get_key_value(cfg_file, g_acquisition_key_edge_flat_top      , acq_parameters.m_edge_flat_top      );
            get_key_value(cfg_file, g_acquisition_key_reset_recovery_time, acq_parameters.m_reset_recovery_time);
            get_key_value(cfg_file, g_acquisition_key_zero_peak_freq     , acq_parameters.m_zero_peak_freq     );
            get_key_value(cfg_file, g_acquisition_key_baseline_samples   , acq_parameters.m_baseline_samples   );

            get_key_value(cfg_file, g_acquisition_key_time_constant      , acq_parameters.m_time_constant      );
            get_key_value(cfg_file, g_acquisition_key_base_offset        , acq_parameters.m_base_offset        );
            get_key_value(cfg_file, g_acquisition_key_overflow_recovery  , acq_parameters.m_overflow_recovery  );
            get_key_value(cfg_file, g_acquisition_key_reset_threshold    , acq_parameters.m_reset_threshold    );
            get_key_value(cfg_file, g_acquisition_key_tail_coefficient   , acq_parameters.m_tail_coefficient   );
            get_key_value(cfg_file, g_acquisition_key_other_param        , acq_parameters.m_other_param        );

            // add the master board parameters into the container
            out_config_parameters.m_acq_parameters.push_back(acq_parameters);
        }
    }
}

/*******************************************************************
 * \brief Select a new section in the configuration file
 * \param[in] in_cfg_file configuration file in memory
 * \param[in] in_section_name name of the new section
 *******************************************************************/
void HardwareDante::select_section(yat::CfgFile & in_cfg_file, const std::string in_section_name)
{
    DEBUG_STRM << "changing read section: " << in_section_name << std::endl;

    // select the new section
    if(!in_cfg_file.set_section(in_section_name, false))
    {
        std::string error_text = "Could not find the section " + in_section_name + " in the configuration file!";
        throw DanteDpp_ns::Exception("LOAD_CONFIG_ERROR", error_text, "HardwareDante::select_section");
    }
}

//------------------------------------------------------------------
// state management
//------------------------------------------------------------------
/*******************************************************************
 * \brief Update the state (use by the StateThread)
*******************************************************************/
void HardwareDante::update_state(void)
{
    try
    {
        Tango::DevState new_state = request_state();

        //- AutoLock the following
        {
            yat::MutexLock scoped_lock(m_state_status.get_mutex());

            if(m_state_status.get_state() != Tango::FAULT)
            {
                if(new_state == Tango::STANDBY)
                {
                    m_state_status.set(Tango::STANDBY, "detector is waiting for request...");
                }
                else
                if(new_state == Tango::RUNNING)
                {
                    m_state_status.set(Tango::RUNNING, "detector is running an acquisition...");
                }
            }
        }
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        m_state_status.on_fault(in_ex);
    }
}

//------------------------------------------------------------------
// CALLBACK MANAGEMENT
//------------------------------------------------------------------
/*******************************************************************
 * \brief callback used to receive the replies of the requests
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 *******************************************************************/
void HardwareDante::reply_callback(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, uint32_t * in_data)
{
    AsyncManager::get_instance()->manage_reply(in_type, in_call_id, in_length, in_data);
}

//------------------------------------------------------------------
// ASYNC REQUEST MANAGEMENT
//------------------------------------------------------------------
/************************************************************************
 * \brief add a request id and manage the errors
 * \param[in]  in_request_id request id returned by the sdk function call
 * \param[out] out_request_ids container of request ids
 * \param[in]  in_module_index index of the board (target of the request)
 * \param[in]  in_modules_nb maximum number of boards
 * \param[in]  in_callerName Class and method name of the caller (for exception management)
 ************************************************************************/
void HardwareDante::add_request_id(uint32_t                in_request_id  ,
                                   std::vector<uint32_t> & out_request_ids, 
                                   uint16_t                in_module_index,
                                   uint16_t                in_modules_nb  ,
                                   const std::string &     in_callerName  )
{
    if(in_request_id == DANTE_DPP_ASYNC_REQUEST_BAD_IDENTIFIER)
    {
        // if there is at least one request which was created, we need to cleanup the request group
        if(in_module_index > 0)
        {
            AsyncManager::get_instance()->remove_request_group(out_request_ids[0]);
        }
        
        std::stringstream temp_stream;
        temp_stream << "Error during the call of a sdk function for board (" << in_module_index << ")";
        manage_last_error("LIBRARY_ERROR", temp_stream.str(), in_callerName);
    }

    out_request_ids.push_back(in_request_id);
    AsyncManager::get_instance()->add_request(out_request_ids[in_module_index], out_request_ids[0], in_modules_nb);
}

/************************************************************************
 * \brief wait the replies of all requests
 * \param[out] in_request_ids container of request ids
 * \return true if all replies were received, else false
 ************************************************************************/
bool HardwareDante::wait_all_replies(const std::vector<uint32_t> & in_request_ids)
{
#ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
    uint32_t group = in_request_ids[0];
    INFO_STRM << "waiting all the replies for group " << group << "..." << std::endl;
#endif
    bool result = AsyncManager::get_instance()->waiting_all_replies_of_group(in_request_ids[0]);

    // in case of error, we remove the async group data
    if(!result)
    {
        AsyncManager::get_instance()->remove_request_group(in_request_ids[0]);
    }

#ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
    if(result)
    {
        INFO_STRM << "ok, all the replies received for group " << group << "..." << std::endl;
    }
    else
    {
        INFO_STRM << "All the replies were not received for group " << group << "!..." << std::endl;
    }
#endif
    return result;
}

/************************************************************************
 * \brief get the group of request for an access to all the replies
 * \param[out] in_request_ids container of request ids
 * \return request group with all the replies
 ************************************************************************/
yat::SharedPtr<AsyncRequestGroup> HardwareDante::get_replies(const std::vector<uint32_t> & in_request_ids)
{
    return AsyncManager::get_instance()->remove_request_group(in_request_ids[0]);
}

/***************************************************************************************************
  * ACQUISITION MANAGEMENT
  **************************************************************************************************/
/***************************************************************************
 * \brief Check the configuration before the start of an acquisition
 ***************************************************************************/
void HardwareDante::check_configuration_before_acquisition() const
{
    // Only snap is allowed
    std::size_t module_data_nb = static_cast<std::size_t>(get_nb_pixels());

    if(module_data_nb == 0LL)
    {
        // throwing exception
        std::ostringstream msg_err;
        msg_err << "Start mode is not allowed for this device! Please use Snap mode." << std::endl;

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                       msg_err.str().c_str(),
                                       "HardwareDante::check_configuration_before_acquisition");
    }

    // check non zero preset value for all triggers except EXTERNAL_GATE
	// because in EXTERNAL_GATE preset value is always 0, so no need to check this case, otherwise an exception is occured !
    if((get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA) || 
       ((get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MAPPING) &&
        (get_trigger_mode() == TriggerMode::TRIGGER_INTERNAL_MULTI)))
    {
        if(get_preset_value() == 0.0)
        {
            // throwing exception
            std::ostringstream msg_err;
            msg_err << "A preset value of 0.0 is not allowed for this acquisition!" << std::endl;

            Tango::Except::throw_exception("TANGO_DEVICE_ERROR",
                                           msg_err.str().c_str(),
                                           "HardwareDante::check_configuration_before_acquisition");
        }
    }
}

/************************************************************************
 * \brief gets the total number of pixels
 * \return number of pixels
 ************************************************************************/
uint32_t HardwareDante::get_nb_pixels() const
{
    return m_nb_pixels;
}

/************************************************************************
 * \brief sets the total number of pixels
 * \param[in] in_nb_pixels new value
 ************************************************************************/
void HardwareDante::set_nb_pixels(const uint32_t & in_nb_pixels)
{
    // MCA mode
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        if(in_nb_pixels != 1)
        {
            std::stringstream ss;
		    ss << "In MCA acquisition mode, the only accepted pixels nb value is 1!" << endl;
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (ss.str()).c_str(), "HardwareDante::set_nb_pixels");
        }
    }
    
    m_nb_pixels = in_nb_pixels;
}

/************************************************************************
 * \brief gets spectrum time
 * \return spectrum time
 ************************************************************************/
uint32_t HardwareDante::get_sp_time() const
{
    return m_sp_time;
}

/************************************************************************
 * \brief sets spectrum time
 * \param[in] in_sp_time new value
 ************************************************************************/
void HardwareDante::set_sp_time(const uint32_t & in_sp_time)
{    
    m_sp_time = in_sp_time;
}

/************************************************************************
 * \brief gets the preset value
 * \return the preset value
 ************************************************************************/
double HardwareDante::get_preset_value() const
{
    double result = get_internal_preset_value();

    // in MAPPING mode with a gate trigger, the preset value is forced to 0.0
    // because it will be useless to the acquisition.
    if((get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MAPPING) &&
       ((get_trigger_mode() == HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATE) ||
       (get_trigger_mode() == HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGRISE) ||
       (get_trigger_mode() == HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGFALL) ||
       (get_trigger_mode() == HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGBOTH) || 
       (get_trigger_mode() == HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATELOW))
      )
    {
        return 0.0;
    }

    return result;
}

/************************************************************************
 * \brief gets the internal preset value
 * \return the internal preset value
 ************************************************************************/
double HardwareDante::get_internal_preset_value() const
{
    return m_preset_value;
}

/************************************************************************
 * \brief sets the preset value
 * \param[in] in_preset_value new value
 ************************************************************************/
void HardwareDante::set_preset_value(const double & in_preset_value)
{
    m_preset_value = in_preset_value;
}

/************************************************************************
 * \brief gets the trigger mode
 * \return trigger mode
 ************************************************************************/
HardwareDante::TriggerMode HardwareDante::get_trigger_mode() const
{
    return m_trigger_mode;
}

/************************************************************************
 * \brief sets the trigger mode
 * \param[in] in_trigger_mode new value
 ************************************************************************/
void HardwareDante::set_trigger_mode(const HardwareDante::TriggerMode & in_trigger_mode)
{
    // MCA mode
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        if(in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_INTERNAL_SINGLE)
        {
            std::stringstream ss;
		    ss << "In MCA acquisition mode, the only accepted trigger mode is INTERNAL_SINGLE!" << endl;
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (ss.str()).c_str(), "HardwareDante::set_trigger_mode");
        }
    }
    else// MAPPING mode
    {
        if((in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_INTERNAL_MULTI) && 
           (in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATE ) &&
           (in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGRISE) &&
           (in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGFALL) &&
           (in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGBOTH) && 
           (in_trigger_mode != HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATELOW))
        {
            std::stringstream ss;
		    ss << "In MAPPING acquisition mode, the only accepted trigger modes are: INTERNAL_MULTI, EXTERNAL_GATE, EXTERNAL_TRIGRISE, EXTERNAL_TRIGFALL, EXTERNAL_TRIGBOTH, EXTERNAL_GATELOW !" << endl;
		    Tango::Except::throw_exception("TANGO_DEVICE_ERROR", (ss.str()).c_str(), "HardwareDante::set_trigger_mode");
        }
    }

    m_trigger_mode = in_trigger_mode;
}

/***************************************************************************************************
  * SINGLETON MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Create the manager
 * \param[in] in_parameters parameters to set in the instance
 *******************************************************************/
void HardwareDante::create(const HardwareDanteInitParameters & in_parameters)
{
    // create the detector : can be hardware or a simulator
    if(in_parameters.m_type == HardwareDanteInitParameters::Type::Dante)
    {
        IHardware::g_singleton.reset(new HwDante());
    }
    else
    if(in_parameters.m_type == HardwareDanteInitParameters::Type::Simulator)
    {
        IHardware::g_singleton.reset(new HwSimulator());
    }

    // to keep the creation parameters
    yat::SharedPtr<HardwareDante> ptr = IHardware::g_singleton;
    ptr->m_init_parameters = in_parameters;
}

//========================================================================================
