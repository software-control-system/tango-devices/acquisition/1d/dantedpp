/*----- PROTECTED REGION ID(DanteDpp.h) ENABLED START -----*/
//=============================================================================
//
// file :        DanteDpp.h
//
// description : Include file for the DanteDpp class
//
// project :     DanteDpp detector TANGO device.
//
// This file is part of Tango device class.
// 
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
// 
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// $HeadURL:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef DanteDpp_H
#define DanteDpp_H

//TANGO
#include <tango.h>
#include <PogoHelper.h>

// YAT/YAT4TANGO
#include <yat4tango/DynamicInterfaceManager.h>
#include <yat4tango/PropertyHelper.h>
#include <yat/memory/SharedPtr.h>

// LOCAL
#include "Controller.h"

/*----- PROTECTED REGION END -----*/	//	DanteDpp.h

/**
 *  DanteDpp class description:
 *    Device for DANTE detectors from XGLab company.
 */

namespace DanteDpp_ns
{
/*----- PROTECTED REGION ID(DanteDpp::Additional Class Declarations) ENABLED START -----*/

//	Additional Class Declarations

/*----- PROTECTED REGION END -----*/	//	DanteDpp::Additional Class Declarations

class DanteDpp : public Tango::Device_4Impl
{

/*----- PROTECTED REGION ID(DanteDpp::Data Members) ENABLED START -----*/

//	Add your own data members
    friend class ConfigView;
    friend class RoisView;

/*----- PROTECTED REGION END -----*/	//	DanteDpp::Data Members

//	Device property data members
public:
	//	MemorizedStreamType:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedStreamType;
	//	MemorizedStreamTargetPath:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedStreamTargetPath;
	//	MemorizedStreamTargetFile:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedStreamTargetFile;
	//	MemorizedStreamNbAcqPerFile:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	Tango::DevULong	memorizedStreamNbAcqPerFile;
	//	ExpertStreamWriteMode:	Only an expert User could change this property<br>
	//  Applicable for StreamNexus Only !<BR>
	//  Available Values :<BR>
	//  - ASYNCHRONOUS<BR>
	//  - SYNCHRONOUS<BR>
	//  - DELAYED<BR>
	string	expertStreamWriteMode;
	//	StreamItems:	Define the list of Items managed by the Streamer. (Nexus, CSV, ...)<BR>
	//  Availables values are :<BR>
	//  Frame<BR>
	vector<string>	streamItems;
	//	SpoolID:	Used only by the FlyScan application
	string	spoolID;
	//	ConfigurationFiles:	Define the list of Configuration ``*.INI`` files and their associated alias.
	vector<string>	configurationFiles;
	//	RoisFiles:	Define the list of Rois files ``*.txt``  and their associated alias.
	vector<string>	roisFiles;
	//	MemorizedConfigAlias:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedConfigAlias;
	//	MemorizedRoisAlias:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedRoisAlias;
	//	MemorizedFileGeneration:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	Tango::DevBoolean	memorizedFileGeneration;
	//	BoardType:	Define the board type :<br>
	//  DANTE<br>
	//  DANTE_SIMULATOR<br>
	string	boardType;
	//	AutoLoad:	Allow to Reload the last used configuration file (*.ini) at each init of the device.
	Tango::DevBoolean	autoLoad;
	//	MemorizedPresetValue:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	Tango::DevDouble	memorizedPresetValue;
	//	MemorizedNbPixels:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	Tango::DevULong	memorizedNbPixels;
	//	MemorizedRoisList:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedRoisList;
	//	MemorizedPresetType:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedPresetType;
	//	MemorizedTriggerMode:	Only the device could modify this property <br>
	//  The User should never change this property<br>
	string	memorizedTriggerMode;
	//	MemorizedSpTime:	Only the device could modify this property <br>
	//  The User should never change this property <br>
	Tango::DevULong	memorizedSpTime;

//	Attribute data members
public:
	Tango::DevString	*attr_currentAlias_read;
	Tango::DevString	*attr_currentConfigFile_read;
	Tango::DevString	*attr_currentRoisAlias_read;
	Tango::DevString	*attr_currentRoisFile_read;
	Tango::DevString	*attr_boardType_read;
	Tango::DevString	*attr_currentMode_read;
	Tango::DevLong	*attr_nbModules_read;
	Tango::DevLong	*attr_nbChannels_read;
	Tango::DevLong	*attr_nbBins_read;
	Tango::DevString	*attr_streamType_read;
	Tango::DevString	*attr_streamTargetPath_read;
	Tango::DevString	*attr_streamTargetFile_read;
	Tango::DevULong	*attr_streamNbAcqPerFile_read;
	Tango::DevBoolean	*attr_fileGeneration_read;
	Tango::DevBoolean	*attr_enableDatasetrealtime_read;
	Tango::DevBoolean	*attr_enableDatasetlivetime_read;
	Tango::DevBoolean	*attr_enableDatasetdeadtime_read;
	Tango::DevBoolean	*attr_enableDatasetinputcountrate_read;
	Tango::DevBoolean	*attr_enableDatasetoutputcountrate_read;
	Tango::DevBoolean	*attr_enableDataseteventsinrun_read;
	Tango::DevBoolean	*attr_enableDatasetspectrum_read;
	Tango::DevBoolean	*attr_enableDatasetroi_read;

//	Constructors and destructors
public:
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	DanteDpp(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	DanteDpp(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly device object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	DanteDpp(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The device object destructor.
	 */	
	~DanteDpp() {delete_device();};


//	Miscellaneous methods
public:
	/*
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/*
	 *	Initialize the device
	 */
	virtual void init_device();
	/*
	 *	Read the device properties from database
	 */
	void get_device_property();
	/*
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	//--------------------------------------------------------
	/*
	 *	Method      : DanteDpp::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	//--------------------------------------------------------
	virtual void read_attr_hardware(vector<long> &attr_list);

/**
 *	Attribute currentAlias related methods
 *	Description: Display the current Alias used to load the *.ini file
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_currentAlias(Tango::Attribute &attr);
	virtual bool is_currentAlias_allowed(Tango::AttReqType type);
/**
 *	Attribute currentConfigFile related methods
 *	Description: Display the path+name of current loaded *.ini file.
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_currentConfigFile(Tango::Attribute &attr);
	virtual bool is_currentConfigFile_allowed(Tango::AttReqType type);
/**
 *	Attribute currentRoisAlias related methods
 *	Description: Display the current Rois Alias used to load the Rois file
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_currentRoisAlias(Tango::Attribute &attr);
	virtual bool is_currentRoisAlias_allowed(Tango::AttReqType type);
/**
 *	Attribute currentRoisFile related methods
 *	Description: Display the path+name of current loaded Rois file.
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_currentRoisFile(Tango::Attribute &attr);
	virtual bool is_currentRoisFile_allowed(Tango::AttReqType type);
/**
 *	Attribute boardType related methods
 *	Description: Get board type<br>
 *               <br>
 *               Available board types are :
 *               DANTE<br>
 *               DANTE_SIMULATOR<br>
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_boardType(Tango::Attribute &attr);
	virtual bool is_boardType_allowed(Tango::AttReqType type);
/**
 *	Attribute currentMode related methods
 *	Description: Display the current Mode (MCA or MAPPING).
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_currentMode(Tango::Attribute &attr);
	virtual bool is_currentMode_allowed(Tango::AttReqType type);
/**
 *	Attribute nbModules related methods
 *	Description: Number of active boards
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_nbModules(Tango::Attribute &attr);
	virtual bool is_nbModules_allowed(Tango::AttReqType type);
/**
 *	Attribute nbChannels related methods
 *	Description: Number of channels of active boards
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_nbChannels(Tango::Attribute &attr);
	virtual bool is_nbChannels_allowed(Tango::AttReqType type);
/**
 *	Attribute nbBins related methods
 *	Description: Number of Bins for each inputs
 *
 *	Data type:	Tango::DevLong
 *	Attr type:	Scalar
 */
	virtual void read_nbBins(Tango::Attribute &attr);
	virtual bool is_nbBins_allowed(Tango::AttReqType type);
/**
 *	Attribute streamType related methods
 *	Description: Avalaible stream types :<br>
 *               NO_STREAM<br>
 *               LOG_STREAM	<br>
 *               CSV_STREAM	<br>
 *               NEXUS_STREAM<br>
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_streamType(Tango::Attribute &attr);
	virtual void write_streamType(Tango::WAttribute &attr);
	virtual bool is_streamType_allowed(Tango::AttReqType type);
/**
 *	Attribute streamTargetPath related methods
 *	Description: Defines the root path for generated Stream files.
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_streamTargetPath(Tango::Attribute &attr);
	virtual void write_streamTargetPath(Tango::WAttribute &attr);
	virtual bool is_streamTargetPath_allowed(Tango::AttReqType type);
/**
 *	Attribute streamTargetFile related methods
 *	Description: Define the file name for generated Stream files.
 *
 *	Data type:	Tango::DevString
 *	Attr type:	Scalar
 */
	virtual void read_streamTargetFile(Tango::Attribute &attr);
	virtual void write_streamTargetFile(Tango::WAttribute &attr);
	virtual bool is_streamTargetFile_allowed(Tango::AttReqType type);
/**
 *	Attribute streamNbAcqPerFile related methods
 *	Description: Define the number of acquisition for each Stream file.
 *
 *	Data type:	Tango::DevULong
 *	Attr type:	Scalar
 */
	virtual void read_streamNbAcqPerFile(Tango::Attribute &attr);
	virtual void write_streamNbAcqPerFile(Tango::WAttribute &attr);
	virtual bool is_streamNbAcqPerFile_allowed(Tango::AttReqType type);
/**
 *	Attribute fileGeneration related methods
 *	Description: File generation after a snap command
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_fileGeneration(Tango::Attribute &attr);
	virtual void write_fileGeneration(Tango::WAttribute &attr);
	virtual bool is_fileGeneration_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetrealtime related methods
 *	Description: Allows to activate or inactivate the Real Time saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetrealtime(Tango::Attribute &attr);
	virtual bool is_enableDatasetrealtime_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetlivetime related methods
 *	Description: Allows to activate or inactivate the Live Time saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetlivetime(Tango::Attribute &attr);
	virtual bool is_enableDatasetlivetime_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetdeadtime related methods
 *	Description: Allows to activate or inactivate the Dead Time saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetdeadtime(Tango::Attribute &attr);
	virtual bool is_enableDatasetdeadtime_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetinputcountrate related methods
 *	Description: Allows to activate or inactivate the Input Count Rate saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetinputcountrate(Tango::Attribute &attr);
	virtual bool is_enableDatasetinputcountrate_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetoutputcountrate related methods
 *	Description: Allows to activate or inactivate the Output Count Rate saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetoutputcountrate(Tango::Attribute &attr);
	virtual bool is_enableDatasetoutputcountrate_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDataseteventsinrun related methods
 *	Description: Allows to activate or inactivate the Events In Run saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDataseteventsinrun(Tango::Attribute &attr);
	virtual bool is_enableDataseteventsinrun_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetspectrum related methods
 *	Description: Allows to activate or inactivate the Spectrum saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetspectrum(Tango::Attribute &attr);
	virtual bool is_enableDatasetspectrum_allowed(Tango::AttReqType type);
/**
 *	Attribute enableDatasetroi related methods
 *	Description: Allows to activate or inactivate the Roi saving in the nexus files.<br>
 *
 *	Data type:	Tango::DevBoolean
 *	Attr type:	Scalar
 */
	virtual void read_enableDatasetroi(Tango::Attribute &attr);
	virtual bool is_enableDatasetroi_allowed(Tango::AttReqType type);


	//--------------------------------------------------------
	/**
	 *	Method      : DanteDpp::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
	//--------------------------------------------------------
	void add_dynamic_attributes();



//	Command related methods
public:
	/**
	 *	Command State related method
	 *	Description: This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
	 *
	 *	@returns State Code
	 */
	virtual Tango::DevState dev_state();
	/**
	 *	Command Snap related method
	 *	Description: Arm/Start the acquisition.
	 *
	 */
	virtual void snap();
	virtual bool is_Snap_allowed(const CORBA::Any &any);
	/**
	 *	Command Stop related method
	 *	Description: Stop the acquisition.
	 *
	 */
	virtual void stop();
	virtual bool is_Stop_allowed(const CORBA::Any &any);
	/**
	 *	Command GetConfigurationsFilesAlias related method
	 *	Description: Get the list of alias of Configurations Files (The contents of ConfigurationFiles property).<br>
	 *
	 *	@returns Array of string of configurations Files alias
	 */
	virtual Tango::DevVarStringArray *get_configurations_files_alias();
	virtual bool is_GetConfigurationsFilesAlias_allowed(const CORBA::Any &any);
	/**
	 *	Command LoadConfigFile related method
	 *	Description: Load a new config file (.ini) designed by its associated alias<br>
	 *               This will initialize the boards/modules in the system<br>
	 *
	 *	@param argin alias of a configuration file
	 */
	virtual void load_config_file(Tango::DevString argin);
	virtual bool is_LoadConfigFile_allowed(const CORBA::Any &any);
	/**
	 *	Command GetRois related method
	 *	Description: Returns the list of rois for each channel.<br>
	 *
	 *	@returns 
	 */
	virtual Tango::DevVarStringArray *get_rois();
	virtual bool is_GetRois_allowed(const CORBA::Any &any);
	/**
	 *	Command GetRoisFilesAlias related method
	 *	Description: Get the list of alias of Rois Files (The contents of RoisFiles property)
	 *
	 *	@returns Array of string of rois Files alias
	 */
	virtual Tango::DevVarStringArray *get_rois_files_alias();
	virtual bool is_GetRoisFilesAlias_allowed(const CORBA::Any &any);
	/**
	 *	Command SetRoisFromFile related method
	 *	Description: Define a set of rois for each channel.<br>
	 *               The set of rois is read from a file indicated by its alias.<br>
	 *
	 *	@param argin alias of a roi file
	 */
	virtual void set_rois_from_file(Tango::DevString argin);
	virtual bool is_SetRoisFromFile_allowed(const CORBA::Any &any);
	/**
	 *	Command SetRoisFromList related method
	 *	Description: Define a set of rois for each channel.<br>
	 *
	 *	@param argin channel_num;roi1_begin;roi1_end;roi2_begin;roi2_end;...
	 */
	virtual void set_rois_from_list(const Tango::DevVarStringArray *argin);
	virtual bool is_SetRoisFromList_allowed(const CORBA::Any &any);
	/**
	 *	Command RemoveRois related method
	 *	Description: Remove all Rois for the selected channel.<br>
	 *               NB: -1 to remove rois for all channels.<br>
	 *
	 *	@param argin Channel number
	 */
	virtual void remove_rois(Tango::DevLong argin);
	virtual bool is_RemoveRois_allowed(const CORBA::Any &any);
	/**
	 *	Command ResetRois related method
	 *	Description: Remove all Rois.<br>
	 *
	 */
	virtual void reset_rois();
	virtual bool is_ResetRois_allowed(const CORBA::Any &any);
	/**
	 *	Command GetDataStreams related method
	 *	Description: Returns the flyscan data streams associated with this device formatted as below :<br>
	 *
	 *	@returns 
	 */
	virtual Tango::DevVarStringArray *get_data_streams();
	virtual bool is_GetDataStreams_allowed(const CORBA::Any &any);
	/**
	 *	Command StreamResetIndex related method
	 *	Description: Reset the stream (Nexus) buffer index to 1.
	 *
	 */
	virtual void stream_reset_index();
	virtual bool is_StreamResetIndex_allowed(const CORBA::Any &any);


/*----- PROTECTED REGION ID(DanteDpp::Additional Method prototypes) ENABLED START -----*/

//	Additional Method prototypes
public :
    //-------------------------------------------------------------	
    // update the device info by recreate it (called by the controller)
    void update_info_attributes(void);

    // memorize the config alias (called by the controller)
    void memorize_config_alias(std::string in_alias);

    // memorize the rois list 
    void memorize_rois_list(std::string in_rois_list);

    // memorize the rois alias 
    void memorize_rois_alias(std::string in_alias);

protected :	
	//-------------------------------------------------------------	
	// create/release methods
	//-------------------------------------------------------------	
	//-------------------------------------------------------------	
    /// instanciate the controller instance
    void create_controller(void);

    /// release the controller instance
    void release_controller(void);

	//-------------------------------------------------------------	
    /// instanciate the log_adapter & inner_appender in order to manage logs
    void create_log_attributes(void);

    /// Release the log_adapter & inner_appender
    void release_log_attributes(void);

    //-------------------------------------------------------------	
    // Create the properties
    void create_properties(Tango::DbData & dev_prop);

    //-------------------------------------------------------------	
    /// instanciate the device info in order to display dependencies versions
    void create_info_attributes(void);

    /// Release the the device info
    void release_info_attributes(void);

	//-------------------------------------------------------------	
    /// Create the static attributes
    void create_static_attributes(void);

    /// Release the static attributes
    void release_static_attributes(void);

    //-------------------------------------------------------------	
    /// Update the hardware with the properties data
    void write_at_init(void);

    //-------------------------------------------------------------	
    // load the aliases
    void load_aliases(void);

    // load the config file (aux method)
    void load_config_file_aux(std::string in_alias);

    // load the rois file (aux method)
    void load_rois_file_aux(std::string in_alias);

    // load config and rois files(aux method)
    void load_config_and_rois_files_aux(std::string in_config_alias, std::string in_rois_alias);

    // load config file and rois list (aux method)
    void load_config_and_rois_list_aux(std::string in_config_alias, std::string in_rois_list);

	//-------------------------------------------------------------	
    // Template methods used to read and write data management
    // in the attributes'callbacks.
	//-------------------------------------------------------------	
	//-------------------------------------------------------------	
    // reads of static attributes
	//-------------------------------------------------------------	
    /// Fill the read attribute with the device informations
    template< typename T1, typename T2>
    void read_static_attribute(Tango::Attribute & out_attr,
                               T1 * out_attr_read,
                               T2 (DanteDpp_ns::Controller::*in_method)() const,
                               const std::string & in_callerName);

    /// Fill the read attribute (string) with the device informations
    template< typename T1>
    void read_string_static_attribute(Tango::Attribute & out_attr,
                                      T1 * out_attr_read,
                                      string (DanteDpp_ns::Controller::*in_method)() const,
                                      const std::string & in_callerName);

	//-------------------------------------------------------------	
    // writes of static attributes
	//-------------------------------------------------------------	
    /// Use the write attribut to set informations in the device
    template< typename T1, typename T2>
    void write_static_attribute(T1 & in_attr_write,
                                void (DanteDpp_ns::Controller::*in_method)(const T2 &),
                                const char * in_optionalMemorizedProperty,
                                const std::string & in_callerName);

    /// Use the write attribut to set informations (string) in the device
    template< typename T1>
    void write_string_static_attribute(const T1 & in_attr_write,
                                       void (DanteDpp_ns::Controller::*in_method)(const std::string &),
                                       const char * in_optionalMemorizedProperty,
                                       const std::string & in_callerName);

	//-------------------------------------------------------------	
    // writes of properties of static attributes
	//-------------------------------------------------------------	
    /// Use to update a static attribute and the hardware with a property value
    template< typename T1, typename T2>
    void write_property_in_static_attribute(const std::string & in_attribute_name,
                                            T1 * out_attr_read,
                                            const T2 & in_memorized_property,
                                            void (DanteDpp_ns::DanteDpp::*in_write_method)(Tango::WAttribute &));

    /// Use to update a string static attribute and the hardware with a property value
    template< typename T1>
    void write_property_in_string_static_attribute(const std::string & in_attribute_name,
                                                   T1 * out_attr_read,
                                                   const std::string & in_memorized_property,
                                                   void (DanteDpp_ns::DanteDpp::*in_write_method)(Tango::WAttribute &));

	//-------------------------------------------------------------	
    // reads of dynamic attributes
	//-------------------------------------------------------------	
    // Fill a dynamic attribute with an information from the controller
    template< typename T1>
    void read_dynamic_user_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                     T1 (DanteDpp_ns::Controller::*in_method)(void) const,
                                     const std::string & in_callerName,
                                     const bool in_is_enabled_during_running = false);

    // Fill a dynamic attribute (with index) with an information from the controller
    template< typename T1>
    void read_dynamic_user_index_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                           T1 (DanteDpp_ns::Controller::*in_method_index)(std::size_t) const,
                                           const std::string & in_callerName,
                                           const bool in_is_enabled_during_running);


    // Fill a dynamic attribute (with double indexes) with an information from the controller
    template< typename T1>
    void read_dynamic_user_double_indexes_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                                    T1 (DanteDpp_ns::Controller::*in_method_indexes)(std::size_t, std::size_t) const,
                                                    const std::string & in_callerName,
                                                    const bool in_is_enabled_during_running);

	//-------------------------------------------------------------	
    // writes of dynamic attributes
	//-------------------------------------------------------------	
    // Use the write dynamic attribut to set informations in the controller
    template< typename T1, typename T2>
    void write_dynamic_user_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                      void (DanteDpp_ns::Controller::*in_method)(const T2 &),
                                      const std::string & in_callerName);

    // Use the write dynamic attribut (with index) to set informations in the controller
    template< typename T1, typename T2>
    void write_dynamic_user_index_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                            void (DanteDpp_ns::Controller::*in_method_index)(const T2 &, std::size_t),
                                            const std::string & in_callerName);

    // Use the write dynamic attribut (with double indexes) to set informations in the controller
    template< typename T1, typename T2>
    void write_dynamic_user_double_indexes_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                                     void (DanteDpp_ns::Controller::*in_method_indexes)(const T2 &, std::size_t, std::size_t),
                                                     const std::string & in_callerName);

	//-------------------------------------------------------------	
    // writes of properties of dynamic attributes
	//-------------------------------------------------------------	
    // Use to update a dynamic attribute and the hardware with a property value
    template< typename T1>
    void write_property_in_dynamic_user_attribute(yat4tango::DynamicInterfaceManager & in_dim,
                                                  const std::string & in_attribute_name,
                                                  void (DanteDpp_ns::DanteDpp::*in_write_method)(yat4tango::DynamicAttributeWriteCallbackData &));

	//-------------------------------------------------------------	
    // callback methods for tango dyn attributes - NULL
    void read_callback_null(yat4tango::DynamicAttributeReadCallbackData& /*cbd*/){/*nop*/}
	
    // callback methods for tango dyn attributes - NULL
    void write_callback_null(yat4tango::DynamicAttributeWriteCallbackData& /*cbd*/){/*nop*/}		

    // Create a dynamic attribute
    template <class F1, class F2>
    void create_dynamic_attribute(yat4tango::DynamicInterfaceManager & out_dim,
                                  const std::string &   name                ,
                                  int                   data_type           ,
                                  Tango::AttrDataFormat data_format         ,
                                  Tango::AttrWriteType  access_type         ,
                                  Tango::DispLevel      disp_level          ,
                                  size_t                polling_period_in_ms,
                                  int                   max_dim_x           ,
                                  const std::string &   unit                ,
                                  const std::string &   format              ,
                                  const std::string &   desc                ,
                                  F1                    read_callback       ,
                                  F2                    write_callback      ,
                                  yat::Any              user_data           );

	//-------------------------------------------------------------	
	// callback methods for tango dynamics attributes
	//-------------------------------------------------------------	
    void read_realtime_callback         (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_livetime_callback         (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_deadtime_callback         (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_input_count_rate_callback (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_output_count_rate_callback(yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_events_in_run_callback    (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_channel_callback          (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_preset_value_callback     (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void write_preset_value_callback    (yat4tango::DynamicAttributeWriteCallbackData & cbd);
    void read_preset_type_callback      (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void write_preset_type_callback     (yat4tango::DynamicAttributeWriteCallbackData & cbd);
    void read_trigger_mode_callback     (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void write_trigger_mode_callback    (yat4tango::DynamicAttributeWriteCallbackData & cbd);
    void read_nb_pixels_callback        (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void read_current_pixel_callback    (yat4tango::DynamicAttributeReadCallbackData  & cbd);
    void write_nb_pixels_callback       (yat4tango::DynamicAttributeWriteCallbackData & cbd);
    void read_roi_callback              (yat4tango::DynamicAttributeReadCallbackData  & cbd);
	void read_sp_time_callback			(yat4tango::DynamicAttributeReadCallbackData  & cbd);
	void write_sp_time_callback			(yat4tango::DynamicAttributeWriteCallbackData  & cbd);

	//-------------------------------------------------------------	
	// Additional Methods prototypes
	//-------------------------------------------------------------	
    /// method which manages DevFailed exceptions
    void manage_devfailed_exception(Tango::DevFailed & in_exception, const std::string & in_caller_method_name);

    /// method which manages dante exceptions
    void manage_dante_exception(const DanteDpp_ns::Exception & in_exception, const std::string & in_caller_method_name);

    /// method which manages dante exceptions
    void manage_unknown_exception(const std::string & in_caller_method_name);

    // return true if the device is correctly initialized in init_device
    bool is_device_initialized();    

    // get the config file from an alias
    std::string get_config_file_from_alias(std::string in_alias);

    // get the rois file from an alias
    std::string get_rois_file_from_alias(std::string in_alias);

private:
	//-------------------------------------------------------------	
	// Attributs
	//-------------------------------------------------------------	
    /// Owner Device server object
    Tango::DeviceImpl * m_device;

    /// init flag
    bool m_is_device_initialized;

    /// status message
    std::stringstream m_status_message;

    // container used to link the configuration alias with the corresponding file 
    std::map<std::string, std::string>  m_alias_configuration_files;

    // container used to link the Roi alias with the corresponding file 
    std::map<std::string, std::string>  m_alias_rois_files;

/*----- PROTECTED REGION END -----*/	//	DanteDpp::Additional Method prototypes
};

/*----- PROTECTED REGION ID(DanteDpp::Additional Classes Definitions) ENABLED START -----*/

//	Additional Classes Definitions
#include "DanteDpp.hpp"

/*----- PROTECTED REGION END -----*/	//	DanteDpp::Additional Classes Definitions

}	//	End of namespace

#endif   //	DanteDpp_H
