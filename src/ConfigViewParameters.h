/*************************************************************************/
/*! 
 *  \file   ConfigViewParameters.h
 *  \brief  class used to manage the ConfigView parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_CONFIG_VIEW_PARAMETERS_H_
#define DANTE_DPP_CONFIG_VIEW_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL
#include "HardwareDanteConfigParameters.h" // for AcqMode

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the ConfigView initialization*/
class ConfigViewParameters
{
friend class Controller;
friend class ConfigView  ;

public:
    // constructor
    ConfigViewParameters();

private :
    std::size_t m_modules_nb ;
    std::size_t m_channels_nb;
    std::size_t m_bins_nb    ;

    HardwareDanteConfigParameters::AcqMode m_acqMode;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_CONFIG_VIEW_PARAMETERS_H_

//###########################################################################
