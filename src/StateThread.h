/*************************************************************************/
/*! 
 *  \file   StateThread.h
 *  \brief  DANTE detector state thread class interface
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_STATE_THREAD_H_
#define DANTE_DPP_STATE_THREAD_H_

// PROJECT
#include "SyncThread.h"

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class StateThread
 * \brief DANTE detector setup thread class
 ***********************************************************************/
class StateThread: public SyncThread
{
public:
    // constructor
    StateThread(const std::string in_name);

protected:
    // the thread entry point - called by yat::Thread::start_undetached. 
    // SyncThread process in managed inside this method.
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg ioa);

    // destructor
    virtual ~StateThread();
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_STATE_THREAD_H_
