/*************************************************************************/
/*! 
 *  \file   StreamNo.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "StreamNo.h"

namespace DanteDpp_ns
{
/*******************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management/controller
 *******************************************************************/
StreamNo::StreamNo()
{
    INFO_STRM << "construction of StreamNo" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamNo::~StreamNo() 
{
    INFO_STRM << "destruction of StreamNo" << std::endl;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_module_data module data to be treated
 *******************************************************************/
void StreamNo::update(yat::SharedPtr<const ModuleData> /*in_module_data*/)
{

}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamNo::reset_index(void)
{
}

//###########################################################################
}