/*************************************************************************/
/*! 
 *  \file   Stream.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "Stream.h"
#include "StreamNo.h"
#include "StreamLog.h"
#include "StreamCsv.h"
#include "StreamNexus.h"
#include "Helper.h"
#include "Log.h"

#include "Controller.h"

namespace DanteDpp_ns
{
//============================================================================================================
// Stream class
//============================================================================================================
/*******************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management/controller
 *******************************************************************/
Stream::Stream()
{
    INFO_STRM << "construction of Stream" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
Stream::~Stream() 
{
    INFO_STRM << "destruction of Stream" << std::endl;
}

/*******************************************************************
 * \brief Get all the parameters
 * \return current parameters
 *******************************************************************/
const StreamParameters & Stream::get_parameters(void) const
{
    return m_parameters;
}

/*******************************************************************
 * \brief Set the parameters
 * \param[in] in_parameters new parameters
 *******************************************************************/
void Stream::set_parameters(const StreamParameters & in_parameters) 
{
    DEBUG_STRM << "Stream::set_parameters" << endl;

    // making a bit copy (no pointer in the class) 
    m_parameters = in_parameters;
}

/*******************************************************************
 * \brief Get the data stream string
 * \return the data stream string (stream file and data items)
 *******************************************************************/
std::string Stream::get_data_streams(void) const
{
    std::string data_streams = m_parameters.m_target_file + ":";

    data_streams += m_parameters.m_label_real_time;
    data_streams += ",";
    data_streams += m_parameters.m_label_live_time;
    data_streams += ",";
    data_streams += m_parameters.m_label_dead_time;
    data_streams += ",";
    data_streams += m_parameters.m_label_input_count_rate;
    data_streams += ",";
    data_streams += m_parameters.m_label_output_count_rate;
    data_streams += ",";
    data_streams += m_parameters.m_label_events_in_run;
    data_streams += ",";
    data_streams += m_parameters.m_label_spectrum;
    data_streams += ",";
    data_streams += m_parameters.m_label_roi;
    data_streams += ",";

    return data_streams;
}

//============================================================================================================
/***************************************************************************
 * \brief Manage the start of acquisition
 ***************************************************************************/
void Stream::start_acquisition(void)
{
    m_state_status.set(Tango::RUNNING, "Stream is running an acquisition...");
}

/*******************************************************************
 * \brief Manage the acquisition stop
 *******************************************************************/
void Stream::stop_acquisition(void)
{
    m_state_status.set(Tango::STANDBY, "Stream is waiting for a request...");
}

//============================================================================================================
/*******************************************************************
 * \brief Getter of the state
*******************************************************************/
Tango::DevState Stream::get_state(void) const
{
    return m_state_status.get_state();
}

/*******************************************************************
 * \brief Getter of the status
*******************************************************************/
std::string Stream::get_status(void) const
{
    return m_state_status.get_status();
}

/***************************************************************************************************
  * SINGLETON MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Create the Stream manager
 * \param[in] in_parameters parameters to set in the stream instance
 *******************************************************************/
void Stream::create(const StreamParameters & in_parameters)
{
    update_parameters(in_parameters);
}

/****************************************************************************
 * \brief Instanciates the correct stream object and/or change the parameters
 * \param[in] in_new_parameters parameters to set in the stream instance
 ***************************************************************************/
void Stream::update_parameters(const StreamParameters & in_new_parameters)
{
    try
    {
        // new stream type
        StreamParameters::Types stream_type = in_new_parameters.m_type;

        // check if we need to re-create or create the stream instance
        bool need_to_change_type = false;

        if(Stream::g_singleton.is_null())
        {
            need_to_change_type = true;
        }
        else
        {
            const StreamParameters & current_parameters = Stream::g_singleton->get_parameters();

            if(current_parameters.m_type != stream_type)
            {
                need_to_change_type = true;
            }
        }
        
        if(need_to_change_type)
        {
            Stream * stream = NULL;

            //creating the stream
            std::string stream_type_label = Helper::convert_stream_type_to_tango(stream_type);

            if(stream_type == StreamParameters::Types::NO_STREAM)
            {
                stream = new StreamNo();
            }
            else
            if(stream_type == StreamParameters::Types::LOG_STREAM)
            {
                stream = new StreamLog();
            }
            else
            if(stream_type == StreamParameters::Types::CSV_STREAM)
            {
                stream = new StreamCsv();
            }
            else
            if(stream_type == StreamParameters::Types::NEXUS_STREAM)
            {
                stream = new StreamNexus();
            }

            Stream::g_singleton.reset(stream);
        }

        Stream::g_singleton->set_parameters(in_new_parameters); 
    }
    catch (Tango::DevFailed & df)
    {
        std::stringstream temp_stream;
        temp_stream << "Error occured in Stream::update_parameters.\n" << endl;
        temp_stream << "reason\t: " << df.errors[0].reason << endl;
        temp_stream << "Origin\t: " << df.errors[0].origin << endl;
        temp_stream << "Desc\t: "   << df.errors[0].desc   << endl;

        ERROR_STRM << temp_stream.str();
    }
}

//###########################################################################
}