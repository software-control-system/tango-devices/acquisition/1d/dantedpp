/*************************************************************************/
/*! 
 *  \file   Exception.h
 *  \brief  DANTE detector exception class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_EXCEPTION_H_
#define DANTE_DPP_EXCEPTION_H_

// SYSTEM
#include <vector>

// LOCAL
#include "Error.h"

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class Exception
 * \brief Exception abstraction base class
 ***********************************************************************/
class Exception
{
public:
    // default constructor
    Exception();

    // constructor
    Exception(const char * in_reason, 
              const char * in_desc  ,
              const char * in_origin,
              int          in_code  = DANTE_DPP_ERROR_CODE_UNKNOWN,
              Error::EnumSeverity in_severity = Error::EnumSeverity::ERROR);

    // constructor
    Exception(const std::string & in_reason,
              const std::string & in_desc  ,
              const std::string & in_origin,
              int                 in_code  = DANTE_DPP_ERROR_CODE_UNKNOWN,
              Error::EnumSeverity in_severity = Error::EnumSeverity::ERROR);

    // Copy constructor
    Exception(const Exception & in_src);

    // = operator
    Exception & operator = (const Exception & in_src);

    // destructor
    virtual ~Exception();

    // Pushes the specified error into the errors list.
    void push_error(const Error & in_error);

    // convert the errors to a complete description string
    std::string to_string() const;

private:
    std::vector<Error> m_errors; // The error list.
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_EXCEPTION_H_

