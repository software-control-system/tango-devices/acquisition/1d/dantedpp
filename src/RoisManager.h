/*************************************************************************/
/*! 
 *  \file   RoisManager.h
 *  \brief  class used to manage the Rois
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROIS_MANAGER_H_
#define DANTE_DPP_ROIS_MANAGER_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Singleton.h"
#include "RoisGroup.h"

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to manage the Rois                                   */
/*************************************************************************/
class RoisManager : public Singleton<RoisManager>
{
    // we need to gain access to the destructor for yat::SharedPtr management of our singleton
    friend class yat::DefaultDeleter<RoisManager>;

    public:
        // Set the channels number
        void set_channels_nb(const std::size_t in_channels_nb);

        // Set the bins number
        void set_bins_nb(const std::size_t in_bins_nb);

        // Get the groups delimiter (between channel rois)
        std::string get_groups_delimiter() const;

        // Clear all the ROIs of all channels
        void reset_rois();

        // Erase all the ROIs of a channel
        void erase_rois(std::size_t in_channel_index);

        // Get the number of rois for all the channels
        std::size_t get_nb_rois() const;

        // Get the number of rois for the channel
        std::size_t get_nb_rois(std::size_t in_channel_index) const;

        // Get access to the roi informations of a channel
        yat::SharedPtr<const RoiRange> get_roi(std::size_t in_channel_index, std::size_t in_roi_index) const;

        // Create a ROI (version with boundaries)
        yat::SharedPtr<RoiRange> create_roi_with_boundaries(std::size_t in_channel_index,
                                                            std::size_t in_roi_index    ,
                                                            std::size_t in_low_index    ,
                                                            std::size_t in_high_index   );

        // Add a ROI to a channel (version with boundaries)
        void add_roi_boundaries(std::size_t in_channel_index,
                                std::size_t in_roi_index    ,
                                std::size_t in_low_index    ,
                                std::size_t in_high_index   );

        // Add a ROI to a channel
        void add_roi(yat::SharedPtr<RoiRange> in_roi_range);

        // Add a ROI to a channel
        void add_roi(std::size_t in_channel_index,
                     std::size_t in_roi_index    ,
                     std::size_t in_bin_index    ,
                     std::size_t in_bin_size     );

        /***************************************************************************************************
          * ROIs file
          **************************************************************************************************/
        // Load a ROIs file
        void load_rois_file(const std::string & in_complete_file_path);

        /***************************************************************************************************
          * ROIs string
          **************************************************************************************************/
        // Get the rois string for all the channels
        std::string get_rois_string() const;

        // Change ROIs using a ROIs string
        void change_rois_string(const std::string & in_rois_string);

        /***************************************************************************************************
          * ROIs list
          **************************************************************************************************/
        // get the Rois list for all channel
        std::vector<std::string> get_rois_list() const;

        // Load a ROIs string
        void load_rois_string(const std::string & in_rois_string);

       /**************************************************************************************************
         * USER ERRORS MANAGEMENT
         **************************************************************************************************/
        // Check the data before the erase of all the ROIs of a channel
        void check_before_erase_rois(int in_channel_index) const;

        // Check the data before the change of ROIs using a ROIs string
        void check_before_change_rois_string(const std::string & in_rois_string) const;

        /***************************************************************************************************
          * SINGLETON MANAGEMENT
          **************************************************************************************************/
         // Create the manager
        static void create();

    protected:
        // constructor
        explicit RoisManager();

        // destructor (needs to be virtual)
        virtual ~RoisManager();

    private :
        /***************************************************************************************************
          * ROIs list
          **************************************************************************************************/
        // Add the ROIs from a rois list
        void add_with_rois_list(const std::vector<std::string> & in_rois);

        // load the rois file content in memory
        std::vector<std::string> load_rois_file_to_rois_list(const std::string & in_file_name);

        // convert a rois string to a rois list
        std::vector<std::string> convert_rois_string_to_rois_list(const std::string & rois_string) const;

        // check the rois list and manage the all channels tag
        void check_rois_list(std::vector<std::string> & in_out_rois_list) const;

    private :
        RoisGroupMap m_groups     ;
        std::size_t  m_channels_nb;
        std::size_t  m_bins_nb    ;

        static const std::string g_groups_delimiter;
        static const std::string g_rois_delimiter  ;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ROIS_MANAGER_H_

//###########################################################################
