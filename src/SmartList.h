/**
 *  \file SmartList.h
 *  \brief header file of SmartList template class
 *  \author C�dric Castel
 *  \version 0.1
 *  \date November 29 2019
 *  Created on: November 07 2019
 */

#ifndef DANTE_DPP_SMARTLIST_H_
#define DANTE_DPP_SMARTLIST_H_

// SYSTEM
#include <cstddef>
#include <stdint.h>
#include <queue>
#include <set>
#include <mutex>
#include <string>
#include <cstdlib>
#include <sstream> // std::stringstream

// YAT
#include <yat/memory/SharedPtr.h>
#include <yat/threading/Mutex.h> // m_mutex
#include <yat/threading/Condition.h>
#include <yat/time/Timer.h>
#include <yat/utils/Logging.h>

// PROJECT
#include "Log.h"
#include "Exception.h"

/**
 * \namespace DanteDpp_ns
 */
namespace DanteDpp_ns
{
/**
  *  \class SmartList
  *  \brief This class is used to manage a FIFO of elements.
  *         It is created empty.
  *
  *         Elements are stored in the container using yat smartpointers to be easily removed and inserted 
  *         from a container to another container without the need of memory management.
  *
  *         Counters allows to know :
  *         - the maximum number of elements in the container during execution
  *         - the total number of elements treated during execution
  *         - the memory (in bytes) of the treated elements during execution
  *         - the elapsed time to insert all elements to be treated during execution
  *         - the elapsed time to remove all elements to be treated during execution
  *         - know the number of producers and customers linked which use the container
  *         A mutex protects the multi-threads access to this container (take and put methods).
  *
  */

template< class Elem>
class SmartList
{
public:
  /**
    * \fn SmartList(std::string in_name, size_t in_element_size_in_bytes)
    * \brief constructor
    * \param Elem : class of the elements stored into the container.
    * \param in_name : name of the instance (used for stats) 
    * \param in_max_elements_to_treat_nb : max number of elements to be treated the container during execution.
    * \param in_element_size_in_bytes : memory size of an element (used for stats)
    * \return none
    */
    SmartList(std::string in_name, std::size_t in_max_elements_to_treat_nb, std::size_t in_element_size_in_bytes);

  /**
    * \fn ~SmartList()
    * \brief destructor - we do not allow this class to be inherited, so this method is not virtual.
    * \param none
    * \return none
    */
    ~SmartList();

  /**
    * \fn const yat::SharedPtr<const Elem> front(bool in_throw_empty_list_exception) const
    * \brief Gives const access to the first element of the container (multi-threads protected).
    *        can throw and exception if the container is empty.
    * \param in_throw_empty_list_exception : throw or not an exception if the container is empty.
    *        A false value is for multi-consumers.
    * \return access to the first element of the container
    */
    yat::SharedPtr<const Elem> front(bool in_throw_empty_list_exception = true) const;

  /**
    * \fn const yat::SharedPtr<Elem> front(bool in_throw_empty_list_exception)
    * \brief Gives write access to the first element of the container (multi-threads protected).
    *        can throw and exception if the container is empty.
    * \param in_throw_empty_list_exception : throw or not an exception if the container is empty.
    *        A false value is for multi-consumers.
    * \return access to the first element of the container
    */
    yat::SharedPtr<Elem> front(bool in_throw_empty_list_exception = true);

  /**
    * \fn void remove_front(bool in_throw_empty_list_exception)
    * \brief removes the first element from the container (multi-threads protected).
    *        can throw and exception if the container is empty.
    * \param in_throw_empty_list_exception : throw or not an exception if the container is empty.
    *        A false value is for multi-consumers.
    * \return none
    */
    void remove_front(bool in_throw_empty_list_exception = true);

  /**
    * \fn yat::SharedPtr<Elem> take(bool in_throw_empty_list_exception)
    * \brief takes an element from the container to treat it (multi-threads protected).
    *        can throw and exception if the container is empty.
    * \param in_throw_empty_list_exception : throw or not an exception if the container is empty.
    *        A false value is for multi-consumers.
    * \return the first element of the container
    */
    yat::SharedPtr<Elem> take(bool in_throw_empty_list_exception = true);

  /**
    * \fn void put(yat::SharedPtr<Elem> in_element)
    * \brief put an element in the container (multi-threads protected).
    * \param in_element : element to be treated which will be put at the end of the container.
    * \return none
    */
    void put(yat::SharedPtr<Elem> in_element);

  /**
    * \fn std::size_t size() const
    * \brief gets the current number of elements in the container (multi-threads protected).
    *        When the container is invalid, it always returns 0 to stop some treatments which could run with a loop.
    * \param none
    * \return current number of elements in the container
    */
    std::size_t size() const;

  /**
    * \fn bool empty() const
    * \brief tells if the container is empty (multi-threads protected).
    * \param none
    * \return true if the container is empty, else false
    */
    bool empty() const;

  /**
    * \fn std::size_t get_elements_nb() const
    * \brief gets the number of elements inside the container (multi-threads protected).
    *        When the container is invalid, this method returns the real number of elements.
    * \param none
    * \return current number of elements treated by the container
    */
    std::size_t get_elements_nb() const;

  /**
    * \fn std::size_t get_total_elements_nb() const
    * \brief gets the number of elements which were treated by the container (multi-threads protected).
    * \param none
    * \return current number of elements treated by the container
    */
    std::size_t get_total_elements_nb() const;

  /**
    * \fn std::size_t get_max_elements_to_treat_nb() const
    * \brief gets the max number of elements to be treated the container during execution.
    * \param none
    * \return max number of elements to be treated the container during execution.
    */
    std::size_t get_max_elements_to_treat_nb() const;

  /**
    * \fn bool completely_filled() const
    * \brief is the container completely filled ?
    * a container is "completely filled" when the total number of elements inserted reaches the maximum elements to treat number.
    * \param none
    * \return true if the container is completely filled, else false
    */
    bool completely_filled() const;

  /**
    * \fn bool completely_emptied() const
    * \brief is the container completely emptied ?
    * a container is "completely emptied" when it is "completely filled" and the container becomes empty.
    * \param none
    * \return true if the container is completely emptied, else false
    */
    bool completely_emptied() const;

  /**
    * \fn bool last_element_to_treat() const
    * \brief is the container soon completely emptied ? This is the last element to which need a treatment.
    * a container is "completely emptied" when it is "completely filled" and the container becomes empty.
    * \param none
    * \return true if the container will be completely emptied after the last elemet treatment, else false
    */
    bool last_element_to_treat() const;

  /**
    * \fn void invalidate()
    * \brief tags the container as invalid.
    * It allows to easily stop some treatments which run in loop.
    * When the container is invalid, size method returns always 0 and empty methods always true.
    * \param none
    * \return none
    */
    void invalidate();

  /**
    * \fn bool is_valid() const
    * \brief is the container tagged as valid ?
    * \param none
    * \return true if the container is valid, else false
    */
    bool is_valid() const;

  /**
    * \fn void waiting_while_empty() const
    * \brief waits till the container is no more empty.
    * \param none
    * \return none
    */
    void waiting_while_empty() const;

  /**
    * \fn void register_producer(std::string in_producer)
    * \brief registers a new producer
    * \param in_producer : producer name
    * \return none
    */
    void register_producer(const std::string & in_producer);

  /**
    * \fn void unregister_producer(std::string in_producer)
    * \brief unregisters a producer
    * \param in_producer : producer name
    * \return none
    */
    void unregister_producer(const std::string & in_producer);

  /**
    * \fn void register_customer(std::string in_customer)
    * \brief registers a new customer
    * \param in_customer : customer name
    * \return none
    */
    void register_customer(const std::string & in_customer);

  /**
    * \fn void unregister_customer(std::string in_customer)
    * \brief unregisters a customer
    * \param in_customer : customer name
    * \return none
    */
    void unregister_customer(const std::string & in_customer);

  /**
    * \fn bool is_useless() const
    * \brief is the container useless (when there are no more producers and cutomers using it)
    * \param none
    * \return true is the container is useless, else false
    */
    bool is_useless() const;

  /**
    * \fn void start_timers()
    * \brief starts statistical timers.
    * \param none
    * \return none
    */
    void start_timers();

  /**
    * \fn std::size_t get_max_in_elements_nb() const
    * \brief gets maximum number of elements inside of the container
    * \param none
    * \return maximum number of elements inside of the container
    */
    std::size_t get_max_in_elements_nb() const;

  /**
    * \fn double get_completely_filled_elapsed_time_ms() const
    * \brief gets the elapsed time between the treatment start and an "completely filled" container
    * \param none
    * \return elapsed time between the treatment start and an "completely filled" container
    */
    double get_completely_filled_elapsed_time_ms() const;

  /**
    * \fn double get_completely_emptied_elapsed_time_ms() const
    * \brief gets the elapsed time between the treatment start and an "completely emptied" container
    * \param none
    * \return elapsed time between the treatment start and an "completely emptied" container
    */
    double get_completely_emptied_elapsed_time_ms() const;

  /**
    * \fn void log_stats()
    * \brief log stats for performance and memory managements - to be used after treatment.
    * \param none
    * \return none
    */
    void log_stats() const;

private:
  /**
    * \fn const std::string build_actor_list(const & std::set<std::string> in_container) const
    * \brief build a string which list the actors from a specific set
    * \param none
    * \return string with actors list
    */
    std::string build_actor_list(const std::set<std::string> & in_container) const;

private:
  /** name of the instance (used for stats).
    */
    std::string m_name;

  /** maximum number of elements in the container during execution (used for stats).
    */
    std::size_t m_max_in_elements_nb;

  /** total number of elements inside the container during execution.
    */
    std::size_t m_total_elements_nb;

  /** max number of elements to be treated the container during execution.
    * allows to build time execution statistics.
    */
    std::size_t m_max_elements_to_treat_nb;

  /** size in memory of an element (used for stats).
    */
    std::size_t m_element_size_in_bytes;

  /** timer used to compute the elapsed time between the treatment start and a "completely filled" container (used for stats).
    * a container is "completely filled" when the total number of elements inserted reaches the maximum elements to treat number.
    */
    yat::Timer completely_filled_timer;

  /** timer used to compute the elapsed time between the treatment start and an "completely emptied" container (used for stats).
    * a container is "completely emptied" when it is "completely filled" and the container becomes empty.
    */
    yat::Timer completely_emptied_timer;

  /** elapsed time between the treatment start and an "completely filled" container (used for stats).
    */
    double completely_filled_elapsed_time_ms;

  /** elapsed time between the treatment start and an "completely emptied" container (used for stats).
    */
    double completely_emptied_elapsed_time_ms;

  /** this boolean is used to tag the container as valid or invalid.
    * When the container is invalid, size method returns always 0 and empty methods always true.
    * It allows to easily stop some treatments which run in loop.
    */
    bool m_valid_tag;

  /** number of producers which use this instance
    */
    std::size_t m_producers_nb;

  /** number of customers which use this instance
    */
    std::size_t m_customers_nb;

  /** producers list which use this instance.
    */
    std::set<std::string> m_producers;

  /** customers list which use this instance
    */
    std::set<std::string> m_customers;

  /** producers list which used this instance (for logs).
    */
    std::set<std::string> m_producers_historic;

  /** customers list which used this instance (for logs).
    */
    std::set<std::string> m_customers_historic;

  /** FIFO used to store the elements.
    */
    std::queue<yat::SharedPtr<Elem>> m_elements;

  /** mutex used to protect the multithread access to this container (take and put methods).
    * The mutable keyword is used to allow using this mutex into const methods.
    */
    mutable yat::Mutex m_mutex;

  /** condition variable used to synchronized producers and customers.
    * The condition is signaled when the list is no more empty.
    */
    mutable yat::Condition m_condition_is_not_empty;

  /** mutex associated with m_is_not_empty_condition.
    */
    mutable yat::Mutex m_mutex_is_not_empty;
};

// include the implementation file of the SmartList class to separate interface and implementation
#include "SmartList.hpp"

} /// namespace DanteDpp_ns

#endif //// DANTE_DPP_SMARTLIST_H_
