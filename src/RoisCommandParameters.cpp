/*************************************************************************/
/*! 
 *  \file   RoisCommandParameters.cpp
 *  \brief  class used to set a rois command
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "RoisCommandParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// RoisCommandParameters class
//============================================================================================================
/**************************************************************************
* \brief constructor
**************************************************************************/
RoisCommandParameters::RoisCommandParameters()
{
    m_command = RoisCommandParameters::RoisCommand::UNSET;
    m_channels_index = ROIS_COMMAND_APPLY_ON_ALL_CHANNELS;
}

//###########################################################################
}