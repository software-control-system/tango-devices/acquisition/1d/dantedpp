/*************************************************************************/
/*! 
 *  \file   ModuleData.h
 *  \brief  class used to store a module acquisition
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_MODULE_DATA_H_
#define DANTE_DPP_MODULE_DATA_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>

// PROJECT
#include "ChannelData.h"
#include "SmartList.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class ModuleData
{
public:
    // constructor
    ModuleData(std::size_t in_index, std::size_t in_channels_nb);

    // get the index of the acquisition data
    std::size_t get_index() const;

    // tell if all the acquisition data of channels were received
    bool is_complete() const;

    // add channel data
    void add_channel(yat::SharedPtr<ChannelData> in_channel_data);

    // get channel data
    yat::SharedPtr<const ChannelData> get_channel(std::size_t in_channels_index) const;

    // compute the size of an instance (used for stats)
    static std::size_t compute_size_in_bytes(std::size_t in_channels_nb, std::size_t in_bins_nb, std::size_t in_rois_nb);

private :
  /** index of the acquisition data
    */
    std::size_t m_index;

  /** number of received channels data.
    * Will be compare to the number of elements in the channels container to know if the data are complete. 
    */
    std::size_t m_received_channels_nb;

  /** received channels data.
    * Before the reception of data, this container has a number of elements which correspond to the number of channels.
    * But, at the begining, the shared pointers have all null values.
    */
    std::vector<yat::SharedPtr<ChannelData>> m_channels;

  /** mutex used to protect the multithread access.
    * The mutable keyword is used to allow using this mutex into const methods.
    */
    mutable yat::Mutex m_mutex;
};

/***********************************************************************
 * ModuleData containers types
 ***********************************************************************/
/**
 * \typedef ModuleDataMap
 * \brief type of container which contains a map of module data
 */
typedef std::map<std::size_t, yat::SharedPtr<ModuleData>> ModuleDataMap;

/**
 * \typedef ModuleDataList
 * \brief type of container which contains a smart list of module data (of smart pointers)
 */
typedef SmartList<ModuleData> ModuleDataList;

} // namespace DanteDpp_ns

#endif // DANTE_DPP_MODULE_DATA_H_

//###########################################################################
