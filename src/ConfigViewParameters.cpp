/*************************************************************************/
/*! 
 *  \file   ConfigViewParameters.cpp
 *  \brief  class used to manage the ConfigView parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "ConfigViewParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// ConfigViewParameters class
//============================================================================================================
/**************************************************************************
* \brief constructor
**************************************************************************/
ConfigViewParameters::ConfigViewParameters()
{
    m_modules_nb  = 0;
    m_channels_nb = 0;
    m_bins_nb     = 0;
}

//###########################################################################
}