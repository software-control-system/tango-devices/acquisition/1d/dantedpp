/*************************************************************************/
/*! 
 *  \file   Log.cpp
 *  \brief  class used to manage the logs
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"

namespace DanteDpp_ns
{
/************************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management
 ************************************************************************/
Log::Log(Tango::DeviceImpl* in_device) : Tango::LogAdapter(in_device), m_device(in_device)
{
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
Log::~Log()
{
}

/***************************************************************************************************
  * SINGLETON MANAGEMENT
  **************************************************************************************************/
/*******************************************************************
 * \brief Create the log manager
 * \param[in] in_device access to tango device for log management/controller
 *******************************************************************/
void Log::create(Tango::DeviceImpl * in_device)
{
    Log::g_singleton.reset(new Log(in_device));

    INFO_STRM << "Creating the log manager." << std::endl;
}

} // namespace DanteDpp_ns

