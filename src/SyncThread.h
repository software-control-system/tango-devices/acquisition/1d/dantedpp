/*************************************************************************/
/*! 
 *  \file   SyncThread.h
 *  \brief  DANTE detector thread class interface - allows to wait for the 
 *          time when the thread is ready to process its data.
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_SYNC_THREAD_H_
#define DANTE_DPP_SYNC_THREAD_H_

// YAT
#include <yat/threading/Thread.h>
#include <yat/threading/Barrier.h>

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class SyncThread
 * \brief SyncThread base class
 ***********************************************************************/
class SyncThread: public yat::Thread
{
public:
    // simple constructor
    SyncThread(const std::string in_name);

    // constructor with verbose log option
    SyncThread(const std::string in_name, bool in_verbose_log);

    // starting thread and waiting for synchronization
    void start();

    // stops the thread treatment but does not wait for thread end (asynchronous stop).
    void stop();

    // stops the decoding and waits the thread end (synchronous stop).
    void exit();

    // waiting for the end of treatment
    void waiting_end_of_treatment(int sleep_ms) const;

    // waiting for the treatment is completed
    void waiting_completed_treatment(int sleep_ms) const;

    // tell if the treatment is always running 
    bool is_treatment_running() const;

protected:
    // the thread entry point - called by yat::Thread::start_undetached. 
    // SyncThread process in managed inside this method.
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg ioa) = 0;

    // destructor
    virtual ~SyncThread();

    // start code to call in the thread method
    void start_treatment();

    // end code to call in the thread method
    void finish_treatment();

    // can the thread's treatment go on ?
    bool can_go_on() const;

    // tag the treatment as completed
    void treatment_completed();

protected:
    std::string m_name ; /// name used to register and unregisters the use of containers

private:
    volatile bool m_go_on; /// volatile is for avoiding compiler optimisations. Should not be store into a register but always read in memory.
    volatile bool m_treatment_is_running;

    yat::Barrier m_start_barrier; /// used to synchronise the main thread (call to start method by Frames instance) and the SyncThread thread

    bool m_verbose_log; // used to log the thread activity (not for a periodic thread)
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_SYNC_THREAD_H_
