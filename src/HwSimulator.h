/*************************************************************************/
/*! 
 *  \file   HwSimulator.h
 *  \brief  DANTE Simulator detector class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_HW_SIMULATOR_H
#define DANTE_DPP_HW_SIMULATOR_H

// SYSTEM
#include <vector>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>

// PROJECT
#include "HardwareDante.h"
#include "ChannelData.h"

/**********************************************************************/
namespace DanteDpp_ns
{
    /***********************************************************************
     * \class HwSimulator
     * \brief Hardware control object interface
     ***********************************************************************/

    class HwSimulator : public HardwareDante
    {
        // we need to gain access to the contructor
        friend class HardwareDante;

    public:
        //------------------------------------------------------------------
        // init/terminate management
        //------------------------------------------------------------------
        // init the detector
        virtual void init(void);

        // terminate the detector
        virtual void terminate(void);

        //------------------------------------------------------------------
        // connection management
        //------------------------------------------------------------------
        // connection to the detector
        virtual void connect(const std::string & in_complete_file_path);

        // disconnection from the detector
        virtual void disconnect(void);

        //------------------------------------------------------------------
        // acquisition management
        //------------------------------------------------------------------
        // start detector acquisition
        virtual bool start_detector_acquisition(void);

        // stop detector acquisition
        virtual void stop_detector_acquisition(void);

        // treat acquisitions
        virtual void treat_acquisitions(void);

    protected:
        // constructor
        explicit HwSimulator();
                             
        // destructor (needs to be virtual)
        virtual ~HwSimulator();

        //------------------------------------------------------------------
        // state management
        //------------------------------------------------------------------
        // requests the states of all boards and computes a general state
        virtual Tango::DevState request_state();

        //------------------------------------------------------------------
        // error management
        //------------------------------------------------------------------
        // manages the current error and throws an exception
        virtual void manage_last_error(const std::string & in_reason,
                                       const std::string & in_desc  ,
                                       const std::string & in_origin);

    private:
        // simulate a request of firmwares versions of all boards
        std::vector<std::string> request_firmwares_versions();

        // create a channel data
        yat::SharedPtr<ChannelData> create_channel_data(std::size_t in_channel_index);

    private:
        // gets a request id to simulate the request system of DANTE sdk
        uint32_t get_request_id();

        // timer used to simulate the mca acquisition (the detector is in running mode till the acquisition time is elapsed)
        yat::Timer mca_timer;

        // used to simulate the request ids of DANTE sdk
        uint32_t m_current_request_id; 

        // used to simulate the acquisition status
        uint32_t m_acquisition_state;
    };
}
#endif // DANTE_DPP_HW_SIMULATOR_H

/*************************************************************************/