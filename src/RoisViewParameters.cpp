/*************************************************************************/
/*! 
 *  \file   RoisViewParameters.cpp
 *  \brief  class used to manage the RoisView parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "RoisViewParameters.h"

namespace DanteDpp_ns
{
//============================================================================================================
// RoisViewParameters class
//============================================================================================================
/**************************************************************************
* \brief constructor
**************************************************************************/
RoisViewParameters::RoisViewParameters()
{
    m_modules_nb  = 0;
    m_channels_nb = 0;
}

//###########################################################################
}