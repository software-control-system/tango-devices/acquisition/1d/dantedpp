/*************************************************************************/
/*! 
 *  \file   DanteDpp.hpp
 *  \brief  class DanteDpp
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/
#ifndef DANTE_DPP_HPP_
#define DANTE_DPP_HPP_

//============================================================================================================
// TEMPLATE IMPLEMENTATION
//============================================================================================================
/*****************************************************************************************
 * \brief Fill the read attribute with the device informations
 * \param[out] out_attr       Tango attribute to update with the Tango attribute member
 * \param[out] out_attr_read  Tango attribute member to filled with the data value
 * \param[in]  in_method      Pointer on a controller method to call to retrieve the data
 * \param[in]  in_callerName  Class and method name of the caller (for exception management)
******************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::read_static_attribute(Tango::Attribute & out_attr,
                                     T1 * out_attr_read,
                                     T2 (DanteDpp_ns::Controller::*in_method)() const,
                                     const std::string & in_callerName)
{
    try
    {
        *out_attr_read = static_cast<T1>((Controller::get_const_instance().get()->*in_method)());
        out_attr.set_value(out_attr_read);
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/********************************************************************************************************
 * \brief Use the write attribut to set informations in the device
 * \param[in]  in_attr_write                Tango value set by the user
 * \param[in]  in_method                    Pointer on a controller method to call to set the data
 * \param[in]  in_optionalMemorizedProperty Optional memorized property name (NULL if not used)
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::write_static_attribute(T1 & in_attr_write,
                                      void (DanteDpp_ns::Controller::*in_method)(const T2 &),
                                      const char * in_optionalMemorizedProperty,
                                      const std::string & in_callerName)
{
    try
    {
        T2 data = static_cast<T2>(in_attr_write);
        (Controller::get_instance().get()->*in_method)(data);

        if(in_optionalMemorizedProperty != NULL)
        {
            yat4tango::PropertyHelper::set_property(this, in_optionalMemorizedProperty, in_attr_write);
        }
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/*************************************************************************************************************
 * \brief Use to update a static attribute and the hardware with a property value
 * \param[in]  T1                    TANGO data type for read attribute variable (Tango::DevFloat for example)
 * \param[in]  T2                    Data type for memorized property (string for example)
 * \param[in]  in_attribute_name     Name of the attribute linked to the property value
 * \param[out] out_attr_read         Tango attribute member to filled with the data value
 * \param[in]  in_memorized_property Property variable
 * \param[in]  in_write_method       Pointer on a controller method to call to set the data
*************************************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::write_property_in_static_attribute(const std::string & in_attribute_name,
                                                  T1 * out_attr_read,
                                                  const T2 & in_memorized_property,
                                                  void (DanteDpp_ns::DanteDpp::*in_write_method)(Tango::WAttribute &))
{
	INFO_STREAM << "Write tango hardware at Init - " << in_attribute_name << "." << endl;

    *out_attr_read = in_memorized_property;
	Tango::WAttribute & attribute = dev_attr->get_w_attr_by_name(in_attribute_name.c_str());
	attribute.set_write_value(*out_attr_read);
    (this->*in_write_method)(attribute);
}

/*****************************************************************************************
 * \brief Fill the read attribute (string) with the device informations
 * \param[in]  T1             TANGO data type for read attribute variable (Tango::DevString)
 * \param[out] out_attr       Tango attribute to update with the Tango attribute member
 * \param[out] out_attr_read  Tango attribute member to filled with the data value
 * \param[in]  in_method      Pointer on a controller method to call to retrieve the data
 * \param[in]  in_callerName  Class and method name of the caller (for exception management)
******************************************************************************************/
template< typename T1>
void DanteDpp::read_string_static_attribute(Tango::Attribute & out_attr,
                                            T1 * out_attr_read,
                                            std::string (DanteDpp_ns::Controller::*in_method)() const,
                                            const std::string & in_callerName)
{
    try
    {
        // there is no const_get() method from SharedPtr class, so we use a static_cast to keep the const
        // in the method pointer definition.
        std::string temp = (static_cast<const Controller *>(Controller::get_instance().get())->*in_method)();
        strcpy(*out_attr_read, temp.c_str());
        out_attr.set_value(out_attr_read);
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/********************************************************************************************************
 * \brief Use the write attribut to set informations (string) in the device
 * \param[in]  T1                           TANGO data type for write attribute variable (Tango::DevString)
 * \param[in]  in_attr_write                Tango value set by the user
 * \param[in]  in_method                    Pointer on a controller method to call to set the data
 * \param[in]  in_optionalMemorizedProperty Optional memorized property name (NULL if not used)
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1>
void DanteDpp::write_string_static_attribute(const T1 & in_attr_write,
                                             void (DanteDpp_ns::Controller::*in_method)(const std::string &),
                                             const char * in_optionalMemorizedProperty,
                                             const std::string & in_callerName)
{
    try
    {
        std::string data = string(in_attr_write);

        (Controller::get_instance().get()->*in_method)(data);

        if(in_optionalMemorizedProperty != NULL)
        {
            yat4tango::PropertyHelper::set_property(this, in_optionalMemorizedProperty, in_attr_write);
        }
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/*************************************************************************************************************
 * \brief Use to update a string static attribute and the hardware with a property value
 * \param[in]  T1                    TANGO data type for read attribute variable (Tango::DevString)
 * \param[in]  in_attribute_name     Name of the attribute linked to the property value
 * \param[out] out_attr_read         Tango attribute member to filled with the data value
 * \param[in]  in_memorized_property Property variable
 * \param[in]  in_write_method       Pointer on a controller method to call to set the data
*************************************************************************************************************/
template< typename T1>
void DanteDpp::write_property_in_string_static_attribute(const std::string & in_attribute_name,
                                                         T1 * out_attr_read,
                                                         const std::string & in_memorized_property,
                                                         void (DanteDpp_ns::DanteDpp::*in_write_method)(Tango::WAttribute &))
{
	INFO_STREAM << "Write tango hardware at Init - " << in_attribute_name << "." << endl;

    strcpy(*out_attr_read, in_memorized_property.c_str());
	Tango::WAttribute & attribute = dev_attr->get_w_attr_by_name(in_attribute_name.c_str());
	attribute.set_write_value(*out_attr_read);
    (this->*in_write_method)(attribute);
}

//============================================================================================================
// CREATE DYNAMIC ATTRIBUTE
//============================================================================================================
template <class F1, class F2>
void DanteDpp::create_dynamic_attribute(yat4tango::DynamicInterfaceManager & out_dim,
                                        const std::string &   name                ,
                                        int                   data_type           ,
                                        Tango::AttrDataFormat data_format         ,
                                        Tango::AttrWriteType  access_type         ,
                                        Tango::DispLevel      disp_level          ,
                                        size_t                polling_period_in_ms,
                                        int                   max_dim_x           ,
                                        const std::string &   unit                ,
                                        const std::string &   format              ,
                                        const std::string &   desc                ,
                                        F1                    read_callback       ,
                                        F2                    write_callback      ,
                                        yat::Any              user_data           )
{
	DEBUG_STREAM << "DanteDpp::create_dynamic_attribute() - [BEGIN]" << endl;
	INFO_STREAM << "\t- Create dynamic attribute [" << name << "]" << endl;

    ////////////////////////////////////////////////////////////////////////////////////////    
	yat4tango::DynamicAttributeInfo dai;
	dai.dev = m_device;
	//- specify the dyn. attr.  name
	dai.tai.name = name;
	//- associate the dyn. attr. with its data
    if(!user_data.empty())
    {
    	dai.set_user_data(user_data);
    }

	//- describe the dynamic attr we want...
	dai.tai.data_type        = data_type;
	dai.tai.data_format      = data_format;
	dai.tai.writable         = access_type;
	dai.tai.disp_level       = disp_level;
	dai.tai.unit             = unit;
	dai.tai.format           = format;
	dai.tai.description      = desc;
    dai.tai.max_dim_x        = max_dim_x;
    dai.polling_period_in_ms = polling_period_in_ms;
    dai.cdb                  = false;

	//- cleanup tango db option: cleanup tango db when removing this dyn. attr. (i.e. erase its properties fom db)
	//	dai.cdb = true;
	
	//- instanciate the read callback (called when the dyn. attr. is read)    
	if(access_type == Tango::READ ||access_type == Tango::READ_WRITE)
	{
		dai.rcb = yat4tango::DynamicAttributeReadCallback::instanciate(*this, read_callback);
	}

	//- instanciate the write callback (called when the dyn. attr. is read)    
	if(access_type == Tango::WRITE ||access_type == Tango::READ_WRITE)
	{
		dai.wcb = yat4tango::DynamicAttributeWriteCallback::instanciate(*this, write_callback);
	}
	
	//- add the dyn. attr. to the device
	DEBUG_STREAM << "\t- add the dyn. attr. to the device [" << m_device << "]" << endl;
	out_dim.dynamic_attributes_manager().add_attribute(dai);
	DEBUG_STREAM << "DanteDpp::create_dynamic_attribute() - [END]" << endl;
}

/********************************************************************************************
 * \brief Fill a dynamic attribute with an information from the controller
 * \param[in]  T1            data type of the device
 * \param[out] out_cbd       Tango attribute to update with the new value
 * \param[in]  in_method     Pointer to a get method to call
 * \param[in]  in_callerName Class and method name of the caller (for exception management)
 * \param[in]  in_is_enabled_during_running Can we read the attribut during running state ?
*********************************************************************************************/
template< typename T1>
void DanteDpp::read_dynamic_user_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                           T1 (DanteDpp_ns::Controller::*in_method)(void) const,
                                           const std::string & in_callerName,
                                           const bool in_is_enabled_during_running)
{
    DEBUG_STREAM << in_callerName << " : " << out_cbd.dya->get_name() << endl;

    try
    {
        Tango::DevState state = is_device_initialized() ? Controller::get_instance()->get_state() : Tango::FAULT;

        if (state == Tango::FAULT || state == Tango::INIT || state == Tango::OFF)
        {
            std::string reason = "It is currently not allowed to read attribute ";
            reason += out_cbd.dya->get_name();

            Tango::Except::throw_exception("TANGO_DEVICE_ERROR" ,
                                           reason.c_str()       ,
                                           in_callerName.c_str());
        }
        else
        if(state == Tango::DISABLE)
        {
            // do not throw an exception during the configuration changes         
            return;
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!out_cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            in_callerName.c_str());
        }

        // set the attribute value
        UserData * temp = reinterpret_cast<UserData *>(out_cbd.dya->get_user_data<UserData>());

        // if in_is_enabled_during_running is false,
        // during an acquisition, we do not allow the access to the modules to avoid problems.
        // We use the last known value
        bool attribut_is_in_alarm = true;
        
        if((in_is_enabled_during_running) || (state != Tango::RUNNING))
        {       
            T1 data;

            // checking the number of indexes to recast the method pointer
            const std::size_t indexes_nb = temp->get_indexes_nb();

            // no index
            if(indexes_nb == 0)
            {
                // call the controller method
                data = (Controller::get_const_instance().get()->*in_method)();
            }
            else
            // one index
            if(indexes_nb == 1)
            {
                T1 (DanteDpp_ns::Controller::*method_index)(std::size_t) const = 
                reinterpret_cast<T1 (DanteDpp_ns::Controller::*)(std::size_t) const>(in_method);   

                // call the controller method with an index
                data = (Controller::get_const_instance().get()->*method_index)(temp->get_index(0));
            }
            else
            // two indexes
            {
                T1 (DanteDpp_ns::Controller::*method_indexes)(std::size_t, std::size_t) const = 
                reinterpret_cast<T1 (DanteDpp_ns::Controller::*)(std::size_t, std::size_t) const>(in_method);

                // call the controller method with two indexes
                 data = (Controller::get_const_instance().get()->*method_indexes)(temp->get_index(0), temp->get_index(1));
            }

            // change the user value
            temp->set_value(reinterpret_cast<const void *>(&data));

            attribut_is_in_alarm = false;
        }

        // copy the user value in the tango value
        temp->fill_read_attr(out_cbd.tga);
        out_cbd.tga->set_quality((attribut_is_in_alarm) ? Tango::ATTR_ALARM : Tango::ATTR_VALID);
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/********************************************************************************************
 * \brief Fill a dynamic attribute (with index) with an information from the controller
 * \param[in]  T1            data type of the device
 * \param[out] out_cbd       Tango attribute to update with the new value
 * \param[in]  in_method     Pointer to a get method to call
 * \param[in]  in_callerName Class and method name of the caller (for exception management)
 * \param[in]  in_is_enabled_during_running Can we read the attribut during running state ?
*********************************************************************************************/
template< typename T1>
void DanteDpp::read_dynamic_user_index_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                                 T1 (DanteDpp_ns::Controller::*in_method_index)(std::size_t) const,
                                                 const std::string & in_callerName,
                                                 const bool in_is_enabled_during_running)
{
    T1 (DanteDpp_ns::Controller::*generic_method)(void) const = 
        reinterpret_cast<T1 (DanteDpp_ns::Controller::*)(void) const>(in_method_index);

    read_dynamic_user_attribute<T1>(out_cbd, generic_method, in_callerName, in_is_enabled_during_running);
}

/********************************************************************************************
 * \brief Fill a dynamic attribute (with double indexes) with an information from the controller
 * \param[in]  T1            data type of the device
 * \param[out] out_cbd       Tango attribute to update with the new value
 * \param[in]  in_method     Pointer to a get method to call
 * \param[in]  in_callerName Class and method name of the caller (for exception management)
 * \param[in]  in_is_enabled_during_running Can we read the attribut during running state ?
*********************************************************************************************/
template< typename T1>
void DanteDpp::read_dynamic_user_double_indexes_attribute(yat4tango::DynamicAttributeReadCallbackData& out_cbd,
                                                          T1 (DanteDpp_ns::Controller::*in_method_indexes)(std::size_t, std::size_t) const,
                                                          const std::string & in_callerName,
                                                          const bool in_is_enabled_during_running)
{
    T1 (DanteDpp_ns::Controller::*generic_method)(void) const = 
        reinterpret_cast<T1 (DanteDpp_ns::Controller::*)(void) const>(in_method_indexes);

    read_dynamic_user_attribute<T1>(out_cbd, generic_method, in_callerName, in_is_enabled_during_running);
}

/********************************************************************************************************
 * \brief Use the write dynamic attribut to set informations in the controller
 * \param[in]  T1                           TANGO data type (Tango::DevFloat for example)
 * \param[in]  T2                           data type of the device
 * \param[in]  in_cbd                       To get the new Tango value set by the user
 * \param[in]  in_method                    Pointer to a controller set method to call
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::write_dynamic_user_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                            void (DanteDpp_ns::Controller::*in_method)(const T2 &),
                                            const std::string & in_callerName)
{
    DEBUG_STREAM << in_callerName << " : " << in_cbd.dya->get_name() << endl;

    try
    {
        Tango::DevState state = is_device_initialized() ? Controller::get_instance()->get_state() : Tango::FAULT;

        if (state == Tango::FAULT || state == Tango::INIT || state == Tango::OFF || state == Tango::DISABLE)
        {
            std::string reason = "It is currently not allowed to write attribute ";
            reason += in_cbd.dya->get_name();

            Tango::Except::throw_exception("TANGO_DEVICE_ERROR" ,
                                           reason.c_str()       ,
                                           in_callerName.c_str());
        }

        //- be sure the pointer to the dyn. attr. is valid
        if(!in_cbd.dya)
        {
            THROW_DEVFAILED("INTERNAL_ERROR",
                            "unexpected NULL pointer to dynamic attribute",
                            in_callerName.c_str());
        }

        // get the write value
        T1 val ;
        in_cbd.tga->get_write_value(val);

        // write the value using the controller
        T2 data = val;

        // get the attribute value
        const UserData * temp = reinterpret_cast<UserData *>(in_cbd.dya->get_user_data<UserData>());

        // checking the number of indexes to recast the method pointer
        const std::size_t indexes_nb = temp->get_indexes_nb();

        // no index
        if(indexes_nb == 0)
        {
            // call the controller method
            (Controller::get_instance().get()->*in_method)(data);
        }
        else
        // one index
        if(indexes_nb == 1)
        {
            void (DanteDpp_ns::Controller::*method_index)(const T2 &, std::size_t) = 
            reinterpret_cast<void (DanteDpp_ns::Controller::*)(const T2 &, std::size_t)>(in_method);   

            // call the controller method with an index
            (Controller::get_instance().get()->*method_index)(data, temp->get_index(0));
        }
        else
        // two indexes
        {
            void (DanteDpp_ns::Controller::*method_indexes)(const T2 &, std::size_t, std::size_t) = 
            reinterpret_cast<void (DanteDpp_ns::Controller::*)(const T2 &, std::size_t, std::size_t)>(in_method);   

            // call the controller method with an index
            (Controller::get_instance().get()->*method_indexes)(data, temp->get_index(0), temp->get_index(1));
        }

        // memorize the value ?
        if(temp->has_memorized_name())
        {
            std::stringstream tempValue; // value to store
            tempValue  << data;

            // yes, memorize the value
            yat4tango::PropertyHelper::set_property(this, temp->get_memorized_name(), tempValue.str());
        }
    }
    catch (Tango::DevFailed& df)
    {
        manage_devfailed_exception(df, in_callerName);
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        manage_dante_exception(in_ex, in_callerName);
    }
    catch (...)
    {
        manage_unknown_exception(in_callerName);
    }
}

/********************************************************************************************************
 * \brief Use the write dynamic attribut (with index) to set informations in the controller
 * \param[in]  T1                           TANGO data type (Tango::DevFloat for example)
 * \param[in]  T2                           data type of the device
 * \param[in]  in_cbd                       To get the new Tango value set by the user
 * \param[in]  in_method                    Pointer to a controller set method to call
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::write_dynamic_user_index_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                                  void (DanteDpp_ns::Controller::*in_method_index)(const T2 &, std::size_t),
                                                  const std::string & in_callerName)
{
    void (DanteDpp_ns::Controller::*generic_method)(const T2 &) = 
        reinterpret_cast<void (DanteDpp_ns::Controller::*)(const T2 &)>(in_method_index);

    write_dynamic_user_attribute<T1, T2>(in_cbd, generic_method, in_callerName);
}

/********************************************************************************************************
 * \brief Use the write dynamic attribut (with double indexes) to set informations in the controller
 * \param[in]  T1                           TANGO data type (Tango::DevFloat for example)
 * \param[in]  T2                           data type of the device
 * \param[in]  in_cbd                       To get the new Tango value set by the user
 * \param[in]  in_method                    Pointer to a controller set method to call
 * \param[in]  in_callerName                Class and method name of the caller (for exception management)
*********************************************************************************************************/
template< typename T1, typename T2>
void DanteDpp::write_dynamic_user_double_indexes_attribute(yat4tango::DynamicAttributeWriteCallbackData & in_cbd,
                                                           void (DanteDpp_ns::Controller::*in_method_indexes)(const T2 &, std::size_t, std::size_t),
                                                           const std::string & in_callerName)
{
    void (DanteDpp_ns::Controller::*generic_method)(const T2 &) = 
        reinterpret_cast<void (DanteDpp_ns::Controller::*)(const T2 &)>(in_method_indexes);

    write_dynamic_user_attribute<T1, T2>(in_cbd, generic_method, in_callerName);
}

/**********************************************************************************************************************
 * \brief Use to update a dynamic attribute and the hardware with a property value
 * \param[in]  T1                TANGO data type for attribute and property variables (Tango::DevFloat for example)
 * \param[in]  in_dim            dynamics attributes manager which contains the attribute
 * \param[in]  in_attribute_name Name of the attribute linked to the property
 * \param[in]  in_write_method   tango write callback
**********************************************************************************************************************/
template< typename T1>
void DanteDpp::write_property_in_dynamic_user_attribute(yat4tango::DynamicInterfaceManager & in_dim,
                                                        const std::string & in_attribute_name,
                                                        void (DanteDpp_ns::DanteDpp::*in_write_method)(yat4tango::DynamicAttributeWriteCallbackData &))
{
    INFO_STREAM << "Write dynamic attribute at Init - " << in_attribute_name << "." << endl;

    // retrieve the attribute using its name
	Tango::WAttribute           & attribute      = dev_attr->get_w_attr_by_name(in_attribute_name.c_str());
    yat4tango::DynamicAttribute & dyn_attribute  = in_dim.dynamic_attributes_manager().get_attribute(in_attribute_name);

    yat4tango::DynamicAttributeWriteCallbackData cbd;
    cbd.tga = &attribute    ;
    cbd.dya = &dyn_attribute;

    // get the attribute user value
    const UserData * temp = reinterpret_cast<UserData *>(dyn_attribute.get_user_data<UserData>());

    // has a memorize value ?
    if(temp->has_memorized_name())
    {
        const std::string property_name = temp->get_memorized_name();

        // special case for dev string - we do not use the tango data type but a std::string instead
        if(dyn_attribute.get_tango_data_type() == Tango::DEV_STRING)
        {
            // retrieve the property value using its name
            std::string memorizedValue = yat4tango::PropertyHelper::get_property<std::string >(this, property_name.c_str());
            Tango::DevString * value;

            // create a temporary DevString and copy the value
            CREATE_DEVSTRING_ATTRIBUTE(value, temp->get_max_size());
            strcpy(*value, memorizedValue.c_str());

            // update the attribute
            attribute.set_write_value(*value);

            // call the write method of the dynamic attribute
            (this->*in_write_method)(cbd);

            // release the temporary DevString
            DELETE_DEVSTRING_ATTRIBUTE(value);
        }
        else
        {
            // retrieve the property value using its name
            T1 memorizedValue = yat4tango::PropertyHelper::get_property<T1>(this, property_name.c_str());

            // update the attribute
            attribute.set_write_value(memorizedValue);

            // call the write method of the dynamic attribute
            (this->*in_write_method)(cbd);
        }
    }
    else
    {
        INFO_STREAM << "No memorized property defined for dynamic attribute: " << in_attribute_name << "." << endl;
    }
}

#endif // DANTE_DPP_HPP_