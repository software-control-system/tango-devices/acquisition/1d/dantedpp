/*************************************************************************/
/*! 
 *  \file   ConfigView.h
 *  \brief  class ConfigView
 *  \author Cédric CASTEL
 */
/*************************************************************************/

#ifndef DANTE_DPP_CONFIG_VIEW_H
#define DANTE_DPP_CONFIG_VIEW_H

// TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>
#include <yat/utils/Callback.h>
#include <yat4tango/DynamicInterfaceManager.h>

// PROJECT
#include "ConfigViewParameters.h"
#include "Log.h"
#include "UserData.h"

namespace DanteDpp_ns
{

/*------------------------------------------------------------------
 *	class:	ConfigView
 *	description: base class for the data storage
 /------------------------------------------------------------------*/
class ConfigView
{
public:
    ConfigView(Tango::DeviceImpl *dev);
    ~ConfigView();

    // Create all dynamic attributes
    void create(const ConfigViewParameters & in_parameters);

    // init all dynamic write attributes
    void init();

private:
    // build a dynamic attribute for a channel index
    std::string build_channel_attribute_name(const std::string & in_label, const std::size_t in_channel_index) const;

    // To remove the dynamics attributes from config file
    void remove_dynamic_attributes();

protected:
    /// Owner Device server object
    Tango::DeviceImpl* m_device;

    // common data
    std::vector<DoubleUserData *>        m_dyn_realtimes;
    std::vector<DoubleUserData *>        m_dyn_livetimes;
    std::vector<DoubleUserData *>        m_dyn_deadtimes;
    std::vector<DoubleUserData *>        m_dyn_input_count_rates;
    std::vector<DoubleUserData *>        m_dyn_output_count_rates;
    std::vector<ULongUserData *>         m_dyn_events_in_runs;
    std::vector<VectorULongUserData *>   m_dyn_channels;
    
    // MCA & MAPPING specific data
    StringUserData * m_dyn_trigger_mode ;
	ULongUserData  * m_dyn_nb_pixels    ;
    ULongUserData  * m_dyn_current_pixel;
    DoubleUserData * m_dyn_preset_value ;
    StringUserData * m_dyn_preset_type  ;
	ULongUserData  * m_dyn_sp_time      ;

    // keep the create parameters
    ConfigViewParameters m_create_parameters;

    // dynamics attributes
    yat4tango::DynamicInterfaceManager m_dim; // dynamics attributes from configuration file
};

} // namespace 

#endif // DANTE_DPP_CONFIG_VIEW_H


