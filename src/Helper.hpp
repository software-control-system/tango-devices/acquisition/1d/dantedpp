/*************************************************************************/
/*! 
 *  \file   Helper.hpp
 *  \brief  class used as a tool class (mostly conversions)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_HELPER_HPP_
#define DANTE_DPP_HELPER_HPP_

//- TANGO
#include <tango.h>

//============================================================================================================
// TEMPLATE IMPLEMENTATION
//============================================================================================================
/*******************************************************************
 * \brief Convert a tango type label to a type enum
 * \param[in] T1 type enum
 * \param[in] in_tango_label Tango label to convert
 * \param[in] in_tango_labels Tango labels container 
 * \param[in] in_type_values type values container
 * \param[in] in_type_info type label used for logging
 * \return the converted type enum
*******************************************************************/
template< typename T1>
T1 Helper::convert_tango_label_to_type(const std::string              & in_tango_label ,
                                       const std::vector<std::string> & in_tango_labels,
                                       const std::vector<T1>          & in_type_values ,
                                       const std::string              & in_type_info   )
{
    T1     enum_type = static_cast<T1>(0); // to avoid the warning "�enum_type� may be used uninitialized in this function" 
    string tango_label = in_tango_label;

    // upper the tango label
    std::transform(tango_label.begin(), tango_label.end(), tango_label.begin(), ::toupper);

    // searching the label
    const std::vector<std::string>::const_iterator 
    iterator = find(in_tango_labels.begin(), in_tango_labels.end(), tango_label);

    // found it
    if (iterator != in_tango_labels.end()) 
    {
        enum_type = in_type_values[iterator - in_tango_labels.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        std::ostringstream MsgMethodName;

        MsgMethodName << "Helper::convert_tango_" << in_type_info << "_to_" << in_type_info << "()";
        MsgErr        << "Incorrect " << in_type_info << " : " << tango_label << std::endl;

        if(in_tango_labels.size() > 1)
        {
            MsgErr << "Available values for " << in_type_info << " are:" << endl;
        }
        else
        {
            MsgErr << "Available value for " << in_type_info << " is only:" << endl;
        }

        for(size_t index = 0 ; index < in_tango_labels.size() ; index++)
        {
            MsgErr << in_tango_labels[index] << std::endl;
        }

        Tango::Except::throw_exception("TANGO_DEVICE_ERROR", MsgErr.str().c_str(), MsgMethodName.str().c_str());
    }

    return enum_type;
}

/*******************************************************************
 * \brief Convert a type enum to a tango label
 * \param[in] T1 type enum
 * \param[in] in_enum_type type enum
 * \param[in] in_tango_labels Tango labels container 
 * \param[in] in_type_values type values container
 * \param[in] in_type_info type label used for logging
 * \return a tango label
*******************************************************************/
template< typename T1>
std::string Helper::convert_type_to_tango_label(const T1                       & in_enum_type   ,
                                                const std::vector<std::string> & in_tango_labels,
                                                const std::vector<T1>          & in_type_values ,
                                                const std::string              & in_type_info   )
{
    string tango_label;

    // we need to convert the type to a tango label
    // search the type in a container
    const typename std::vector<T1>::const_iterator 
    iterator = find(in_type_values.begin(), in_type_values.end(), in_enum_type);

    // found it
    if (iterator != in_type_values.end()) 
    {
        tango_label = in_tango_labels[iterator - in_type_values.begin()]; // calculation gives the index
    }
    else
    {
        std::ostringstream MsgErr;
        std::ostringstream MsgMethodName;

        MsgErr        << "Impossible to found the " << in_type_info << " " << in_enum_type << std::endl;
        MsgMethodName << "Helper::convert_" << in_type_info << "_to_tango_" << in_type_info << "()";

        Tango::Except::throw_exception("LOGIC_ERROR", MsgErr.str().c_str(), MsgMethodName.str().c_str());
    }

    return tango_label;
}


#endif // DANTE_DPP_HELPER_HPP_

//###########################################################################
