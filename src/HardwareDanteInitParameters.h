/*************************************************************************/
/*! 
 *  \file   HardwareDanteInitParameters.h
 *  \brief  class used to manage the HardwareDante init parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_HARDWARE_DANTE_INIT_PARAMETERS_H_
#define DANTE_DPP_HARDWARE_DANTE_INIT_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the HardwareDante initialization*/
class HardwareDanteInitParameters
{
    friend class HardwareDante;

    public:
        // detector type values
        typedef enum 
        {
            Dante = 0,
            Simulator,
        } Type;

        // simple constructor
        HardwareDanteInitParameters();
        
        // constructor
        HardwareDanteInitParameters(HardwareDanteInitParameters::Type in_type);

    private :
        HardwareDanteInitParameters::Type m_type; // type of HardwareDante
    };

} // namespace DanteDpp_ns

#endif // DANTE_DPP_HARDWARE_DANTE_INIT_PARAMETERS_H_

//###########################################################################
