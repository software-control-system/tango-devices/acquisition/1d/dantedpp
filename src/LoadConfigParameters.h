/*************************************************************************/
/*! 
 *  \file   LoadConfigParameters.h
 *  \brief  class used to setup a load configuration command
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_LOAD_CONFIG_PARAMETERS_H_
#define DANTE_DPP_LOAD_CONFIG_PARAMETERS_H_

//TANGO
#include <tango.h>

// LOCAL

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for a load configuration command*/
class LoadConfigParameters
{
    friend class Controller;

public:
    // constructor
    LoadConfigParameters();

private :
    std::string m_config_alias      ; 
    std::string m_complete_file_path; 
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_LOAD_CONFIG_PARAMETERS_H_

//###########################################################################
