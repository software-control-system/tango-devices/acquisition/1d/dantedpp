/*************************************************************************/
/*! 
 *  \file   StreamLog.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "StreamLog.h"
#include "IHardware.h"
#include "RoisManager.h"

namespace DanteDpp_ns
{
/*******************************************************************
 * \brief constructor
 *******************************************************************/
StreamLog::StreamLog()
{
    INFO_STRM << "construction of StreamLog" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamLog::~StreamLog() 
{
    INFO_STRM << "destruction of StreamLog" << std::endl;
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_module_data module data to be treated
 *******************************************************************/
void StreamLog::update(yat::SharedPtr<const ModuleData> in_module_data)
{
    DEBUG_STRM << "===============================" << std::endl;
    DEBUG_STRM << "StreamLog::update - received data (" << in_module_data->get_index() << ")" << std::endl;

    std::size_t channels_nb = IHardware::get_const_instance()->get_channels_nb();

    for (std::size_t channels_index = 0 ; channels_index < channels_nb ; channels_index++)
    {
        const std::size_t rois_nb_for_channel = RoisManager::get_const_instance()->get_nb_rois(channels_index);
        yat::SharedPtr<const ChannelData> channel_data = in_module_data->get_channel(channels_index);

        INFO_STRM << "-------------------------------" << std::endl;
        INFO_STRM << "channel data ("     << channel_data->get_index() << ")"      << std::endl;
        INFO_STRM << "real time: "        << channel_data->get_real_time()         << std::endl;
        INFO_STRM << "live time: "        << channel_data->get_live_time()         << std::endl;
        INFO_STRM << "dead time: "        << channel_data->get_dead_time()         << std::endl;
        INFO_STRM << "input count rate: " << channel_data->get_input_count_rate()  << std::endl;
        INFO_STRM << "ouput count rate: " << channel_data->get_output_count_rate() << std::endl;
        INFO_STRM << "events in run: "    << channel_data->get_events_in_run()     << std::endl;
        INFO_STRM << "spectrum size: "    << channel_data->get_spectrum().size()   << std::endl;

        if(rois_nb_for_channel > 0)
        {
            for (std::size_t roi_index = 0 ; roi_index < rois_nb_for_channel ; roi_index++)
            {
                INFO_STRM << "roi(" << roi_index << "): " << channel_data->get_rois(roi_index) << std::endl;
            }
        }
    }
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamLog::reset_index(void)
{
}

//###########################################################################
}