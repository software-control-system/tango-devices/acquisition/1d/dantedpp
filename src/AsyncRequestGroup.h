/*************************************************************************/
/*! 
 *  \file   AsyncRequestGroup.h
 *  \brief  class used to manage the AsyncRequest container
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_ASYNC_REQUEST_GROUP_H_
#define DANTE_DPP_ASYNC_REQUEST_GROUP_H_

//TANGO
#include <tango.h>

// SYSTEM
#include <vector>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>
#include <yat/threading/Condition.h>
#include <yat/memory/SharedPtr.h>

// PROJECT
#include "Log.h"
#include "AsyncRequest.h"

/*************************************************************************/
// define this to activate the debug logs of async management
//#define DANTE_DPP_DEBUG_ASYNC_MANAGEMENT

namespace DanteDpp_ns
{
/*************************************************************************/
class AsyncRequestGroup
{
    public:
        // constructor
        AsyncRequestGroup(std::size_t in_max_nb_replies);

        // add a new request 
        void add_request(uint32_t in_request_id);

        // manage the reply of a request
        void manage_reply(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data);

        // get the number of requests 
        std::size_t get_requests_nb();

        // get a request 
        yat::SharedPtr<AsyncRequest> get_request(std::size_t in_request_index);

        // manage the wait of all replies
        bool waiting_all_replies() const;

        // check if all replies were received
        bool all_replies_received() const;

    private:
        // search a request 
        yat::SharedPtr<AsyncRequest> search_request(uint32_t in_request_id);

    private:
      /** container used to store the requests in the creation order
        */
        std::vector<yat::SharedPtr<AsyncRequest>> m_requests;

      /** current number of replies for the requests
        */
        std::size_t m_nb_replies;

      /** maximum number of replies for the requests
        */
        std::size_t m_max_nb_replies;

      /** mutex used to protect the multithread access to m_requests & m_nb_replies.
        * The mutable keyword is used to allow using this mutex into const methods.
        */
        mutable yat::Mutex m_mutex;

      /** condition variable used to wake up the thread which waits for the reception of all replies.
        */
        mutable yat::Condition m_condition_is_not_full;

      /** mutex associated with m_condition_is_not_full.
        */
        mutable yat::Mutex m_mutex_is_not_full;

};

/***********************************************************************
 * AsyncRequestGroup containers types
 ***********************************************************************/
/**
 * \typedef AsyncRequestGroupMap
 * \brief type of container which contains a map of async replies
 *        all the replies'identifiers 
 */
typedef std::map<uint32_t, yat::SharedPtr<AsyncRequestGroup>> AsyncRequestGroupMap;

} // namespace DanteDpp_ns

#endif // DANTE_DPP_ASYNC_REQUEST_GROUP_H_

//###########################################################################
