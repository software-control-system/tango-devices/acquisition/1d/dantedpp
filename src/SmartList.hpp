/**
 *  \file SmartList.hpp
 *  \brief implementation file of SmartList template class
 *         Should not be included, use only SmartList.h as include file.
 *  \author C�dric Castel
 *  \version 0.1
 *  \date November 29 2019
 *  Created on: November 07 2019
 */

// DEBUG DEFINES
#define SMARTLIST_DEBUG_ACTORS

/**
  *  SmartList::SmartList
  */
template <class Elem>
SmartList<Elem>::SmartList(std::string in_name                    , 
                           std::size_t in_max_elements_to_treat_nb,
                           std::size_t in_element_size_in_bytes   ) : m_condition_is_not_empty(m_mutex_is_not_empty)
{
    // storing the name of the instance (for stats)
    m_name = in_name;

    // storing the size of an elements for allowing memory stats
    m_element_size_in_bytes = in_element_size_in_bytes;

    // initing the elements counters
    m_max_in_elements_nb       = 0;
    m_total_elements_nb        = 0;
    m_max_elements_to_treat_nb = in_max_elements_to_treat_nb;

    // initing the elapsed times
    completely_filled_elapsed_time_ms  = 0.0;
    completely_emptied_elapsed_time_ms = 0.0;

    // initing the number of actors
    m_producers_nb = 0;
    m_customers_nb = 0;

    // valid at start
    m_valid_tag = true;
}

/**
  *  SmartList::~SmartList
  */
template <class Elem>
SmartList<Elem>::~SmartList()
{
}

/**
  *  SmartList::front
  */
template <class Elem>
yat::SharedPtr<const Elem> SmartList<Elem>::front(bool in_throw_empty_list_exception) const
{
    yat::SharedPtr<const Elem> element;

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // checking if the container is empty
    if(m_elements.empty())
    {
        if(in_throw_empty_list_exception)
        {
            std::stringstream temp_stream;
            temp_stream << "cannot access an element of container named " << m_name << " because it is empty!";
            std::string error_text = temp_stream.str();

            throw DanteDpp_ns::Exception("Container Management", error_text.c_str(), "SmartList::front (const)");
        }
    }
    else
    {
        // getting access to the first element of the container (should increase the smart pointeur counter)
        element = m_elements.front();
    }

    return element;
}

/**
  *  SmartList::front
  */
template <class Elem>
yat::SharedPtr<Elem> SmartList<Elem>::front(bool in_throw_empty_list_exception)
{
    yat::SharedPtr<Elem> element;

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // checking if the container is empty
    if(m_elements.empty())
    {
        if(in_throw_empty_list_exception)
        {
            std::stringstream temp_stream;
            temp_stream << "cannot access an element of container named " << m_name << " because it is empty!";
            std::string error_text = temp_stream.str();

            throw DanteDpp_ns::Exception("Container Management", error_text.c_str(), "SmartList::front");
        }
    }
    else
    {
        // getting access to the first element of the container (should increase the smart pointeur counter)
        element = m_elements.front();
    }

    return element;
}

/**
  *  SmartList::remove_front
  */    
template <class Elem>
void SmartList<Elem>::remove_front(bool in_throw_empty_list_exception)
{
    take(in_throw_empty_list_exception);
}

/**
  *  SmartList::take
  */    

template <class Elem>
yat::SharedPtr<Elem> SmartList<Elem>::take(bool in_throw_empty_list_exception)
{
    yat::SharedPtr<Elem> element;

    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // checking if the container is empty
    if(m_elements.empty())
    {
        if(in_throw_empty_list_exception)
        {
            std::stringstream temp_stream;
            temp_stream << "cannot take an element of container named " << m_name << " because it is empty!";
            std::string error_text = temp_stream.str();

            throw DanteDpp_ns::Exception("Container Management", error_text.c_str(), "SmartList::take");
        }
    }
    else
    // the container is not empty
    {
        // getting the first element of the container
        element = m_elements.front();
        m_elements.pop();

        // timer management if the container becomes "completely emptied"
        if(completely_emptied())
        {
            completely_emptied_elapsed_time_ms = completely_emptied_timer.elapsed_msec();
        }
    }

    return element;
}

/**
  *  SmartList::put_back
  */
template <class Elem>
void SmartList<Elem>::put(yat::SharedPtr<Elem> in_element)
{
    bool was_empty;

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_mutex);

        was_empty = m_elements.empty();

        // putting the element at the end of the container
        m_elements.push(in_element);

        // computing the elements counters
        size_t current_in_elements_nb  = m_elements.size();

        if(m_max_in_elements_nb < current_in_elements_nb)
            m_max_in_elements_nb = current_in_elements_nb;

        m_total_elements_nb++; // one more element inserted into the container

        // timer management if the container becomes "completely filled"
        if(completely_filled())
        {
            completely_filled_elapsed_time_ms = completely_filled_timer.elapsed_msec();
        }
    }

    // unblocking threads if no more empty
    if(was_empty)
    {
        yat::AutoMutex<> lock(m_mutex_is_not_empty);
        m_condition_is_not_empty.broadcast();
    }
}

/**
  *  SmartList::size
  */
template <class Elem>
std::size_t SmartList<Elem>::size() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // When the container is invalid, size method returns always 0 to stop some treatments which run in loop.
    return ((m_valid_tag) ? m_elements.size() : 0);
}

/**
  *  SmartList::empty
  */
template <class Elem>
bool SmartList<Elem>::empty() const
{
    return (size() == 0);
}

/**
  *  SmartList::get_elements_nb
  */
template <class Elem>
std::size_t SmartList<Elem>::get_elements_nb() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // When the container is invalid, this method returns the real number of elements.
    return m_elements.size();
}

/**
  *  SmartList::get_total_elements_nb
  */
template <class Elem>
std::size_t SmartList<Elem>::get_total_elements_nb() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return m_total_elements_nb;
}

/**
  *  SmartList::get_max_elements_to_treat_nb
  */
template <class Elem>
std::size_t SmartList<Elem>::get_max_elements_to_treat_nb() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return m_max_elements_to_treat_nb;
}

/**
  *  SmartList::completely_filled
  */
template <class Elem>
bool SmartList<Elem>::completely_filled() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return ((m_max_elements_to_treat_nb) && (m_total_elements_nb == m_max_elements_to_treat_nb));
}

/**
  *  SmartList::completely_emptied
  */
template <class Elem>
bool SmartList<Elem>::completely_emptied() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return (empty() && completely_filled());
}

/**
  *  SmartList::last_element_to_treat
  */
template <class Elem>
bool SmartList<Elem>::last_element_to_treat() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return ((size() == 1) && completely_filled());
}

/**
  *  SmartList::invalidate
  */
template <class Elem>
void SmartList<Elem>::invalidate()
{
    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_mutex);
        m_valid_tag = false;
    }

    // unblocking threads 
    {
        yat::AutoMutex<> lock(m_mutex_is_not_empty);
        m_condition_is_not_empty.broadcast();
    }
}

/**
  *  SmartList::is_valid
  */
template <class Elem>
bool SmartList<Elem>::is_valid() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return m_valid_tag;
}

/**
  *  SmartList::waiting_while_empty
  */
template <class Elem>
void SmartList<Elem>::waiting_while_empty() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex_is_not_empty);

    // is the container is invalid or not empty, do not block.
    // if all the elements were inserted and treated, do not block.
    if(m_valid_tag && empty() && (!completely_emptied()))
    {
        m_condition_is_not_empty.wait();
    }
}

/**
  *  SmartList::build_actor_list
  */
template <class Elem>
std::string SmartList<Elem>::build_actor_list(const std::set<std::string> & in_container) const
{
    std::string result;
    std::set<std::string>::const_iterator it;

    if(in_container.empty())
    {
        result = "[EMPTY]";
    }
    else
    {
        for (it = in_container.begin(); it != in_container.end(); ++it) 
        {
            if(it != in_container.begin()) result += " ";
            result += "[" + *it + "]";
        }
    }

    return result;
}

/**
  *  SmartList::register_producer
  */
template <class Elem>
void SmartList<Elem>::register_producer(const std::string & in_producer)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // try to insert and check if the producer was already registered
    std::pair<std::set<std::string>::iterator,bool> already_here;

    already_here = m_producers.insert(in_producer);

    // error : producer already registered
    if (already_here.second==false)
    {
        std::set<std::string>::iterator it;
        it = already_here.first;

        ERROR_STRM << "try to register a known producer (" << (*it) << ")" 
                   << " for SmartList " << m_name << std::endl;
    }    
    else
    // ok : new producer
    {
        #ifdef SMARTLIST_DEBUG_ACTORS
            INFO_STRM << "registering a producer (" << in_producer << ")" 
                      << " for SmartList " << m_name << std::endl;
        #endif

        m_producers_nb++; // one more

        // historization of the producer
        m_producers_historic.insert(in_producer);
    }
}

/**
  *  SmartList::unregister_producer
  */
template <class Elem>
void SmartList<Elem>::unregister_producer(const std::string & in_producer)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    if(m_producers_nb > 0)
    {
        std::size_t nb = m_producers.erase(in_producer);
        
        if(nb)
        {
        #ifdef SMARTLIST_DEBUG_ACTORS
            INFO_STRM << "unregistering the producer (" << in_producer << ")" 
                      << " for SmartList " << m_name << std::endl;
        #endif

            m_producers_nb--; // one less

            // is this an error ? no more producers and container not completely filled 
            if((m_valid_tag) && (m_producers_nb == 0) && (!completely_filled()))
            {
                ERROR_STRM << "error - needs more producers for SmartList " << m_name << std::endl;
                invalidate();
            }
        }
        else
        {
            ERROR_STRM << "try to unregister an unknown producer (" << in_producer << ")" 
                       << " for SmartList " << m_name << std::endl;
        }
    }
    else
    {
        ERROR_STRM << "no more producers to unregister (" << in_producer << ")"
                   << " in the SmartList " << m_name << std::endl;
    }
}

/**
  *  SmartList::register_customer
  */
template <class Elem>
void SmartList<Elem>::register_customer(const std::string & in_customer)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // try to insert and check if the customer was already registered
    std::pair<std::set<std::string>::iterator,bool> already_here;

    already_here = m_customers.insert(in_customer);

    // error : customer already registered
    if (already_here.second==false)
    {
        std::set<std::string>::iterator it;
        it = already_here.first;

        ERROR_STRM << "try to register an known customer (" << (*it) << ")" 
                   << " for SmartList " << m_name << std::endl;
    }    
    else
    // ok : new customer
    {
        #ifdef SMARTLIST_DEBUG_ACTORS
            INFO_STRM << "registering a customer (" << in_customer << ")" 
                      << " for SmartList " << m_name << std::endl;
        #endif

        m_customers_nb++; // one more

        // historization of the customer
        m_customers_historic.insert(in_customer);
    }
}

/**
  *  SmartList::unregister_customer
  */
template <class Elem>
void SmartList<Elem>::unregister_customer(const std::string & in_customer)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    if(m_customers_nb > 0)
    {
        std::size_t nb = m_customers.erase(in_customer);
        
        if(nb)
        {
        #ifdef SMARTLIST_DEBUG_ACTORS
            INFO_STRM << "unregistering the customer (" << in_customer << ")" 
                      << " for SmartList " << m_name << std::endl;
        #endif

            m_customers_nb--; // one less

            // is this an error ? no more customers and container not completely emptied
            if((m_valid_tag) && (m_customers_nb == 0) && (!completely_emptied()))
            {
                ERROR_STRM << "error - needs more customers for SmartList " << m_name << std::endl;
                invalidate();
            }
        }
        else
        {
            ERROR_STRM << "try to unregister an unknown customer (" << in_customer << ")" 
                       << " for SmartList " << m_name << std::endl;
        }
    }
    else
    {
        ERROR_STRM << "no more customers to unregister (" << in_customer << ")"
                   << " in the SmartList " << m_name << std::endl;
    }
}

/**
  *  SmartList::is_useless
  */
template <class Elem>
bool SmartList<Elem>::is_useless() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);
    return ((!m_producers_nb) && (!m_customers_nb));
}

/**
  *  SmartList::start_timers
  */
template <class Elem>
void SmartList<Elem>::start_timers()
{
    completely_filled_timer.restart ();
    completely_emptied_timer.restart();
}

/**
 *  SmartList::get_max_in_elements_nb
 */
template <class Elem>
std::size_t SmartList<Elem>::get_max_in_elements_nb() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return m_max_in_elements_nb;
}

/**
 *  SmartList::completely_filled_elapsed_time_ms
 */
template <class Elem>
double SmartList<Elem>::get_completely_filled_elapsed_time_ms() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return completely_filled_elapsed_time_ms;
}

/**
 *  SmartList::get_completely_emptied_elapsed_time_ms
 */
template <class Elem>
double SmartList<Elem>::get_completely_emptied_elapsed_time_ms() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return completely_emptied_elapsed_time_ms;
}

/**
  *  SmartList::log_stats
  */
template <class Elem>
void SmartList<Elem>::log_stats() const
{
    INFO_STRM << "-----------------------------------------------------" << std::endl;
    INFO_STRM << " Execution stats for SmartList " << m_name << std::endl;
    INFO_STRM << "-----------------------------------------------------" << std::endl;

    INFO_STRM << "maximum number of elements to be treated = " << get_max_elements_to_treat_nb() << std::endl;
    INFO_STRM << "maximum number of elements inside of the container = " << get_max_in_elements_nb() << std::endl;
    INFO_STRM << "total number of elements treated = " << get_total_elements_nb() << std::endl;
    INFO_STRM << "current number of elements = " << get_elements_nb() << std::endl;
    INFO_STRM << "current number of producers (should be 0) = " << m_producers_nb << std::endl;
    INFO_STRM << "current producers (should be EMPTY) = " << build_actor_list(m_producers) << std::endl;
    INFO_STRM << "historic of producers = " << build_actor_list(m_producers_historic) << std::endl;
    INFO_STRM << "current number of customers (should be 0) = " << m_customers_nb << std::endl;
    INFO_STRM << "current customers (should be EMPTY) = " << build_actor_list(m_customers) << std::endl;
    INFO_STRM << "historic of customers = " << build_actor_list(m_customers_historic) << std::endl;
    INFO_STRM << "completely filled = " << completely_filled() << std::endl;
    INFO_STRM << "completely emptied = " << completely_emptied() << std::endl;
    INFO_STRM << "completely filled elapsed time = " << get_completely_filled_elapsed_time_ms() << " ms" << std::endl;
    INFO_STRM << "completely emptied elapsed time = " << get_completely_emptied_elapsed_time_ms() << " ms" << std::endl;
    INFO_STRM << "is valid = " << is_valid() << std::endl;
    INFO_STRM << "is useless = " << is_useless() << std::endl;

    double memory_used = static_cast<double>(get_max_in_elements_nb() * m_element_size_in_bytes);

    std::stringstream temp_stream;

    temp_stream << std::fixed
                << "memory used by elements = " 
                << memory_used/1024.0/1024.0/1024.0 << " GB, "
                << memory_used/1024.0/1024.0 << " MB, "
                << memory_used/1024.0 << " KB, "
                << memory_used << " B";

    INFO_STRM << temp_stream.str() << std::endl;
}

