/*************************************************************************/
/*! 
 *  \file   ChannelData.h
 *  \brief  class used to store a channel acquisition
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_CHANNEL_DATA_H_
#define DANTE_DPP_CHANNEL_DATA_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>

namespace DanteDpp_ns
{
/*************************************************************************/
class ChannelData
{
public:
    // constructor
    ChannelData(std::size_t in_index, std::size_t in_bins_nb, std::size_t in_rois_nb);

    // get the index of the acquisition data
    std::size_t get_index() const;

    // get the real time value
    double get_real_time() const;

    // set the real time value
    void set_real_time(const double in_real_time);

    // get the live time value
    double get_live_time() const;

    // set the live time value
    void set_live_time(const double in_live_time);

    // get the dead time value
    double get_dead_time() const;

    // set the dead time value
    void set_dead_time(const double in_dead_time);

    // get the input count rate value
    double get_input_count_rate() const;

    // set the input count rate value
    void set_input_count_rate(const double in_input_count_rate);

    // get the output count rate value
    double get_output_count_rate() const;

    // set the output count rate value
    void set_output_count_rate(const double in_output_count_rate);

    // get the event in run value
    uint32_t get_events_in_run() const;

    // set the event in run value
    void set_events_in_run(const uint32_t in_events_in_run);

    // get read-only access to the spectrum 
    const std::vector<uint32_t> & get_spectrum() const;

    // copy spectrum data
    void copy_to_spectrum(const uint64_t * in_source_spectrum,
                          std::size_t      in_dest_bin_index ,
                          std::size_t      in_copy_bins_nb   );

    // get read-only access to the rois values 
    const std::vector<uint32_t> & get_rois() const;

    // get a ROI value
    uint32_t get_rois(std::size_t in_roi_index) const;

    // compute a roi value
    void compute_roi(std::size_t in_roi_index,
                     std::size_t in_bin_index,
                     std::size_t in_bin_size );

    // compute the size of several instances (used for stats)
    static std::size_t compute_size_in_bytes(std::size_t in_channels_nb, 
                                             std::size_t in_bins_nb    , 
                                             std::size_t in_rois_nb    );

private :
    std::size_t           m_index            ;
    std::vector<uint32_t> m_spectrum         ;
    double                m_real_time        ;
    double                m_live_time        ;
    double                m_dead_time        ; 
    double                m_input_count_rate ;
    double                m_output_count_rate;
    uint32_t              m_events_in_run    ;
    std::vector<uint32_t> m_rois             ;
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_CHANNEL_DATA_H_

//###########################################################################
