/*************************************************************************/
/*! 
 *  \file   StreamCsv.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_STREAM_CSV_H_
#define DANTE_DPP_STREAM_CSV_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Stream.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class StreamCsv : public Stream
{
    // we need to gain access to the contructor
    friend class Stream;

    public:
        // Update the stream with new acquisition data
        virtual void update(yat::SharedPtr<const ModuleData> in_module_data);

        // Reset the file index
        virtual void reset_index(void);

    protected:
        // destructor
        virtual ~StreamCsv();

    private:
        // constructor
        StreamCsv();

    private:
        int m_file_index; // index used to build csv files names
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_STREAM_CSV_H_

//###########################################################################
