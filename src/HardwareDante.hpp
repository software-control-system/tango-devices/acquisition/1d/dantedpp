/********************************************************************************/
/*! 
 *  \file   HardwareDante.hpp
 *  \brief  implementation file of template methods for HardwareDante class
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 *  Should not be included, use only HardwareDante.h as include file.
*/
/********************************************************************************/

#ifndef DANTE_DPP_HARDWARE_DANTE_HPP
#define DANTE_DPP_HARDWARE_DANTE_HPP

// SYSTEM
#include <cstdlib>
#include <sstream> // std::istringstream

// PROJECT
#include "Log.h"

//============================================================================================================
// TEMPLATE IMPLEMENTATION
//============================================================================================================
/*******************************************************************
 * \brief get a key value (of the current section) in the configuration file
 * \param[in] T1 type enum
 * \param[in] in_cfg_file configuration file in memory
 * \param[in] in_key_name name of key to read
 * \param[out] out_value variable to fill with key value
 *******************************************************************/
template< typename T1>
void HardwareDante::get_key_value(yat::CfgFile & in_cfg_file, const std::string & in_key_name, T1 & out_value) const
{
    if(in_cfg_file.has_parameter(in_key_name))
    {
        std::string value = in_cfg_file.get_param_value(in_key_name, false);

        if(!value.empty())
        {
            try
            {
                std::istringstream ss(value);
                ss >> out_value;

                if (ss.fail() || (ss.rdbuf()->in_avail() > 0))
                {
                    ss.clear();
                    throw DanteDpp_ns::Exception();
                }
                else
                {
                    INFO_STRM << "key: " << in_key_name << " value: " << out_value << std::endl;
                }
            }
            catch(...)
            {
                std::ostringstream error_text;
                error_text << "Incorrect value: " << value << " for key: " << in_key_name;
                throw DanteDpp_ns::Exception("LOAD_CONFIG_ERROR", error_text.str(), "HardwareDante::get_key_value");
            }
        }
        else
        {
            DEBUG_STRM << "No value found for key: " << in_key_name << ". We will use default value [" << out_value << "]" << std::endl;
        }
    }
    else
    {
        DEBUG_STRM << "Could not found key: " << in_key_name << ". We will use default value [" << out_value << "]" << std::endl;
    }
}

/*******************************************************************
 * \brief get a key value enum (of the current section) in the configuration file
 * \param[in]  T1 type enum
 * \param[in]  in_cfg_file configuration file in memory
 * \param[in]  in_key_name name of key to read
 * \param[out] out_value variable to fill with key value
 * \param[in]  in_labels container for the labels
 * \param[in]  in_values container for the values for each label
 ******************************************************************/
template< typename T1>
void HardwareDante::get_key_value_enum(yat::CfgFile                   & in_cfg_file, 
                                       const std::string              & in_key_name,
                                       T1                             & out_value  ,
                                       const std::vector<std::string> & in_labels  ,
                                       const std::vector<T1>          & in_values  ) const
{
    std::string temp_value;

    {
        T1 default_value = out_value;

        // searching the label which correspond to the default value
        typedef typename std::vector<T1>::const_iterator itType;

        itType iterator = find(in_values.begin(), in_values.end(), default_value);

        // found it
        if (iterator != in_values.end()) 
        {
            temp_value = in_labels[iterator - in_values.begin()]; // calculation gives the index
        }
        else
        {
            std::ostringstream error_text;
            error_text << "Could not find the label for value [" << out_value << "] for the key: " << in_key_name;
            throw DanteDpp_ns::Exception("LOAD_CONFIG_ERROR", error_text.str(), "HardwareDante::get_key_value_enum");
        }
    }
    
    get_key_value(in_cfg_file, in_key_name, temp_value);

    {
        // searching the label
        const std::vector<std::string>::const_iterator iterator 
            = find(in_labels.begin(), in_labels.end(), temp_value);

        // found it
        if (iterator != in_labels.end()) 
        {
            out_value = in_values[iterator - in_labels.begin()]; // calculation gives the index
        }
        else
        {
            std::ostringstream error_text;
            error_text << "Incorrect value [" << temp_value << "] for the key: " << in_key_name;
            throw DanteDpp_ns::Exception("LOAD_CONFIG_ERROR", error_text.str(), "HardwareDante::get_key_value_enum");
        }
    }
}

#endif // DANTE_DPP_HARDWARE_DANTE_HPP
/*************************************************************************/