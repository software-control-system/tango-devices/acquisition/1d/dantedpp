/*************************************************************************/
/*! 
 *  \file   SyncThread.cpp
 *  \brief  DANTE detector thread class implementation
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"
#include "SyncThread.h"

namespace DanteDpp_ns
{

/*******************************************************************
 * \brief constructor
 * \param[in] in_name thread name (for logging)
*******************************************************************/
// m_start_barrier(2) : there are two involved threads : 
// - the main thread (call to start method by Frames instance)
// - the SyncThread thread.
SyncThread::SyncThread(const std::string in_name) : yat::Thread(), m_start_barrier(2) 
{
    m_name                 = in_name;
    m_verbose_log          = true   ;
    m_go_on                = false  ;
    m_treatment_is_running = true   ;

    INFO_STRM << "entering " << m_name << "..." << std::endl;
}

/*******************************************************************
 * \brief constructor
 * \param[in] in_name thread name (for logging)
 * \param[in] in_verbose_log  used to log the thread activity (not for a periodic thread)
*******************************************************************/
// m_start_barrier(2) : there are two involved threads : 
// - the main thread (call to start method by Frames instance)
// - the SyncThread thread.
SyncThread::SyncThread(const std::string in_name, bool in_verbose_log) : yat::Thread(), m_start_barrier(2) 
{
    m_name                 = in_name;
    m_verbose_log          = in_verbose_log;
    m_go_on                = false;
    m_treatment_is_running = true ;

    if(m_verbose_log)
    {
        INFO_STRM << "entering " << m_name << "..." << std::endl;
    }
}

/*******************************************************************
 * \brief destructor
*******************************************************************/
SyncThread::~SyncThread()
{
    if(m_verbose_log)
    {
        INFO_STRM << "leaving " << m_name << "..." << std::endl;
    }
}

/*******************************************************************
 * \brief starting thread and waiting for synchronization
*******************************************************************/
void SyncThread::start()
{
    m_go_on = true;
    start_undetached();
    m_start_barrier.wait();
}

/*******************************************************************
 * \brief stops the thread treatment but does not wait for
*         thread end (asynchronous stop).
*******************************************************************/
void SyncThread::stop()
{
    if(m_go_on)
    {
        // forcing the thread to exit
        if(m_verbose_log)
        {
            INFO_STRM << "forcing " << m_name << " to exit." << std::endl;
        }
        m_go_on = false;
    }
}

/*******************************************************************
 * \brief stops the thread treatment and waits the thread end 
 *        (synchronous stop).
*******************************************************************/
void SyncThread::exit()
{
    std::string name = m_name; // we keep the thread name because this member will not be useable after the join

    stop();
    yat::Thread::IOArg ioa = 0;

    if(m_verbose_log)
    {
        INFO_STRM << "waiting the exit of " << name << "." << std::endl;
    }

    join(&ioa);

    if(m_verbose_log)
    {
        INFO_STRM << name << " exited." << std::endl;
    }
}

/*******************************************************************
 * \brief waiting for the end of treatment
 * \param[in] sleep_ms delay to sleep while waiting for the end
*******************************************************************/
void SyncThread::waiting_end_of_treatment(int sleep_ms) const
{
    while(m_treatment_is_running)
    {
        ::usleep(sleep_ms * 1000);
    }
}

/*******************************************************************
 * \brief waiting for the treatment is completed
 * \param[in] sleep_ms delay to sleep while waiting for the end
*******************************************************************/
void SyncThread::waiting_completed_treatment(int sleep_ms) const
{
    while(can_go_on())
    {
        ::usleep(sleep_ms * 1000);
    }
}

/*******************************************************************
 * \brief start code to call in the thread method
*******************************************************************/
void SyncThread::start_treatment()
{
    m_start_barrier.wait();

    if(m_verbose_log)
    {
        INFO_STRM << "starting " << m_name << "." << std::endl;
    }
}

/*******************************************************************
 * \brief end code to call in the thread method
*******************************************************************/
void SyncThread::finish_treatment()
{
    if(m_verbose_log)
    {
        INFO_STRM << "ending " << m_name << "." << std::endl;
    }

    m_treatment_is_running = false;
}

/*******************************************************************
 * \brief tell if the treatment is always running 
 * \return true if the treatment is always running, else false 
*******************************************************************/
bool SyncThread::is_treatment_running() const
{
    return m_treatment_is_running;
}

/*******************************************************************
 * \brief can the thread's treatment go on ?
 * \return true if the treatment can go on, else false
*******************************************************************/
bool SyncThread::can_go_on() const
{
    return m_go_on;
}

/*******************************************************************
 * \brief tag the treatment as completed
*******************************************************************/
void SyncThread::treatment_completed()
{
    // the treatment is completed, we must exit the treatment loop.
    if(m_verbose_log)
    {
        INFO_STRM << m_name << " treatment is complete." << std::endl;
    }

    m_go_on = false;
}

} /// namespace DanteDpp_ns
