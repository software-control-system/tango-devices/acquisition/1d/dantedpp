/*************************************************************************/
/*! 
 *  \file   HwSimulator.cpp
 *  \brief  DANTE simpulator detector class implementation
 *  \author Cedric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

// SYSTEM
#include <cstdlib>
#include <cmath>
#include <math.h>

// PROJECT
#include "HwSimulator.h"
#include "Log.h"
#include "Exception.h"
#include "AsyncManager.h"
#include "SyncThread.h"
#include "DataStore.h"
#include "RoisManager.h"

using namespace DanteDpp_ns;

#define DANTE_DPP_HW_SIMULATOR_MODULES_NB  (8)
#define DANTE_DPP_HW_SIMULATOR_LIB_VERSION "1.0.0"

/***********************************************************************
 * \class GetFirmwaresThread
 * \brief Simulate the get firmware function of DANTE sdk
 ***********************************************************************/
class GetFirmwaresThread: public SyncThread
{
public:
    /*******************************************************************
     * \brief constructor
     * \param[in] in_name thread name (for logging)
     * \param[in] in_request_ids requests identifiers 
     *******************************************************************/
    GetFirmwaresThread(const std::string in_name, const std::vector<uint32_t> & in_request_ids) : SyncThread(in_name, false)
    {
        m_request_ids = in_request_ids;
    }

protected:
    /*******************************************************************
     * \brief The thread entry point called by yat::Thread::start_undetached.
     *        SyncThread process in managed inside this method.
     *******************************************************************/
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg /*ioa*/)
    {
        SyncThread::start_treatment();

        try
        {
            yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
            std::size_t modules_nb = hardware->get_modules_nb();

            // simulate the replies reception
            uint16_t type    = DANTE_DPP_ASYNC_REQUEST_OP_TYPE_READ;
            uint32_t version = 1;

            for(std::size_t module_index = 0 ; module_index < modules_nb ; module_index++)
            {
                ::usleep(10 * 1000);

                std::vector<uint32_t> request_data;
                request_data.push_back(version++); // major
                request_data.push_back(version++); // minor
                request_data.push_back(version++); // build

                hardware->reply_callback(type, m_request_ids[module_index], request_data.size(), request_data.data());
            }
        }
        catch(const DanteDpp_ns::Exception & in_ex)
        {
            std::cout << "Exception occured!"  << std::endl << in_ex.to_string();
        }

        SyncThread::treatment_completed();
        SyncThread::finish_treatment();

        return 0;
    }

private:
    std::vector<uint32_t> m_request_ids;
};

/***********************************************************************
 * \class GetStateThread
 * \brief Simulate the get state function of DANTE sdk
 ***********************************************************************/
class GetStateThread: public SyncThread
{
public:
    /*******************************************************************
     * \brief constructor
     * \param[in] in_name thread name (for logging)
     * \param[in] in_request_ids requests identifiers 
     *******************************************************************/
    GetStateThread(const std::string in_name, const std::vector<uint32_t> & in_request_ids, uint32_t in_state) : SyncThread(in_name, false)
    {
        m_request_ids = in_request_ids;
        m_state = in_state;
    }

protected:
    /*******************************************************************
     * \brief The thread entry point called by yat::Thread::start_undetached.
     *        SyncThread process in managed inside this method.
     *******************************************************************/
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg /*ioa*/)
    {
        SyncThread::start_treatment();

        try
        {
            yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
            std::size_t modules_nb = hardware->get_modules_nb();

            // simulate the replies reception
            uint16_t type  = DANTE_DPP_ASYNC_REQUEST_OP_TYPE_READ;
            uint32_t state = m_state;

            for(std::size_t module_index = 0 ; module_index < modules_nb ; module_index++)
            {
                ::usleep(10 * 1000);

                std::vector<uint32_t> request_data;
                request_data.push_back(state);

                hardware->reply_callback(type, m_request_ids[module_index], request_data.size(), request_data.data());
            }
        }
        catch(const DanteDpp_ns::Exception & in_ex)
        {
            std::cout << "Exception occured!"  << std::endl << in_ex.to_string();
        }

        SyncThread::treatment_completed();
        SyncThread::finish_treatment();

        return 0;
    }

private:
    std::vector<uint32_t> m_request_ids;
    uint32_t              m_state      ;
};

/************************************************************************
 * \brief constructor
 ************************************************************************/
HwSimulator::HwSimulator()
{
    INFO_STRM << "construction of HwSimulator" << std::endl;

    m_acquisition_state = 0;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
HwSimulator::~HwSimulator()
{
    INFO_STRM << "destruction of HwSimulator" << std::endl;
}

//------------------------------------------------------------------
// init/terminate management
//------------------------------------------------------------------
/************************************************************************
 * \brief init the detector
 ************************************************************************/
void HwSimulator::init(void)
{
    //throw DanteDpp_ns::Exception("INTERNAL_ERROR", "not implemented!", "HwSimulator::init");
    HardwareDante::init();

    m_current_request_id = DANTE_DPP_ASYNC_REQUEST_BAD_IDENTIFIER;
}

/************************************************************************
 * \brief terminate the detector
 ************************************************************************/
void HwSimulator::terminate(void)
{
    HardwareDante::terminate();
}

//------------------------------------------------------------------
// connection management
//------------------------------------------------------------------
/************************************************************************
 * \brief connection to the detector
 ************************************************************************/
void HwSimulator::connect(const std::string & in_complete_file_path)
{
    // start generic code - load the configuration file in memory
    HardwareDante::generic_start_code_for_connect(in_complete_file_path);

    // set system attributes
    m_modules_nb = DANTE_DPP_HW_SIMULATOR_MODULES_NB;

    // boards number
    INFO_STRM << "Boards number: " << m_modules_nb << std::endl;

    // library version
    m_library_version = DANTE_DPP_HW_SIMULATOR_LIB_VERSION;
    log_library_version();

    // log the firmware versions
    m_firmwares_versions = request_firmwares_versions();
    log_firmwares_versions();

    // end generic code 
    HardwareDante::generic_end_code_for_connect();
}

/************************************************************************
 * \brief disconnection to the detector
 ************************************************************************/
void HwSimulator::disconnect(void)
{
    // start generic code
    HardwareDante::generic_start_code_for_disconnect();

    // end generic code 
    HardwareDante::generic_end_code_for_disconnect();
}

//------------------------------------------------------------------
// system informations
//------------------------------------------------------------------
/************************************************************************
 * \brief requests the firmwares versions of all boards
 * \return boards firmwares versions
 ************************************************************************/
std::vector<std::string> HwSimulator::request_firmwares_versions()
{
    // simulate the requests
    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id = get_request_id();
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwSimulator::request_firmwares_versions");
    }

    // creating and starting the thread to simulate the replies reception
    GetFirmwaresThread * get_firmware_thread;
    get_firmware_thread = new GetFirmwaresThread("Get firmware thread", request_ids);
    get_firmware_thread->start();

    // wait all the replies
    wait_all_replies(request_ids);

    // stops the thread and waits for its end
    if(get_firmware_thread != NULL)
    {
        get_firmware_thread->exit();
        get_firmware_thread = NULL;
    }

    // simulate the final treatment of the replies
    yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
    std::vector<std::string> firmwares_versions;

    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);
        const std::vector<uint32_t> & data    = request->get_data();
        
        std::stringstream temp_stream;
        temp_stream << data[0] << "." << data[1] << "." << data[2]; 
        firmwares_versions.push_back(temp_stream.str());
    }

    return firmwares_versions;
}

/************************************************************************
 * \brief gets a request id to simulate the request system of DANTE sdk
 * \return request id
 ************************************************************************/
uint32_t HwSimulator::get_request_id()
{
    m_current_request_id++;

    if(!m_current_request_id) // can not be 0, which corresponds to a failed request id
        m_current_request_id++;

    return m_current_request_id;
}

//------------------------------------------------------------------
// state management
//------------------------------------------------------------------
/************************************************************************
 * \brief requests the states of all boards and computes a general state
 * \return hardware state
 ************************************************************************/
Tango::DevState HwSimulator::request_state()
{
    std::vector<uint32_t> request_ids;
    uint16_t module_index;

    // request the informations
    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        uint32_t request_id = get_request_id();
        add_request_id(request_id, request_ids, module_index, m_modules_nb, "HwSimulator::request_state");
    }

    // creating and starting the thread to simulate the replies reception
    GetStateThread * get_state_thread;
    get_state_thread = new GetStateThread("Get state thread", request_ids, m_acquisition_state);
    get_state_thread->start();

    // wait all the replies
    wait_all_replies(request_ids);

    // stops the thread and waits for its end
    if(get_state_thread != NULL)
    {
        get_state_thread->exit();
        get_state_thread = NULL;
    }

    // treatment of the replies
    yat::SharedPtr<AsyncRequestGroup> request_group = get_replies(request_ids);
    uint32_t is_running = 0;

    for(module_index = 0 ; module_index < m_modules_nb ; module_index++)
    {
        yat::SharedPtr<AsyncRequest>  request = request_group->get_request(module_index);
        is_running |= request->get_data()[0]; // if there is at least a 1 for a board, isRunning value will be also 1
    }

    return (is_running) ? Tango::RUNNING : Tango::STANDBY;
}

//------------------------------------------------------------------
// acquisition management
//------------------------------------------------------------------
/************************************************************************
 * \brief start detector acquisition
 * \return true if detector is properly started, FAIL otherwise
 ************************************************************************/
bool HwSimulator::start_detector_acquisition(void)
{
    INFO_STRM << "---------------------------------------------------" << std::endl;
    INFO_STRM << "HwSimulator::start_detector_acquisition"             << std::endl;

    bool result = true;

    try
    {
        mca_timer.restart();

        // HwSimulator is running an acquisition
        m_acquisition_state = 1;

        // forcing a state request
        update_state();

        // waiting for the status change to running
        while(m_state_status.get_state() != Tango::RUNNING)
        {
            ::usleep(10 * 1000);
        }
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        m_state_status.on_fault(in_ex);
        result = false;
    }

    return result;
}

/************************************************************************
 * \brief stop detector acquisition
 ************************************************************************/
void HwSimulator::stop_detector_acquisition(void)
{
    try
    {
        if(m_state_status.get_state() == Tango::RUNNING)
        {
            INFO_STRM << "--------------------------------------------------" << std::endl;
            INFO_STRM << "HwSimulator::stop_detector_acquisition"             << std::endl;

            // HwSimulator is stopping the acquisition
            m_acquisition_state = 0;
        }
    }
    catch(const DanteDpp_ns::Exception & in_ex)
    {
        m_state_status.on_fault(in_ex);
    }
}

/************************************************************************
 * \brief treat acquisitions
 ************************************************************************/
void HwSimulator::treat_acquisitions(void)
{
    // MCA acquisition ?        
    if(get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MCA)
    {
        if(m_acquisition_state)
        {
            double elapsed_time_ms = mca_timer.elapsed_msec();

            if(elapsed_time_ms > (m_preset_value * 1000.0))
            {
                m_acquisition_state = 0;
            }
        }

        if(m_state_status.get_state() != Tango::RUNNING)
        {
            std::size_t channels_nb = get_channels_nb();

            for(std::size_t channel_index = 0; channel_index < channels_nb; channel_index++)
            {
                yat::SharedPtr<ChannelData> channel_data = create_channel_data(channel_index);

                DataStore::get_instance()->add_channel_data(0, channel_data);
            }
        }
    }
    else
    // MAPPING acquisition ?        
    if((get_acquisition_mode() == HardwareDanteConfigParameters::AcqMode::MAPPING) && m_acquisition_state)
    {
        double      elapsed_time_ms          = mca_timer.elapsed_msec();
        std::size_t theorical_current_pixels = static_cast<std::size_t>((elapsed_time_ms / (m_preset_value * 1000.0)) + 0.5);
        std::size_t current_pixels           = DataStore::get_const_instance()->get_current_pixel();
        std::size_t nb_pixels                = static_cast<std::size_t>(get_nb_pixels());
        std::size_t nb_pixels_to_create      = 0;
        std::size_t channels_nb              = get_channels_nb();
		std::size_t sp_time                  = static_cast<std::size_t>(get_sp_time());

        if(theorical_current_pixels > nb_pixels)
            theorical_current_pixels = nb_pixels;

        if(theorical_current_pixels < current_pixels)
            theorical_current_pixels = current_pixels;

        nb_pixels_to_create = theorical_current_pixels - current_pixels;

        while(nb_pixels_to_create--)
        {
            for(std::size_t channel_index = 0; channel_index < channels_nb; channel_index++)
            {
                yat::SharedPtr<ChannelData> channel_data = create_channel_data(channel_index);

                DataStore::get_instance()->add_channel_data(current_pixels, channel_data);
            }

            current_pixels++;
        }

        if(current_pixels == nb_pixels)
        {
            m_acquisition_state = 0;
        }
    }
}

/************************************************************************
 * \brief create a channel data
 * \return a new simulation of channel data
 ************************************************************************/
const double Pi = 3.141592654f;

inline double DegToRad(uint64_t x)
{
    return ((static_cast<double>(x % 360)) * Pi) / 180;
}

yat::SharedPtr<ChannelData> HwSimulator::create_channel_data(std::size_t in_channel_index)
{
    static std::size_t offset = 0LL;

    yat::SharedPtr<ChannelData> channel_data;

    const std::size_t fct_nb = 8;
    std::size_t fct = in_channel_index % fct_nb;

    std::size_t bins_nb = get_bins_nb();
    std::size_t rois_nb = RoisManager::get_const_instance()->get_nb_rois(in_channel_index);

    channel_data = new ChannelData(in_channel_index, bins_nb, rois_nb);

    channel_data->set_real_time        (1.0);
    channel_data->set_live_time        (2.0);
    channel_data->set_dead_time        (3.0);
    channel_data->set_input_count_rate (4.0);
    channel_data->set_output_count_rate(5.0);
    channel_data->set_events_in_run    (6.0);

    uint64_t * source_spectrum = new uint64_t[bins_nb];

    for(std::size_t bin_index = 0; bin_index < bins_nb; bin_index++)
    {
        uint64_t temp = bin_index + offset; // X -> X by default (fct = 0)

        if(fct == 1)
        {
            temp = temp * temp;
        }
        else
        if(fct == 2)
        {
            temp = static_cast<uint64_t>(sqrt(static_cast<double>(temp)));
        }
        else
        if(fct == 3)
        {
            temp = static_cast<uint64_t>(pow(sin(DegToRad(temp)),2) * static_cast<double>(temp));
        }
        else
        if(fct == 4)
        {
            temp = static_cast<uint64_t>(100000 * fabs(sin(DegToRad(temp)))/(static_cast<double>(temp) + 1));
        }
        else
        if(fct == 5)
        {
            temp = static_cast<uint64_t>((bins_nb + offset + 1)/(temp + 1));
        }
        else
        if(fct == 6)
        {
            temp = static_cast<uint64_t>(pow(sin(DegToRad(temp)),2) * static_cast<double>(temp));
            temp = (temp / 100) * 100;
        }
        else
        if(fct == 7)
        {
            temp = static_cast<uint64_t>(pow(cos(DegToRad(temp)),3) * static_cast<double>(temp));
        }

        source_spectrum[bin_index] = temp;
    }

    channel_data->copy_to_spectrum(source_spectrum, 0LL, bins_nb);
    
    delete [] source_spectrum;

    // compute the ROIs data
    if(rois_nb > 0)
    {
        for(std::size_t roi_index = 0; roi_index < rois_nb; roi_index++)
        {
            yat::SharedPtr<const RoiRange> roi_range = RoisManager::get_const_instance()->get_roi(in_channel_index, roi_index);
            channel_data->compute_roi(roi_index, roi_range->get_bin_index(), roi_range->get_bin_size());
        }
    }

    offset = (offset + 1) % bins_nb;

    return channel_data;
}

//------------------------------------------------------------------
// error management
//------------------------------------------------------------------
/************************************************************************
 * \brief manages the current error and throws an exception
 * \param[in] in_reason   error reason
 * \param[in] in_desc     error description
 * \param[in] in_origin   error origin
 ************************************************************************/
void HwSimulator::manage_last_error(const std::string & in_reason,
                                    const std::string & in_desc  ,
                                    const std::string & in_origin)
{
    uint16_t error_code = DANTE_DPP_ERROR_CODE_UNKNOWN;
    throw DanteDpp_ns::Exception(in_reason, in_desc, in_origin, error_code, Error::EnumSeverity::ERROR);
}

//========================================================================================
