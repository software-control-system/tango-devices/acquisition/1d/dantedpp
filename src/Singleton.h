/**
 *  \file Singleton.h
 *  \brief header file of Singleton template class
 *  \author C�dric Castel
 *  \version 0.1
 *  \date June 19 2020
 *  Created on: June 19 2020
 */

#ifndef DANTE_DPP_SINGLETON_H_
#define DANTE_DPP_SINGLETON_H_

// SYSTEM
#include <cstddef>
#include <stdint.h>
#include <cstdlib>

// YAT
#include <yat/memory/SharedPtr.h>

/**
 * \namespace DanteDpp_ns
 */
namespace DanteDpp_ns
{
/**
  *  \class Singleton
  *  \brief This class is used to manage a singleton.
  */
    template< class Elem>
    class Singleton
    {
    public:
        // release the singleton
        static void release();

        // return the singleton
        static yat::SharedPtr<Elem> get_instance();

        // return the singleton (const version)
        static yat::SharedPtr<const Elem> get_const_instance();

        // release the singleton (execute a specific code for an derived class)
        virtual void specific_release();

    protected:
        // constructor
        explicit Singleton();

        // destructor (needs to be virtual)
        virtual ~Singleton();

    protected:
      /***************************************************************************************************
        * CLASS GENERAL DATA
        **************************************************************************************************/
      /** used to store the singleton
        */
        static yat::SharedPtr<Elem> g_singleton;
    };

    // include the implementation file of the Singleton class to separate interface and implementation
    #include "Singleton.hpp"

} /// namespace DanteDpp_ns

#endif //// DANTE_DPP_SINGLETON_H_
