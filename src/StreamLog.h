/*************************************************************************/
/*! 
 *  \file   StreamLog.h
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_STREAM_LOG_H_
#define DANTE_DPP_STREAM_LOG_H_

//TANGO
#include <tango.h>

// LOCAL
#include "Stream.h"

namespace DanteDpp_ns
{
/*************************************************************************/
class StreamLog : public Stream
{
    // we need to gain access to the contructor
    friend class Stream;

    public:
        // Update the stream with new acquisition data
        virtual void update(yat::SharedPtr<const ModuleData> in_module_data);

        // Reset the file index
        virtual void reset_index(void);

    protected:
        // destructor
        virtual ~StreamLog();

    private:
        // constructor
        StreamLog();

    private :
};

} // namespace DanteDpp_ns

#endif // DANTE_DPP_STREAM_LOG_H_

//###########################################################################
