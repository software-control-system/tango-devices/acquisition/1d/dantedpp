/*************************************************************************/
/*! 
 *  \file   AsyncRequest.h
 *  \brief  DANTE detector asynchronous reply from a command
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_ASYNC_REQUEST_H_
#define DANTE_DPP_ASYNC_REQUEST_H_

// YAT
#include <yat/memory/SharedPtr.h> // definition of AsyncRequestMap

// SYSTEM
#include <string>
#include <vector>
#include <map>

/**********************************************************************/
namespace DanteDpp_ns
{
#define DANTE_DPP_ASYNC_REQUEST_BAD_IDENTIFIER (0)

// operation type of the request used the reply callback
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_READ  (1)
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_WRITE (2)
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_ERROR (0)

/***********************************************************************
 * \class AsyncRequest
 * \brief used to store the reply informations
 ***********************************************************************/
class AsyncRequest
{
    friend class AsyncRequestGroup; // for access to the attributes

public:
    // constructor
    AsyncRequest(uint32_t in_request_id);

    // destructor
    virtual ~AsyncRequest();

    // get the identifier of the request
    uint32_t get_request_id();

    // get the data of the request
    const std::vector<uint32_t> & get_data();

private :
    uint32_t              m_request_id; // request identifier
    std::vector<uint32_t> m_data      ; // reply data
    int                   m_error_code; // error code (DLL_NO_ERROR if no error, DANTE_DPP_ERROR_CODE_UNKNOWN if there is no reply yet)
                                        // DANTE_DPP_ERROR_CODE_ASYNC_REQUEST_PROBLEM if an error occured during the call
                                        // DANTE_DPP_ERROR_CODE_ASYNC_REPLY_PROBLEM if there is an error in the reply
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_ASYNC_REQUEST_H_

