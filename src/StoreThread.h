/*************************************************************************/
/*! 
 *  \file   StoreThread.h
 *  \brief  DANTE detector store thread class interface
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/*************************************************************************/

#ifndef DANTE_DPP_STORE_THREAD_H_
#define DANTE_DPP_STORE_THREAD_H_

// PROJECT
#include "SyncThread.h"

/**********************************************************************/
namespace DanteDpp_ns
{
/***********************************************************************
 * \class StoreThread
 * \brief DANTE detector store thread class
 ***********************************************************************/
class StoreThread: public SyncThread
{
public:
    // Create the store thread
    static void create();

    // Release the store thread
    static void release();

    // return the singleton
    static StoreThread * get_instance();

    // return the singleton (const version)
    static const StoreThread * get_const_instance();

    // tell if the store thread was created
    static bool exist();

protected:
    // constructor
    StoreThread(const std::string in_name);

    // the thread entry point - called by yat::Thread::start_undetached. 
    // SyncThread process in managed inside this method.
    virtual yat::Thread::IOArg run_undetached(yat::Thread::IOArg ioa);

    // destructor
    virtual ~StoreThread();

private:
    // thread used to store the acquisition data
    static StoreThread * g_thread;
};

} /// namespace DanteDpp_ns

#endif /// DANTE_DPP_STORE_THREAD_H_
