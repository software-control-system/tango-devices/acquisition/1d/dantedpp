/*************************************************************************/
/*! 
 *  \file   Helper.cpp
 *  \brief  class used as a tool class (mostly conversions)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#include "Helper.h"

namespace DanteDpp_ns
{
//-------------------------------------------------------------------------
// STREAM TYPE
//-------------------------------------------------------------------------
/*******************************************************************
 * \brief Convert a tango stream type label to a stream type enum
 * \param[in] in_tango_stream_type Tango stream type in label
 * \return the stream type enum
*******************************************************************/
StreamParameters::Types Helper::convert_tango_to_stream_type(const std::string & in_tango_stream_type)
{
    return convert_tango_label_to_type(in_tango_stream_type, 
                                       TANGO_STREAM_TYPE_LABELS,
                                       TANGO_STREAM_TYPE_LABELS_TO_TYPE,
                                       "stream type");
}

/*******************************************************************
 * \brief Convert a stream type enum to a tango stream type label
 * \param[in] in_stream_type stream type enum
 * \return a tango stream type label
*******************************************************************/
std::string Helper::convert_stream_type_to_tango(const StreamParameters::Types & in_stream_type)
{
    return convert_type_to_tango_label(in_stream_type,
                                       TANGO_STREAM_TYPE_LABELS,
                                       TANGO_STREAM_TYPE_LABELS_TO_TYPE,
                                       "stream type");
}

//-------------------------------------------------------------------------
// STREAM WRITE MODE
//-------------------------------------------------------------------------
/************************************************************************
 * \brief Convert a tango stream write mode label to a stream write mode
 * \param[in] in_tango_stream_write_mode Tango stream write mode in label
 * \return the stream write mode enum
*************************************************************************/
StreamParameters::WriteModes Helper::convert_tango_to_stream_write_mode(const std::string & in_tango_stream_write_mode)
{
    return convert_tango_label_to_type(in_tango_stream_write_mode, 
                                       TANGO_STREAM_WRITE_MODE_LABELS,
                                       TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE,
                                       "stream write");
}

/*******************************************************************
 * \brief Convert a stream write mode enum to a tango stream write mode label
 * \param[in] in_stream_write_mode Stream write mode enum
 * \return a tango stream write mode
*******************************************************************/
std::string Helper::convert_stream_write_mode_to_tango(const StreamParameters::WriteModes & in_stream_write_mode)
{
    return convert_type_to_tango_label(in_stream_write_mode,
                                       TANGO_STREAM_WRITE_MODE_LABELS,
                                       TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE,
                                       "stream write");
}

//-------------------------------------------------------------------------
// STREAM MEMORY MODE
//-------------------------------------------------------------------------
/************************************************************************
 * \brief Convert a tango stream memory mode label to a stream memory mode
 * \param[in] in_tango_stream_memory_mode Tango stream memory mode in label
 * \return the stream memory mode enum
*************************************************************************/
StreamParameters::MemoryModes Helper::convert_tango_to_stream_memory_mode(const std::string & in_tango_stream_memory_mode)
{
    return convert_tango_label_to_type(in_tango_stream_memory_mode, 
                                       TANGO_STREAM_MEMORY_MODE_LABELS,
                                       TANGO_STREAM_MEMORY_MODE_LABELS_TO_MEMORY_MODE,
                                       "stream memory");
}

/*******************************************************************
 * \brief Convert a stream memory mode enum to a tango stream memory mode label
 * \param[in] in_stream_memory_mode Stream memory mode enum
 * \return a tango stream memory mode
*******************************************************************/
std::string Helper::convert_stream_memory_mode_to_tango(const StreamParameters::MemoryModes & in_stream_memory_mode)
{
    return convert_type_to_tango_label(in_stream_memory_mode,
                                       TANGO_STREAM_MEMORY_MODE_LABELS,
                                       TANGO_STREAM_MEMORY_MODE_LABELS_TO_MEMORY_MODE,
                                       "stream memory");
}

//-------------------------------------------------------------------------
// TRIGGERS
//-------------------------------------------------------------------------
/*******************************************************************
 * \brief Convert a tango trigger mode to a trigger mode enum
 * \param[in] in_tango_trigger_mode Tango trigger mode in label
 * \return the trigger mode enum
*******************************************************************/
HardwareDante::TriggerMode Helper::convert_tango_to_trigger_mode(const std::string & in_tango_trigger_mode)
{
    return convert_tango_label_to_type(in_tango_trigger_mode, 
                                       TANGO_TRIGGER_MODES_LABELS,
                                       TANGO_TO_HARDWARE_TRIGGER_MODE,
                                       "trigger mode");
}

/*******************************************************************
 * \brief Convert a trigger mode enum to a tango trigger mode label
 * \param[in] in_trigger_mode trigger mode
 * \return a tango trigger mode label
*******************************************************************/
std::string Helper::convert_trigger_mode_to_tango(const HardwareDante::TriggerMode & in_trigger_mode)
{
    return convert_type_to_tango_label(in_trigger_mode,
                                       TANGO_TRIGGER_MODES_LABELS,
                                       TANGO_TO_HARDWARE_TRIGGER_MODE,
                                       "trigger mode");
}

//-------------------------------------------------------------------------
// DETECTOR TYPE
//-------------------------------------------------------------------------
/*******************************************************************
 * \brief Convert a tango detector type to a detector type enum
 * \param[in] in_tango_detector_type Tango detector type in label
 * \return the detector type enum
*******************************************************************/
HardwareDanteInitParameters::Type Helper::convert_tango_to_detector_type(const std::string & in_tango_detector_type)
{
    return convert_tango_label_to_type(in_tango_detector_type, 
                                       TANGO_DETECTOR_TYPE_LABELS,
                                       TANGO_DETECTOR_TYPE_LABELS_TO_TYPE,
                                       "detector type");
}

/*******************************************************************
 * \brief Convert a detector type enum to a tango detector type label
 * \param[in] in_detector_type detector type
 * \return a tango detector type label
*******************************************************************/
std::string Helper::convert_detector_type_to_tango(const HardwareDanteInitParameters::Type & in_detector_type)
{
    return convert_type_to_tango_label(in_detector_type,
                                       TANGO_DETECTOR_TYPE_LABELS,
                                       TANGO_DETECTOR_TYPE_LABELS_TO_TYPE,
                                       "detector type");
}


//-------------------------------------------------------------------------
// ACQUISITION MODES
//-------------------------------------------------------------------------
/*******************************************************************
 * \brief Convert a tango acquisition mode to an acquisition mode enum
 * \param[in] in_tango_acq_mode Tango acquisition mode in label
 * \return the acquisition mode enum
*******************************************************************/
HardwareDanteConfigParameters::AcqMode Helper::convert_tango_to_acq_mode(const std::string & in_tango_acq_mode)
{
    return convert_tango_label_to_type(in_tango_acq_mode, 
                                       TANGO_ACQ_MODES_LABELS,
                                       TANGO_TO_HARDWARE_ACQ_MODES,
                                       "acquisition mode");
}

/*******************************************************************
 * \brief Convert an acquisition mode enum to a tango acquisition mode label
 * \param[in] in_acq_mode acquisition mode
 * \return a tango acquisition mode label
*******************************************************************/
std::string Helper::convert_acq_mode_to_tango(const HardwareDanteConfigParameters::AcqMode & in_acq_mode)
{
    return convert_type_to_tango_label(in_acq_mode,
                                       TANGO_ACQ_MODES_LABELS,
                                       TANGO_TO_HARDWARE_ACQ_MODES,
                                       "acquisition mode");
}

//-------------------------------------------------------------------------
// PRESET TYPES
//-------------------------------------------------------------------------
/*******************************************************************
 * \brief Convert a tango preset type to a preset type enum
 * \param[in] in_tango_preset_type Tango preset type in label
 * \return the preset type enum
*******************************************************************/
HardwareDante::PresetType Helper::convert_tango_to_preset_type(const std::string & in_tango_preset_type)
{
    return convert_tango_label_to_type(in_tango_preset_type, 
                                       TANGO_PRESET_TYPES_LABELS,
                                       TANGO_TO_HARDWARE_PRESET_TYPE,
                                       "preset type");
}

/*******************************************************************
 * \brief Convert a trigger mode enum to a tango trigger mode label
 * \param[in] in_preset_type preset type enum
 * \return a tango preset type label
*******************************************************************/
std::string Helper::convert_preset_type_to_tango(const HardwareDante::PresetType & in_preset_type)
{
    return convert_type_to_tango_label(in_preset_type,
                                       TANGO_PRESET_TYPES_LABELS,
                                       TANGO_TO_HARDWARE_PRESET_TYPE,
                                       "preset type");
}

//###########################################################################
}