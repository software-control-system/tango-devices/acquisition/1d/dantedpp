/*************************************************************************/
/*! 
 *  \file   HardwareDanteConfigParameters.h
 *  \brief  class used to manage the HardwareDante config parameters
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_HARDWARE_DANTE_CONFIG_PARAMETERS_H_
#define DANTE_DPP_HARDWARE_DANTE_CONFIG_PARAMETERS_H_

//TANGO
#include <tango.h>

// SYSTEM
#include <vector>
#include <map>

namespace DanteDpp_ns
{
/*************************************************************************/
/*\brief Class used to pass the needed data for the HardwareDante initialization*/
class HardwareDanteConfigParameters
{
    friend class HardwareDante;
    friend class HwDante      ;
    friend class HwSimulator  ;

    public:
        typedef enum 
        {
	        MCA    ,
	        MAPPING,
        } AcqMode;

        typedef enum 
        {
	        DC_HighImp, // DC coupling, 10 K Ohm input R
	        DC_LowImp , // DC coupling, 1  K Ohm input R
	        AC_Slow   , // AC coupling, 22 us time constant
	        AC_Fast   , // AC coupling, 2.2  us time constant
        } InputMode;

        // acquisition parameters (for configure function)
        typedef struct 
        {
            std::size_t m_board_index        ; // board index (starts at 0 -> for master board)
            uint32_t    m_fast_filter_thr    ; // Detection threshold [unit: spectrum BINs]
            uint32_t    m_energy_filter_thr  ; // Detection threshold [unit: spectrum BINs]
            uint32_t    m_energy_baseline_thr; // Energy threshold for baseline [unit: spectrum BINs]
            double      m_max_risetime       ; // Max risetime setting for pileup detection 
            double      m_gain               ; // Main gain: (spectrum BIN)/(ADC's LSB)
            uint32_t    m_peaking_time       ; // Main energy filter peaking time   [unit: 32 ns samples]
            uint32_t    m_max_peaking_time   ; // Max energy filter peaking time   [unit: 32 ns samples]
            uint32_t    m_flat_top           ; // Main energy filter flat top   [unit: 32 ns samples]
            uint32_t    m_edge_peaking_time  ; // Edge detection filter peaking time   [unit: 8 ns samples]
            uint32_t    m_edge_flat_top      ; // Edge detection filter flat top   [unit: 8 ns samples]
            uint32_t    m_reset_recovery_time; // Reset recovery time [unit: 8 ns samples]
            double      m_zero_peak_freq     ; // Zero peak rate [kcps]
            uint32_t    m_baseline_samples   ; // Baseline samples for baseline correction [32 ns samples]
            bool        m_inverted_input     ; // pulse inversion
            double      m_time_constant      ; // Time constant for continuous reset signal
            uint32_t    m_base_offset        ; // Baseline of the continuous reset signal [ADC's LSB]
            uint32_t    m_overflow_recovery  ; // Overflow recovery time [8ns samples]
            uint32_t    m_reset_threshold    ; // Reset detection threshold [ADC's LSB]
            double      m_tail_coefficient   ; // Tail coefficient
            uint32_t    m_other_param        ; // currently ignored
        } AcqParameters;

        // simple constructor
        HardwareDanteConfigParameters();

    private :
        // connection parameters
        std::string m_master_board_ip;

        // acquisition - system parameters
        HardwareDanteConfigParameters::AcqMode m_acquisition_mode;
        uint32_t m_nb_bins;

        // default parameters for master board
        AcqParameters m_default_acq_parameters;

        // acquisition - boards parameters
        // At least, contains one structure for the master board.
        // Could, also, contain structures for other boards which have different settings.
        // Boards with no specific settings will have the same settings as the master board.
        std::vector<AcqParameters> m_acq_parameters;

        // parameters for configure_offset function
        uint32_t m_offset_val_digpot1; // First offset value
        uint32_t m_offset_val_digpot2; // Second offset value.

        // parameters for configure_input function
        HardwareDanteConfigParameters::InputMode m_input_mode;

        // parameters for shifts by spectrum type (depending of the bins number)
        map<uint32_t, uint32_t> m_shift_by_spectrum_bins;
    };

} // namespace DanteDpp_ns

#endif // DANTE_DPP_HARDWARE_DANTE_CONFIG_PARAMETERS_H_

//###########################################################################
