/*************************************************************************/
/*! 
 *  \file   AsyncManager.cpp
 *  \brief  class used to manage the asynchronous replies
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// YAT/YAT4TANGO
#include <yat/utils/String.h>

// PROJECT
#include "Log.h"
#include "AsyncManager.h"
#include "Exception.h"

namespace DanteDpp_ns
{
/************************************************************************
 * \brief constructor
 ************************************************************************/
AsyncManager::AsyncManager()
{
    INFO_STRM << "construction of AsyncManager" << std::endl;
}

/************************************************************************
 * \brief destructor
 ************************************************************************/
AsyncManager::~AsyncManager()
{
    INFO_STRM << "destruction of AsyncManager" << std::endl;
}

/*******************************************************************
 * \brief add a new request 
 * \param[in] in_request_id identifier of the request
 * \param[in] in_group_request_id identifier of the group.
 * \param[in] in_max_nb_replies maximum number of replies for the requests
 *******************************************************************/
void AsyncManager::add_request(uint32_t in_request_id, uint32_t in_group_request_id, std::size_t in_max_nb_replies)
{
    // 1) check the request id
    if(in_request_id == DANTE_DPP_ASYNC_REQUEST_BAD_IDENTIFIER)
    {
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", "incoherent in_request_id in the call!", "AsyncManager::add_request");
    }

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_requests_mutex);

        yat::SharedPtr<AsyncRequestGroup> request_group;

        // 2) check if the request group already exists in the container 
        AsyncRequestGroupMap::iterator search = m_requests.find(in_group_request_id);

        // request group was already created
        if(search != m_requests.end()) 
        {
            request_group = search->second;
        }
        // new request group
        else
        {
        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
            INFO_STRM << "Creating new request group: " << in_group_request_id << std::endl;
        #endif
            request_group.reset(new AsyncRequestGroup(in_max_nb_replies));

            // inserting the group in the map container.
            std::pair<AsyncRequestGroupMap::iterator, bool> insert_result = m_requests.insert(std::make_pair(in_group_request_id, request_group));

            // this should never happen.
            if(!insert_result.second)
            {
                std::stringstream temp_stream;
                temp_stream << "Problem! Request group (" << in_group_request_id << ") should not be in the requests container!";
                throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "AsyncManager::add_request");
            }
        }

        // 3) add the request to the group
    #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
        INFO_STRM << "Adding new request: " << in_request_id << " to group: " << in_group_request_id << std::endl;
    #endif

        request_group->add_request(in_request_id);

        // 4) if this is not the first request of the group, create the link between the request and the group
        if(in_request_id != in_group_request_id)
        {
            // inserting the group in the map container.
            std::pair<AsyncRequestGroupMap::iterator, bool> insert_result = m_requests.insert(std::make_pair(in_request_id, request_group));

            // this should never happen.
            if(!insert_result.second)
            {
                std::stringstream temp_stream;
                temp_stream << "Problem! Request (" << in_request_id << ") should not be in the requests container!";
                throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "AsyncManager::add_request");
            }
        }
    }
}

/*******************************************************************
 * \brief manage the reply of a request (internal version which allows to temporary store the unknown requests)
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 * \param[in] in_store_unknown_request an unknown request can be stored or raise an error 
 *******************************************************************/
void AsyncManager::manage_reply_aux(uint16_t in_type, uint32_t in_call_id, 
                                    uint32_t in_length, const uint32_t * in_data,
                                    bool in_store_unknown_request)
{
    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_requests_mutex);

        yat::SharedPtr<AsyncRequestGroup> request_group;

        // 1) search the request group
        AsyncRequestGroupMap::iterator search = m_requests.find(in_call_id);

        if(search != m_requests.end())
        {
            request_group = search->second;
        }
        else
        {
            if(in_store_unknown_request)
            {
                // add an unkown request to a container
                // it will be treated later.
                yat::SharedPtr<AsyncBuffer> unknown_request;
                unknown_request.reset(new AsyncBuffer(in_type, in_call_id, in_length, in_data));
                m_unknown_requests_buffer.push_back(unknown_request);

        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
                INFO_STRM << "Storing an unknown request: Request id (" << in_call_id << ")." << std::endl;
        #endif
            }
        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
            else
            {
                INFO_STRM << "Problem! Request id (" << in_call_id << ") is not present in the requests container!" << std::endl;
            }
        #endif
            return;
        }
    
        //  2) update the request in the group
        request_group->manage_reply(in_type, in_call_id, in_length, in_data);
    }
}

/*******************************************************************
 * \brief manage the reply of a request
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 *******************************************************************/
void AsyncManager::manage_reply(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data)
{
    manage_reply_aux(in_type, in_call_id, in_length, in_data, true);
}

/*******************************************************************
 * \brief get a request group
 * \param[in] in_group_request_id identifier of the group.
 * \return request group
 *******************************************************************/
yat::SharedPtr<AsyncRequestGroup> AsyncManager::get_request_group(uint32_t in_group_request_id)
{
    // 1) checking the request id
    if(in_group_request_id == DANTE_DPP_ASYNC_REQUEST_BAD_IDENTIFIER)
    {
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", "incoherent in_group_request_id in the call!", "AsyncManager::remove_request_group");
    }

    yat::SharedPtr<AsyncRequestGroup> request_group;

    // protecting the multi-threads access
    {
        yat::AutoMutex<> lock(m_requests_mutex);

        // 2) searching the request group in the container 
        AsyncRequestGroupMap::iterator search = m_requests.find(in_group_request_id);

        // request group was already created
        if(search != m_requests.end()) 
        {
            request_group = search->second;
        }
        // new request group
        else
        {
        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
            INFO_STRM << "Problem! Request id (" << in_group_request_id << ") is not present in the requests container!" << std::endl;
        #endif
            return request_group;
        }
    }

    return request_group;
}


/*******************************************************************
 * \brief remove a request group
 * \param[in] in_group_request_id identifier of the group.
 * \return request group
 *******************************************************************/
yat::SharedPtr<AsyncRequestGroup> AsyncManager::remove_request_group(uint32_t in_group_request_id)
{
    yat::SharedPtr<AsyncRequestGroup> request_group = get_request_group(in_group_request_id);

    if(!request_group.is_null())
    {
        // protecting the multi-threads access
        yat::AutoMutex<> lock(m_requests_mutex);

        // removing all the requests in the map container
        std::size_t nb_requests = request_group->get_requests_nb();

        for(std::size_t request_index = 0 ; request_index < nb_requests ; request_index++)
        {
            yat::SharedPtr<AsyncRequest> request = request_group->get_request(request_index);
            m_requests.erase(request->get_request_id());            
        }
    }

    return request_group;
}

/*******************************************************************
 * \brief manage the wait of all replies
 * \param[in] in_group_request_id identifier of the group.
 * \return true if all replies were received, else false
 *******************************************************************/
bool AsyncManager::waiting_all_replies_of_group(uint32_t in_group_request_id)
{
    // flushing the unknown request
    // protecting the multi-threads access (recursive, do not block the manage_reply_aux calls)
    {
        yat::AutoMutex<> lock(m_requests_mutex);

        while(m_unknown_requests_buffer.size() > 0)
        {
            // accessing first data in the container
            yat::SharedPtr<AsyncBuffer> unknown_request = m_unknown_requests_buffer.front();

            // removing the data from the container
            m_unknown_requests_buffer.erase(m_unknown_requests_buffer.begin());

            // treating the request
            uint16_t         type    = unknown_request->m_type       ;
            uint32_t         call_id = unknown_request->m_call_id    ;
            uint32_t         length  = unknown_request->m_data.size();
            const uint32_t * data    = (length > 0) ? unknown_request->m_data.data() : NULL;

        #ifdef DANTE_DPP_DEBUG_ASYNC_MANAGEMENT
            INFO_STRM << "Flushing an request: Request id (" << call_id << ")." << std::endl;
        #endif

            manage_reply_aux(type, call_id, length, data, false);
        }
    }

    yat::SharedPtr<AsyncRequestGroup> request_group = get_request_group(in_group_request_id);
    return request_group.is_null() ? false : request_group->waiting_all_replies();
}

/**************************************************************************************************
 * SINGLETON MANAGEMENT
 **************************************************************************************************/
/*******************************************************************
 * \brief Create the manager
 *******************************************************************/
void AsyncManager::create()
{
    AsyncManager::g_singleton.reset(new AsyncManager());
}

} // namespace DanteDpp_ns
