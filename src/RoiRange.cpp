/*************************************************************************/
/*! 
 *  \file   RoiRange.cpp
 *  \brief  class used to manage the range of a Roi
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "RoiRange.h"

namespace DanteDpp_ns
{
//============================================================================================================
// RoiRange class
//============================================================================================================
/**************************************************************************
* \brief complete constructor
* \param[in] in_channel_index channel index
* \param[in] in_roi_index roi index
* \param[in] in_bin_index index of the first bin of the range
* \param[in] in_bin_size number of bin of the range
**************************************************************************/
RoiRange::RoiRange(std::size_t in_channel_index,
                   std::size_t in_roi_index    ,
                   std::size_t in_bin_index    ,
                   std::size_t in_bin_size     )
{
    m_channel_index = in_channel_index;
    m_roi_index     = in_roi_index    ;
    m_bin_index     = in_bin_index    ;
    m_bin_size      = in_bin_size     ;
}

/**************************************************************************
* \brief get the channel index
* \return channel index
**************************************************************************/
std::size_t RoiRange::get_channel_index() const
{
    return m_channel_index;
}

/**************************************************************************
* \brief get the roi index
* \return roi index
**************************************************************************/
std::size_t RoiRange::get_roi_index() const
{
    return m_roi_index;
}

/**************************************************************************
* \brief get the bin index
* \return bin index
**************************************************************************/
std::size_t RoiRange::get_bin_index() const
{
    return m_bin_index;
}

/**************************************************************************
* \brief get the bin size
* \return bin size
**************************************************************************/
std::size_t RoiRange::get_bin_size() const
{
    return m_bin_size;
}

/**************************************************************************
* \brief get the low index (first bin of the ROI)
* \return low index
**************************************************************************/
std::size_t RoiRange::get_low_index() const
{
    return get_bin_index();
}

/**************************************************************************
* \brief get the high index (last bin of the ROI)
* \return high index
**************************************************************************/
std::size_t RoiRange::get_high_index() const
{
    return (get_low_index() + get_bin_size() - 1);
}

//###########################################################################
}