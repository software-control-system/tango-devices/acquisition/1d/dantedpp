/*************************************************************************/
/*! 
 *  \file   ModuleData.cpp
 *  \brief  class used to store a module acquisition
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "ModuleData.h"
#include "Exception.h"

namespace DanteDpp_ns
{
//============================================================================================================
// ModuleData class
//============================================================================================================
/**************************************************************************
* \brief constructor
* \param[in] in_index index of the acquisition data
* \param[in] in_channels_nb number of channels
**************************************************************************/
ModuleData::ModuleData(std::size_t in_index, std::size_t in_channels_nb)
{
    m_index = in_index;
    m_received_channels_nb = 0;
	m_channels.resize(in_channels_nb);
}

/**************************************************************************
* \brief get the index of the acquisition data
* \return index of the acquisition data
**************************************************************************/
std::size_t ModuleData::get_index() const
{
    return m_index;
}

/**************************************************************************
* \brief tell if all the acquisition data of channels were received
* \return true if the reception is complete, else false
**************************************************************************/
bool ModuleData::is_complete() const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    return (m_received_channels_nb == m_channels.size());
}

/**************************************************************************
* \brief add channel data
* \param[in] in_channel_data channel data
**************************************************************************/
void ModuleData::add_channel(yat::SharedPtr<ChannelData> in_channel_data)
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    // get the channel index
    std::size_t channel_index = in_channel_data->get_index();

    // check the data
    if(m_received_channels_nb == m_channels.size())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Module data n�(" << m_index << ") should not be already complete!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::add_channel");
    }

    if(channel_index >= m_channels.size())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Overflow when trying to add a channel data to Module data n�(" << m_index << ")!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::add_channel");
    }

    if(!m_channels[channel_index].is_null())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Channel data " << channel_index << " of Module data n�(" << m_index << ") was already received!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::add_channel");
    }

	m_channels[channel_index] = in_channel_data;
    m_received_channels_nb++;
}

/**************************************************************************
* \brief get channel data
* \param[in] in_channels_index channel index
* \return channel data
**************************************************************************/
yat::SharedPtr<const ChannelData> ModuleData::get_channel(std::size_t in_channels_index) const
{
    // protecting the multi-threads access
    yat::AutoMutex<> lock(m_mutex);

    if(in_channels_index >= m_channels.size())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Overflow when trying to get the channel data n�" << in_channels_index 
                    << "of Module data n�(" << m_index << ")!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::get_channel");
    }

    if(m_channels[in_channels_index].is_null())
    {
        std::stringstream temp_stream;
        temp_stream << "Problem! Channel data n�" << in_channels_index 
                    << " of Module data n�(" << m_index << ") was never received!";
        throw DanteDpp_ns::Exception("INTERNAL_ERROR", temp_stream.str(), "ModuleData::get_channel");
    }

    return m_channels[in_channels_index];
}

/**************************************************************************
* \brief compute the size of an instance (used for stats)
* \param[in] in_channels_nb number of channels
* \param[in] in_bins_nb number of bins for the channels (same for all)
* \param[in] in_rois_nb total number of rois (for all channels)
* \return size in bytes of an instance
**************************************************************************/
std::size_t ModuleData::compute_size_in_bytes(std::size_t in_channels_nb, std::size_t in_bins_nb, std::size_t in_rois_nb)
{
    return sizeof(ModuleData) + (sizeof(yat::SharedPtr<ChannelData>) * in_channels_nb) +
           ChannelData::compute_size_in_bytes(in_channels_nb, in_bins_nb, in_rois_nb);
}

//###########################################################################
}