/*************************************************************************/
/*! 
 *  \file   StreamNexus.cpp
 *  \brief  class used to manage the stream of acquisition data
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "StreamNexus.h"
#include "Controller.h"
#include "RoisManager.h"

namespace DanteDpp_ns
{

#if defined(USE_NX_FINALIZER)
    nxcpp::NexusDataStreamerFinalizer StreamNexus::m_data_streamer_finalizer;
    bool StreamNexus::m_is_data_streamer_finalizer_started = false;
#endif
    
/*******************************************************************
 * \brief constructor
 * \param[in] in_device access to tango device for log management/controller
 *******************************************************************/
StreamNexus::StreamNexus()
{
    INFO_STRM << "construction of StreamNexus" << std::endl;

    yat::MutexLock scoped_lock(m_data_lock);

	m_writer = NULL;
	
#if defined(USE_NX_FINALIZER)
    if (!StreamNexus::m_is_data_streamer_finalizer_started )
    {
        DEBUG_STRM << "starting the underlying NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        StreamNexus::m_data_streamer_finalizer.start();
        StreamNexus::m_is_data_streamer_finalizer_started = true;
        DEBUG_STRM << "starting the underlying NexusDataStreamerFinalizer - [END]" << std::endl;
    }
#endif

    DEBUG_STRM << "StreamNexus::StreamNexus() - [END]" << std::endl;
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StreamNexus::~StreamNexus() 
{
    INFO_STRM << "destruction of StreamNexus" << std::endl;

    yat::MutexLock scoped_lock(m_data_lock);

    if (m_writer)
    {
        //- this might be required, in case we could not pass the writer to the finalizer	 
        delete m_writer;
        m_writer = NULL;
    }
}

/*******************************************************************
 * \brief Update the stream with new acquisition data
 * \param[in] in_module_data module data to be treated
 *******************************************************************/
void StreamNexus::update(yat::SharedPtr<const ModuleData> in_module_data)
{
    DEBUG_STRM << "===============================" << std::endl;
    DEBUG_STRM << "StreamNexus::update - received data (" << in_module_data->get_index() << ")" << std::endl;

    if (m_writer)
    {
        const    std::size_t channels_nb = IHardware::get_const_instance()->get_channels_nb();
        double   temp_double;
        uint32_t temp_uint32;
        uint64_t temp_uint64;

        for (std::size_t channels_index = 0 ; channels_index < channels_nb ; channels_index++)
        {
            const std::size_t rois_nb_for_channel = RoisManager::get_const_instance()->get_nb_rois(channels_index);
            yat::SharedPtr<const ChannelData> channel_data = in_module_data->get_channel(channels_index);

            temp_double = channel_data->get_real_time();
            m_writer->PushData(m_real_time_names[channels_index], &temp_double);

            temp_double = channel_data->get_live_time();
            m_writer->PushData(m_live_time_names[channels_index], &temp_double);

            temp_double = channel_data->get_dead_time();
            m_writer->PushData(m_dead_time_names[channels_index], &temp_double);

            temp_double = channel_data->get_input_count_rate();
            m_writer->PushData(m_input_count_rate[channels_index], &temp_double);

            temp_double = channel_data->get_output_count_rate();
            m_writer->PushData(m_output_count_rate[channels_index], &temp_double);

            temp_uint32 = channel_data->get_events_in_run();
            m_writer->PushData(m_events_in_run[channels_index], &temp_uint32);

            const std::vector<uint32_t> & spectrum = channel_data->get_spectrum();
            m_writer->PushData(m_spectrum_names[channels_index], spectrum.data());

            if(rois_nb_for_channel > 0)
            {
                for (std::size_t roi_index = 0 ; roi_index < rois_nb_for_channel ; roi_index++)
                {
                    temp_uint32 = channel_data->get_rois(roi_index);
                    m_writer->PushData(m_roi_names[channels_index][roi_index], &temp_uint32);
                }
            }
        }
    }
}

/*******************************************************************
 * \brief Reset the file index
 *******************************************************************/
void StreamNexus::reset_index(void)
{
    DEBUG_STRM << "StreamNexus::reset_index() - [BEGIN]" << endl;
    DEBUG_STRM << "- ResetBufferIndex()" << endl;

    yat::MutexLock scoped_lock(m_data_lock);
    nxcpp::DataStreamer::ResetBufferIndex();

    DEBUG_STRM << "StreamNexus::reset_index() - [END]" << endl;
}

/*******************************************************************
 * \brief Manage a nexus exception
 *******************************************************************/
void StreamNexus::OnNexusException(const nxcpp::NexusException &ex)
{
    DEBUG_STRM << "StreamNexus::OnNexusException() - [BEGIN]" << endl;
    ostringstream ossMsgErr;
    ossMsgErr << endl;
    ossMsgErr << "===============================================" << endl;
    ossMsgErr << "Origin\t: " << ex.errors[0].origin << endl;
    ossMsgErr << "Desc\t: "   << ex.errors[0].desc   << endl;
    ossMsgErr << "Reason\t: " << ex.errors[0].reason << endl;
    ossMsgErr << "===============================================" << endl;
    ERROR_STRM << ossMsgErr.str() << std::endl;
    yat::MutexLock scoped_lock(m_data_lock);        

    //1 - finalize the DataStreamer , this will set m_writer = 0 in order to avoid any new push data
    if (m_writer)
    {   
        abort();
    }

    //2 -  inform the controller in order to stop acquisition & to set the state to FAULT
    m_state_status.on_fault(ex.errors[0].desc);

    Controller::get_instance()->abort(ex.errors[0].desc);
    DEBUG_STRM << "StreamNexus::OnNexusException() - [END]" << endl;        
};

/***************************************************************************
 * \brief Manage the start of acquisition
 ***************************************************************************/
void StreamNexus::start_acquisition(void)
{
    Stream::start_acquisition();

    DEBUG_STRM << "StreamNexus::start_acquisition() - [BEGIN]" << endl;

    yat::MutexLock scoped_lock(m_data_lock);

    std::size_t channels_nb   = IHardware::get_const_instance()->get_channels_nb();
    std::size_t bins_nb       = IHardware::get_const_instance()->get_bins_nb();
    std::size_t rois_nb       = RoisManager::get_const_instance()->get_nb_rois();
    std::size_t channels_index;

    INFO_STRM << "- Create DataStreamer :" << endl;
    INFO_STRM << "\t- target path     = "  << m_parameters.m_target_path     << std::endl;
    INFO_STRM << "\t- file name       = "  << m_parameters.m_target_file     << std::endl;
    INFO_STRM << "\t- write mode      = "  << m_parameters.m_write_mode      << std::endl;
    INFO_STRM << "\t- nb data per acq = "  << m_parameters.m_nb_data_per_acq << std::endl;
    INFO_STRM << "\t- nb acq per file = "  << m_parameters.m_nb_acq_per_file << std::endl;
    INFO_STRM << "\t- nb channels     = "  << channels_nb                    << std::endl;
    INFO_STRM << "\t- nb bins         = "  << bins_nb                        << std::endl;
    INFO_STRM << "\t- nb rois         = "  << rois_nb                        << std::endl;

    INFO_STRM << "\t- write data for : ";
    INFO_STRM << "\t\t. " << m_parameters.m_label_real_time         << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_live_time         << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_dead_time         << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_input_count_rate  << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_output_count_rate << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_events_in_run     << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_spectrum          << std::endl;
    INFO_STRM << "\t\t. " << m_parameters.m_label_roi               << std::endl;

    DEBUG_STRM << std::endl;

    m_spectrum_names.clear();
    m_real_time_names.clear();
    m_live_time_names.clear();
    m_dead_time_names.clear();
    m_input_count_rate.clear();
    m_output_count_rate.clear();
    m_events_in_run.clear();
    m_roi_names.clear();

    try
    {
		m_writer = new nxcpp::DataStreamer(m_parameters.m_target_file, 
                                           (std::size_t)m_parameters.m_nb_data_per_acq,
                                           (std::size_t)m_parameters.m_nb_acq_per_file);

        DEBUG_STRM << "- Initialize()" << std::endl;
        m_writer->Initialize(m_parameters.m_target_path);

        DEBUG_STRM << "- SetExceptionHnadler()" << std::endl;
        m_writer->SetExceptionHandler(this);

        DEBUG_STRM << "- Prepare Data Items :" << std::endl;

        // spectrum
        for (channels_index = 0 ; channels_index < channels_nb ; channels_index++)
        {
            std::string label;

            // real time
            label = m_parameters.m_label_real_time + yat::String::str_format("%02d", channels_index);
            m_real_time_names.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_real_time_names[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_real_time_names[channels_index]);

            // live time
            label = m_parameters.m_label_live_time + yat::String::str_format("%02d", channels_index);
            m_live_time_names.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_live_time_names[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_live_time_names[channels_index]);

            // dead time
            label = m_parameters.m_label_dead_time + yat::String::str_format("%02d", channels_index);
            m_dead_time_names.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_dead_time_names[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_dead_time_names[channels_index]);

            // input count rate
            label = m_parameters.m_label_input_count_rate + yat::String::str_format("%02d", channels_index);
            m_input_count_rate.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_input_count_rate[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_input_count_rate[channels_index]);

            // output count rate
            label = m_parameters.m_label_output_count_rate + yat::String::str_format("%02d", channels_index);
            m_output_count_rate.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_output_count_rate[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_output_count_rate[channels_index]);

            // events in run
            label = m_parameters.m_label_events_in_run + yat::String::str_format("%02d", channels_index);
            m_events_in_run.push_back(label);

            DEBUG_STRM << "\t- AddDataItem0D(" << m_events_in_run[channels_index] << ")" << std::endl;
            m_writer->AddDataItem0D(m_events_in_run[channels_index]);

            // spectrum
            label = m_parameters.m_label_spectrum + yat::String::str_format("%02d", channels_index);
            m_spectrum_names.push_back(label);

            DEBUG_STRM << "\t- AddDataItem1D(" << m_spectrum_names[channels_index] << "," << bins_nb << ")" << std::endl;
            m_writer->AddDataItem1D(m_spectrum_names[channels_index], bins_nb);

            // roi
            std::size_t rois_nb_for_channel = RoisManager::get_const_instance()->get_nb_rois(channels_index);

            if(rois_nb_for_channel > 0)
            {
                m_roi_names.push_back(std::vector<std::string>());

                for (std::size_t roi_index = 0 ; roi_index < rois_nb_for_channel ; roi_index++)
                {
                    label = m_parameters.m_label_roi + yat::String::str_format("%02d_%02d", channels_index, roi_index + 1);
                    m_roi_names[channels_index].push_back(label);

                    DEBUG_STRM << "\t- AddDataItem1D(" << m_roi_names[channels_index][roi_index] << ")" << std::endl;
                    m_writer->AddDataItem0D(m_roi_names[channels_index][roi_index], bins_nb);
                }
            }
        }

        // configure the Writer mode 
        INFO_STRM << "- Configure the Writer mode :" << std::endl;

        //by default is ASYNCHRONOUS			
        if (m_parameters.m_write_mode == StreamParameters::WriteModes::SYNCHRONOUS )
        {
            INFO_STRM << "\t- SYNCHRONOUS" << std::endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::SYNCHRONOUS);
        }
        else
        if (m_parameters.m_write_mode == StreamParameters::WriteModes::DELAYED )
        {
            INFO_STRM << "\t- DELAYED" << std::endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::DELAYED);
        }
        else
        if (m_parameters.m_write_mode == StreamParameters::WriteModes::ASYNCHRONOUS )
        {
            INFO_STRM << "\t- ASYNCHRONOUS" << std::endl;
            m_writer->SetWriteMode(nxcpp::NexusFileWriter::ASYNCHRONOUS);
        }
    }
    catch(const nxcpp::NexusException & ex)
    {
        Tango::Except::throw_exception((ex.errors[0].reason).c_str(),
                                       (ex.errors[0].desc  ).c_str(),
                                       (ex.errors[0].origin).c_str());
    }

    DEBUG_STRM << "StreamNexus::start_acquisition() - [END]" << std::endl;
}

/*******************************************************************
 * \brief Manage the acquisition stop
 *******************************************************************/
void StreamNexus::stop_acquisition(void)
{
    DEBUG_STRM << "StreamNexus::stop_acquisition() - [BEGIN]" << std::endl;

    yat::MutexLock scoped_lock(m_data_lock);            

    if (m_writer)
    {        
#if defined (USE_NX_FINALIZER) 
        //- Use NexusFinalizer to optimize the finalize which was extremely long !!!
        DEBUG_STRM << "passing DataStreamer to the NexusDataStreamerFinalizer - [BEGIN]" << std::endl;
        nxcpp::NexusDataStreamerFinalizer::Entry *e = new nxcpp::NexusDataStreamerFinalizer::Entry();
        e->data_streamer = m_writer;
        m_writer = NULL;
        StreamNexus::m_data_streamer_finalizer.push(e);
        DEBUG_STRM << "passing DataStreamer to the NexusDataStreamerFinalizer - [END]" << std::endl;
#else  
        DEBUG_STRM << "- Finalize() - [BEGIN]" << std::endl;
        m_writer->Finalize();
        delete m_writer;
        m_writer = NULL;
        DEBUG_STRM << "- Finalize() - [END]" << std::endl;
#endif 
    }

    Stream::stop_acquisition();

    DEBUG_STRM << "StreamNexus::stop_acquisition() - [END]" << std::endl;
}

/*******************************************************************
 * \brief Abort the stream
 *******************************************************************/
void StreamNexus::abort(void)
{
    DEBUG_STRM << "StreamNexus::abort() - [BEGIN]" << std::endl;

    yat::MutexLock scoped_lock(m_data_lock);    

    if (m_writer)
    {        
        DEBUG_STRM << "- Stop() - [BEGIN]" << std::endl;
        m_writer->Stop();
        delete m_writer;
        m_writer = 0;
        DEBUG_STRM << "- Stop() - [END]" << std::endl;
    }

    DEBUG_STRM << "StreamNexus::abort() - [END]" << std::endl;
}

//###########################################################################
}