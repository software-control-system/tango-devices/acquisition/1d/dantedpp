/*************************************************************************/
/*! 
 *  \file   RoisView.h
 *  \brief  class RoisView
 *  \author Cédric CASTEL
 */
/*************************************************************************/

#ifndef DANTE_DPP_ROIS_VIEW_H
#define DANTE_DPP_ROIS_VIEW_H

// TANGO
#include <tango.h>

// YAT/YAT4TANGO
#include <yat/memory/SharedPtr.h>
#include <yat/utils/Callback.h>
#include <yat4tango/DynamicInterfaceManager.h>

// PROJECT
#include "RoisViewParameters.h"
#include "Log.h"
#include "UserData.h"

namespace DanteDpp_ns
{

/*------------------------------------------------------------------
 *	class:	RoisView
 *	description: base class for the data storage
 /------------------------------------------------------------------*/
class RoisView
{
public:
    RoisView(Tango::DeviceImpl *dev);
    ~RoisView();

    // Create all dynamic attributes
    void create(const RoisViewParameters & in_parameters);

private:
    // To remove the dynamics attributes from ROIs file
    void remove_dynamic_attributes();

protected:
    /// Owner Device server object
    Tango::DeviceImpl* m_device;

    // common data
    std::vector<ULongUserData *> m_dyn_rois;
    
    // keep the create parameters
    RoisViewParameters m_create_parameters;

    // dynamics attributes
    yat4tango::DynamicInterfaceManager m_dim  ; // dynamics attributes from ROIs file
};

} // namespace 

#endif // DANTE_DPP_ROIS_VIEW_H


