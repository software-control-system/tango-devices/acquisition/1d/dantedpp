/********************************************************************************/
/*! 
 *  \file   StateStatus.h
 *  \brief  State and status class interface 
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
*/
/********************************************************************************/

#ifndef DANTE_DPP_STATE_STATUS_H_
#define DANTE_DPP_STATE_STATUS_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/threading/Mutex.h>

// SYSTEM
#include <cstddef>
#include <stdint.h>

// LOCAL
#include "Exception.h"

namespace DanteDpp_ns
{
    /***********************************************************************
     * \class StateStatus
     * \brief defines an info class used to store state and status informations 
     ***********************************************************************/

    class StateStatus
    {
    public:
        // constructor without parameter
        StateStatus();

        // Setter of the state and status
        void set(const Tango::DevState & in_state ,
                 const std::string     & in_status);

        // Setter of the state
        void set_state(const Tango::DevState & in_state);

        // Getter of the state
        Tango::DevState get_state(void) const;

        // Setter of the status
        void set_status(const std::string & in_status);

        // Getter of the status
        std::string get_status(void) const;

        // Manage the FAULT status with a TANGO exception
        void on_fault(const Tango::DevFailed & in_df);

        // Manage the FAULT status with a YAT exception
        void on_fault(const yat::Exception & in_ex);

        // Manage the FAULT status with an error message
        void on_fault(const std::string & in_status_message);

        // Manage the FAULT status with a DANTE exception
        void on_fault(const DanteDpp_ns::Exception & in_ex);

        // Return the status mutex
        yat::Mutex & get_mutex(void);

    private:
        Tango::DevState     m_state ; // state
        std::stringstream   m_status; // status (state label)

        // mutex used to protect state and status access
        // mutable keyword is used to allow const methods even if they use this class member
        mutable yat::Mutex  m_lock  ;
    }; 
}
#endif // DANTE_DPP_STATE_STATUS_H_

/*************************************************************************/