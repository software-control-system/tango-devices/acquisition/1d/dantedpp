/*************************************************************************/
/*! 
 *  \file   StateThread.cpp
 *  \brief  DANTE detector state thread class implementation
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"
#include "StateThread.h"
#include "HardwareDante.h"
#include "Controller.h"

#define STATE_THREAD_WAITING_TIME_MS_BETWEEN_STATE_UPDATE (1000)

namespace DanteDpp_ns
{

/*******************************************************************
 * \brief constructor
 * \param[in] in_name thread name (for logging)
 *******************************************************************/
StateThread::StateThread(const std::string in_name) : SyncThread(in_name)
{
}

/*******************************************************************
 * \brief destructor
 *******************************************************************/
StateThread::~StateThread()
{
}

/*******************************************************************
 * \brief The thread entry point called by yat::Thread::start_undetached.
 *        SyncThread process in managed inside this method.
 *******************************************************************/
yat::Thread::IOArg StateThread::run_undetached(yat::Thread::IOArg /*ioa*/)
{
    // to manage the exceptions
    StateStatus thread_status;

    SyncThread::start_treatment();

    // get status loop
    while (SyncThread::can_go_on())
    {
        try
        {
            try
            {
                yat::SharedPtr<HardwareDante> hardware = IHardware::get_instance();
                hardware->update_state();

                // waiting time between two update calls
                ::usleep(STATE_THREAD_WAITING_TIME_MS_BETWEEN_STATE_UPDATE * 1000);
            }
            catch (Tango::DevFailed & in_ex)
            {
                thread_status.on_fault(in_ex);
                throw;
            }
            catch (yat::Exception & in_ex)
            {
                in_ex.dump();
                thread_status.on_fault(in_ex);
                throw;
            }
            catch(const DanteDpp_ns::Exception & in_ex)
            {
                thread_status.on_fault(in_ex);
                throw;
            }
            catch(...)
            {
                thread_status.on_fault("Unknown exception!");
                throw;
            }
        }
        catch(...)
        {
            // at the moment, we do not throw the exception but only log it
            std::cout << "Exception occured!" << thread_status.get_status() << std::endl;
        }
    }

    SyncThread::finish_treatment();

    return 0;
}

} /// namespace DanteDpp_ns
