/*************************************************************************/
/*! 
 *  \file   AsyncBuffer.cpp
 *  \brief  class used to temporary store an async reply
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

// PROJECT
#include "Log.h"
#include "Error.h"
#include "AsyncBuffer.h"

namespace DanteDpp_ns
{
/*******************************************************************
 * \brief constructor
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 *******************************************************************/
AsyncBuffer::AsyncBuffer(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, const uint32_t * in_data)
{
    m_type    = in_type   ; // type of operation
    m_call_id = in_call_id; // request identifier

    // manage the special case : data is null and length = 0
    if((!in_length) || (!in_data))
    {
        m_data.clear();
    }
    else
    {
        m_data.resize(in_length);
        memcpy(m_data.data(), in_data, sizeof(uint32_t) * in_length);
    }
}

} // namespace DanteDpp_ns
