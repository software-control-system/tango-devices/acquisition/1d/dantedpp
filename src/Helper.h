/*************************************************************************/
/*! 
 *  \file   Helper.h
 *  \brief  class used as a tool class (mostly conversions)
 *  \author C�dric Castel - SOLEIL (MEDIANE SYSTEME - IT consultant) 
 */
/*************************************************************************/

#ifndef DANTE_DPP_HELPER_H_
#define DANTE_DPP_HELPER_H_

//TANGO
#include <tango.h>

//- YAT/YAT4TANGO
#include <yat/Version.h>

// PROJECT
#include "HardwareDante.h"
#include "StreamParameters.h"
#include "HardwareDanteConfigParameters.h"

// LOCAL

namespace DanteDpp_ns
{
    //-------------------------------------------------------------------------
    // CONFIGURATIONS
    //-------------------------------------------------------------------------
    static const size_t TANGO_CONFIG_ALIAS_SIZE_MAX = 20  ; // alias size max
    static const size_t TANGO_CONFIG_FILE_SIZE_MAX  = 4096; // file path size max

    //-------------------------------------------------------------------------
    // ROIs
    //-------------------------------------------------------------------------
    static const size_t TANGO_ROIS_ALIAS_SIZE_MAX = 20  ; // ROIs size max
    static const size_t TANGO_ROIS_FILE_SIZE_MAX  = 4096; // file path size max

    //-------------------------------------------------------------------------
    // ACQUISITION MODES
    //-------------------------------------------------------------------------
    // labels of trigger modes in the TANGO device
    static const std::vector<string> 
        TANGO_ACQ_MODES_LABELS{"MCA", "MAPPING" };

    static const size_t TANGO_ACQ_MODES_LABELS_SIZE_MAX = 20; // Acquisition modes label size max

    // used to convert a tango trigger mode (from TANGO_ACQ_MODES_LABELS) to an acquisition mode
    static const std::vector<HardwareDanteConfigParameters::AcqMode> 
        TANGO_TO_HARDWARE_ACQ_MODES{HardwareDanteConfigParameters::AcqMode::MCA,
                                    HardwareDanteConfigParameters::AcqMode::MAPPING};

    //-------------------------------------------------------------------------
    // TRIGGERS
    //-------------------------------------------------------------------------
    // labels of trigger modes in the TANGO device
    static const std::vector<string> 
        TANGO_TRIGGER_MODES_LABELS{"INTERNAL_SINGLE", "INTERNAL_MULTI", "EXTERNAL_GATE","EXTERNAL_TRIGRISE",
            "EXTERNAL_TRIGFALL"  ,
            "EXTERNAL_TRIGBOTH"  ,
            "EXTERNAL_GATELOW"   ,  };

    static const size_t TANGO_TRIGGER_MODES_LABELS_SIZE_MAX = 20; // label size max

    // used to convert a tango trigger mode (from TANGO_TRIGGER_MODES_LABELS) to a hardware trigger mode
    static const std::vector<HardwareDante::TriggerMode> 
        TANGO_TO_HARDWARE_TRIGGER_MODE{HardwareDante::TriggerMode::TRIGGER_INTERNAL_SINGLE,
                                       HardwareDante::TriggerMode::TRIGGER_INTERNAL_MULTI ,
                                       HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATE,
                                       HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGRISE  ,
                                       HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGFALL  ,
                                       HardwareDante::TriggerMode::TRIGGER_EXTERNAL_TRIGBOTH  ,
                                       HardwareDante::TriggerMode::TRIGGER_EXTERNAL_GATELOW  };

    //-------------------------------------------------------------------------
    // STREAM
    //-------------------------------------------------------------------------
    static const size_t TANGO_STREAM_TARGET_PATH_SIZE_MAX = 4096; // size max
    static const size_t TANGO_STREAM_TARGET_FILE_SIZE_MAX = 255 ; // size max

    //-------------------------------------------------------------------------
    // STREAM TYPE
    //-------------------------------------------------------------------------
    static const std::vector<std::string> 
        TANGO_STREAM_TYPE_LABELS{"NO_STREAM", "LOG_STREAM", "CSV_STREAM", "NEXUS_STREAM" }; // labels of stream type

    static const size_t TANGO_STREAM_TYPE_SIZE_MAX = 20 ; // size max

    // used to convert a tango stream type label (from STREAM_TYPE_LABELS) to a stream type
    static const std::vector<StreamParameters::Types> 
        TANGO_STREAM_TYPE_LABELS_TO_TYPE{StreamParameters::Types::NO_STREAM   ,
                                         StreamParameters::Types::LOG_STREAM  ,
                                         StreamParameters::Types::CSV_STREAM  ,
                                         StreamParameters::Types::NEXUS_STREAM};

    //-------------------------------------------------------------------------
    // STREAM WRITE MODE
    //-------------------------------------------------------------------------
    static const std::vector<std::string> 
        TANGO_STREAM_WRITE_MODE_LABELS{"ASYNCHRONOUS", "SYNCHRONOUS", "DELAYED"}; // labels of stream write mode

    // used to convert a tango stream write mode (from TANGO_STREAM_WRITE_MODE_LABELS) to a stream write mode
    static const std::vector<StreamParameters::WriteModes> 
        TANGO_STREAM_WRITE_MODE_LABELS_TO_WRITE_MODE{StreamParameters::WriteModes::ASYNCHRONOUS,
                                                     StreamParameters::WriteModes::SYNCHRONOUS ,
                                                     StreamParameters::WriteModes::DELAYED     };

    //-------------------------------------------------------------------------
    // STREAM MEMORY MODE
    //-------------------------------------------------------------------------
    static const std::vector<std::string> 
        TANGO_STREAM_MEMORY_MODE_LABELS{"NO_COPY", "COPY"}; // labels of stream memory mode

    // used to convert a tango stream write mode (from TANGO_STREAM_MEMORY_MODE_LABELS) to a stream memory mode
    static const std::vector<StreamParameters::MemoryModes> 
        TANGO_STREAM_MEMORY_MODE_LABELS_TO_MEMORY_MODE{StreamParameters::MemoryModes::NO_COPY,
                                                       StreamParameters::MemoryModes::COPY   };

    //-------------------------------------------------------------------------
    // DETECTOR TYPE
    //-------------------------------------------------------------------------
    static const std::vector<std::string> 
        TANGO_DETECTOR_TYPE_LABELS{"DANTE", "DANTE_SIMULATOR"}; // labels of detector type

    static const size_t TANGO_DETECTOR_TYPE_LABELS_SIZE_MAX = 20; // size max

    // used to convert a tango detector type (from TANGO_DETECTOR_TYPE_LABELS) to a detector type
    static const std::vector<HardwareDanteInitParameters::Type> 
        TANGO_DETECTOR_TYPE_LABELS_TO_TYPE{HardwareDanteInitParameters::Type::Dante    ,
                                           HardwareDanteInitParameters::Type::Simulator};

    //-------------------------------------------------------------------------
    // PRESET TYPE
    //-------------------------------------------------------------------------
    // labels of preset types in the TANGO device
    static const std::vector<string> 
        TANGO_PRESET_TYPES_LABELS{"FIXED_REAL"};

    static const size_t TANGO_PRESET_TYPES_LABELS_SIZE_MAX = 11; // label size max

    // used to convert a tango trigger mode (from TANGO_PRESET_TYPES_LABELS) to a hardware preset type
    static const std::vector<HardwareDante::PresetType> 
        TANGO_TO_HARDWARE_PRESET_TYPE{HardwareDante::PresetType::PRESET_TYPE_FIXED_REAL};

    /***********************************************************************
     * \class Helper
     * \brief tool class interface
     ***********************************************************************/
    class Helper
    {
    public:
    
        //-------------------------------------------------------------------------
        // STREAM TYPE
        //-------------------------------------------------------------------------
        /// Convert a tango stream type label to a stream type enum
        static StreamParameters::Types convert_tango_to_stream_type(const std::string & in_tango_stream_type);

        /// Convert a stream type enum to a tango stream type label
        static std::string convert_stream_type_to_tango(const StreamParameters::Types & in_stream_type);

        //-------------------------------------------------------------------------
        // STREAM WRITE MODE
        //-------------------------------------------------------------------------
        /// Convert a tango stream write mode label to a stream write mode
        static StreamParameters::WriteModes convert_tango_to_stream_write_mode(const std::string & in_tango_stream_write_mode);

        /// Convert a stream write mode enum to a tango stream write mode label
        static std::string convert_stream_write_mode_to_tango(const StreamParameters::WriteModes & in_stream_write_mode);

        //-------------------------------------------------------------------------
        // STREAM MEMORY MODE
        //-------------------------------------------------------------------------
        /// Convert a tango stream memory mode label to a stream memory mode
        static StreamParameters::MemoryModes convert_tango_to_stream_memory_mode(const std::string & in_tango_stream_memory_mode);

        /// Convert a stream memory mode enum to a tango stream memory mode label
        static std::string convert_stream_memory_mode_to_tango(const StreamParameters::MemoryModes & in_stream_memory_mode);

        //-------------------------------------------------------------------------
        // TRIGGERS
        //-------------------------------------------------------------------------
        /// Convert a tango trigger mode to a trigger mode enum
        static HardwareDante::TriggerMode convert_tango_to_trigger_mode(const std::string & in_tango_trigger_mode);

        /// Convert a trigger mode enum to a tango trigger mode label
        static std::string convert_trigger_mode_to_tango(const HardwareDante::TriggerMode & in_trigger_mode);

        //-------------------------------------------------------------------------
        // DETECTOR TYPE
        //-------------------------------------------------------------------------
        // Convert a tango detector type to a detector type enum
        static HardwareDanteInitParameters::Type convert_tango_to_detector_type(const std::string & in_tango_detector_type);

        // Convert a detector type enum to a tango detector type label
        static std::string convert_detector_type_to_tango(const HardwareDanteInitParameters::Type & in_detector_type);

        //-------------------------------------------------------------------------
        // ACQUISITION MODES
        //-------------------------------------------------------------------------
        // Convert a tango acquisition mode to an acquisition mode enum
        static HardwareDanteConfigParameters::AcqMode convert_tango_to_acq_mode(const std::string & in_tango_acq_mode);

        // Convert an acquisition mode enum to a tango acquisition mode label
        static std::string convert_acq_mode_to_tango(const HardwareDanteConfigParameters::AcqMode & in_acq_mode);

        //-------------------------------------------------------------------------
        // PRESET TYPES
        //-------------------------------------------------------------------------
        // Convert a tango preset type to a preset type enum
        static HardwareDante::PresetType convert_tango_to_preset_type(const std::string & in_tango_preset_type);

        // Convert a trigger mode enum to a tango trigger mode label
        static std::string convert_preset_type_to_tango(const HardwareDante::PresetType & in_preset_type);

    private:

        /// Convert a tango type label to a type enum
        template< typename T1>
        static T1 convert_tango_label_to_type(const std::string              & in_tango_label ,
                                              const std::vector<std::string> & in_tango_labels,
                                              const std::vector<T1>          & in_type_values ,
                                              const std::string              & in_type_info   );

        /// Convert a type enum to a tango label
        template< typename T1>
        static std::string convert_type_to_tango_label(const T1                       & in_enum_type   ,
                                                       const std::vector<std::string> & in_tango_labels,
                                                       const std::vector<T1>          & in_type_values ,
                                                       const std::string              & in_type_info   );
    };

//	Additional Classes Definitions
#include "Helper.hpp"

} // namespace DanteDpp_ns

#endif // DANTE_DPP_HELPER_H_

//###########################################################################
