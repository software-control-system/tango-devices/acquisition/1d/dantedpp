//------------------------------------------------------------------------------------------------------
#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>

#include <cstdlib>
#include <cstring>

#include <stdint.h>
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>
#include <sstream>
#include <unistd.h>
#include <stdexcept>
#include <sys/time.h>

// SDK
#include "DLL_DPP_Callback.h"

#define HW_DANTE_WAITING_TIME_MS_AFTER_ADD_TO_QUERY_CALL (4000)
#define HW_DANTE_WAITING_TIME_MS_AFTER_FUNCTION_CALL     (10000)
#define HW_DANTE_WAITING_TIME_MS_AFTER_STATE_CALL        (1000)
#define HW_DANTE_WAITING_TIME_MS_AFTER_GATING_CALL       (1000)
#define HW_DANTE_WAITING_TIME_MS_AFTER_START_CALL        (2000)
#define HW_DANTE_WAITING_TIME_MS_DURING_ACQUISITION      (1000)

//------------------------------------------------------------------
// error management
//------------------------------------------------------------------
#define ERROR_CODE_UNKNOWN               (-1)
#define ERROR_CODE_ASYNC_REQUEST_PROBLEM (100)
#define ERROR_CODE_ASYNC_REPLY_PROBLEM   (101)

// operation type of the request used the reply callback
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_READ  (1)
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_WRITE (2)
#define DANTE_DPP_ASYNC_REQUEST_OP_TYPE_ERROR (0)

typedef std::map<int, std::string> ErrorsMap;

// errors list
const ErrorsMap::value_type g_errors_values[] = 
{
    // error(s) added for SOLEIL device:
    ErrorsMap::value_type(ERROR_CODE_UNKNOWN              , "Unknown error."),
    ErrorsMap::value_type(ERROR_CODE_ASYNC_REQUEST_PROBLEM, "Error during the call of an asynchronous function to the detector."),
    ErrorsMap::value_type(ERROR_CODE_ASYNC_REPLY_PROBLEM  , "Error in the reply of an asynchronous function of the detector."),

    // official Dante sdk errors:
    ErrorsMap::value_type(DLL_NO_ERROR                        , "No errors."),
    ErrorsMap::value_type(DLL_MULTI_THREAD_ERROR              , "Another thread has a lock on the library functions."),
    ErrorsMap::value_type(DLL_NOT_INITIALIZED                 , "DLL is not initialized. Call InitLibrary()."),
    ErrorsMap::value_type(DLL_CHAR_STRING_SIZE                , "Supplied char buffer size is too short. Return value updated with minimum length."),
    ErrorsMap::value_type(DLL_ARRAY_SIZE                      , "An array passed as parameter to a function has not enough space to contain all the data that the function should return."),
    ErrorsMap::value_type(DLL_ALREADY_INITIALIZED             , "DLL is already initialized."),
    ErrorsMap::value_type(DLL_COMM_ERROR                      , "Communication error or timeout."),
    ErrorsMap::value_type(DLL_ARGUMENT_OUT_OF_RANGE           , "An argument supplied to the library is out of valid range."),
    ErrorsMap::value_type(DLL_WRONG_ID                        , "The supplied identifier (serial or IP) is not present among the connected systems."),
    ErrorsMap::value_type(DLL_TIMEOUT                         , "An operation timed out. Result is unspecified."),
    ErrorsMap::value_type(DLL_CLOSING                         , "Error during library closing."),
    ErrorsMap::value_type(DLL_RUNNING                         , "The operation cannot be completed while the system is running."),
    ErrorsMap::value_type(DLL_WRONG_MODE                      , "The function called is not appropriate for the current mode."),
    ErrorsMap::value_type(DLL_DECRYPT_FAILED                  , "An error occured during decryption."),
    ErrorsMap::value_type(DLL_INVALID_BITSTREAM               , "Trying to upload an invalid bistream file."),
    ErrorsMap::value_type(DLL_FILE_NOT_FOUND                  , "The specified file hasn't been found."),
    ErrorsMap::value_type(DLL_INVALID_FIRMWARE                , "Invalid firmware detected on one board. Upload a new one with load_firmware function."),
    ErrorsMap::value_type(DLL_UNSUPPORTED_BY_FIRMWARE         , "Function not supported by current firmware of the board. Or firmware on the board is not present or corrupted."),
    ErrorsMap::value_type(DLL_THREAD_COMM_ERROR               , "Error during communication between threads."),
    ErrorsMap::value_type(DLL_MISSED_SPECTRA                  , "One ore more spectra missed."),
    ErrorsMap::value_type(DLL_MULTIPLE_INSTANCES              , "Library open by multiple processes."),
    ErrorsMap::value_type(DLL_THROUGHPUT_ISSUE                , "Download rate from boards at least 15% lower than expected, check your connection speed."),
    ErrorsMap::value_type(DLL_INCOMPLETE_CMD                  , "A communication error caused a command to be truncated."),
    ErrorsMap::value_type(DLL_MEMORY_FULL                     , "The hardware memory became full during the acquisition. Likely the effective throughput is not enough to handle all the data."),
    ErrorsMap::value_type(DLL_SLAVE_COMM_ERROR                , "Communication error with slave boards."),
    ErrorsMap::value_type(DLL_SOFTWARE_MEMORY_FULL            , "Allowed memory for the DLL became full. Likely some data/event have been discarded."),
    // debug Dante sdk error - If following errors are returned, contact the XGLab team for further investigation:
    ErrorsMap::value_type(DLL_WIN32API_FAILED_INIT            , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_GET_DEVICE             , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API                        , "Generic WIN32 API error."),
    ErrorsMap::value_type(DLL_WIN32API_INIT_EVENTS            , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_RD_INIT_EVENTS         , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_REPORTED_FAILED_INIT   , "Win32 errors - Debugging - Possible hardware/driver error."),
    ErrorsMap::value_type(DLL_WIN32API_UNEXPECTED_FAILED_INIT , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_MULTI_THREAD_INIT_SET  , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_LOCK_HMODULE_SET       , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_HWIN_SET               , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_WMSG_SET               , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_READ_SET               , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_GET_DEVICE_SIZE        , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_DEVICE_UPD_LOCK_G      , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_FT_CREATE_IFL                   , "FT errors - Debugging."),
    ErrorsMap::value_type(DLL_FT_GET_IFL                      , "FT errors - Debugging."),
    ErrorsMap::value_type(DLL_CREATE_DEVCLASS_RUNTIME         , "Library errors - Debugging - Possible hardware/driver error."),
    ErrorsMap::value_type(DLL_CREATE_DEVCLASS_ARGUMENT        , "Library errors - Debugging."),
    ErrorsMap::value_type(DLL_CREATE_DEVCLASS_COMM            , "Library errors - Debugging."),
    ErrorsMap::value_type(DLL_RUNTIME_ERROR                   , "Generic runtime error."),
    ErrorsMap::value_type(DLL_WIN32API_HMODULE                , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_WIN32API_DEVICE_UPD_LOCK_F      , "Win32 errors - Debugging."),
    ErrorsMap::value_type(DLL_INIT_EXCEPTION                  , "Library errors - Debugging."),
};

// errors labels
ErrorsMap g_errors_labels; 

/*******************************************************************
 * \brief convert the error code to a description string
 * \return the error code description
 *******************************************************************/
std::string get_description_of_error_code(const int in_error_code)
{
    std::string description = "Unknown error code received from Dante Sdk!";

    // map is not filled ?
    if(g_errors_labels.size() == 0)
    {
        const int num_elements = sizeof(g_errors_values) / sizeof(g_errors_values[0]);
        g_errors_labels = ErrorsMap(g_errors_values, g_errors_values + num_elements);
    }

    ErrorsMap::iterator search = g_errors_labels.find(in_error_code);

    // found the error
    if(search != g_errors_labels.end()) 
    {
        description = search->second;
    }

    return description;
}

/************************************************************************
 * \brief manages the current error and throws an exception
 * \param[in] in_reason   error reason
 ************************************************************************/
void manage_last_error(const std::string & in_reason)
{
    uint16_t error_code = DLL_NO_ERROR;
    bool result = getLastError(error_code);

    if(!result)
    {
        error_code = ERROR_CODE_UNKNOWN;
    }
    else
    {
        resetLastError();
    }

    throw std::runtime_error(in_reason + " Error (" + get_description_of_error_code(error_code) + ")");
}

/************************************************************************
 * \brief checks the content of the last error
 * \param[in] in_error_code checks this error code
 * \return true if the last error code corresponds to in_error_code, else false
 ************************************************************************/
bool check_last_error(const uint16_t in_error_code)
{
    uint16_t error_code = DLL_NO_ERROR;
    bool result = getLastError(error_code);

    if(!result)
    {
        error_code = ERROR_CODE_UNKNOWN;
    }

    return (error_code == in_error_code);
}

//------------------------------------------------------------------------------------------------------
// tools functions & macros
//------------------------------------------------------------------------------------------------------
#define PRINT_SEPARATOR() std::cout << "============================================" << std::endl;

//======================================================================================================
// test configuration
//======================================================================================================
std::string     g_test_ip         = "192.168.0.1"; // default ip address
enum GatingMode g_gating_mode     = FreeRunning;
int             g_mca_test_nb     = 2  ;
int             g_mapping_test_nb = 2  ;
double          g_sp_time_sec     = 1.0;

//------------------------------------------------------------------
// callback management
//------------------------------------------------------------------
typedef struct
{
    uint32_t              m_call_id;
    std::vector<uint32_t> m_data   ;
}
call_back_request;

typedef struct
{
    uint32_t              m_call_id; 
    std::vector<uint32_t> m_data   ;
}
call_back_answer;

typedef enum 
{
    CMD_FIRMWARE, // get firmware
    CMD_START   , // start
    CMD_STOP    , // stop
    CMD_STATE   , // state
    CMD_GATING  , // configure gating
} cmd_type;

std::vector<call_back_request> requests;
std::vector<call_back_answer>  answers ;

pthread_mutex_t lock;

void clear_requests()
{
    requests.clear();
    answers.clear();
}

void add_request(uint32_t in_call_id)
{
    call_back_request new_request;
    new_request.m_call_id = in_call_id;
    new_request.m_data.clear();
    requests.push_back(new_request);
}

int search_request_id(uint32_t in_call_id)
{
    for(std::vector<call_back_request>::iterator it = requests.begin(); it != requests.end(); it++)
    {
        if((*it).m_call_id == in_call_id)
        {
            return (it - requests.begin());
        }
    }

    return (-1);
}

void check_answers_nb(cmd_type type)
{
    if(answers.empty())
    {
        std::cout << ">>>>>>>>>>>>>>> Error! NO ANSWER WAS RECEIVED!" << std::endl;
    }
    else
    if(requests.size() != answers.size() )
    {
        std::cout << ">>>>>>>>>>>>>>> Error! RECEIVED ONLY " << answers.size() << " ANSWERS!" << std::endl;
    }
    else
    {
        std::cout << "Ok. All request answers received." << std::endl;

        for(std::vector<call_back_answer>::iterator it = answers.begin(); it != answers.end(); it++)
        {
            int index = search_request_id((*it).m_call_id);
            
            if(index != -1)
            {
                requests[index].m_data = (*it).m_data;
                const uint32_t * data = requests[index].m_data.data();

                if(type == CMD_FIRMWARE)
                {
                    std::cout << "firmware " << data[0] << "." << data[1] << "." << data[2] << " for module: " << index << std::endl;
                }
                else
                if(type == CMD_START)
                {
                    std::cout << "answer for start: " << data[0] << std::endl;
                }
                else
                if(type == CMD_STOP)
                {
                    std::cout << "answer for stop: " << data[0] << std::endl;
                }
                else
                if(type == CMD_STATE)
                {
                    std::cout << "answer for state: " << data[0] << " for module: " << index << std::endl;
                }
                if(type == CMD_GATING)
                {
                    std::cout << "answer for configure gating: " << data[0] << std::endl;
                }
            }
            else
            {
                std::cout << "unknown reply" << std::endl;
                std::cout << "in_call_id: " << (*it).m_call_id << std::endl;
                std::cout << "in_length: " << (*it).m_data.size() << std::endl;
            }
        }
    }
}

/*******************************************************************
 * \brief callback used to receive the replies of the requests
 * \param[in] in_type type of operation
 * \param[in] in_call_id request identifier
 * \param[in] in_length lenght of the data
 * \param[in] in_data lenght of the data
 *******************************************************************/
void reply_callback(uint16_t in_type, uint32_t in_call_id, uint32_t in_length, uint32_t * in_data)
{
    // check if the type, the in_length and the data pointer are correct    
    if((in_type == DANTE_DPP_ASYNC_REQUEST_OP_TYPE_ERROR)||(!in_length)||(!in_data))
    {
        std::cout << "Problem! " << get_description_of_error_code(ERROR_CODE_ASYNC_REPLY_PROBLEM) << std::endl;
        return;
    }

    // 3) check if the lenght is coherent with a write type
    if((in_type == DANTE_DPP_ASYNC_REQUEST_OP_TYPE_WRITE)&&(in_length > 1))
    {
        std::cout << "Problem! Incoherent lenght (" << in_length << ") for a write request id (" << in_call_id << ")!" << std::endl;
        std::cout << "Problem! " << get_description_of_error_code(ERROR_CODE_ASYNC_REPLY_PROBLEM) << std::endl;
        return;
    }

    call_back_answer answer;
    answer.m_call_id = in_call_id;
    answer.m_data.resize(in_length);
    memcpy(answer.m_data.data(), in_data, sizeof(uint32_t) * in_length);
    answers.push_back(answer);

    //std::cout << "add: " << in_call_id << " length: " << in_length << std::endl;
}

/************************************************************************
 * \brief requests the library version
 * \return library version
 ************************************************************************/
std::string request_library_version()
{
    std::string   library_version;
    uint32_t      version_size   = 0   ;
    char        * version        = NULL;
    
    // first, answering the string size
    if(!libVersion(version, version_size))
    {
        // normal error -> to get the string size
        if(!check_last_error(DLL_CHAR_STRING_SIZE))
        {
            manage_last_error("Function failed!");
        }
    }
    else
    {
        throw std::runtime_error("libVersion incoherent function result!");
    }
    
    version_size++; // for the end of string character

    // secondly, getting the data
    version = new char[version_size];

    if(!libVersion(version, version_size))
    {
        manage_last_error("libVersion function failed!");
    }

    library_version = version;

    delete [] version;

    return library_version;
}

/************************************************************************
 * \brief gets the master identifier
 * \return master identifier
 ************************************************************************/
std::string get_master_identifier()
{
    std::string master_identifier ;
    uint16_t    identifier_size   = 0   ;
    char    *   identifier        = NULL;
    uint16_t    nb                = 0   ;
    
    if(!get_ids(identifier, nb, identifier_size))
    {
        // normal error -> to get the string size
        if(!check_last_error(DLL_CHAR_STRING_SIZE))
        {
            manage_last_error("get_master_identifier failed!");
        }
    }
    else
    {
        throw std::runtime_error("get_master_identifier Incoherent function result!");
    }
    
    identifier_size++; // for the end of string character

    // secondly, getting the data
    identifier = new char[identifier_size];

    if(!get_ids(identifier, nb, identifier_size))
    {
        manage_last_error("get_ids failed!");
    }

    master_identifier = identifier;

    delete [] identifier;

    return master_identifier;
}

//------------------------------------------------------------------------------------------------------
// read_simple_option
//------------------------------------------------------------------------------------------------------
bool read_simple_option(int argc, char* argv[], const char * in_option_name)
{
    int option_index = 1;

    while(option_index < argc)
    {
        if (strcmp(argv[option_index], in_option_name) == 0)
        {
            std::cout << "Found option:" << in_option_name << std::endl;
            return true;
        }

        option_index++;
    }

    return false;
}

//------------------------------------------------------------------------------------------------------
// read_option_value
//------------------------------------------------------------------------------------------------------
template <typename T> bool read_option_value(int argc, char* argv[], const char * in_option_name, T & out_option_value)
{
    int option_index = 1;

    while(option_index < argc)
    {
        if (strcmp(argv[option_index], in_option_name) == 0)
        {
            option_index++;

            if(option_index < argc)
            {
                std::stringstream ss(std::string(argv[option_index]));
                ss >> out_option_value;
                std::cout << "Found option: " << in_option_name << " " << out_option_value << std::endl;
                return true;
            }
        }

        option_index++;
    }

    return false;
}

//------------------------------------------------------------------------------------------------------
// stop the previous acquisition & get the state before a new acquisition
//------------------------------------------------------------------------------------------------------
void before_acquisition(std::string master_identifier, std::size_t modules_nb)
{
    PRINT_SEPARATOR();

    uint32_t request_id;
    uint16_t module_index;

    // stopping previous acquisition
    std::cout << "Stopping the previous acquisition..." << std::endl;

    clear_requests();

    request_id	= stop(master_identifier.c_str());
    add_request(request_id);

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_FUNCTION_CALL * 1000);
    check_answers_nb(CMD_STOP);

    // getting the state
    std::cout << "getting the state before the acquisition..." << std::endl;

    clear_requests();

    request_id = isRunning_system(master_identifier.c_str(), 0);
    add_request(request_id);

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_STATE_CALL * 1000);
    check_answers_nb(CMD_STATE);
}

//------------------------------------------------------------------------------------------------------
// treat mca acquisition data
//------------------------------------------------------------------------------------------------------
void treat_mca_data(std::string master_identifier, std::size_t modules_nb, uint16_t bins)
{
    uint16_t module_index;

    std::cout << "reading acquired data..." << std::endl;

    uint32_t spectra_size = bins;
    uint64_t data[spectra_size];
    uint32_t id;
    statistics stats;

    for(module_index = 0 ; module_index < modules_nb ; module_index++)
    {
        memset(&stats, 0, sizeof(statistics)); 

        if(!getData(master_identifier.c_str(), module_index, data, id, stats, spectra_size))
        {
            std::stringstream temp_stream;
            temp_stream << "Could not get the spectrum of board:" << module_index << "!";
            manage_last_error(temp_stream.str());
        }

        std::cout << "channel index [" << module_index << "] ";
        std::cout << "real_time [" << stats.real_time << "] ";
        std::cout << "live_time [" << stats.live_time << "] ";
        std::cout << "ICR [" << stats.ICR << "] ";
        std::cout << "OCR [" << stats.OCR << "] ";
        std::cout << "measured [" << stats.measured << "]" << std::endl;
    }
}

//------------------------------------------------------------------------------------------------------
// test mca
//------------------------------------------------------------------------------------------------------
void Test_mca(std::string master_identifier, std::size_t modules_nb, double time_sec, uint16_t bins)
{
    PRINT_SEPARATOR();

    uint32_t request_id;
    uint16_t module_index;

    // stopping previous acquisition
    before_acquisition(master_identifier, modules_nb);

    // clearing chain
    clear_chain(master_identifier.c_str());

    // changing the trigger mode
    std::cout << "configure gating: " << (int)FreeRunning << std::endl;

    clear_requests();

    for(module_index = 0 ; module_index < modules_nb ; module_index++)
    {
        request_id = configure_gating(master_identifier.c_str(), FreeRunning, module_index);
        add_request(request_id);
    }

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_GATING_CALL * 1000);
    check_answers_nb(CMD_GATING);

    // start a mca acquisition
    std::cout << "starting a mca acquisition..." << std::endl;

    struct timeval t1, t2;
    double elapsedTime;

    clear_requests();

    std::cout << "acquisition time: " << (time_sec * 1000) << " ms" << std::endl;
    std::cout << "bins: " << bins << std::endl;

    request_id = start(master_identifier.c_str(), time_sec, bins);
    add_request(request_id);

    // starting timer
    gettimeofday(&t1, NULL);

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_START_CALL * 1000);
    check_answers_nb(CMD_START);

    // waiting for end of acquisition
    for(;;)
    {
        clear_requests();

        for(module_index = 0 ; module_index < modules_nb ; module_index++)
        {
            request_id = isRunning_system(master_identifier.c_str(), module_index);
            add_request(request_id);
        }

        // waiting for a few seconds
        ::usleep(HW_DANTE_WAITING_TIME_MS_DURING_ACQUISITION * 1000);
        check_answers_nb(CMD_STATE);

        if(requests[0].m_data.size() == 0)
        {
            std::cout << "Answer was not received." << std::endl;
        }
        else
        {
            if(requests[0].m_data[0] == 0)
            {
                std::cout << "Master is not running." << std::endl;
                break;
            }
            else
            {
                std::cout << "Master is running." << std::endl;
            }
        }
    }

    // stop timer
    gettimeofday(&t2, NULL);

    // compute and print the elapsed time in millisec
    elapsedTime  = (t2.tv_sec  - t1.tv_sec ) * 1000.0; // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // us to ms
    std::cout << "elapsed time: " << elapsedTime << " ms"  << std::endl;

    // read acquired data
    treat_mca_data(master_identifier, modules_nb, bins);
}

bool     g_first_spectrum_received; // to compute the spectrum index
uint32_t g_first_spectrum_id      ; // to compute the spectrum index
uint32_t g_received_spectra_nb    ; // number of current received spectra 

//------------------------------------------------------------------------------------------------------
// treat mapping acquisition data
//------------------------------------------------------------------------------------------------------
bool treat_mapping_data(std::string master_identifier, std::size_t modules_nb, uint32_t bins)
{
    bool result = false;

    for(std::size_t channel_index = 0; channel_index < modules_nb; channel_index++)
    {
        uint32_t spectra_number;

        // check the number of spectra available for the board
        if(!getAvailableData(master_identifier.c_str(), channel_index, spectra_number))
        {
            std::stringstream temp_stream;
            temp_stream << ">>>>>>>>>>>>>>> Error! Could not get the number of spectra of board:" << channel_index << "!";
            std::cout << temp_stream.str() << std::endl;
        }

        if(spectra_number == 0)
            continue;

        result = true;

        // prepare the data to store the spectra and statistics
        std::vector<uint16_t> spectra_16;
        std::vector<uint32_t> ids       ;
        std::vector<double>   stats     ;
        std::vector<uint64_t> advstats  ;

        const int nb_elem_basic_stats = 4 ;
        const int nb_elem_adv_stats   = 22;

        spectra_16.resize(spectra_number * bins);
        ids.resize(spectra_number);
        stats.resize(spectra_number * nb_elem_basic_stats);
        advstats.resize(spectra_number * nb_elem_adv_stats);
        
        if(!getAllData(master_identifier.c_str(),
                       channel_index, 
                       spectra_16.data(),
                       ids.data(),
                       stats.data(),
                       advstats.data(),
                       bins,
                       spectra_number))
        {
            std::stringstream temp_stream;
            temp_stream << ">>>>>>>>>>>>>>> Error! Could not get the spectra of board:" << channel_index << "!";
            std::cout << temp_stream.str() << std::endl;
        }

        std::cout << "reading " << spectra_number << " spectra for channel: " << channel_index << std::endl;

        g_received_spectra_nb += spectra_number;

        // manage the first spectrum received id 
        if(!g_first_spectrum_received)
        {
            g_first_spectrum_id       = ids[0];
            g_first_spectrum_received = true  ;
        }

        for(std::size_t spectrum_index = 0; spectrum_index < spectra_number; spectrum_index++)
        {
            std::cout << "channel index [" << channel_index << "] ";
            std::cout << "id [" << ids[spectrum_index] << "] ";
            std::cout << "spectrum index [" << (ids[spectrum_index] - g_first_spectrum_id) << "] ";
            std::cout << "real_time [" << stats[spectrum_index * nb_elem_basic_stats] << "] ";
            std::cout << "live_time [" << stats[spectrum_index * nb_elem_basic_stats + 1] << "] ";
            std::cout << "ICR [" << stats[spectrum_index * nb_elem_basic_stats + 2] << "] ";
            std::cout << "OCR [" << stats[spectrum_index * nb_elem_basic_stats + 3] << "] ";
            std::cout << "measured [" << advstats[spectrum_index * nb_elem_adv_stats + 2] << "]" << std::endl;
        }
    }

    return result;
}

//------------------------------------------------------------------------------------------------------
// test mapping
//------------------------------------------------------------------------------------------------------
void Test_mapping(std::string master_identifier, std::size_t modules_nb, double sp_time_sec, uint32_t points, uint16_t bins)
{
    PRINT_SEPARATOR();

    uint32_t request_id;
    uint16_t module_index;

    // stopping previous acquisition
    before_acquisition(master_identifier, modules_nb);

    // clearing chain
    clear_chain(master_identifier.c_str());

    // changing the trigger mode
    std::cout << "configure gating: " << (int)g_gating_mode << std::endl;

    clear_requests();

    for(module_index = 0 ; module_index < modules_nb ; module_index++)
    {
        request_id = configure_gating(master_identifier.c_str(), g_gating_mode, module_index);
        add_request(request_id);
    }

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_GATING_CALL * 1000);
    check_answers_nb(CMD_GATING);

    // starting a mapping acquisition
    std::cout << "starting a mapping acquisition..." << std::endl;

    // reiniting the first spectrum management
    g_first_spectrum_received = false; 
    g_first_spectrum_id       = 0;
    g_received_spectra_nb     = 0;

    uint32_t sp_time = static_cast<uint32_t>(sp_time_sec * 1000.0); // in milliseconds

    struct timeval t1, t2;
    double elapsedTime;

    clear_requests();

    std::cout << "point time: " << sp_time << " ms" << std::endl;
    std::cout << "points nb: " << points << std::endl;
    std::cout << "acquisition time: " << (sp_time * points) << " ms" << std::endl;
    std::cout << "bins: " << bins << std::endl;

    request_id = start_map(master_identifier.c_str(), sp_time, points, bins);
    add_request(request_id);

    // starting timer
    gettimeofday(&t1, NULL);

    // waiting for a few seconds
    ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_START_CALL * 1000);
    check_answers_nb(CMD_START);

    // read acquired data
    for(;;)
    {
        for(;;)
        {
            if(!treat_mapping_data(master_identifier, modules_nb, bins))
                break;
        }

        // get current time
        gettimeofday(&t2, NULL);

        // all spectra were received ?
        if(g_received_spectra_nb == (points * modules_nb))
            break;

        // compute and print the elapsed time in millisec
        elapsedTime  = (t2.tv_sec  - t1.tv_sec ) * 1000.0; // sec to ms
        elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // us to ms
        //std::cout << "elapsed time: " << elapsedTime << " ms"  << std::endl;

        if(elapsedTime >= ((sp_time * points) + 5000.0))
        {
            std::cout << ">>>>>>>>>>>>>>> Error! Did not received all spectra!" << std::endl;
            std::cout << "received:" << g_received_spectra_nb << std::endl;
            std::cout << "should be:" << (points * modules_nb) << std::endl;
            break;
        }
    }

    // compute and print the elapsed time in millisec
    elapsedTime  = (t2.tv_sec  - t1.tv_sec ) * 1000.0; // sec to ms
    elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // us to ms
    std::cout << "elapsed time: " << elapsedTime << " ms"  << std::endl;
}

//------------------------------------------------------------------------------------------------------
// test
//------------------------------------------------------------------------------------------------------
void Test(void)
{
    try
    {
        PRINT_SEPARATOR();
        std::cout << "Starting test..." << std::endl;
        PRINT_SEPARATOR();

        std::cout << "DANTE Sdk init library" << std::endl;
        if(!InitLibrary())
        {
            manage_last_error("Library not initialized!");
        }

        std::cout << "register the replies callback" << std::endl;
        if(!register_callback(reply_callback))
        {
            manage_last_error("Replies callback not registered!");
        }

        // check if we are working with an IP or USB connection
        if(!g_test_ip.empty())
        {
            if(!add_to_query(const_cast<char*>(g_test_ip.c_str())))
            {
                std::stringstream temp_stream;
                temp_stream << "Problem with the IP: " << g_test_ip;
                manage_last_error(temp_stream.str());
            }
            else
            {
                std::cout << "Connection using IP:" << g_test_ip << std::endl;
            }
        }
        else
        {
            std::cout << "USB connection selected." << std::endl;
        }

        // waiting for a few seconds (from XGLab advice)
        ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_ADD_TO_QUERY_CALL * 1000);

        // library version
        std::cout << "Library version: " << request_library_version() << std::endl;

        // check if there is a detector available
        uint16_t    detectors  = 0;
        std::size_t modules_nb = 0;
        std::string master_identifier;

        if(!get_dev_number(detectors))
        {
            manage_last_error("get_dev_number function failed!");
        }

        std::cout << "Detectors number found: " << detectors << std::endl;

        if(detectors == 0)
        {
            std::stringstream temp_stream;
            temp_stream << "Could not find any detector using IP: " << g_test_ip;
            throw std::runtime_error(temp_stream.str());
        }
        else
        if(detectors > 1)
        {
            std::stringstream temp_stream;
            temp_stream << "An IP address conflict occurs. Found several detector which use IP: " << g_test_ip;
            throw std::runtime_error(temp_stream.str());
        }
        else
        {
            master_identifier = get_master_identifier();
            std::cout << "Master identifier: " << master_identifier << std::endl;
            
            uint16_t boards_nb;

            if(!get_boards_in_chain(master_identifier.c_str(), boards_nb))
            {
                manage_last_error("get_boards_in_chain function failed!");
            }

            modules_nb = static_cast<std::size_t>(boards_nb);
            std::cout << "Boards number: " << modules_nb << std::endl;
        }

        uint32_t request_id;

        // request firmware informations
        clear_requests();

        uint16_t module_index;

        for(module_index = 0 ; module_index < modules_nb ; module_index++)
        {
            request_id	= getFirmware(master_identifier.c_str(), module_index);
            add_request(request_id);
        }

        // waiting for a few seconds
        ::usleep(HW_DANTE_WAITING_TIME_MS_AFTER_FUNCTION_CALL * 1000);
        check_answers_nb(CMD_FIRMWARE);

        // mca test(s)
        if(g_mca_test_nb > 0)
        {
            for(int mca_test_index = 0 ; mca_test_index < g_mca_test_nb ; mca_test_index++)
                Test_mca(master_identifier, modules_nb, 20.0, 4096);
        }

        // mapping test(s)
        if(g_mapping_test_nb > 0)
        {
            for(int mapping_test_index = 0 ; mapping_test_index < g_mapping_test_nb ; mapping_test_index++)
                Test_mapping(master_identifier, modules_nb, g_sp_time_sec, 20, 4096);
        }

        // closing library
        std::cout << "DANTE Sdk closing library" << std::endl;
        if(!CloseLibrary())
        {
            manage_last_error("Library not closed!");
        }
    }
    catch (const std::runtime_error & ex)
    {
        std::cout << "runtime exception: " << ex.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception!" << std::endl;
    }
}

//------------------------------------------------------------------------------------------------------
// main
//------------------------------------------------------------------------------------------------------
int main (int argc, char* argv[])
{
    if(read_simple_option(argc, argv, "-help") || read_simple_option(argc, argv, "--help"))
    {
        PRINT_SEPARATOR();
        std::cout << "Options:" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << "-ip <value> -> set ip value (for example: -ip 192.168.0.1)" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << "-tt <value> -> set trigger type value (for example: -tt 1)" << std::endl;
        std::cout << "use \"-tt 0\" for FreeRunning" << std::endl;
        std::cout << "use \"-tt 1\" for TriggerRising" << std::endl;
        std::cout << "use \"-tt 2\" for TriggerFalling" << std::endl;
        std::cout << "use \"-tt 3\" for TriggerBoth" << std::endl;
        std::cout << "use \"-tt 4\" for GatedHigh" << std::endl;
        std::cout << "use \"-tt 5\" for GatedLow" << std::endl;
        std::cout << "The default value is 0 for FreeRunning" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << "-mca <value> -> set mca tests number (for example: -mca 2)" << std::endl;
        std::cout << "The default value is 2" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << "-mapping <value> -> set mapping tests number (for example: -mapping 2)" << std::endl;
        std::cout << "The default value is 2" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << "-spt <value> -> set point time (seconds) in mapping acquisition (for example: -spt 1.0)" << std::endl;
        std::cout << "The default value is 1.0 (for 1 second)" << std::endl;
        std::cout << "-------------------------------------------------------------------------" << std::endl;
        std::cout << std::endl;
        std::cout << "example: ./sdk-test -ip 192.168.0.1 -tt 0 -mca 1 -mapping 1 -spt 1.0" << std::endl;
        PRINT_SEPARATOR();
        return 0;
    }

    // ip address
    std::string ip;

    if(read_option_value(argc, argv, "-ip", ip))
    {
        g_test_ip = ip;
    }

    // trigger mode
    int tt;

    if(read_option_value(argc, argv, "-tt", tt))
    {
        if((tt >= 0)&&(tt <= static_cast<int>(GatedLow))) 
        {
            g_gating_mode = static_cast<enum GatingMode>(tt);
            std::cout << "Selected trigger mode: " << tt << std::endl;
        }
        else
        {
            std::cout << "Incorrect trigger mode: " << tt << "!" << std::endl;
        }
    }

    // number of mca tests
    int mca;

    if(read_option_value(argc, argv, "-mca", mca))
    {
        g_mca_test_nb = mca;
        std::cout << "Selected number of mca tests: " << g_mca_test_nb << std::endl;
    }

    // number of mapping tests
    int mapping;

    if(read_option_value(argc, argv, "-mapping", mapping))
    {
        g_mapping_test_nb = mapping;
        std::cout << "Selected number of mapping tests: " << g_mapping_test_nb << std::endl;
    }

    // point time in mapping mode
    double sp_time_sec;

    if(read_option_value(argc, argv, "-spt", sp_time_sec))
    {
        g_sp_time_sec = sp_time_sec;
        std::cout << "Selected time in seconds of points in mapping acquisition: " << sp_time_sec << std::endl;
    }

    Test();

    std::cout << "================== ENDING ==================" << std::endl;

    return 0;
}

//------------------------------------------------------------------------------------------------------
